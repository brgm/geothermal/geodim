""" A serie of utilities for testing the module """
import numpy as np
import pathlib
import sys
import yaml

# Path to lib
CWD = pathlib.Path(__file__).parent
SOURCEDIR = pathlib.Path(CWD).parent

# Pytest specific
PYTEST_ARGUMENTS_FILE = 'arguments.yml'
PYTEST_OPTIONS_FILE = 'options.yml'
PYTEST_RESULTS_FILE = 'results.npz'
USE_AGGREGATION_KW = 'use_aggregation'
USE_CYTHON_KW = 'use_cython'
RUN_KIND_KW = 'run_kind'
RUN_TYPE_DIRECT = 'direct'
RUN_TYPE_DIMENSIONING = 'dimensioning'
RUN_TYPES = (RUN_TYPE_DIRECT, RUN_TYPE_DIMENSIONING)
ERROR_TOL_ABS = 1.e-5
ERROR_TOL_REL = 1.e-8

def add_geodim_headdir_to_path(do_import=False):
    """ Adding geodim's head directory to sys.path
    
    Parameters
    ----------
    do_import: bool,
        If True, module is imported on the fly
    
    """
    if not SOURCEDIR in sys.path:
        sys.path.append(str(SOURCEDIR))
    if do_import:
        import geodim
    
def extract_direct_results(results, building=False):
    """ Extract only some elements of a direct run results
    
    Notice that for hourly data, only last year results are exported to limit
    memory usage.
    
    Parameters
    ----------
    results: list/tuple,
        Results as returned by calcul.simul function
    building: bool,
        True if direct run was performed on a building (= borehole heat exch.)
    
    Returns
    -------
    out: dict,
        Selection of some results
        
    """
    if not building:
        raise NotImplementedError('Not implemented for single house so far')
    cops = results[0]
    nbdt_1year = 8760
    t_Tb, t_Tflinhe, results_building = results[-1]
    t_Tflouthe = results_building['Theatexch_out']
    t_Pheatexch = results_building['Pheatexch']
    t_Pemitter = results_building['Pemitter']
    t_Pelec_HP = results_building['Pelec_HP']
    t_Pbackup_heat = results_building['Pbackup_heat']
    t_Pbackup_AC = results_building['Pbackup_AC']
    results_selection = dict(
        cops=cops,
        t_Tflinhe=t_Tflinhe[-nbdt_1year:],
        t_Tflouthe=t_Tflouthe[-nbdt_1year:],
        t_Tb=t_Tb[-nbdt_1year:],
        t_Pheatexch=t_Pheatexch[-nbdt_1year:],
        t_Pemitter=t_Pemitter[-nbdt_1year:],
        t_Pelec_HP=t_Pelec_HP[-nbdt_1year:],
        t_Pbackup_heat=t_Pbackup_heat[-nbdt_1year:],
        t_Pbackup_AC=t_Pbackup_AC[-nbdt_1year:],
        )
    return results_selection
        
def extract_dimensioning_results(results):
    """ Extract only some elements of a dimensioning run results
    
    Parameters
    ----------
    results: list/tuple,
        Results as returned by calcul.dimensionnement function
    
    Returns
    -------
    out: dict,
        Selection of some results
    
    """
    dim = np.array(results[0], dtype=float)
    cops = results[1]
    e_prod, e_cons = [
        np.array(
            [   r
                if not r is None
                else np.nan
                for r in res
                ],
            dtype=float
            )
        for res in results[4]
        ]
    t_heat, t_dhw, t_cool, t_AC = [ hourly_needs
                                    if not hourly_needs is None
                                    else np.nan
                                    for hourly_needs in results[-1]
                                    ]
    # only available for direct run results ( additional_data)
    # t_Tb, t_Tflinhe = results[-1][:2]
    results_selection = dict(
        dim=dim,
        cops=cops,
        e_prod=e_prod,
        e_cons=e_cons,
        t_heat=t_heat,
        t_dhw=t_dhw,
        t_cool=t_cool,
        t_AC=t_AC
        )
    return results_selection
    
def export_run_results(
    subfolder, data, results, use_aggregation, use_cython, run_kind, building,
    arguments_file=PYTEST_ARGUMENTS_FILE, res_file=PYTEST_RESULTS_FILE,
    options_file=PYTEST_OPTIONS_FILE,
    **kwargs
    ):
    """ Exports data to files. Useful for storing reference results (// pytest)
    
    Parameters
    ----------
    subfolder: str/pathlib.Path,
        Path to folder where to export results.
    data: dict,
        Arguments used for the run.
    results: list/tuple,
        The results to export.
    use_aggregation: bool,
        Whether aggregation was used.
    use_cython: bool,
        Whether cython was used.
    run_kind: str,
        Kind of run used. Must be one of RUN_TYPES items.
    building: bool,
        True if run performed on a building (= borehole heat exch).
    arguments_file: str,
        Name of yaml file to dump run input to.
    res_file: str,
        Name of npz file to dump run results to.
    options_file: str,
        Name of yaml file to dump run options to.
    
    """
    assert run_kind in RUN_TYPES, f'run_kind must be one of {RUN_TYPES}'
    subfolder = pathlib.Path(subfolder)
    with open(subfolder/arguments_file, 'w') as wt:
        yaml.dump(data, wt)
    if run_kind==RUN_TYPE_DIMENSIONING:
        ref_results = extract_dimensioning_results(results)
    elif run_kind==RUN_TYPE_DIRECT:
        ref_results = extract_direct_results(results, building)
    np.savez_compressed(subfolder/res_file, **ref_results)
    options = kwargs.copy()
    options[USE_AGGREGATION_KW]=use_aggregation
    options[USE_CYTHON_KW]=use_cython
    options[RUN_KIND_KW]=run_kind
    with open(subfolder/options_file, 'w') as wt:
        yaml.dump(options, wt)
    
def export_dimensioning_results(
    subfolder, data, results, use_aggregation, use_cython,
    arguments_file=PYTEST_ARGUMENTS_FILE, res_file=PYTEST_RESULTS_FILE,
    options_file=PYTEST_OPTIONS_FILE,
    **kwargs
    ):
    """ Exports data to files. Useful for storing reference results (// pytest)
    
    Parameters
    ----------
    subfolder: str/pathlib.Path,
        Path to folder where to export results.
    data: dict,
        Arguments used for the run.
    results: list/tuple,
        The results to export.
    use_aggregation: bool,
        Whether aggregation was used.
    use_cython: bool,
        Whether cython was used.
    arguments_file: str,
        Name of yaml file to dump run input to.
    res_file: str,
        Name of npz file to dump run results to.
    options_file: str,
        Name of yaml file to dump run options to.
    
    """
    # For backward compatibility
    kwargs.pop(RUN_KIND_KW, None)
    export_run_results(
        subfolder, data, results, use_aggregation, use_cython,
        run_kind=RUN_TYPE_DIMENSIONING, arguments_file=arguments_file,
        res_file=res_file, options_file=options_file,
        **kwargs
        )
        
def load_run_results(
    subfolder,
    arguments_file=PYTEST_ARGUMENTS_FILE,
    res_file=PYTEST_RESULTS_FILE,
    options_file=PYTEST_OPTIONS_FILE
    ):
    """ Load dimensioning results exported by export_run_results func
    
    Parameters
    ----------
    subfolder: str/pathlib.Path,
        Path to folder where to load results from.
    arguments_file: str,
        Name of yaml file to load run input from.
    res_file: str,
        Name of npz file to load run results from.
    options_file: str,
        Name of yaml file to load run options from.
    
    """
    subfolder = pathlib.Path(subfolder)
    arguments_file = subfolder/arguments_file
    results_file = subfolder/res_file
    options_file = subfolder/options_file
    ref_results = np.load(results_file)
    with arguments_file.open() as rd:
        run_input = yaml.safe_load(rd)
    with options_file.open() as rd:
        options = yaml.safe_load(rd)
    assert options[RUN_KIND_KW] in RUN_TYPES, (
            f'Run type must be one of {RUN_TYPES}'
            )
    return run_input, ref_results, options
        
def load_field_layout_results(
    subfolder, arguments_file=PYTEST_ARGUMENTS_FILE,
    res_file=PYTEST_RESULTS_FILE
    ):
    """ Load positions of a a BHE field
    
    Parameters
    ----------
    subfolder: str/pathlib.Path,
        Path to folder where to load results from.
    arguments_file: str,
        Name of yaml file to load input from.
    res_file: str,
        Name of npz file to load results from.
    
    """
    subfolder = pathlib.Path(subfolder)
    arguments_file = subfolder/arguments_file
    results_file = subfolder/res_file
    ref_results = np.load(results_file)['field_points']
    with arguments_file.open() as rd:
        run_input = yaml.safe_load(rd)
    return run_input, ref_results
        
def load_cop_table_results(
    subfolder,
    arguments_file=PYTEST_ARGUMENTS_FILE,
    res_file=PYTEST_RESULTS_FILE
    ):
    """ Load COP table
    
    Parameters
    ----------
    subfolder: str/pathlib.Path,
        Path to folder where to load results from.
    arguments_file: str,
        Name of yaml file to load input from.
    res_file: str,
        Name of npz file to load results from.
    
    """
    subfolder = pathlib.Path(subfolder)
    arguments_file = subfolder/arguments_file
    results_file = subfolder/res_file
    ref_results = dict(
        (key[4:], val)
        for key, val in np.load(results_file).items()
        if key.startswith('cop_')
        )
    with arguments_file.open() as rd:
        run_input = yaml.safe_load(rd)
    return run_input, ref_results
    
def results_allclose(
    results, ref_results, rtol=ERROR_TOL_REL, atol=ERROR_TOL_ABS, equal_nan=True
    ):
    """ Testing that two sets of results are really close
    
    Parameters
    ----------
    results: dict,
        First set of results.
    ref_results: dict,
        Second set of results (considered as the reference results)
        Keys in results must contain all keys in ref_results.
    rtol: float,
        Relative tolerance in np.allclose.
    atol: float,
        Absolute tolerance in np.allclose.
    equal_nan: bool,
        Whether nans are considered equals.
    
    Returns
    -------
    out: bool,
        True if all results are close, False otherwise
    
    """
    for res_kw, ref_res in ref_results.items():
        res = results[res_kw]
        no_res = np.isnan(ref_res).all()
        if no_res:
            no_res_either = np.isnan(res).all()
            if not no_res_either:
                return False
        if not np.allclose( res, ref_res, rtol=rtol, atol=atol,
                            equal_nan=equal_nan
                            ):
            return False
    return True