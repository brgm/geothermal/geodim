#coding: utf8

"""
Tests on functions performance increase due to cython implementation.

"""

import sys
sys.path.append('..')

import simul_cython
import time
import numpy as np

# ##############################
# # test G_courtterme --> ok x20
# ##############################
# dt = 3600.
# HCORB = 2.7
# RCORB = 0.5*(1.18+1.10)/2
# lambda_sable_alluv = 1.9
# rhocp_sable_alluv = 2.3e6
# def G_courtterme(y):
    # _la = lambda_sable_alluv*rhocp_sable_alluv
    # return (1-np.sqrt(1-y)) * np.sqrt(dt) / (2*np.pi**1.5*HCORB*RCORB*np.sqrt(_la))
    
# ntests = 170000
# sample = np.linspace(0., 1., ntests)
# t0 = time.time()
# res = [G_courtterme(y) for y in sample]
# t1 = time.time()
# G_courtterme_cython = simul_cython.G_courtterme(lambda_sable_alluv,
                                                # rhocp_sable_alluv,
                                                # 1)
# res_cython = [G_courtterme_cython(y) for y in sample]
# t2 = time.time()

# print('Runtime: {}s'.format(t1-t0))
# print('Runtime cython: {}s ({:.2f}x faster)'.format(t2-t1, (t1-t0)/(t2-t1)))

# #################################
# # test f_calc_Temission --> ok x5
# #################################
# from os.path import join
# # Data from underfloor heating 
# Temission1=35.
# Text1=-10.
# Temission2=24.
# Text2=7.
# # Data from GNEO 18H
# Temission_minimum = 22
# # External temp
# Text_year = np.loadtxt(join('..', '..', '..', 'meteo', 'TH2b.txt'))

# def f_calc_Temission(Text):
    # dt = Text1-Text2
    # return max(
        # Temission_minimum,
        # Temission1 * (Text-Text2) / dt - Temission2 * (Text-Text1)/dt
    # )
# nruns = 10
# t0 = time.time()
# for irun in range(nruns):
    # res = [ f_calc_Temission(Text) for Text in Text_year]
# t1 = time.time()
# for irun in range(nruns):
    # res_cython = [  simul_cython.f_calc_Temission(  Text,
                                                    # Text1,
                                                    # Temission1,
                                                    # Text2,
                                                    # Temission2,
                                                    # Temission_minimum
                                                    # )
                    # for Text in Text_year]
# t2 = time.time()

# print('Runtime: {}s'.format(t1-t0))
# print('Runtime cython: {}s ({:.2f}x faster)'.format(t2-t1, (t1-t0)/(t2-t1)))

# ############################
# # test interpolate --> ok x2
# ############################
# def interpolate(a, b, fa, fb, x):
    # return (x-a)*(fb-fa)/(b-a)+fa
    
# ntests = 17000
# a = 1+np.random.rand(ntests)    # [1;2]
# b = 4+np.random.rand(ntests)    # [4;5]
# fa = 10+np.random.rand(ntests)  # [10;11]
# fb = 2+np.random.rand(ntests)   # [2;3]
# sample = np.random.rand(ntests) # [0;1]

# t0 = time.time()
# res = [ interpolate(aa,bb,faa,fbb,x)
        # for aa,bb,faa,fbb,x in zip(a,b,fa,fb,sample)
        # ]
# t1 = time.time()
# res_cython = [  simul_cython.interpolate(aa,bb,faa,fbb,x)
                # for aa,bb,faa,fbb,x in zip(a,b,fa,fb,sample)
                # ]
# t2 = time.time()

# print('Runtime: {}s'.format(t1-t0))
# print('Runtime cython: {}s ({:.2f}x faster)'.format(t2-t1, (t1-t0)/(t2-t1)))

# ############################################
# # test digitize --> ok but not really faster
# ############################################
# ntests = 170000
# bins = np.array([0, 5, 10, 15, 20], dtype=float)
# # bins = np.linspace(0., 20., 1000)
# step = bins[1]-bins[0]
# sample = sorted(20*np.random.rand(ntests-1)) + [20.]

# t0 = time.time()
# res = [np.digitize(x, bins) for x in sample]
# t1 = time.time()
# res_cython = [simul_cython.digitize(x, bins) for x in sample]
# t2 = time.time()
# # res_cython2 = [simul_cython.digitize2(x, bins, step=step) for x in sample]
# # t3 = time.time()

# print('Runtime: {}s'.format(t1-t0))
# print('Runtime cython: {}s ({:.2f}x faster)'.format(t2-t1, (t1-t0)/(t2-t1)))
# # print('Runtime cython: {}s ({:.2f}x faster)'.format(t3-t2, (t1-t0)/(t3-t2)))

# #######################################################
# # test RectBivariateSpline --> ok but not really faster
# #######################################################
# from scipy.interpolate import RectBivariateSpline
# ntests = 17000

# T_out_condens = np.array([25, 35, 45, 55], dtype=float)
# T_in_evap = np.array([0, 5, 10, 15], dtype=float)
# Pc = np.array([ [5.47, 6.29, 7.22, 7.84],
                # [5.20, 5.98, 6.86, 7.28],
                # [4.95, 5.69, 6.53, 6.93],
                # [4.83, 5.55, 6.37, 6.76],
                # ])
# T_in_evap_cor = np.concatenate((T_in_evap,np.array([25])))
# Pc_duplique = np.transpose(np.array([Pc[:,-1]]))
# Pc_cor = np.concatenate((Pc,Pc_duplique),axis=1)
# Pc = RectBivariateSpline(T_out_condens, T_in_evap_cor, Pc_cor)
# Pc_cython = simul_cython.RectBivariateSpline(T_out_condens,T_in_evap_cor,Pc_cor)

# npts_1d = int(np.sqrt(ntests))+1
# ntests = npts_1d**2
# xs = np.linspace(T_out_condens[0], T_out_condens[-1], npts_1d)
# ys = np.linspace(T_in_evap_cor[0], T_in_evap_cor[-1], npts_1d)

# res = [0]*ntests
# res_cython = [0]*ntests

# t0 = time.time()
# for i,x in enumerate(xs):
    # for j,y in enumerate(ys):
        # res[i+j*npts_1d] = Pc(x,y)[0][0]
# t1 = time.time()
# for i,x in enumerate(xs):
    # for j,y in enumerate(ys):
        # res_cython[i+j*npts_1d] = Pc_cython(x,y)
# t2 = time.time()

# print('Runtime: {}s'.format(t1-t0))
# print('Runtime cython: {}s ({:.2f}x faster)'.format(t2-t1, (t1-t0)/(t2-t1)))
# error = np.linalg.norm(res_cython - np.array(res)) / np.linalg.norm(res)
# print('Error: {} o/o'.format(error*100))

# ################################
# # test Tflee function --> ok x20
# ################################
# ntests = 17000

# T_out_condens = np.array([25, 35, 45, 55], dtype=float)
# T_in_evap = np.array([0, 5, 10, 15], dtype=float)
# Pc = np.array([ [5.47, 6.29, 7.22, 7.84],
                # [5.20, 5.98, 6.86, 7.28],
                # [4.95, 5.69, 6.53, 6.93],
                # [4.83, 5.55, 6.37, 6.76],
                # ])
# Pa = np.array([ [1.12, 1.12, 1.12, 1.12],
                # [1.30, 1.30, 1.30, 1.30],
                # [1.59, 1.59, 1.59, 1.59],
                # [1.89, 1.89, 1.89, 1.89],
                # ])
# T_in_evap_cor = np.concatenate((T_in_evap,np.array([25])))
# Pc_duplique = np.transpose(np.array([Pc[:,-1]]))
# Pc_cor = np.concatenate((Pc,Pc_duplique),axis=1)
# Pa_duplique = np.transpose(np.array([Pa[:,-1]]))
# Pa_cor = np.concatenate((Pa,Pa_duplique),axis=1)
# rho_eau_g = 1002
# Cp_eau_g = 3700 
# Rb =  0.0021296006556527063 #corbeille
# dim = 10 #corbeille
# debit_gth = 1.15 #gneo 18h
# Tc = T_out_condens
# Tf = T_in_evap_cor
# Pf_tab = Pc_cor-Pa_cor

# def interpolate(a, b, fa, fb, x):
    # return (x-a)*(fb-fa)/(b-a)+fa
# _c1 =  1/(2*rho_eau_g*Cp_eau_g*debit_gth)
# _Tf = Tf.reshape((1, -1))
# # calcul temperature entree evaporateur
# def Tflee(dim, Tc_obj, Tb_obj):
    # alpha = Rb / dim -  _c1 #changement +_c1 en -_c1 pour prise Tinevap
    # Tb = Pf_tab * alpha + _Tf  #numexpr ?
    # Ic = np.digitize([Tc_obj], Tc)[0]
    # If_left = np.digitize([Tb_obj], Tb[Ic-1,:])[0]
    # If_right = np.digitize([Tb_obj], Tb[Ic,:])[0]
    # Tf_left = interpolate(Tb[Ic-1, If_left-1], Tb[Ic-1, If_left], Tf[If_left-1], Tf[If_left], Tb_obj)
    # Tf_right = interpolate(Tb[Ic, If_right-1], Tb[Ic, If_right], Tf[If_right-1], Tf[If_right], Tb_obj)
    # Tf_est = interpolate(Tc[Ic-1], Tc[Ic], Tf_left, Tf_right, Tc_obj)
    # return Tf_est


# Tflee_cython = simul_cython.Tflee_func(rho_eau_g,
                                        # Cp_eau_g,
                                        # Rb,
                                        # debit_gth,
                                        # Tc,
                                        # Tf,
                                        # Pf_tab
                                        # )
                                        
# x = T_out_condens
# y = T_in_evap_cor
# npts_1d = int(np.sqrt(ntests))+1
# ntests = npts_1d**2
# xs = np.linspace(x[0], x[-1], npts_1d, endpoint=False)
# ys = np.linspace(y[0], y[-1], npts_1d, endpoint=False)

# res = [0]*ntests
# res_cython = [0]*ntests

# t0 = time.time()
# for i,x in enumerate(xs):
    # for j,y in enumerate(ys):
        # res[i+j*npts_1d] = Tflee(dim,x,y)
        # # Tflee(dim,x,y)
# t1 = time.time()
# for i,x in enumerate(xs):
    # for j,y in enumerate(ys):
        # res_cython[i+j*npts_1d] = Tflee_cython(dim,x,y)
        # # Tflee_cython(dim,x,y)
# t2 = time.time()


# print('Runtime: {}s'.format(t1-t0))
# print('Runtime cython: {}s ({:.2f}x faster)'.format(t2-t1, (t1-t0)/(t2-t1)))
# error = np.linalg.norm(res_cython - np.array(res)) / np.linalg.norm(res)
# print('Error: {} o/o'.format(error*100))

############################
# test update_puissance_mode_RAF
# --> raf only x?
# --> ecs only x?
# --> chauf only x?
# --> all 3 + Tb x2
############################
import yaml
data_file = 'test_data.yaml'
with open(data_file, 'r') as rd:
    ref_data = yaml.load(rd)

do_ECS = ref_data['do_ECS']
do_raf = ref_data['do_raf']
dim = float(ref_data['dim'])
nbdt = ref_data['nbdt']
chargesinternes = ref_data['chargesinternes']
Rb = ref_data['Rb']
_GSh = ref_data['_GSh']
_RdGSh = ref_data['_RdGSh']
_H_bat_geoc = ref_data['_H_bat_geoc']
t_Tb_ref = ref_data['t_Tb']
t_Text = ref_data['t_Text']
t_raf = ref_data['t_raf']
t_Tinitsol = ref_data['t_Tinitsol']
t_dG_echangeur_gth = ref_data['t_dG_echangeur_gth']
nb_h_ECS = 0
P_chauf_report = 0.
heat_exch_type = 1 # corbeille

T_coupure_PAC = ref_data['T_coupure_PAC']
Temission_minimum = ref_data['Temission_minimum']
Temission1 = ref_data['Temission1']
Temission2 = ref_data['Temission2']
Text1 = ref_data['Text1']
Text2 = ref_data['Text2']
rho_eau_g = ref_data['rho_eau_g']
Cp_eau_g  = ref_data['Cp_eau_g'] 
lambda_sol = ref_data['lambda_sol']
rhocp_sol = ref_data['rhocp_sol']
debit_gth = ref_data['debit_gth']
T_condens_ECS = ref_data['T_condens_ECS'] 
T_coupure_PAC_ECS = ref_data['T_coupure_PAC_ECS']
nb_h_reduit = ref_data['nb_h_reduit']
Tc = ref_data['T_out_condens']
Tf = ref_data['T_in_evap']
Pc_tab = ref_data['Pc_tab']
Pa_tab = ref_data['Pa_tab']
Pf_tab = Pc_tab-Pa_tab
Pc = ref_data['Pc']
Pa = ref_data['Pa']
Pf = ref_data['Pf']

t_ECS_ref = ref_data['t_ECS']
t_chauf_ref = ref_data['t_chauf']
t_Pf_ref = ref_data['t_Pf']
t_Pa_ref = ref_data['t_Pa']
t_charge_ref = ref_data['t_charge']
t_Tbat_ref = ref_data['t_Tbat']

t_ECS = np.array(ref_data['t_ECS_unmod'])
t_chauf = np.array(ref_data['t_chauf_unmod'])
t_Pf = np.zeros_like(t_Pf_ref)
t_Pa = np.zeros_like(t_Pa_ref)
t_charge = np.zeros_like(t_charge_ref)
t_Tbat = np.array(ref_data['t_Tbat_unmod'])
t_puissance = np.empty(nbdt)
t_Tb = np.empty(nbdt+1)
t_Tb[0] = t_Tinitsol[0]
integrand_convol_storage = np.zeros(nbdt)

t_ECS_cython = np.array(ref_data['t_ECS_unmod'])
t_chauf_cython = np.array(ref_data['t_chauf_unmod'])
t_Pf_cython = np.zeros_like(t_Pf_ref)
t_Pa_cython = np.zeros_like(t_Pa_ref)
t_charge_cython = np.zeros_like(t_charge_ref)
t_Tbat_cython = np.array(ref_data['t_Tbat_unmod'])
t_Tb_cython = np.empty(nbdt+1)

indices_ECS = t_ECS_ref[:nbdt] > 0
indices_raf = np.logical_and(np.logical_not(indices_ECS), t_raf > 0)
indices_chauf = np.logical_not(np.logical_or(indices_ECS, indices_raf))

# indices_tested = indices_chauf
indices_tested = range(nbdt)
dt = 3600.
HCORB = 2.7
RCORB = 0.5*(1.18+1.10)/2
def G_courtterme(y):
    _la = lambda_sol*rhocp_sol
    return (1-np.sqrt(1-y)) * np.sqrt(dt) / (2*np.pi**1.5*HCORB*RCORB*np.sqrt(_la))
    
# # A TEST ON VECTORIZING G: only slightly faster
# npts = 10000
# step = 1./(npts-1)
# xs = np.linspace(0., 1., npts)
# t_G_courtterme = G_courtterme(xs)

# def G_courtterme2(y, step=step, y0=0., y1=1.):
    # if y<y0 or y>y1:
        # raise Exception('Set a value in the range [{}; {}]'.format(y0, y1))
    # if y==y1:
        # return t_G_courtterme[-1]
    # i = int((y-y0)/step)
    # return t_G_courtterme[i] + (y-i*step)*(t_G_courtterme[i+1]-t_G_courtterme[i])

# sample = np.linspace(0., 1., 17000)
# # aa = G_courtterme(sample)
# t0 = time.time()
# aa = [G_courtterme(x) for x in sample]
# t1 = time.time()
# bb = [G_courtterme2(x) for x in sample]
# t2 = time.time()
# print 'G init ', t1-t0
# print 'G new ', t2-t1, ' (time x', (t1-t0)/(t2-t1), ')'
# print 'G error new/init ', np.linalg.norm(aa-np.array(bb))/np.linalg.norm(aa)

# # A TEST ON VECTORIZING Pc: only slightly faster
# npts = 1500
# xs = np.linspace(Tc[0], Tc[-1], npts)
# ys = np.linspace(Tf[0], Tf[-1], npts)
# stepx = xs[1]-xs[0]
# stepy = ys[1]-ys[0]

# t0 = time.time()
# t_Pc_test = Pc(xs,ys)
# def Pc_test(x, y, stepx=stepx, stepy=stepy, x0=Tc[0], x1=Tc[-1], y0=Tf[0], y1=Tf[-1]):
    # if x < x0:
        # x = x0
    # elif x > x1:
        # x = x1
    # if y < y0:
        # y = y0
    # elif y > y1:
        # y = y1
    # # Previous neighbor
    # i1 = int((x-x0)/stepx)
    # j1 = int((y-y0)/stepy)
    # return t_Pc_test[i1,j1]
    # # # Bilinear interpolation: more precise but slower
    # # i1 = int((x-x0)/stepx)
    # # i2 = min(i1+1, len(xs)-1)
    # # j1 = int((y-y0)/stepy)
    # # j2 = min(j1+1, len(ys)-1)
    # # xl = x0+i1*stepx
    # # xr = x0+i2*stepx
    # # yl = y0+j1*stepy
    # # yr = y0+j2*stepy
    # # return 1./(stepx*stepy)*(   t_Pc_test[i1,j1]*(xr-x)*(yr-y)
                                # # + t_Pc_test[i2,j1]*(x-xl)*(yr-y)
                                # # + t_Pc_test[i1,j2]*(xr-x)*(y-yl)
                                # # + t_Pc_test[i2,j2]*(x-xl)*(y-yl)
                                # # )
# t1 = time.time()
# nsamples1d = int(np.sqrt(17000)+1)
# Tc_sample = Tc[0] + (Tc[-1]-Tc[0])*np.random.rand(nsamples1d)
# Tf_sample = Tf[0] + (Tf[-1]-Tf[0])*np.random.rand(nsamples1d)
# aa = [[Pc(x,y)[0][0] for y in Tf_sample] for x in Tc_sample]
# t2 = time.time()
# bb = [[Pc_test(x,y) for y in Tf_sample] for x in Tc_sample]
# t3 = time.time()

# print 'Pc build ', t1-t0
# print 'Pc init ', t2-t1
# print 'Pc new ', t3-t2, ' (time x', (t2-t1)/(t3-t2), ')'
# print 'Pc error new/init ', np.linalg.norm(aa-np.array(bb))/np.linalg.norm(aa)
    
# def Pa_test(x, y, stepx=stepx, stepy=stepy, x0=Tc[0], x1=Tc[-1], y0=Tf[0], y1=Tf[-1]):
    # if x < x0:
        # x = x0
    # elif x > x1:
        # x = x1
    # if y < y0:
        # y = y0
    # elif y > y1:
        # y = y1
    # # Previous neighbor
    # i1 = int((x-x0)/stepx)
    # j1 = int((y-y0)/stepy)
    # return t_Pa_test[i1,j1]
# def Pf_test(x, y, stepx=stepx, stepy=stepy, x0=Tc[0], x1=Tc[-1], y0=Tf[0], y1=Tf[-1]):
    # if x < x0:
        # x = x0
    # elif x > x1:
        # x = x1
    # if y < y0:
        # y = y0
    # elif y > y1:
        # y = y1
    # # Previous neighbor
    # i1 = int((x-x0)/stepx)
    # j1 = int((y-y0)/stepy)
    # return t_Pf_test[i1,j1]
# t_Pa_test = Pa(xs,ys)
# t_Pf_test = Pf(xs,ys)
    
class SousDimensionnementError(Exception):
    pass

def build_Tflinev_func():
    
    def interpolate(a, b, fa, fb, x):
        return (x-a)*(fb-fa)/(b-a)+fa

    _c1 =  1/(2*rho_eau_g*Cp_eau_g*debit_gth)
    _Tf = Tf.reshape((1, -1))
    
    # calcul temperature entree evaporateur
    def Tflee(dim, Tc_obj, Tb_obj):
        alpha = Rb / dim -  _c1 #changement +_c1 en -_c1 pour prise Tinevap
        Tb = Pf_tab * alpha + _Tf  #numexpr ?
        Ic = np.digitize([Tc_obj], Tc)[0]
        If_left = np.digitize([Tb_obj], Tb[Ic-1,:])[0]      
        If_right = np.digitize([Tb_obj], Tb[Ic,:])[0] 
        Tf_left = interpolate(Tb[Ic-1, If_left-1], Tb[Ic-1, If_left], Tf[If_left-1], Tf[If_left], Tb_obj)
        Tf_right = interpolate(Tb[Ic, If_right-1], Tb[Ic, If_right], Tf[If_right-1], Tf[If_right], Tb_obj)
        Tf_est = interpolate(Tc[Ic-1], Tc[Ic], Tf_left, Tf_right, Tc_obj)
        return Tf_est

    return Tflee
Tflee_func = build_Tflinev_func()

def f_calc_Temission(Text):
    dt = Text1-Text2
    return max(
        Temission_minimum,
        Temission1 * (Text-Text2) / dt - Temission2 * (Text-Text1)/dt
    )


def update_puissance_mode_ECS(  dim, i, t_Tb,
                                t_Pf, t_Pa, t_charge,
                                t_chauf, t_ECS,
                                nb_h_ECS, P_chauf_report,
                                (nb_h_reduit, T_condens_ECS, T_coupure_PAC_ECS,
                                Tflee_func, Pc, Pf, Pa)
                                ):
    nb_h_ECS += 1
    if nb_h_ECS > nb_h_reduit:
        raise SousDimensionnementError(
            "Pas assez de puissance pour produire l'ECS "
            "pendant la période de réduit de nuit"
        )
    P_chauf_report += t_chauf[i]
    t_chauf[i] = 0.
    Tflee = Tflee_func(dim, T_condens_ECS, t_Tb[i])
    if Tflee < T_coupure_PAC_ECS:
        raise SousDimensionnementError(
            "Température trop basse à l'évaporateur "
            "pour production ECS"
       )
    t_Pc_ECS = Pc(T_condens_ECS, Tflee)
    t_Pf[i] = Pf(T_condens_ECS, Tflee)
    t_Pa[i] = Pa(T_condens_ECS, Tflee)
    t_charge[i] = t_ECS[i] / t_Pc_ECS
    if t_charge[i] > 1:
        t_charge[i] = 1.
        t_ECS[i+1] += (t_ECS[i] - t_Pc_ECS)
        t_ECS[i] = t_Pc_ECS
    else:
        if P_chauf_report > 0:
            t_chauf[i+1:i+1+nb_h_reduit-nb_h_ECS] += P_chauf_report/(nb_h_reduit-nb_h_ECS)
            if nb_h_ECS == nb_h_reduit:
                raise SousDimensionnementError(
                    "Plus de temps pour récupérer "
                    "le retard de chauffage nocturne"
                )
        nb_h_ECS = 0
        P_chauf_report = 0.
    return nb_h_ECS, P_chauf_report
    
def update_puissance_mode_RAF(  dim, i, t_Tb,
                                t_Pf, t_charge,
                                t_Text, t_raf, t_Tbat,
                                (chargesinternes, Rb, _GSh, _H_bat_geoc)
                                ):

    _alpha = _H_bat_geoc / (1+_H_bat_geoc*Rb/dim)
    Tbat =(
        (
            _GSh * t_Text[i]
            + chargesinternes
            + _alpha * t_Tb[i]
        )
        / (_GSh + _alpha)
    )
    t_Pf[i] = - max(_GSh * (t_Text[i] - Tbat) + chargesinternes, 0)
    t_charge[i] = min(- t_raf[i] / t_Pf[i],1) if t_Pf[i] < 0 else 0.
    Pf_extraite = -t_Pf[i]*t_charge[i]
    t_Tbat[i] = t_Text[i]-(Pf_extraite - chargesinternes)/_GSh
    
def update_puissance_mode_chauf(dim, i, t_Tb,
                                t_Pf, t_Pa, t_charge,
                                t_Text, t_chauf,
                                (T_coupure_PAC, Tflee_func, f_calc_Temission, Pc, Pf, Pa),
                                ):
    Tflee = Tflee_func(dim, f_calc_Temission(t_Text[i]), t_Tb[i])
    # Attention : ceci suppose que l'écart de température à l'évaporateur 
    # est constant (non-nul), c'est-à-dire que la PAC tourne : 
    # d'où un décalage par rapport à la température sans perturbation
    if Tflee < T_coupure_PAC:
        raise SousDimensionnementError(
            "Température trop basse à l'évaporateur"
        )
    Temission = f_calc_Temission(t_Text[i])
    t_Pc = Pc(Temission, Tflee)
    t_Pf[i] = Pf(Temission, Tflee)
    t_Pa[i] = Pa(Temission, Tflee)
    t_charge[i] = t_chauf[i] / t_Pc
    if t_charge[i] > 1:
        raise SousDimensionnementError(
            "PAC sous-dimensionnée pour l'appel de puissance en chauffage"
        )
        
def update_Tb(
    dim, i,
    t_Pf,
    t_charge,
    t_puissance,
    (t_Tinitsol, G_courtterme, t_dG_echangeur_gth),
    (nbdt, integrand_convol_storage)
):
    correction = - t_Pf[i] / dim * G_courtterme(t_charge[i])


    t_puissance[i] = t_charge[i] * t_Pf[i]
    # Default
    integrand_convol = t_puissance[:i] * t_dG_echangeur_gth[1:i+1][::-1]
    Tb = t_Tinitsol[i+1] - np.sum(integrand_convol) / dim
    
    # # Taking advantage of vectorization
    # integrand_convol_storage[i+1:] += (
                                    # t_puissance[i]
                                    # *t_dG_echangeur_gth[1:nbdt-i]
                                    # )
    # integrand_convol = integrand_convol_storage[i]
    # Tb = t_Tinitsol[i+1] - integrand_convol / dim
    
    Tb = Tb + correction
    return Tb

    
float_parameters = np.array([chargesinternes, Rb, _GSh, _H_bat_geoc,
                            T_condens_ECS, T_coupure_PAC_ECS, rho_eau_g,
                            Cp_eau_g, debit_gth, T_coupure_PAC, Text1,
                            Temission1, Text2, Temission2, Temission_minimum,
                            lambda_sol, rhocp_sol])
int_parameters = np.array([nb_h_reduit, heat_exch_type], dtype=int)
cython_loop = simul_cython.make_loop(
                    nbdt,
                    int(do_ECS),
                    int(do_raf),
                    t_chauf_cython,
                    t_ECS_cython,
                    t_raf,
                    t_Tb_cython,
                    t_Pf_cython,
                    t_Pa_cython,
                    t_charge_cython,
                    t_Tbat_cython,
                    t_Text,
                    t_Tinitsol,
                    t_dG_echangeur_gth,
                    np.array(Tc, dtype=float),
                    np.array(Tf, dtype=float),
                    Pc_tab,
                    Pa_tab,
                    Pf_tab,
                    float_parameters,
                    int_parameters
                    )


# # Main loop
# nloops = 10
# t0 = time.time()
# for tt in range(nloops):
    # t_ECS[:] = ref_data['t_ECS_unmod']
    # t_chauf[:] = ref_data['t_chauf_unmod']
    # nb_h_ECS = 0
    # P_chauf_report = 0.
    # for i in xrange(nbdt):
        # if do_ECS and t_ECS[i] > 0.:
            # pass
            # nb_h_ECS, P_chauf_report = update_puissance_mode_ECS(
                # dim, i, t_Tb,
                # t_Pf, t_Pa, t_charge,
                # t_chauf, t_ECS,
                # nb_h_ECS, P_chauf_report,
                # (nb_h_reduit, T_condens_ECS, T_coupure_PAC_ECS, Tflee_func, Pc, Pf, Pa)
                # # (nb_h_reduit, T_condens_ECS, T_coupure_PAC_ECS, Tflee_func, Pc_test, Pf_test, Pa_test)
            # )
            # # Tflee = Tflee_func(dim, T_condens_ECS, t_Tb[i])
            # # Pf_extraite = t_Pf[i]*t_charge[i]
            # # t_Tfleech[i] = Tflee + Pf_extraite/Vfl_PAC/cpfl_V
        # elif do_raf and t_raf[i] > 0.: #Pas de besoin d'ECS & Calcul du rafraichissement
            # pass
            # update_puissance_mode_RAF(
                # dim, i, t_Tb,
                # t_Pf, t_charge,
                # t_Text, t_raf, t_Tbat,
                # (chargesinternes, Rb, _GSh, _H_bat_geoc)
            # )
        # elif t_chauf[i] > 0. :
            # pass
            # update_puissance_mode_chauf(
                # dim, i, t_Tb,
                # t_Pf, t_Pa, t_charge,
                # t_Text, t_chauf,
                # (T_coupure_PAC, Tflee_func, f_calc_Temission, Pc, Pf, Pa)
                # # (T_coupure_PAC, Tflee_func, f_calc_Temission, Pc_test, Pf_test, Pa_test)
            # )
            # # Tflee = Tflee_func(dim, f_calc_Temission(t_Text[i]), t_Tb[i])
            # # Pf_extraite = t_Pf[i]*t_charge[i]
            # # t_Tfleech[i] = Tflee + Pf_extraite/Vfl_PAC/cpfl_V #
        # #
        # t_Tb[i+1] = update_Tb(
            # dim, i,
            # t_Pf,
            # t_charge,
            # t_puissance,
            # (t_Tinitsol, G_courtterme, t_dG_echangeur_gth),
            # (nbdt, integrand_convol_storage)
            # # (t_Tinitsol, G_courtterme2, t_dG_echangeur_gth)
        # )
# t1 = time.time()
                            
# for tt in range(nloops):
    # # cython_loop.make_loop_ECS(  dim,
                                # # np.array(indices_ECS, dtype=int)
                                # # )
    # # cython_loop.make_loop_RAF(  dim,
                                # # np.array(indices_raf, dtype=int),
                                # # t_Text
                                # # )
    # # cython_loop.make_loop_chauf(dim,
                                # # np.array(indices_chauf, dtype=int),
                                # # t_Text
                                # # )
    # cython_loop(dim)
# t2 = time.time()
        
# # Checking
# def assert_error(arr, ref_arr, epsilon = 1.e-2):
    # """same as numpy.testing.assert_allclose(arr, ref_arr, rtol=epsilon)..."""
    # arr = arr[:nbdt][indices_tested]
    # ref_arr = ref_arr[:nbdt][indices_tested]
    # assert np.linalg.norm(arr - ref_arr) < epsilon * np.linalg.norm(ref_arr)
# assert_error(t_ECS, t_ECS_ref)
# assert_error(t_chauf, t_chauf_ref)
# assert_error(t_Pf, t_Pf_ref)
# assert_error(t_Pa, t_Pa_ref)
# assert_error(t_charge, t_charge_ref)
# assert_error(t_Tbat, t_Tbat_ref)
# assert_error(t_Tb, t_Tb_ref)


# assert_error(t_ECS_cython, t_ECS_ref)
# assert_error(t_chauf_cython, t_chauf_ref)
# assert_error(t_Pf_cython, t_Pf_ref)
# assert_error(t_Pa_cython, t_Pa_ref)
# assert_error(t_charge_cython, t_charge_ref)
# assert_error(t_Tbat_cython, t_Tbat_ref)
# assert_error(t_Tb_cython, t_Tb_ref)


# print('Runtime: {}s'.format(t1-t0))
# print('Runtime cython: {}s ({:.2f}x faster)'.format(t2-t1, (t1-t0)/(t2-t1)))

# # Profiling: Python
# # https://cython.readthedocs.io/en/latest/src/tutorial/profiling_tutorial.html
# import pstats, cProfile

# def make_run():
    # t_ECS[:] = ref_data['t_ECS_unmod']
    # t_chauf[:] = ref_data['t_chauf_unmod']
    # nb_h_ECS = 0
    # P_chauf_report = 0.
    # for i in xrange(nbdt):
        # if do_ECS and t_ECS[i] > 0.:
            # pass
            # nb_h_ECS, P_chauf_report = update_puissance_mode_ECS(
                # dim, i, t_Tb,
                # t_Pf, t_Pa, t_charge,
                # t_chauf, t_ECS,
                # nb_h_ECS, P_chauf_report,
                # (nb_h_reduit, T_condens_ECS, T_coupure_PAC_ECS, Tflee_func, Pc, Pf, Pa)
                # # (nb_h_reduit, T_condens_ECS, T_coupure_PAC_ECS, Tflee_func, Pc_test, Pf_test, Pa_test)
            # )
            # # Tflee = Tflee_func(dim, T_condens_ECS, t_Tb[i])
            # # Pf_extraite = t_Pf[i]*t_charge[i]
            # # t_Tfleech[i] = Tflee + Pf_extraite/Vfl_PAC/cpfl_V
        # elif do_raf and t_raf[i] > 0.: #Pas de besoin d'ECS & Calcul du rafraichissement
            # pass
            # update_puissance_mode_RAF(
                # dim, i, t_Tb,
                # t_Pf, t_charge,
                # t_Text, t_raf, t_Tbat,
                # (chargesinternes, Rb, _GSh, _H_bat_geoc)
            # )
        # elif t_chauf[i] > 0. :
            # pass
            # update_puissance_mode_chauf(
                # dim, i, t_Tb,
                # t_Pf, t_Pa, t_charge,
                # t_Text, t_chauf,
                # (T_coupure_PAC, Tflee_func, f_calc_Temission, Pc, Pf, Pa)
                # # (T_coupure_PAC, Tflee_func, f_calc_Temission, Pc_test, Pf_test, Pa_test)
            # )
            # # Tflee = Tflee_func(dim, f_calc_Temission(t_Text[i]), t_Tb[i])
            # # Pf_extraite = t_Pf[i]*t_charge[i]
            # # t_Tfleech[i] = Tflee + Pf_extraite/Vfl_PAC/cpfl_V #
        # #
        # t_Tb[i+1] = update_Tb(
            # dim, i,
            # t_Pf,
            # t_charge,
            # t_puissance,
            # (t_Tinitsol, G_courtterme, t_dG_echangeur_gth),
            # (nbdt, integrand_convol_storage)
            # # (t_Tinitsol, G_courtterme2, t_dG_echangeur_gth)
        # )


# cProfile.runctx("make_run()", globals(), locals(), "Profile.prof")

# s = pstats.Stats("Profile.prof")
# s.strip_dirs().sort_stats("time").print_stats()

# Profiling: Cython
# https://cython.readthedocs.io/en/latest/src/tutorial/profiling_tutorial.html
import pstats, cProfile

cProfile.runctx("cython_loop(dim)", globals(), locals(), "Profile.prof")

s = pstats.Stats("Profile.prof")
s.strip_dirs().sort_stats("time").print_stats()