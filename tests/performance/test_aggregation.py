#coding: utf8


""" Testing Bernier et al 2004 multiple load aggregation algorithm method """

import numpy as np
import matplotlib.pyplot as plt

# Synthetic symmetric load
def synth_load(nyears, A=2000., B=2190., C=80., D=2., E=0.01, F=0., G=0.95):
    """ Synthetic real symmetric load in W of a heat-exchanger
    
    nyears: int, number of years to cover
    A to G: floats, function parameters
    
    NB: see Bernier et al (2004) for more details
    """
    h_per_y = 8760
    hours = np.arange(h_per_y, dtype=float)
    def f_func(x, A=A, B=B, C=C, N1=12., N2=84):
        return (A
                * np.sin(np.pi/N1*(x-B))
                * np.sin(np.pi/4380*(x-B))
                * ( (168.-C)/168
                    + np.sum(   [   1/(i*np.pi)
                                    * (np.cos(C*np.pi*i/N2)-1)
                                    * np.sin(np.pi*i/N2*(x-B))
                                    for i in range(1, 4)
                                    ],
                                axis=0
                                )
                    )
                )
    global a
    global b
    global c
    global pows
    pows = (-1)**np.floor(D/8760.*(hours-B))
    a = f_func(hours)
    b = pows * np.abs(f_func(hours))
    c = E * pows * np.sign(np.cos(D*np.pi/4380*(hours-F))+G)
    load =  np.tile(f_func(hours)
                    + ( pows
                        * np.abs(f_func(hours))
                        )
                    + ( E
                        * pows
                        * np.sign(np.cos(D*np.pi/4380*(hours-F))+G)
                        ),
                    nyears
                    )
    return load
# test_Bernier = synth_load(1)
# plt.plot(-test_Bernier) # NB: q < 0 for heat rejection to the ground
# plt.show()

def aggreg_Bernier( dim, t_Tb, t_pow, t_Pf, t_charge, t_dG, G_courtterme,
                    Xh, Xd, Xw, Xm
                    ):
    """ Multiple Load Aggregation Algorithm by Bernier et al (2004) """
    nbdt = len(t_pow)
    for i in range(nbdt):
        correction = - t_Pf[i] / dim * G_courtterme(t_charge[i])
        t_pow[i] = t_charge[i] * t_Pf[i]
        nhours = i+1 # /!\ index i starts at 0
        Nh = min(nhours, Xh)
        Nd = 0
        Nw = 0
        Nm = 0
        if nhours >= Xh+Xd:
            Nd = Xd
            if nhours >= Xh+Xd+Xw:
                Nw = Xw
                if nhours >= Xh+Xd+Xw+Xm:
                    Nm = Xm
        Ny = nhours - (Nh+Nd+Nw+Nm)
        
        # ih = i-Nh
        
        
        
        # print('i', i)
        # print('Nh', Nh, i-(Nh-1), '->', i-1)
        # print('Nd', Nd, Ny+Nm+Nw, '->', Ny+Nm+Nw+Nd-1)
        # print('Nw', Nw, Ny+Nm, '->', Ny+Nm+Nw-1)
        # print('Nm', Nm, Ny, '->', Ny+Nm-1)
        # print('Ny', Ny, 0, '->', Ny)
        
        # A = t_G[i]
        # B = t_G[i-Ny]
        # C = t_G[i-Ny-Nm]
        # D = t_G[i-Ny-Nm-Nw]
        # E = t_G[i-Ny-Nm-Nw-Nd]
        # t_Tb[i+1] = 
        integrand_convol = (
                    np.mean(t_pow[:Ny])*t_dG[i-Ny+1:i+1] #  t_G[i]-t_G[i-Ny]
                    + np.mean(t_pow[Ny:Ny+Nm])*t_dG[i-Ny-Nm+1:
                                                    i-Ny+1
                                                    ] # t_G[i-Ny] - t_G[i-Ny-Nm]
                    + np.mean(t_pow[Ny+Nm:Ny+Nm+Nw])*t_dG[
                                                    i-Ny-Nm-Nw+1:
                                                    i-Ny-Nm+1
                                                    ] # t_G[i-Ny-Nm] - t_G[i-Ny-Nm-Nw]
                    + np.mean(t_pow[Ny+Nm+Nw:Ny+Nm+Nw+Nd])*t_dG[
                                                    i-Ny-Nm-Nw-Nd+1:
                                                    i-Ny-Nm-Nw+1
                                                    ] #t_G[i-Ny-Nm-Nw]-t_G[i-Ny-Nm-Nw-Nd]
                    + np.dot(   t_pow[i-(Nh-1):i],
                                t_dG[1:Nh][::-1]
                                )
                    )
        Tb = t_Tinitsol[i+1] - integrand_convol / dim
        t_Tb[i+1] = Tb + correction
                
# def Gfunc_CHS(Fo)
    # """ G func. for the Cylindrical Heat Source (Carslaw and Jaeger, 1947)
    
    # Fo: float or array, adimensionned time alpha*t/rb**2
    
    # NB: solution at the borehole wall (r=rb)
    
    # """
    
    

""" Testing aggregation implementation (error and speed) """
import cProfile, pstats
import numpy as np
import os
import sys
mgeo_headdir = os.path.join('..', '..')
sys.path.append(mgeo_headdir)
import geodim.donnees as donnees
import geodim.echangeur_gth as echangeur_gth
import geodim.calcul as calcul
# import allclose

def compute(data, ech_gth, dim, Tflee_func, Tfloutcond_func, use_aggregation):
    """ From dicts brut_data and ech_gth, returns direct run result for dim """
    result = calcul.dimensionnement.calcul_cop( data, ech_gth, dim, Tflee_func,
                                                Tfloutcond_func, None, False
                                                )
    # result = calcul.calcul_dimensionnement(data, ech_gth, print_messages=True)
    return result

data_brut = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.65,
    nb_occupants=5,
    heatpump='GNEO_18H',
    P_appoint=0,
    emitter='PC',
    heat_exchanger='vertical',
    ground_type='sable_alluv',
    scop_dim=4.4,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=20,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
    
# import yaml
# tabs = yaml.load(open('test_aggregation_data.yaml', 'r'), Loader=yaml.Loader)

# t_Pf=       tabs['t_Pf']
# t_charge=   tabs['t_charge']
# t_Tinitsol= tabs['t_Tinitsol']
# t_puissance0 = tabs['t_puissance']
# t_Tb0       = tabs['t_Tb']

# dim = 120
# data = donnees.traitement(data_brut)
# ech = echangeur_gth.build(data)
# G_courtterme = ech['G_courtterme']
# t_dG_echangeur_gth = ech['t_dG_echangeur_gth']
# nbdt = len(t_puissance0)
# t_puissance1 = np.empty_like(t_puissance0)
# t_Tb1 = np.array(t_Tb0)
# def foo():
    # for i in range(nbdt):
        # correction = - t_Pf[i] / dim * G_courtterme(t_charge[i])
        # t_puissance1[i] = t_charge[i] * t_Pf[i]
        # integrand_convol = np.dot(  t_puissance1[:i],
                                    # t_dG_echangeur_gth[1:i+1][::-1]
                                    # )
     
        # Tb = t_Tinitsol[i+1] - integrand_convol / dim
        # t_Tb1[i+1] = Tb + correction

# # aggregated
# n_steps_agg = 1440
# n_steps_imm = 360
# n_agg = 0
# i_next = n_steps_agg + n_steps_imm 
# i_prev = 0 
# is_agg = [i_next, i_prev, n_agg]
# n_steps_agg_max = nbdt//n_steps_agg + 1
# t_puissance_agg = np.zeros(n_steps_agg_max)
# t_dG_agg = np.zeros(n_steps_agg_max)

# t_puissance2 = np.empty_like(t_puissance0)
# t_Tb2 = np.array(t_Tb0)

# def foo_agg():
    # for i in range(nbdt):
        # correction = - t_Pf[i] / dim * G_courtterme(t_charge[i])
        # t_puissance2[i] = t_charge[i] * t_Pf[i]
        # i_next, i_prev, n_agg = is_agg
        # if i == i_next:
            # # When reached, increases number of aggregated blocks
            # t_puissance_agg[n_agg] = np.mean(
                                        # t_puissance2[i_prev:i_prev+n_steps_agg]
                                        # )
            # # print('range', i)
            # # print(i_prev,i_prev+n_steps_agg)
            # # print(i-n_steps_imm,i-n_steps_agg-n_steps_imm)
            # t_dG_agg[n_agg] = np.sum(t_dG_echangeur_gth[i-n_steps_agg+1:i+1]) # = G[i] - G[i-n_steps_agg]
            # i_next += n_steps_agg # index to reach for updating number of aggregations
            # i_prev += n_steps_agg # index for aggregation end / immediate start
            # n_agg += 1 # number of aggregations
            # is_agg[:] = i_next, i_prev, n_agg
        # # Immediate
        # integrand_convol = np.dot(  t_puissance2[i_prev:i],
                                    # t_dG_echangeur_gth[1:i-i_prev+1][::-1]
                                    # )
        # # Long-term (aggregated)
        # integrand_convol += np.dot(t_puissance_agg[:n_agg], t_dG_agg[:n_agg])
     
        # Tb = t_Tinitsol[i+1] - integrand_convol / dim
        # t_Tb2[i+1] = Tb + correction
        
# # aggregated, Bernier
# Xh = 12
# Xd = 48
# Xw = 168
# Xm = 360

# t_puissance3 = np.empty_like(t_puissance0)
# t_Tb3 = np.array(t_Tb0)

# def foo_agg_bernier():
    # aggreg_Bernier( dim, t_Tb3, t_puissance3, t_Pf, t_charge, t_dG_echangeur_gth, G_courtterme,
                    # Xh, Xd, Xw, Xm
                    # )

# prof_file = 'Run_profiling.prof'
# prof_file_agg = 'Run_profiling_agg.prof'
# prof_file_agg_bernier = 'Run_profiling_agg.prof'
# nstats_to_print = 8
# cProfile.runctx(
        # 'foo()',
        # globals(), locals(), prof_file
        # )
# s = pstats.Stats(prof_file)
# s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
# t0 = s.total_tt
# cProfile.runctx(
        # 'foo_agg()',
        # globals(), locals(), prof_file_agg
        # )
# s = pstats.Stats(prof_file_agg)
# s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
# # cProfile.runctx(
        # # 'foo_agg_bernier()',
        # # globals(), locals(), prof_file_agg_bernier
        # # )
# # s = pstats.Stats(prof_file_agg_bernier)
# # s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)

# # plt.plot(t_Tb0[-8761:-1], label='true')
# # plt.plot(t_Tb2[-8761:-1], label='agg')
# # # plt.plot(t_Tb3[-8761:-1], label='agg ber')
# # plt.legend()
# # plt.show()
# print('Direct run')
# print('max dT', np.max(np.abs(t_Tb2-t_Tb0)))
# print('      ', np.max(np.abs(1-t_Tb0/t_Tb2)), '%')
# print('rms dT', np.sqrt(1./nbdt)*np.linalg.norm(t_Tb2-t_Tb0))
# print('      ', np.sqrt(1./nbdt)*np.linalg.norm(1-t_Tb0/t_Tb2), '%')
# print('speed gain', t0/s.total_tt)
# # print(np.max(np.abs(t_Tb3-t_Tb0)))

prof_file = 'Run_profiling.prof'
prof_file_agg = 'Run_profiling_agg.prof'
nstats_to_print = 8   
dim = 120
use_aggregation = False
data = donnees.traitement(data_brut, use_aggregation=use_aggregation)
ech_gth = echangeur_gth.build(data)
Tflee_func = calcul.dimensionnement.build_Tflinev_func(data, ech_gth)
Tfloutcond_func = calcul.dimensionnement.build_Tfloutcond_func(data, ech_gth)
cProfile.runctx(
        'res = compute(data, ech_gth, dim, Tflee_func, Tfloutcond_func,'
        ' use_aggregation)',
        globals(), locals(), prof_file
        )
s = pstats.Stats(prof_file)
s.strip_dirs().sort_stats("time").print_stats()
# res = compute(  data, ech_gth, dim, Tflee_func, Tfloutcond_func,
                # use_aggregation
                # )
use_aggregation = True
data = donnees.traitement(data_brut, use_aggregation=use_aggregation)
ech_gth = echangeur_gth.build(data)
Tflee_func = calcul.dimensionnement.build_Tflinev_func(data, ech_gth)
Tfloutcond_func = calcul.dimensionnement.build_Tfloutcond_func(data, ech_gth)
# res_agg = compute(  data, ech_gth, dim, Tflee_func, Tfloutcond_func,
                # use_aggregation
                # )
cProfile.runctx(
        'res_agg = compute(data, ech_gth, dim, Tflee_func, Tfloutcond_func,'
        ' use_aggregation)',
        globals(), locals(), prof_file_agg
        )
s_agg = pstats.Stats(prof_file_agg)
s_agg.strip_dirs().sort_stats("time").print_stats()
tb = res[-1][0]
tb_agg = res_agg[-1][0]
fig, ax = plt.subplots()
ax.plot(tb[::24], 'k-', label='sans agg.')
ax.plot(tb_agg[::24], 'r--', label='avec agg.')
ax.set_title('Forage vertical, long. foree = {:.2f} m'.format(dim))
ax.set_xlabel(r'$t [h]$')
ax.set_ylabel(r'$T_{paroi} [C]$')
ax.legend()
nbdt = len(tb)
dT = np.abs(tb-tb_agg)
dT_rel = np.abs(1.-tb_agg/tb)*100
dTrms = np.sqrt(1./nbdt)*np.linalg.norm(tb-tb_agg)
dTrms_rel = np.sqrt(1./nbdt)*np.linalg.norm(1.-tb_agg/tb)*100
print('Dim')
print('max dT', np.max(dT))
print('      ', np.max(dT_rel), '%')
print('rms dT', dTrms)
print('      ', dTrms_rel, '%')
print('speed gain', s.total_tt/s_agg.total_tt)
# print('max dP / rms dP')
# for tt, tab, tab_agg, ax in zip(('Chauffage', 'ECS', 'RAF'),
                                # res[-1][:-1],
                                # res_agg[-1][:-1],
                                # axes
                                # ):
    # if (tab is None
        # or tab is None
        # ):
        # ax.set_title('{} (non pris en compte)'.format(tt))
        # continue
    # ax.plot(tab[::24]/1000., 'k-', label='sans agg.')
    # ax.plot(tab_agg[::24]/1000., 'r--', label='avec agg.')
    # ax.set_xlabel('t [h]')
    # ax.set_title(tt)
    # ax.legend()
    # mask = np.logical_and(tab > 0, tab_agg > 0)
    # tab = tab[mask]
    # tab_agg = tab_agg[mask]
    # print(' - {}    : {:.2f} /{:.2f}'.format(   tt,
                                            # np.max(np.abs(tab-tab_agg)),
                                            # np.sqrt(1./len(tab))*np.linalg.norm(tab-tab_agg)
                                            # )
                                        # )
    # print('{}: {:.2f} /{:.2f} %'.format(    ' '*(len(tt)+7),
                                            # 100.*np.max(np.abs(1-tab_agg/tab)),
                                            # 100.*np.sqrt(1./len(tab))*np.linalg.norm(1-tab_agg/tab)
                                            # )
                                        # )


