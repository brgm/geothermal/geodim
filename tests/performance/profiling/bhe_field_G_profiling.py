#coding: utf8

""" Testing G function for Heat Exchanger Fields """
import cProfile, pstats
import io
import numpy as np
import os
import sys
mgeo_headdir = os.path.join('..', '..')
sys.path.append(mgeo_headdir)
import geodim.bhe_field.bhe_field as bf
    
if __name__ == '__main__':
    data = None
    heat_exch = bf.BHE_field_exchg(data)
    # # Gfunc building speed
    # times = np.arange(8760) + 24*8760 # NB: short term times give 0 G
    # rb = 0.08  # rayon [m]
    # lm = lambda_sable_alluv = 1.9    #W/m/K
    # rcp = rhocp_sable_alluv = 2.3e6   #J/m3/K
    # ts = lm/rcp * times/rb**2
    # hs = np.linspace(5., 50., 5)/rb
    # ds = np.linspace(10., 100., 5)/rb
    # # - vectorized
    # prof_file = "Profile.prof"
    # cProfile.runctx("G = heat_exch.Gfunc(hs, ds, ts)", globals(), locals(),
                    # prof_file
                    # )
    # s = pstats.Stats(prof_file)
    # s.strip_dirs().sort_stats("time").print_stats()
    # # - sequential
    # # Recommended not to exceed len(hs) * len(ds) * len(ts) ~100k (~10s run)
    # def G_bf(Hstar, dstar, tstar):
        # """ A bruteforce version of G function """
        # G = np.empty(shape=(len(Hstar),len(dstar),len(tstar)))
        # for i,h in enumerate(Hstar):
            # for j,d in enumerate(dstar):
                # for k,t in enumerate(tstar):
                    # G [i,j,k] = heat_exch.Gfunc_scal(h, d, t)
        # return G
    # cProfile.runctx("Gbf = G_bf(hs, ds, ts)", globals(), locals(), prof_file)
    # s = pstats.Stats(prof_file)
    # s.strip_dirs().sort_stats("time").print_stats()
    # assert np.allclose(G, Gbf)
    
    # # t_dG_echangeur building speed
    # nyears = 25
    # times = np.arange(nyears*8760)
    # rb = 0.08  # rayon [m]
    # lm = lambda_sable_alluv = 1.9    #W/m/K
    # rcp = rhocp_sable_alluv = 2.3e6   #J/m3/K
    # H = 100. # np.array((100.,200.))  # longueur unitaire SGV [m]
    # dist = 2.5
    # ts = lm/rcp * times/rb**2
    # hs = H/rb
    # ds = dist/rb
    # nx = 7
    # ny = 5
    # x = np.arange(0, nx)*ds
    # y = np.arange(0, ny)*ds
    # x,y = np.meshgrid(x, y)
    # pts = np.vstack((x.flatten(), y.flatten()))
    # heat_exch.tstar = ts
    # heat_exch.field_points = pts.T
    # heat_exch.t_dG_echangeur = np.empty_like(times)
    
    # # - full
    # prof_file = "Profile.prof"
    # cProfile.runctx("heat_exch.build_t_dG_echangeur_gth(hs)",
                    # globals(), locals(), prof_file
                    # )
    # dG = np.array(heat_exch.t_dG_echangeur)
    # s = pstats.Stats(prof_file)
    # s.strip_dirs().sort_stats("time").print_stats()
    # prof_file = "Profile.prof"
    # # - linearized
    # heat_exch.t_dG_echangeur[:] = 0.
    # cProfile.runctx("heat_exch.build_t_dG_echangeur_gth(hs, ngeomsteps=100)",
                    # globals(), locals(), prof_file
                    # )
    # dG_lin = np.array(heat_exch.t_dG_echangeur)
    # s = pstats.Stats(prof_file)
    # s.strip_dirs().sort_stats("time").print_stats()
    
    # ts_geom = ( np.geomspace(   ts[0]+1, ts[-1]+1,
                                # num=100
                                # )
                # - 1
                # )
    # # Recommended not to exceed len(hs) * len(ds) * len(ts) ~100k (~10s run)
    # def G_bf(Hstar, dstar, tstar):
        # """ A bruteforce version of G function """
        # G = np.empty(shape=(len(Hstar),len(dstar),len(tstar)))
        # for i,h in enumerate(Hstar):
            # for j,d in enumerate(dstar):
                # for k,t in enumerate(tstar):
                    # G [i,j,k] = heat_exch.Gfunc_scal(h, d, t)
        # return G
    # cProfile.runctx("Gbf = G_bf(hs, ds, ts)", globals(), locals(), prof_file)
    # s = pstats.Stats(prof_file)
    # s.strip_dirs().sort_stats("time").print_stats()
    # assert np.allclose(G, Gbf)

    # # Checking spatial superposition
    # nyears = 25
    # times = np.arange(nyears*8760)
    # rb = 0.08  # rayon [m]
    # lm = lambda_sable_alluv = 1.9    #W/m/K
    # rcp = rhocp_sable_alluv = 2.3e6   #J/m3/K
    # H = 100. # np.array((100.,200.))  # longueur unitaire SGV [m]
    # dist = 2.5
    # ts = lm/rcp * times/rb**2
    # hs = H/rb
    # ds = dist/rb
    # nx = 7
    # ny = 5
    # x = np.arange(0, nx)*ds
    # y = np.arange(0, ny)*ds
    # x,y = np.meshgrid(x, y)
    # pts = np.vstack((x.flatten(), y.flatten()))
    # # Gs = Gfunc_field(hs, ts, pts.T)
    # import pstats, cProfile
    # import io
    # mem_file = io.BytesIO()
    # cProfile.runctx("Gs = Gfunc_field(hs, ts, pts.T)", globals(), locals(), "Profile.prof")
    # # s = pstats.Stats(mem_file)
    # # s.strip_dirs().sort_stats("time").print_stats(5)
    pass