#coding: utf8

""" Test les performances apportées par l'usage de Cython """
import numpy as np
import os
import pathlib
import sys
import time

CWD = pathlib.Path(__file__).parent
geodim_headdir = CWD.parents[2]
sys.path.append(geodim_headdir.as_posix())
import geodim.donnees as donnees
import geodim.echangeur_gth as echangeur_gth
import geodim.calcul as calcul

def compute(brut_data, use_aggregation):
    """ Running times for data processing, heat exch build and dimensionning """
    t0 = time.time()
    data = donnees.traitement(brut_data, use_aggregation=use_aggregation)
    t1 = time.time()
    ech_gth = echangeur_gth.build(data)
    t2 = time.time()
    result = calcul.calcul_dimensionnement(data, ech_gth)
    # result = []
    t3 = time.time()
    return t1-t0, t2-t1, t3-t2

            
data = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.65,
    nb_occupants=5,
    heatpump='GNEO_18H',
    P_appoint=0,
    emitter='PC',
    heat_exchanger=None,
    ground_type='sable_alluv',
    scop_dim=4.4,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=20,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
    
use_aggregation = True
    
ech_to_test = [
                # 'corbeille',
                # 'horizontal',
                'mvertical',
                # 'vertical',
                ]
for ech in ech_to_test:
    print('{}:'.format(ech))
    ech_module = getattr(echangeur_gth, ech) # converts ech from str to module
    # if not ech_module.use_cython:
        # print(' - Cython implementation not found')
        # print('')
        # continue
    data['heat_exchanger'] = ech
    # Using cython
    times = compute(data, use_aggregation)
    # Without cython
    ech_module.use_cython = False
    calcul.dimensionnement.use_cython = False
    times_ref = compute(data, use_aggregation)
    print(' - times:')
    print('                 | Build | Run   | Total |')
    print('    o no Cython: |{:<7.2f}|{:<7.2f}|{:<7.2f}| s'.format(
                                                    times_ref[1],
                                                    times_ref[2],
                                                    sum(times_ref),
                                                    ))
    print('    o Cython:    |{:<7.2f}|{:<7.2f}|{:<7.2f}| s'.format(
                                                    times[1],
                                                    times[2],
                                                    sum(times),
                                                    ))
    speed_gains = np.divide(times_ref, times)
    print('    o speed gain:|{:<7.2f}|{:<7.2f}|{:<7.2f}| x'.format(
                                                    speed_gains[1],
                                                    speed_gains[2],
                                                    sum(times_ref)/sum(times),
                                                    ))
    print('')
          