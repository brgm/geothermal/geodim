import cProfile, pstats
import numpy as np
import scipy.interpolate as sciint

# Pairs of (Text, Temission) temperatures
loi_deau = np.array((
    (-10., 35.),
    (-5. , 35.),
    (0.  , 35.),
    (5.  , 30.),
    (10. , 25.),
    (15. , 20.),
    (16. , 20.)
    ))
ext_temps,em_temps = loi_deau.T
    
interp_func = sciint.interp1d(
    *loi_deau.T, bounds_error=False, fill_value=(em_temps[0], em_temps[-1])
    )

    
# def interp_1(Text):
    # dt = Text1-Text2
    
def interp_1(Text):
    return interp_func(Text)
    
def interp_two_values(Text, Text1, Text2, Temission1, Temission2):
    dt = Text1-Text2
    return Temission1 * (Text-Text2) / dt - Temission2 * (Text-Text1)/dt

def interp_2(Text):
    i = np.digitize(Text, ext_temps)-1
    if i<0:
        return em_temps[0]
    if i >= len(ext_temps)-1:
        return em_temps[-1]
    return interp_two_values(
        Text, ext_temps[i], ext_temps[i+1], em_temps[i], em_temps[i+1]
        )

Text_test = np.linspace(-15,25,100000)

def many_calls_1():
    return np.array([interp_1(Text) for Text in Text_test])
    
def many_calls_2():
    return np.array([interp_2(Text) for Text in Text_test])
        
prof_file_1 = 'interp_1.prof'
prof_file_2 = 'interp_2.prof'
nstats_to_print = 5

cProfile.runctx('res1 = many_calls_1()', globals(), locals(), prof_file_1)
s = pstats.Stats(prof_file_1)
s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
t_1 = s.total_tt

cProfile.runctx('res2 = many_calls_2()', globals(), locals(), prof_file_2)
s = pstats.Stats(prof_file_2)
s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
t_2 = s.total_tt

print(f'Speed gain: {t_1/t_2:.2f}x')
    