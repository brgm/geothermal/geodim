#coding: utf8

""" Teste les performances apportées par l'usage de Cython (building) """
import cProfile, pstats
import numpy as np
import os
import pathlib
import sys
import time

CWD = pathlib.Path(__file__).parent
geodim_headdir = CWD.parents[2]
sys.path.append(geodim_headdir.as_posix())
import geodim.donnees as donnees
import geodim.echangeur_gth as echangeur_gth
import allclose

def compute(brut_data):
    """ From dict brut_data, returns initialized heat exchanger """
    data = donnees.traitement(brut_data)
    ech_gth = echangeur_gth.build(data)
    return ech_gth

prof_file_py = 'Build_profiling_py.prof'
prof_file_cy = 'Build_profiling_cy.prof'
nstats_to_print = 5
data = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.65,
    nb_occupants=5,
    heatpump='GNEO_18H',
    P_appoint=0,
    emitter='PC',
    heat_exchanger=None,
    ground_type='sable_alluv',  # 5: sable / alluvions
    scop_dim=4.4,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=20,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
    
ech_and_attr_to_test = {
    'corbeille': [  't_dG_echangeur_gth',
                    't_G_echangeur_gth'
                    ],
    'horizontal': [ 't_dG_echangeur_gth',
                    't_G_echangeur_gth'
                    ],
    'mvertical': [  't_dG_echangeur_gth',
                    't_G_echangeur_gth'
                    ],
    'vertical': [   't_dG_echangeur_gth',
                    't_G_echangeur_gth'
                    ]
    }
for ech, attributes in ech_and_attr_to_test.items():
    print('{}:'.format(ech))
    ech_module = getattr(echangeur_gth, ech) # converts ech from str to module
    if not ech_module.use_cython:
        print(' - Cython implementation not found')
        print('')
        continue
    data['heat_exchanger'] = ech
    # Using cython
    cProfile.runctx('ech_data = compute(data)', globals(), locals(),
                    prof_file_cy
                    )
    s = pstats.Stats(prof_file_cy)
    s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
    results = [ech_data[attr] for attr in attributes]
    # Without cython
    ech_module.use_cython = False
    cProfile.runctx('ech_data_ref = compute(data)', globals(), locals(),
                    prof_file_py
                    )
    s = pstats.Stats(prof_file_py)
    s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
    results_ref = [ech_data_ref[attr] for attr in attributes]
    errors = [  allclose.allclose(attr, attr_ref)
                for attr, attr_ref in zip(results, results_ref)
                ]
    print(' - Results errors are negligible:')
    for attr, err_bool in zip(attributes, errors):
        print ('    o {}: {}'.format(attr, err_bool))
    assert np.all(errors)
    print('')

for file in (prof_file_py, prof_file_cy):
    os.remove(file)