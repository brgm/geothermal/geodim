#coding: utf8

""" Teste les performances apportées par l'usage de Cython (calcul direct) """
import cProfile, pstats
import numpy as np
import os
import pathlib
import sys
import time

CWD = pathlib.Path(__file__).parent
geodim_headdir = CWD.parents[2]
sys.path.append(geodim_headdir.as_posix())
import geodim.donnees as donnees
import geodim.echangeur_gth as echangeur_gth
import geodim.calcul as calcul
import allclose

def compute(data, ech_gth, dim, Tflee_func, Tfloutcond_func, cython_data):
    """ From dicts brut_data and ech_gth, returns direct run result for dim """
    result = calcul.dimensionnement.calcul_cop(
        data, ech_gth, dim, Tflee_func, Tfloutcond_func, cython_data, False
        )
    return result

prof_file_py = 'Run_profiling_py.prof'
prof_file_cy = 'Run_profiling_cy.prof'
nstats_to_print = 5    
data_brut = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.65,
    nb_occupants=5,
    heatpump='GNEO_18H',
    P_appoint=0,
    emitter='PC',
    heat_exchanger=None,
    ground_type='sable_alluv',
    scop_dim=4.4,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=20,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
    
use_aggregation = False
    
ech_and_dim_to_test = {
                        'corbeille': 10,
                        'horizontal': 225,
                        'mvertical': 20,
                        'vertical': 120,
                        }
for ech, dim in ech_and_dim_to_test.items():
    print('{}:'.format(ech))
    ech_module = getattr(echangeur_gth, ech) # converts ech from str to module
    if not ech_module.use_cython:
        print(' - Cython implementation not found')
        print('')
        continue
    data_brut['heat_exchanger'] = ech
    data = donnees.traitement(data_brut, use_aggregation=use_aggregation)
    ech_gth = echangeur_gth.build(data)
    # Using cython
    Tflee_func = None
    Tfloutcond_func = None
    cython_data = calcul.dimensionnement.prepare_cython_loop(data, ech_gth)
    cProfile.runctx(
            'results=compute(data, ech_gth, dim, Tflee_func,'
            ' Tfloutcond_func, cython_data)',
            globals(), locals(), prof_file_cy
            )
    s = pstats.Stats(prof_file_cy)
    s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
    # Without cython
    calcul.dimensionnement.use_cython = False
    Tflee_func = calcul.dimensionnement.build_Tflinev_func(data, ech_gth)
    Tfloutcond_func = calcul.dimensionnement.build_Tfloutcond_func( data,
                                                                    ech_gth
                                                                    )
    cython_data = None
    cProfile.runctx(
            'results_ref=compute(data, ech_gth, dim, Tflee_func,'
            ' Tfloutcond_func, cython_data)',
            globals(), locals(), prof_file_py
            )
    s = pstats.Stats(prof_file_py)
    s.strip_dirs().sort_stats("time").print_stats(nstats_to_print)
    
    
    errors = [  allclose.allclose(res, res_ref)
                for res, res_ref in zip(results, results_ref)
                ]
    same_res = np.all(errors)
    print(' - Results errors are negligible:', same_res)
    assert same_res
    print('')
    

# for file in (prof_file_py, prof_file_cy):
    # os.remove(file)