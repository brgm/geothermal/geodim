#coding: utf8
""" np.allclose-like function able to deal with types not handled by default """

from numpy import allclose as npallclose

def allclose(a, b):
    """ Check that a and b are almost equal """
    if isinstance(a, (list, tuple)) and isinstance(b, (list, tuple)):
        return all([allclose(suba, subb) for suba, subb in zip(a, b)])
    try:
        return npallclose(a, b)
    except TypeError:
        return a == b
        
        
if __name__ == '__main__':
    import numpy as np
    a = np.linspace(0., 1.)
    b = np.array(a)
    b[10] += 1.e-6
    tests = (   ('Arrays', (a, b)),
                ('None', (None, None)),
                ('str', ('hello world', 'feelin alright?')),
                ('tuple', ((1,np.pi), (1,np.pi))),
                ('tuple (nested)', (((1,np.pi), (9632, 'yop')),
                                    ((1,np.pi), (9632, 'yop')))
                                    ),
                )
    
    for tested_type, vals in tests:
        print(tested_type)
        try:
            result = np.allclose(*vals)
        except TypeError:
            result = 'fail to pass test'
        print('    - np.allclose: ', result)
        print('    - allclose:    ', allclose(*vals))
    