#coding: utf8

""" Test les performances apportées par l'usage de Cython """
import numpy as np
import os
import pathlib
import sys
import time

CWD = pathlib.Path(__file__).parent
geodim_headdir = CWD.parents[1]
sys.path.append(geodim_headdir.as_posix())
from geodim.models import load_models

import geodim.echangeur_gth.corbeille as corbeille
import geodim.echangeur_gth.horizontal as horizontal
import geodim.echangeur_gth.mvertical as mvertical
import geodim.echangeur_gth.vertical as vertical

# Pour le test, seules les propriétés du sol doivent être réalistes
Text = np.ones(8760)*10.
ground = load_models.load_ground('sable_alluv')
lambda_sol = ground.get_thermal_conductivity()
rhocp_sol = ground.get_volumetric_heat_capacity()
p_predim_SGV_sol = ground.get_p_predim_SGV()
p_predim_mSGV_sol = ground.get_p_predim_mSGV()
diffusivite_sol = ground.get_thermal_diffusivity()
Pf = lambda x,y: 1.
Vfl_PAC = 1.
data = dict(Text = Text,
            lambda_sol = lambda_sol,
            rhocp_sol = rhocp_sol,
            diffusivite_sol = diffusivite_sol,
            p_predim_SGV_sol = p_predim_SGV_sol,
            p_predim_mSGV_sol = p_predim_mSGV_sol,
            Pf = Pf,
            debit_gth = Vfl_PAC
            )
            
# Echangeurs à tester et attributs correspondants à vérifier
ech_and_attr_to_test = {
                        'corbeille': ('t_dG_echangeur_gth',),
                        'horizontal': ('t_dG_echangeur_gth',),
                        'mvertical': ('t_dG_echangeur_gth',),
                        'vertical': ('t_dG_echangeur_gth',),
                        }
for ech, attributes in ech_and_attr_to_test.items():
    print('{}:'.format(ech))
    ech = globals()[ech]
    if not ech.use_cython:
        print(' - Cython implementation not found')
        print('')
        continue
    # Using cython
    t0 = time.time()
    egth = ech.build(data)
    build_time = time.time()-t0
    results = [egth[attr] for attr in attributes]
    # Without cython
    ech.use_cython = False
    t0 = time.time()
    egth = ech.build(data)
    build_time_ref = time.time()-t0
    results_ref = [egth[attr] for attr in attributes]
    errors = [  np.linalg.norm(attr-attr_ref)
                for attr, attr_ref in zip(results, results_ref)
                ]
    print(' - build times:')
    print('    o no Cython: {:.2f} s'.format(build_time_ref))
    print('    o with Cython: {:.2f} s'.format(build_time))
    if build_time > 0:
        print('    o speed gain: {:.2f} x'.format(build_time_ref/build_time))
    else:
        print('    o speed delta new-ref: {:.2f} s'.format(build_time-build_time_ref))
    print(' - results errors:')
    for attr, err in zip(attributes, errors):
        print ('    o {}: {}'.format(attr, err))
    print('')