# -*- coding: utf-8 -*-

##############################
# prepare path to load package
import os
import sys

GEODIM_RELATIVE_PATH = '../..'
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),
                                 GEODIM_RELATIVE_PATH)))
# sys.path.append(GEODIM_RELATIVE_PATH)  
### remplacer la ligne d'avant par celle ci-dessus si execution depuis python
### en execfile("run.py") sinon exécuter depuis la console en python "run.py"
##############################

from geodim import main, donnees, echangeur_gth

#geodim.main.compute_cli()

print_messages = True

# Commenter/décommenter selon le(s) cas test à exécuter
test_cases = [
            'gkub33h',
            'gneo18h'
            ]

if 'gkub33h' in test_cases:
    # 1- Test sur G-KUB 33H, qui a servi de support aux calculs de corrélation de Pcal et Pabs
    data_brut_0 = dict(
        nom_fmeteo='TH1c',
        altitude='moy',
        isolation=0.53,
        surface=145,
        hauteur=3,
        nb_occupants=5,
        heatpump='GKUB_33H',
        P_appoint=0,
        emitter='PC',
        heat_exchanger='horizontal',
        ground_type='sable_alluv',
        scop_dim=3.8,
        do_ECS=False,
        do_raf=False,

        user_id=666,
        debit_nominal_maPAC=0.,
        puissance_reduit_maPAC=0.,
        Pc_maPAC=0.,
        Pa_maPAC=0. 
    )
    main.compute(data_brut_0, print_messages=print_messages)

    # Exemple avec corrélation
    data_brut_1 = dict(
        nom_fmeteo='TH1c',
        altitude='moy',
        isolation=0.53,
        surface=145,
        hauteur=3,
        nb_occupants=5,
        heatpump='mapac',
        P_appoint=0,
        emitter='PC',
        heat_exchanger='horizontal',
        ground_type='sable_alluv',
        scop_dim=3.8,
        do_ECS=False,
        do_raf=False,
        
        user_id=666,
        debit_nominal_maPAC=1.7,
        puissance_reduit_maPAC=12.,
        Pc_maPAC=7.04,
        Pa_maPAC=1.60 
    )
    main.compute(data_brut_1, print_messages=print_messages)

    # # Exemple avec maPAC, mais sans corrélation (i.e., tableaux Pcal et Pabs donnés par l'utilisateur).
    # /!\ N'est plus ouvert via l'interface
    # import numpy as np

    # PC_as_array = np.array([[7.40, 8.51, 9.77, 10.62],
                            # [7.04, 8.10, 9.29, 9.86],
                            # [6.70, 7.70, 8.84, 9.38],
                            # [6.62, 7.62, 8.74, 9.27]
                            # ])
                            
    # PA_as_array = np.array([[1.38, 1.38, 1.38, 1.38],
                            # [1.60, 1.60, 1.60, 1.60],
                            # [1.95, 1.95, 1.95, 1.95],
                            # [2.35, 2.35, 2.35, 2.35]
                            # ])

    # data_brut_2 = dict(
        # nom_fmeteo='TH1c',
        # altitude='moy',
        # isolation=0.53,
        # surface=145,
        # hauteur=3,
        # nb_occupants=5,
        # heatpump='mapac',
        # P_appoint=0,
        # emitter='PC',
        # heat_exchanger='horizontal',
        # ground_type='sable_alluv',
        # scop_dim=3.8,
        # do_ECS=False,
        # do_raf=False,
        
        # user_id=667,
        # debit_nominal_maPAC=1.7,
        # puissance_reduit_maPAC=12.,
        # Pc_maPAC=PC_as_array,
        # Pa_maPAC=PA_as_array 
    # )
    # main.compute(data_brut_2, print_messages=print_messages)

if 'gneo18h' in test_cases:
    # 2-Test avec G-NEO 18H (pour test robustesse correlation)
    data_brut_3 = dict(
        nom_fmeteo='TH1c',
        altitude='moy',
        isolation=0.53,
        surface=145,
        hauteur=3,
        nb_occupants=5,
        heatpump='GNEO_18H',
        P_appoint=0,
        emitter='PC',
        heat_exchanger='horizontal',
        ground_type='sable_alluv',
        scop_dim=3.8,
        do_ECS=False,
        do_raf=False,

        user_id=666,
        debit_nominal_maPAC=0.,
        puissance_reduit_maPAC=0.,
        Pc_maPAC=0.,
        Pa_maPAC=0. 
    )

    main.compute(data_brut_3, print_messages=print_messages)

    data_brut_4 = dict(
        nom_fmeteo='TH1c',
        altitude='moy',
        isolation=0.53,
        surface=145,
        hauteur=3,
        nb_occupants=5,
        heatpump='mapac',
        P_appoint=0,
        emitter='PC',
        heat_exchanger='horizontal',
        ground_type='sable_alluv',
        scop_dim=3.8,
        do_ECS=False,
        do_raf=False,
        
        user_id=667,
        debit_nominal_maPAC=1.15,
        puissance_reduit_maPAC=12.,
        Pc_maPAC=5.20,
        Pa_maPAC=1.30 
    )

    main.compute(data_brut_4, print_messages=print_messages)


