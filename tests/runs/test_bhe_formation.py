""" One of the test cases presented during the course (as of 03/2022)  """

import pathlib
import sys

CWD = pathlib.Path(__file__).parent
HEADDIR = CWD.parent
sys.path.append(str(HEADDIR))

import tests_utils
tests_utils.add_geodim_headdir_to_path()
import geodim
import geodim.bhe_field as bf
import geodim.bhe_field.global_variables as gv
from geodim.donnees import regression_lineaire
from geodim.models import params_base
import geodim.calcul as calcul

data_bhe_course = dict(
    t_behavior=2,
    sorted=True,
    do_heat=True,
    do_ECS=False,
    do_raf=False,
    do_AC=True,
    location=4,
    type=3,
    # pbat_chaud_total_custom=None,
    # eecs_custom=None,
    # pbat_froid_custom=None,
    hp_pc=150,
    hp_cop_ref=4.5,
    powextra_source=1,
    powextra_cop=3,
    emitter_tcool=17,
    bhe_Rb=0.11,
    bhe_depth=110,
    bhe_glyc_perc=30,
    layout_input_mode=1,
    preset_layout=1,
    bhe_dist=25,
    bhe_nb_x=5,
    bhe_nb_y=3,
    # bhe_nb_rows=,
    ground_tini=15,
    ground_t_depth_update=False,
    ground_gth_grad=3,
    ground_sample_bh_depth=100,
    # ground_lat=,
    # ground_long=,
    ground_thermal_conductivity=2,
    ground_heat_capacity=2,
    t_ini_mode=1,
    t_prop_mode=1,
    # ground_type=0,
    emitter='PC'
    )

if __name__ == '__main__':
    import time
    heat_exch = bf.bhe_field.BHE_field_exchg(None)
    use_aggregation = True
    use_cython = False
    t1 = time.time()
    my_res = heat_exch.direct_run(data_bhe_course, use_aggregation)
    t2 = time.time()
    print('Runtime (direct)', t2-t1)
    
    # tests_utils.export_run_results(
        # '.', data_bhe_course, my_res,
        # use_aggregation=use_aggregation,
        # use_cython=use_cython,
        # run_kind='direct',
        # building=True
        # )