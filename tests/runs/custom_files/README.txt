Fichiers des besoins thermiques du bâtiment (chauffage, ECS, et géocooling)

1-  fichiers standards, créés avec les données de l'outil de dimensionnement
1'- mêmes fichiers que ci-dessus, mais le chauffage et le géocooling sont coupés
    lorsque l'ECS est actif
2-  fichiers avec une température alternative, créés à partir du relevé de température annuel de La Rochelle en 2017
3-  fichiers standards, avec un bruit gaussien de 10%
4-  fichiers fournis pour les différentes formations
5-  fichiers spécifiques au champ de sondes