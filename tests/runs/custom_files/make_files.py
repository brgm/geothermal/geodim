import numpy as np
import os
import sys
sys.path.append(os.path.join('..', '..', '..', 'geodim'))
sys.path.append(os.path.join('..', '..', '..', 'geodim', 'models'))
import altitudes
import params_base as pb
import global_variables as gv

# Savefiles
outfile_text = 'text.dat'
outfile_pbat_chaud = 'pbat_chaud.dat'
outfile_ecs = 'ecs.dat'
outfile_pbat_froid = 'pbat_froid.dat'
# Data used for the test case
data_brut = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.5,
    nb_occupants=5,
    heatpump='GNEO_18H',  # 4: G-neo 18h
    P_appoint=0,
    emitter='PC', # 1:plancher chauffant
    heat_exchanger='corbeille',
    ground_type='sable_alluv',  # 5: sable / alluvions
    scop_dim=4.4,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=18,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
# Additional data
Gbat = data_brut['isolation']
Sbat = data_brut['surface']
hspbat = data_brut['hauteur']
nbhab = data_brut['nb_occupants']
GSh = Gbat*Sbat*hspbat
Eecs = nbhab*pb.Eecsunit/3600.
chargesinternes = ( nbhab*pb.chargeinternehab + Sbat*pb.chargesinternesbat)
# Text, default
nom_fmeteo = data_brut['nom_fmeteo']
altitude = data_brut['altitude']
fmeteo = os.path.join('..', '..', 'geodim', 'meteo', '{}.txt'.format(nom_fmeteo))
Text = np.loadtxt(fmeteo, dtype=float)
cor_T = getattr(altitudes, 'cor_T_{}'.format(altitude))
Text += cor_T
# Temp chaud, default
t_T_chaud_jour = np.tile(pb.T_cons_chaud_reduit, 24)
t_T_chaud_jour[ pb.heure_debutchauffage:
                pb.heure_arretchauffage] = pb.T_cons_chaud
t_T_chaud = np.tile(t_T_chaud_jour, 365)


# Regular case (default tables)
headdir = '1-regular'
ass = Pbat_chaud_total = np.maximum(  Gbat*Sbat*hspbat*(t_T_chaud-Text)
                                - chargesinternes, 0)
t_Eecs = np.zeros_like(Pbat_chaud_total)
t_Eecs[pb.heure_arretchauffage::24] = Eecs
Pbat_froid = np.maximum(Gbat*Sbat*hspbat*(Text-pb.T_cons_froid)
                        +chargesinternes, 0)
                        
# # Regular case (no overlap)
# headdir = '1-regular_no-overlap'
# Pbat_chaud_total = np.maximum(  Gbat*Sbat*hspbat*(t_T_chaud-Text)
                                # - chargesinternes, 0)
# t_Eecs = np.zeros_like(Pbat_chaud_total)
# t_Eecs[pb.heure_arretchauffage::24] = Eecs
# Pbat_froid = np.maximum(Gbat*Sbat*hspbat*(Text-pb.T_cons_froid)
                        # +chargesinternes, 0)
# ecs_on = t_Eecs > 0
# Pbat_chaud_total[ecs_on] = 0.
# Pbat_froid[ecs_on] = 0.

# # Alternate temperatures case
# headdir = '2-alt_text'
# Text = np.loadtxt(os.path.join(headdir, '2017-text_la-rochelle.txt'), dtype=float)
# # Pbat chaud
# Pbat_chaud_total = np.maximum(  Gbat*Sbat*hspbat*(t_T_chaud-Text)
                                # - chargesinternes, 0)
# # Eecs
# t_Eecs = np.zeros_like(Pbat_chaud_total)
# t_Eecs[pb.heure_arretchauffage::24] = Eecs
# # Pbat froid
# Pbat_froid = np.maximum(Gbat*Sbat*hspbat*(Text-pb.T_cons_froid)
                        # +chargesinternes, 0)
                        
# # Noised case
# headdir = '3-noisy'
# # Pbat chaud
# noise = np.random.normal(1, 0.1, size=len(Text))
# Pbat_chaud_total = np.maximum(  Gbat*Sbat*hspbat*(t_T_chaud-Text)
                                # - chargesinternes, 0)
# Pbat_chaud_total *= noise
# # Eecs
# noise = np.random.normal(1, 0.1, size=len(Text))
# t_Eecs = np.zeros_like(Pbat_chaud_total)
# t_Eecs[pb.heure_arretchauffage::24] = Eecs
# t_Eecs *= noise
# # Pbat froid
# noise = np.random.normal(1, 0.1, size=len(Text))
# Pbat_froid = np.maximum(Gbat*Sbat*hspbat*(Text-pb.T_cons_froid)
                        # +chargesinternes, 0)
# Pbat_froid *= noise
                                                
# Saving
# np.savetxt(os.path.join(headdir, outfile_text), Text)
# np.savetxt(os.path.join(headdir, outfile_pbat_chaud), Pbat_chaud_total)
# np.savetxt(os.path.join(headdir, outfile_ecs), t_Eecs)
# np.savetxt(os.path.join(headdir, outfile_pbat_froid), Pbat_froid)

