import matplotlib.pyplot as plt
import numpy as np
import os

figsize = 12
lwidth = 0.3
figsize = (figsize, figsize/np.sqrt(2))

# Regular case
source_folder = '1-regular'
text = np.loadtxt(os.path.join(source_folder, 'text.dat'))
pbat_chaud = np.loadtxt(os.path.join(source_folder, 'pbat_chaud.dat'))
ecs = np.loadtxt(os.path.join(source_folder, 'ecs.dat'))
pbat_froid = np.loadtxt(os.path.join(source_folder, 'pbat_froid.dat'))
# Create plot
fig, ax = plt.subplots(ncols=2, nrows=2, sharex=True, figsize = figsize)
ax[0][0].plot(text, label='TH2b')
ax[0][0].set_ylabel('$T_{ext}$ [C]')
ax[0][1].plot(pbat_chaud, label='$Bes_{ch.bat}$')
ax[0][1].set_ylabel('Energy needs [W]')
ax[1][0].plot(ecs, 'b+', label='$E_{ECS}$')
ax[1][0].set_ylabel('Energy needs [W]')
ax[1][1].plot(pbat_froid, label='$Bes_{raf.bat}$')
ax[1][1].set_ylabel('Energy needs [W]')

# # Alternate temperatures case
# source_folder = '2-alt_text'
# text = np.loadtxt(os.path.join(source_folder, 'text.dat'))
# pbat_chaud = np.loadtxt(os.path.join(source_folder, 'pbat_chaud.dat'))
# ecs = np.loadtxt(os.path.join(source_folder, 'ecs.dat'))
# pbat_froid = np.loadtxt(os.path.join(source_folder, 'pbat_froid.dat'))
# # Create plot
# ax[0][0].plot(text, label='TH2b - alt.', linewidth = lwidth)
# ax[0][0].set_ylabel('$T_{ext}$ [C]')
# ax[0][1].plot(pbat_chaud, label='$Bes_{ch.bat}$ - alt.', linewidth = lwidth)
# ax[0][1].set_ylabel('Energy needs [W]')
# ax[1][0].plot(ecs, 'g+', label='$E_{ECS}$ - alt.')
# ax[1][0].set_ylabel('Energy needs [W]')
# ax[1][1].plot(pbat_froid, label='$Bes_{raf.bat}$ - alt.', linewidth = lwidth)
# ax[1][1].set_ylabel('Energy needs [W]')

# Noisy case
source_folder = '3-noisy'
text = np.loadtxt(os.path.join(source_folder, 'text.dat'))
pbat_chaud = np.loadtxt(os.path.join(source_folder, 'pbat_chaud.dat'))
ecs = np.loadtxt(os.path.join(source_folder, 'ecs.dat'))
pbat_froid = np.loadtxt(os.path.join(source_folder, 'pbat_froid.dat'))
# Create plot
ax[0][0].plot(text, label='TH2b - noisy', linewidth = lwidth)
ax[0][0].set_ylabel('$T_{ext}$ [C]')
ax[0][1].plot(pbat_chaud, label='$Bes_{ch.bat}$ - noisy', linewidth = lwidth)
ax[0][1].set_ylabel('Energy needs [W]')
ax[1][0].plot(ecs, 'g+', label='$E_{ECS}$ - noisy')
ax[1][0].set_ylabel('Energy needs [W]')
ax[1][1].plot(pbat_froid, label='$Bes_{raf.bat}$ - noisy', linewidth = lwidth)
ax[1][1].set_ylabel('Energy needs [W]')

# Labels and save
for irow, row in enumerate(ax):
    for col in row:
        col.legend(loc='best')
    if irow:
        col.set_xlabel('Days in year')
fig.tight_layout()
fig.savefig('{}.png'.format(source_folder))

# text_larochelle = np.loadtxt('3-alt_text/2017-text_la-rochelle.txt')
# plt.plot(text)


# plt.plot(text)
# plt.plot(text_larochelle)
# plt.show()