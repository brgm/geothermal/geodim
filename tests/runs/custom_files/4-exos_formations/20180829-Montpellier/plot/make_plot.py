import numpy as np
import os
import matplotlib.pyplot as plt

source_folder = '../3'
chauf_file = 'pbat_chaud.dat'
ecs_file = 'ecs.dat'
pbat_chaud = np.loadtxt(os.path.join(source_folder, chauf_file))
ecs = np.loadtxt(os.path.join(source_folder, ecs_file))

nrows = 36 # Dealing with a limiting number of hours
pbat_chaud = pbat_chaud[:nrows]
ecs = ecs[:nrows]
support = range(1, nrows+1)
# Rebuilds initial, unmodified files
ecs_init = np.array(ecs)
index_excess = 22
p_ecs_limit = ecs_init[22]
ecs_init[22] += ecs_init[23]
ecs_init[23] = 0.

# Would be nice to automate stuff
# numerical_zero = 1.e-16
# last_hour_excess = False
# p_ecs_limit = None
# for i, p_ecs in enumerate(ecs_init):
    # if last_hour_excess:
        # if p_ecs > numerical_zero:
            # ecs_init[i-1] += p_ecs
            # ecs_init[i] = 0.
        # else:
            # last_hour_excess = False
    # if p_ecs > numerical_zero:
        # last_hour_excess = True

# LINEWIDTH = 1.
# fig,(ax_ecs,ax_chauf) = plt.subplots(nrows=2, sharex=True)
# ax_ecs.bar(support, ecs_init, color='white', edgecolor='black', linestyle='--', linewidth = LINEWIDTH)
# ax_ecs.bar(support, ecs, color='white', edgecolor='black', linewidth = LINEWIDTH*1.1)
# ax_chauf.bar(support, pbat_chaud, color='white', edgecolor='black', linewidth = LINEWIDTH*1.1)
# ax_ecs.set_yticks([])
# ax_ecs.set_ylabel('$P_{ECS} [kW]$')
# ax_chauf.set_yticks([])
# ax_chauf.set_ylabel('$P_{bat} [kW]$')
# ax_chauf.set_xlim((0.5, nrows+0.5))
# ax_chauf.set_xticks(support[3::4])
# plt.show()

nhours = 100
P_appoint = 5600
supp = range(nhours)
pbatreftot = np.loadtxt(os.path.join('../0', chauf_file))
Pmax_PAC_ref = np.max(pbatreftot) - P_appoint
pbatref = np.maximum(np.minimum(pbatreftot, Pmax_PAC_ref), 0)
E_appoint_ref = np.sum(pbatreftot-pbatref)
pbatreftot = pbatreftot[:nhours]
pbatref = pbatref[:nhours]

pbattot = np.loadtxt(os.path.join(source_folder, chauf_file))
Pmax_PAC = np.max(pbattot) - P_appoint
pbat = np.maximum(np.minimum(pbattot, Pmax_PAC), 0)
E_appoint = np.sum(pbattot-pbat)
pbattot = pbattot[:nhours]
pbat = pbat[:nhours]

plt.bar(supp, pbatreftot,label='ref')
plt.bar(supp, pbatref,label='ref-cut', color=(0,0,0,0.5))
plt.bar(supp, pbattot,label='3', color=(0,0,0,0), edgecolor='b')
plt.bar(supp, pbat,label='3-cut', color=(0,0,0,0),edgecolor='b',linestyle='--')
plt.plot(plt.xlim(), [Pmax_PAC_ref+P_appoint]*2, 'r-')
plt.legend()
plt.show()

# # Erreur creation energie
# P_appoint = 0
# source_folder = '../../20180920-Dijon/3/bis'
# pbatreftot = np.loadtxt(os.path.join('../../20180920-Dijon/3', chauf_file))
# Pmax_PAC_ref = np.max(pbatreftot) - P_appoint
# pbatref = np.maximum(np.minimum(pbatreftot, Pmax_PAC_ref), 0)
# E_appoint_ref = np.sum(pbatreftot-pbatref)
# Ebatreftot = np.sum(pbatreftot)
# Ebatref = np.sum(pbatref)
# pecsref = np.loadtxt(os.path.join('../../20180920-Dijon/3', ecs_file))
# Eecsref = np.sum(pecsref)

# pbattot = np.loadtxt(os.path.join(source_folder, chauf_file))
# Pmax_PAC = np.max(pbattot) - P_appoint
# pbat = np.maximum(np.minimum(pbattot, Pmax_PAC), 0)
# E_appoint = np.sum(pbattot-pbat)
# Ebattot = np.sum(pbatreftot)
# Ebat = np.sum(pbat)
# pecs = np.loadtxt(os.path.join(source_folder, ecs_file))
# Eecs = np.sum(pecs)

# print '##### CHAUFF #####'
# if P_appoint:
    # print 'Erreur appoint: {} o/o'.format(   abs(E_appoint_ref-E_appoint)
                                            # /E_appoint_ref
                                            # *100
                                            # )
# print 'Erreur ref/nouv tot: {} o/o'.format(  abs(Ebatreftot-Ebattot)
                                            # /Ebatreftot
                                            # *100
                                            # )
# print 'Erreur ref/nouv: {} o/o'.format(  abs(Ebatref-Ebat)
                                        # /Ebatref
                                        # *100
                                        # )
# print '##### ECS #####'
# print 'Erreur ref/nouv: {} o/o'.format(  abs(Eecsref-Eecs)
                                        # /Eecsref
                                        # *100
                                        # )