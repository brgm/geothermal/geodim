﻿Fichiers des besoins thermiques du bâtiment (chauffage, et ECS)

0-      fichiers standards, créés avec les données de l'outil de dimensionnement
        (fichiers non modifiés)
1-      fichiers standards, créés avec les données de l'outil de dimensionnement
        l'ECS est actif à pleine puissance 1 fois par jour et le chauffage est coupé
        sur ces créneaux
2-      idem, mais l'ECS est réparti sur deux créneaux par jour
3-      fichiers créés puis modifiés par l'outil de dimensionnement.
        Dans ces fichiers, récupérés à l'issu du calcul, le chauffage est coupé pendant
        l'ECS mais la puissance correspondante est redistribuée sur les 8h suivantes.
        La puissance ECS est maximisée par la capacité de la PAC, et l'excédent est
        redistribué sur l'heure suivante.
plot-   à partir des fichiers du dossier 3-, illustre le report de l'ECS et du chauffage sur une journée.
    
Intitulé de l'exercice ayant permis de créer les fichiers:

La chaudière de M. Julien est défaillante. Il souhaite donc la remplacer à l’occasion
de la rénovation de son domicile. Son voisin, qui chauffe son logement par géothermie,
est très satisfait de sa PAC. M. Julien souhaite à son tour s’orienter vers cette solution.
Habitant à Mont Louis avec sa famille depuis 15 ans, il vous sollicite pour l’aider.

Caractéristiques de son habitation :

> Maison bâtie dans les années 70 
> Équipée de radiateurs haute température 
> Coefficient de déperdition : 1,45 W/°C mètre cube
> 163 mètres carrés habitables + 100 mètres carrés de jardin
> Hauteur sous plafond : 2,65 m 
> Habitée par 4 personnes
> Appoint : poêle à bois de 5,6 kW
> Type de sol rencontré sur sa parcelle : granite

Compte-tenu de ses besoins, M. Julien souhaite que la PAC puisse assurer la production d’eau chaude sanitaire en plus du chauffage.

Vous lui proposez donc une PAC en régime eau glycolée/eau de l’un de vos fournisseurs avec les caractéristiques suivantes : 
> Débit normal : 1,9 mètres cubes par heure
> Puissance de veille : 41 W
> Puissance calorifique : 7,5 kW
> Puissance absorbée compresseurs : 1,78 kWe
