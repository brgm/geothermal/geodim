﻿Fichiers des besoins thermiques du bâtiment (chauffage, et ECS)





3-  fichiers créés puis modifiés par l'outil de dimensionnement.
    Dans ces fichiers, récupérés à l'issu du calcul, le chauffage est coupé pendant
    l'ECS mais la puissance correspondante est redistribuée sur les 8h suivantes.
    La puissance ECS est maximisée par la capacité de la PAC, et l'excédent est
    redistribué sur l'heure suivante.
    
Vous êtes sollicité par M. & Mme Dupont qui s’intéressent à l’énergie propre et
discrète qu’est la géothermie et qui souhaitent installer une PAC chez eux. Résidant
depuis peu à Dijon, ils ont récemment acquis une maison qu’ils partagent avec leur 3 enfants. 

Caractéristiques de leur habitation : 

> Maison plutôt récente de 150 m²
> Hauteur sous plafond : 2,65 m
> Radiateurs basse température
> Coefficient de déperdition volumique : 0,95 W/°C m³
> Type de sol : sable

La PAC doit être en capacité d’assurer seule la production de chauffage de la maison. 

> Comportement thermique à sélectionner : besoins horaires précalculés (fichier fourni par mail)

