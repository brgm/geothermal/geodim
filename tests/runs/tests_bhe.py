""" A bunch of tests for borefield heat exchangers implementations """
import pathlib
import sys
import time

import matplotlib.pyplot as plt
import numpy as np

CWD = pathlib.Path(__file__).parent
HEADDIR = CWD.parent
sys.path.append(str(HEADDIR))

import tests_utils
tests_utils.add_geodim_headdir_to_path()
import geodim
import geodim.bhe_field as bf
import geodim.bhe_field.global_variables as gv
from geodim.donnees import regression_lineaire
from geodim.models import params_base
import geodim.calcul as calcul

def check_values(v1,v2):
    """ Checking that all values in v1 and v2 are the same
    
    v1 can be a single value, or a container with mixed nested containers
    and single values.
    v2 must have the same structure
    
    Function will raise an assertion error if at least 1 value differs in v1 and
    v2
    If function passes, all content in v1 and v2 are the same
    
    """
    if isinstance(v1, (list, tuple, np.ndarray)):
        for vv1,vv2 in zip(v1,v2):
            check_values(vv1,vv2)
    elif isinstance(v1, dict):
        keys = v1.keys()
        v1 = [v1[key] for key in keys]
        v2 = [v2[key] for key in keys]
        for vv1,vv2 in zip(v1,v2):
            check_values(vv1,vv2)
    else:
        assert np.all(v1==v2)

# NB: BH radius forced to 0.07 m
# Preset_needs
data_in = dict(
    # Power needs
    t_behavior=2,
    location='Davos',                   # Davos or 5
    type='House_High_Insulation',       # House_High_Insulation or 0
    do_heat=True,
    do_ECS=False,                       # ignored
    do_AC=True,
    # COP info,
    hp_pc = 10.,                        # kW
    hp_cop_ref = 4.5,
    powextra_cop=3,
    # emitter_theat=35                  # Ignored
    emitter='PC',
    emitter_tcool=17,
    # Heat exchanger
    bhe_Rb = 0.08,                      # K.m/W
    bhe_glyc_perc = 30,                 # %
    bhe_depth = 100.,                   # m, single BH length
    layout_input_mode = 1,              # Preset
    preset_layout = 1,                  # Rectangle
    bhe_dist = 25,
    bhe_nb_x = 2,
    bhe_nb_y = 2,
    bhe_nb_rows = 2,                    # NB: ignored with rectangle
    t_ini_mode = 1,                     # ini temp given
    ground_tini = 15,
    ground_t_depth_update = True,
    ground_gth_grad = 3,                # ignored if ground_t_depth_update false
    ground_sample_bh_depth = 250.,      # ignored if ground_t_depth_update false
    t_prop_mode = 1,                    # custom input
    ground_thermal_conductivity = 2.,   # W/m/K
    ground_heat_capacity = 2.2,         # MJ/m3/K
    nyears = 20,
    )

# Custom needs: load same model to validate loading process
data_in2 = data_in.copy()
# - power needs
location = data_in2.pop('location')
btype = data_in2.pop('type')
building = geodim.models.load_models.load_building(f'{btype}_{location}')
heat = building.heat
cool = building.cool
outside = geodim.models.load_models.load_outside(location)
text = outside.temperatures
# - emitter
emitter = data_in2.pop('emitter')
emitter = geodim.models.load_models.load_emitter(emitter)
emitter = dict(
    (attr, getattr(emitter, attr))
    for attr in (
        'name', 'external_temperatures', 'emission_temperatures', 'h_geocooling'
        )
    )
# - borehole positions
pos_kwargs = dict(
    preset_layout=data_in2.pop('preset_layout'),
    dist=data_in2.pop('bhe_dist'),
    nb_x=data_in2.pop('bhe_nb_x'),
    nb_y=data_in2.pop('bhe_nb_y'),
    nb_rows=data_in2.pop('bhe_nb_rows', None)
    )
positions = np.vstack(
    bf.data_processing.set_field_preset_layout(**pos_kwargs)
    ).T
data_in2.update(
    t_behavior=1,   # custom needs
    pbat_chaud_total_custom = heat,
    pbat_froid_custom = cool,
    external_temp_custom = text,
    emitter=emitter,
    layout_input_mode=2,
    bhe_layout_points=positions
    )

if __name__ == '__main__':
    use_aggregation = True
    
    # Direct
    heat_exch_dir = bf.bhe_field.BHE_field_exchg(None)
    t1 = time.time()
    test_dir = heat_exch_dir.direct_run(data_in, use_aggregation)
    t2 = time.time()
    print('Runtime (direct)', t2-t1)
    # Custom (same needs)
    heat_exch_dir2 = bf.bhe_field.BHE_field_exchg(None)
    t1 = time.time()
    test_dir2 = heat_exch_dir.direct_run(data_in2, use_aggregation)
    t2 = time.time()
    print('Runtime (direct, custom)', t2-t1)
    # Verify
    check_values(test_dir,test_dir2)
    print('Results match')
    
    # # Another test
    # heat_exch_dir = bf.bhe_field.BHE_field_exchg(None)
    # heat_exch_dim = bf.bhe_field.BHE_field_exchg(None)
    # data_in_dir = data_in.copy()
    # mydim = data_in_dir['bhe_depth']
    # target_cop = 6.
    # t0 = time.time()
    # test_dim = heat_exch_dim.dimensioning_run(
        # data_in, target_cop, use_aggregation, print_messages=True
        # )
    # t1 = time.time()
    # mydim = test_dim[0]
    # print('Runtime (dim)', t1-t0)
    # data_in_dir['bhe_depth'] = mydim
    # t1 = time.time()
    # test_dir = heat_exch_dir.direct_run(data_in_dir, use_aggregation)
    # t2 = time.time()
    # print('Runtime (direct)', t2-t1)
    # # - plotting
    # (   
        # t_Tb,
        # t_Tfleech,
        # results_building
        # ) = test_dir[-1]
    # times = np.arange(heat_exch_dir.nyears*8760)
    # t_heat, t_dhw, t_geoc, t_ac = test_dir[-2]
    # fig,axes = plt.subplots(nrows=3, sharex=True)
    # lim = -1
    # # all times
    # axes[0].plot(times[-lim:-1], t_Tb[-lim:-1], label='Tparoi')
    # axes[0].plot(times[-lim:-1], t_Tfleech[-lim:-1], label='Tfleech')
    # # axes[0].plot(times[-lim:-1], t_Tflsech[-lim:-1], label='Tflsech')
    # # axes[0].plot(times[-lim:], t_Tflmoyech[-lim:], label='Tflmoyech')
    # # axes[1].plot(times[-lim:], t_puissance[-lim:], label='t puissance (échangeur)')
    # # axes[1].plot(times[-lim:], t_Pheatexch[-lim:], label='t Pheatexch')
    # # axes[2].plot(times[-lim:], t_ac[-lim:], label='t_clim')
    # # axes[2].plot(times[-lim:], -t_puissance_em[-lim:], label='-t_puissance (émetteur)')
    # # axes[2].plot(times[-lim:], t_Pbackup_AC[-lim:], label='t Pbackup_AC')
    # # axes[2].plot(times[-lim:], Pelec_HP_AC[-lim:], label='Pelec_HP_AC')
    # # # ax2=axes[2].twinx()
    # # # ax2.plot(times[-lim:], t_charge[-lim:], 'k--', label='t_charge')
    # axes[0].legend()
    # # axes[0].set_xscale('log')
    # # axes[1].legend()
    # # axes[2].legend()
    # plt.show()