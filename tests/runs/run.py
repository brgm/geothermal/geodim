""" Some tests runs for the single house system
"""
import pathlib
import sys
import yaml

CWD = pathlib.Path(__file__).parent
sys.path.append(str(CWD.parent))
import tests_utils
tests_utils.add_geodim_headdir_to_path()

from geodim import main, donnees, echangeur_gth

print_messages = True

data_brut_2019 = dict(
    nom_fmeteo='TH1c',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.5,
    nb_occupants=5,
    heatpump='GNEO_18H',
    P_appoint=0,
    emitter='PC', # 1:plancher chauffant
    # heat_exchanger='corbeille',
    heat_exchanger='horizontal',
    # heat_exchanger='mvertical',
    # heat_exchanger='vertical',
    ground_type='sable_alluv',  # 5: sable / alluvions
    scop_dim=1,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=18,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )

# my_res = main.compute(  data_brut_2019, print_messages=print_messages,
                        # use_aggregation=True,
                        # )
# tests_utils.export_dimensioning_results(
                            # '.', data_brut_2019, my_res, use_aggregation=True,
                            # use_cython=True, run_kind='dimensioning'
                            # )

data_brut_2021 = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=250,
    hauteur=2.5,
    nb_occupants=4,
    heatpump='GNEO_18H',
    P_appoint=0,
    emitter='PC', # 1:plancher chauffant
    heat_exchanger='corbeille',
    # heat_exchanger='vertical',
    ground_type='calcaire',
    scop_dim=4.4,
    do_ECS=False,
    do_raf=False,
    h_geocooling=3,
    tfbi_min=18,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )

# my_res = main.compute(data_brut_2021, print_messages=print_messages)

data_brut_2022 = dict(
    nom_fmeteo='TH1b',
    altitude='bas',
    isolation=0.65,
    surface=120,
    hauteur=2.7,
    nb_occupants=4,
    heatpump='ECOTOUCH_5112DT',
    P_appoint=0,
    emitter='PC',
    heat_exchanger='corbeille',
    # heat_exchanger='horizontal',
    # heat_exchanger='vertical',
    # heat_exchanger='mvertical',
    ground_type='sable_alluv',
    scop_dim=1.,
    do_ECS=False,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=18,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
    
my_res = main.compute(
    data_brut_2022,
    print_messages=print_messages,
    # dim = 5 # corbeille
    # dim = 150. # horizontal
    # dim = 150. # vertical
    # dim = 35 # micro vertical
    )


# # Custom weather compensation: same as model for underfloor heating
# # Should be the same as my_res just above
# emitter_custom = dict(
    # name='custom emitter',
    # external_temperatures=[-10.,7.],
    # emission_temperatures=[35.,24.],
    # h_geocooling=3
    # )
# data_brut_2022_custom = data_brut_2022.copy()
# data_brut_2022_custom['emitter'] = emitter_custom
# my_res2 = main.compute(data_brut_2022_custom, print_messages=print_messages)

# Data for unittests
data_unittest_spiral_coil = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.5,
    nb_occupants=5,
    heatpump='GNEO_18H',
    P_appoint=0,
    emitter='PC',
    heat_exchanger='corbeille',
    ground_type='sable_alluv',
    scop_dim=1,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=18,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
# my_res = main.compute(
    # data_unittest_spiral_coil,
    # print_messages=print_messages,
    # use_aggregation=True
    # )
# tests_utils.export_dimensioning_results(
    # '.', data_unittest_spiral_coil, my_res,
    # use_aggregation=True,
    # use_cython=True,
    # run_kind='dimensioning'
    # )