""" Testing file submission process (Python functions) """
import pathlib
import sys

import numpy as np

CWD = pathlib.Path(__file__).parent
sys.path.append(str(CWD.parent))
import tests_utils
tests_utils.add_geodim_headdir_to_path()

from geodim import main, donnees, echangeur_gth

SOURCEDIR = CWD / 'custom_files'
print_messages = True

# Regular
data_brut_2018 = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    isolation=0.65,
    surface=150,
    hauteur=2.65,
    nb_occupants=5,
    heatpump='GNEO_18H',  # 4: G-neo 18h
    P_appoint=0,
    emitter='PC', # 1:plancher chauffant
    heat_exchanger='corbeille',
    ground_type='sable_alluv',  # 5: sable / alluvions
    scop_dim=4.4,
    do_ECS=True,
    do_raf=True,
    h_geocooling=3,
    tfbi_min=18,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
my_res = main.compute(data_brut_2018, print_messages=print_messages)

# With custom files
data_brut_2018_custom = dict(
    nom_fmeteo='TH2b',
    altitude='bas',
    surface=150,
    heatpump='GNEO_18H', # 4: G-neo 18h
    P_appoint=0,
    emitter='PC',  # 1:plancher chauffant
    heat_exchanger='corbeille',
    ground_type='sable_alluv',  # 5: sable / alluvions
    scop_dim=4.4,
    do_ECS=True,
    do_raf=True,
    T_cons_froid=26.,
    h_geocooling=3,
    tfbi_min=18,

    user_id=666,
    debit_nominal_maPAC=0.,
    puissance_reduit_maPAC=0.,
    Pc_maPAC=0.,
    Pa_maPAC=0.
    )
source_folder = '1-regular_no-overlap'
# NB:   to test the files below, deactivate the file modification in function 
#       get_data_files() from ./app_wsgi.py
# source_folder = '1-regular'
# source_folder = '2-alt_text'
# source_folder = '3-noisy'
data = dict(
    (fname, np.loadtxt(SOURCEDIR / source_folder / f'{fname}.dat'))
    for fname in ('pbat_chaud', 'ecs', 'pbat_froid')
    )
besoins_thermiques_custom = dict(
    Pbat_chaud_total_custom = data['pbat_chaud'],
    Eecs_custom = data['ecs'],
    Pbat_froid_custom = data['pbat_froid'],
    T_cons_froid = 26.
    )
my_res_custom = main.compute(
    data_brut_2018_custom,
    besoins_thermiques_custom=besoins_thermiques_custom,
    print_messages=print_messages
    )