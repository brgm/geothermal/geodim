""" Debugging faulty BHE unit test """

import pathlib
import pytest
import sys

import numpy as np
import yaml

CWD = pathlib.Path(__file__).parent
TESTS_HEADDIR = CWD.parent
REF_DATA_DIR = CWD/'ref_data'

sys.path.append(str(TESTS_HEADDIR))
import tests_utils
tests_utils.add_geodim_headdir_to_path()
import geodim.bhe_field as bf

res_dir = REF_DATA_DIR/'run_borehole_he'
ref_results_borehole_he = tests_utils.load_run_results(res_dir)
    

run_input, ref_results, options = ref_results_borehole_he
heat_exch = bf.bhe_field.BHE_field_exchg(None)
results = heat_exch.direct_run(run_input, print_messages=False, **options)
results = tests_utils.extract_direct_results(results, building=True)
assert tests_utils.results_allclose(results, ref_results)