""" Testing direct calls to computations

pytest test_runs.py --verbosity=1

TODO:
- factorize data loading,
- factorize results extraction
- use global variable names?
"""
import pathlib
import pytest
import sys

import numpy as np
import yaml

CWD = pathlib.Path(__file__).parent
TESTS_HEADDIR = CWD.parent
REF_DATA_DIR = CWD/'ref_data'

sys.path.append(str(TESTS_HEADDIR))
import tests_utils

@pytest.fixture(scope='session')
def import_mgeo():
    """ Loading geodim module """
    tests_utils.add_geodim_headdir_to_path()
    import geodim
    return geodim
    
@pytest.fixture(scope='function')
def ref_results_spiral_coil_he():
    """ Reference results for spiral coil heat exchanger run """
    res_dir = REF_DATA_DIR/'run_spiral_coil'
    return tests_utils.load_run_results(res_dir)
    
@pytest.fixture(scope='function')
def ref_results_borehole_he():
    """ Reference results for borehole field heat exchanger run """
    res_dir = REF_DATA_DIR/'run_borehole_he'
    return tests_utils.load_run_results(res_dir)
    
def test_spiral_coil_he(import_mgeo, ref_results_spiral_coil_he):
    """ Spiral coil run """
    geodim = import_mgeo
    from geodim import main
    run_input, ref_results, options = ref_results_spiral_coil_he
    results = main.compute(run_input, print_messages=False, **options)
    results = tests_utils.extract_dimensioning_results(results)
    assert tests_utils.results_allclose(results, ref_results)
    
def test_spiral_coil_he(import_mgeo, ref_results_spiral_coil_he):
    """ Spiral coil run """
    geodim = import_mgeo
    from geodim import main
    run_input, ref_results, options = ref_results_spiral_coil_he
    results = main.compute(run_input, print_messages=False, **options)
    results = tests_utils.extract_dimensioning_results(results)
    assert tests_utils.results_allclose(results, ref_results)
        
def test_spiral_coil_he_custom(import_mgeo, ref_results_spiral_coil_he):
    """ Spiral coil run using custom files
    
    Custom files are recreated from reference inputs.
    
    """
    geodim = import_mgeo
    from geodim import main, donnees
    from geodim.models import params_base as pb
    run_input, ref_results, options = ref_results_spiral_coil_he
    data = donnees.traitement(run_input, **options)
    t_heat = data['tab_chauf']
    t_dhw = data['tab_ECS']
    if (run_input['do_ECS']
        and t_dhw is None
        ):
        t_dhw = np.zeros_like(t_heat)
        t_dhw[pb.heure_arretchauffage::24] = data['energie_ECS']
    t_cool = data['tab_raf']
    T_cons_froid = pb.T_cons_froid
    custom_power_needs = dict(  Pbat_chaud_total_custom = t_heat,
                                Eecs_custom = t_dhw,
                                Pbat_froid_custom = t_cool,
                                T_cons_froid = T_cons_froid
                                )
    results = main.compute( run_input, print_messages=False,
                            besoins_thermiques_custom=custom_power_needs,
                            **options
                            )
    results = tests_utils.extract_dimensioning_results(results)
    assert tests_utils.results_allclose(results, ref_results)
    
def test_borehole_he(import_mgeo, ref_results_borehole_he):
    """ Borehole heat exchanger run """
    geodim = import_mgeo
    import geodim.bhe_field as bf
    run_input, ref_results, options = ref_results_borehole_he
    heat_exch = bf.bhe_field.BHE_field_exchg(None)
    results = heat_exch.direct_run(run_input, print_messages=False, **options)
    results = tests_utils.extract_direct_results(results, building=True)
    assert tests_utils.results_allclose(results, ref_results)