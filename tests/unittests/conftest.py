""" Options for pytest

NB: for pytest < 7.0.0, pytest hooks does not handle pathlib.Path objects by
    default. os is used because it's more generic

"""

import os

TESTED_FILES = [
    'test_runs.py',
    ]

def pytest_ignore_collect(path):
    """ pytest hook for choosing whether item should be ignored while collecting
    
    path: py._path.local.LocalPath, full path to item.
          For pytest >= 7.0.0, pathlib.Path object
          
    NB: only files in TESTED_FILES will be tested
                  
    Returns: bool, True if item must be ignored in collect
    
    """
    fname = os.path.basename(path)
    ignore_item = not fname in TESTED_FILES
    return ignore_item
