"""
The command line interface (CLI)

Warning
-------
Not implemented yet, this file is deprecated but left for future support.

"""

def cli():
    data = {}
    
    # météo
    data['nom_fmeteo'] = raw_input('nom du fichier météo ?')
    
    # bâtiment
    data['isolation'] = input('Valeur de G [W/m3/K] ?')
    data['surface'] = input('Surface habitable [m2] ?')
    data['hauteur'] = input('Hauteur sous plafond [m] ?')
    data['nb_occupants'] = input("Nombre d'occupants ?")
    
    # pompe à chaleur
    num_PAC = input("Indiquez le numéro du modèle de PAC choisi:\n"+\
    "1: aurea2 30h\n"+"2: aurea2 40h\n"+"3: G-kub 20h\n"+"4: G-néo 18h\n"+"5: Agéo+20h\n")
    if num_PAC==1:
        heatpump="aurea230h"
    elif num_PAC==2:
        heatpump="aurea240h"
    elif num_PAC==3:
        heatpump="gkub20h"
    elif num_PAC==4:
        heatpump="gneo18h"
    elif num_PAC==5:
        heatpump="ageo20h"
    else:
        raise Exception("la PAC choisie n'existe pas !")
    data['heatpump'] = heatpump
    
    # puissance d'appoint
#    data['P_appoint'] = 1000*input("Puissance de l'appoint de chauffage [kW] ?")
    data['P_appoint'] = input("Puissance de l'appoint de chauffage [kW] ?")
    
    # choix du type d'émetteur
    num_emetteur = input("Indiquez le numéro d'émetteur de chaleur parmi les propositions suivantes\n"+\
    "1:plancher chauffant\n"+"2:radiateurs basse température\n"+"3:radiateurs haute température\n")
    if num_emetteur==1:
        emitter="PC"
    elif num_emetteur==2:
        emitter="RBT"
    elif num_emetteur==3:
        emitter="RHT"
    else:
        raise Exception("l'émetteur choisi n'existe pas !")
    data['emitter'] = emitter
        
    # choix du type d'échangeur géothermique
    num_ech_gth = input("Indiquez le numéro d'échangeur géothermique parmi les propositions suivantes\n"+\
    "1:corbeille\n"+"2:horizontal\n"+"3:vertical\n"+"4:micro-vertical\n")
    # assert num_ech_gth in [1, 2, 3, 4], "l'échangeur choisi n'existe pas !"
    if num_ech_gth==1:
        nom_ech_gth="corbeille"
    elif num_ech_gth==2:
        nom_ech_gth="horizontal"
    elif num_ech_gth==3:
        nom_ech_gth="vertical"
    elif num_ech_gth==4:
        nom_ech_gth="mvertical"
    else:
        raise Exception("l'échangeur choisi n'existe pas !")
    data['heat_exchanger'] = nom_ech_gth
    
    # Données du sol correspondantes
    num_sol = input("Indiquez le numéro du type de sous-sol en présence parmi les propositions suivantes\n"+\
    "1:classique\n"+"2:sable sec\n"+"3:sable saturé d'eau\n"+"4:marne, grès, limon\n"+"5:sédiments saturés d'eau\n")
    if num_sol==1:
        ground_type="classique"
    elif num_sol==2:
        ground_type="sable_sec"
    elif num_sol==3:
        ground_type="sable_sat"
    elif num_sol==4:
        ground_type="marne"
    elif num_sol==5:
        ground_type="sediments_sat"
    else:
        raise Exception("le type de sol choisi ne correspond pas !")
    data['ground_type'] = ground_type
    
    # Critère de dimensionnement
    scop_dim=input("Quelle valeur de SCOP souhaitez-vous obtenir pour ce dimensionnement ?")
    data['scop_dim'] = scop_dim
    
    return data


def afficher(result):
    conso_compresseur_resultant_kwh=sum((t_Pa_resultant*t_chargePAC_chauf_resultant+t_Pa_ECS_resultant*t_chargePAC_ECS_resultant)[(nb_annees-1)*365*24:])/1000. #kWh/an pour chauffage et ECS
    messages = ["Le calcul a convergé vers le dimensionnement : {}".format(
                                                                    dim_max,
                                                                    unite_dim
                                                                    )
                    ]
    messages.append(
            "Le COP annuel de la dernière année vaut {} (dont veille)".format(
                                                        round(COP_resultant,2)
                                                        )
                    )
    messages.append(
            "{} de la puissance nécessaire au chauffage du bâtiment {}".format(
                        round(100*Pmax_PAC/(Pmax_PAC+P_appoint),1),
                        "est couverte par la PAC (le reste par l'appoint)"
                        )
                    )
    messages.append(
            "Les besoins de chauffage sont de {} kWh (couverture à {}{}".format(
                        rint((sum(tab_chauf)+E_appoint)/1000.),
                        round(100*sum(tab_chauf)/(sum(tab_chauf)+E_appoint),1),
                        "% par la PAC), et ceux pour l'ECS {}kWh.".format(                            
                            sum(tab_ECS/1000.)
                            )
                        )
                    )
    messages.append(
            "L'appoint a fourni {}kWh pour le chauffage du bâtiment".format(
                                                rint(sum(E_appoint)/1000.)
                                                )
                    )
    messages.append(
            "Les besoins en électricité sont de {}kWh pour le chauffage et "
            "l'ECS et de {}kWh pour la veille, soit un total de {} {}".format(
                        rint(conso_compresseur_resultant_kwh),
                        rint(consommation_reduit_annuel_resultant/3600000.),
                        rint(   conso_compresseur_resultant_kwh
                                + consommation_reduit_annuel_resultant/3600000.
                                ),
                        "kWh d'électricité par an (hors géocooling)"
                        )
                    )
    messages.append(
            "Le compresseur a tourné {} heures annuelles {}{}, soit un ".format(
                        rint(sum(t_chargePAC_chauf_resultant[
                                                        (nb_annees-1)*365*24:
                                                        ])),
                        "pour le chauffage et {} pour l'ECS".format(
                            rint(sum(t_chargePAC_ECS_resultant[
                                                            (nb_annees-1)*365*24:
                                                            ]))
                            ),
                        "total de {} heures annuelles de fonctionnement".format(
                            rint(sum((
                                t_chargePAC_chauf_resultant
                                + t_chargePAC_ECS_resultant
                                )[(nb_annees-1)*365*24:]))
                            )
                        )
                    )
    messages.append(
        "A l'année, le géocooling a fonctionné "
        "{} heures et a fourni {}kWh pour rafraîchir le bâtiment.".format(
                        rint(sum(t_chargeRAF_resultant[(nb_annees-1)*365*24:])),
                        rint(sum((
                            t_Praf_resultant*t_chargeRAF_resultant
                            )[(nb_annees-1)*365*24:])/1000.),
                        )
                    )
    for message in messages:
        print(message)
    
    # Calcul de la température du bâtiment
    t_Tbat_reel=t_Text+(chargesinternes+t_chauf_resultant-t_Praf_resultant*t_chargeRAF_resultant)/(Gbat*Sbat*hspbat)
    
    plt.clf()
    plt.figure(1)
    plt.plot(dt/86400.*np.arange(nbdt),t_Tfluide_resultant)#ATTENTION : il s'agit de la température Tflee obtenue si on allume la PAC en mode chauffage à la "date [i]"
    plt.plot(dt/86400.*np.arange(nbdt+1),t_Tsol_resultant)
    plt.plot(dt/86400.*np.arange(nbdt+1),t_Tinitsol)
    plt.figure(2)
    plt.plot(dt/86400.*np.arange(nbdt),t_chargePAC_ECS_resultant)
    plt.plot(dt/86400.*np.arange(nbdt),t_chargePAC_chauf_resultant)
    plt.plot(dt/86400.*np.arange(nbdt),t_chargeRAF_resultant)
    plt.figure(3)
    plt.plot(dt/86400.*np.arange(nbdt),t_ECS_resultant)
    plt.plot(dt/86400.*np.arange(nbdt),t_chauf_resultant)
    plt.plot(dt/86400.*np.arange(nbdt),t_Praf_resultant*t_chargeRAF_resultant)
    plt.figure(4)
    plt.plot(dt/86400.*np.arange(nbdt),t_Tbat_reel)
    #plt.plot(dt/86400.*np.arange(nbdt),t_Text+chargesinternes/(Gbat*Sbat*hspbat)) #température du bâitment sans PAC, ni géocooling
    plt.show()