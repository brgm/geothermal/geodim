""" Solving physical system on each timestep.

Also, compute COP.

"""
import numpy as np
from scipy.signal import fftconvolve

from ..models import params_base


class SousDimensionnementError(Exception):
    pass

def repeat_without_copy(arr, rep):
    """ repeat flatten 'arr' 'rep' time without copy of 'arr'
        return a 1-d array
    """
    return np.lib.stride_tricks.as_strided(
        arr, (rep, arr.size), (0, arr.itemsize)
    ).reshape(-1)

def calcul_cop( data, ech, dim, Tflee_func, Tfloutcond_func, cython_data,
                print_messages=False
                ):
    """ Computing coefficient of performance for a given system
    
    data: dict, overall system data (ground, heatpump...)
    ech: dict, heat exchanger data
    dim: float/int, considered system dimension
    Tflee_func: function, computes evaporator inlet temperature (heating)
    Tfloutcond_func: function, computes condenser outlet temperature (cooling)
    cython_data: tuple, data for cython computations
        (see dimensionnement prepare_cython_loop)
    print_messages: bool, turns print() commands on/off (debugging)
    
    Returns: tuple:
        - computed COP
        - annual delivered energies
        - annual consumed energies
        - discomfort hours
        - some temperature and/or power tables
    
    """

    use_cython = not cython_data is None
    
    # Paramètres
    nyears = ech['nb_annees']
    nhours = 24*365
    jour_arretchauffage = params_base.jour_arretchauffage
    jour_debutchauffage = params_base.jour_debutchauffage
    jour_debutrafraichissement = params_base.jour_debutrafraichissement
    jour_arretrafraichissement = params_base.jour_arretrafraichissement
    puissance_reduit = data['puissance_reduit']
    Eecs = data['energie_ECS']
    tab_chauf = data['tab_chauf']
    tab_ECS = data['tab_ECS']
    do_heat = data['do_heat']
    do_ECS = data['do_ECS']
    do_raf = data['do_raf']
    do_AC = data['do_AC']
    T_cons_froid = params_base.T_cons_froid
    # Critère [°C] pour vérifier si la consigne de froid est respectée 
    delta_T = 0.1 #1.e-6 * T_cons_froid
    exch_type = data['heat_exchanger']
    dealing_with_building = exch_type == 'BHE_field'

    # Boucle de calcul
    if use_cython:
        if do_ECS:
            if tab_ECS is None:
                Eecs_annuel = Eecs * 365
            else:
                Eecs_annuel = sum(tab_ECS)
        error_messages = [  "Pas assez de puissance pour produire l'ECS "
                            "pendant la période de réduit de nuit",
                            "Température trop basse à l'évaporateur pour "
                            "production ECS",
                            "Plus de temps pour récupérer le retard de "
                            "chauffage nocturne",
                            "Température trop basse à l'évaporateur",
                            "PAC sous-dimensionnée pour l'appel de puissance "
                            "en chauffage"
                            ]
        cython_loop, cython_arrays = cython_data
        (   t_chauf, t_ECS, t_raf, t_Pa, t_Pf, t_puissance, t_charge, t_Tbat,
            t_Tfleech, t_Tfbi, t_Tb
            ) = cython_arrays
        if do_raf:
            # Redundancy with cython_loop.reset_settings() but otherwise
            # nb_h_inconfort_sans_raf is erroneous. Optimize this later.
            cython_loop.reset_Tbat()
            t_Tbat_inconf = t_Tbat[-nhours:]-T_cons_froid > delta_T
            nb_h_inconfort_sans_raf = sum(t_Tbat_inconf)
        else:
            nb_h_inconfort_sans_raf = None
        error_status = cython_loop(dim)
        if error_status:
            raise SousDimensionnementError(error_messages[error_status-1])
        # Return copy of arrays instead of pointer to array
        t_chauf = np.array(t_chauf)
        t_ECS = np.array(t_ECS)
        t_raf = np.array(t_raf)
        Rb = ech['Rb']
    else:
        heure_arretchauffage = params_base.heure_arretchauffage
        heure_debutchauffage = params_base.heure_debutchauffage
        tab_raf = data['tab_raf']
        tab_AC = data['tab_AC']
        Text = data['Text']
        nbdt = ech['nbdt']
        Pf = data['Pf']
        Pc = data['Pc']
        Pa = data['Pa']
        T_condens_ECS = params_base.T_condens_ECS
        Rb = ech['Rb']
        T_coupure_PAC_ECS = data['T_coupure_PAC_ECS']
        t_dG_echangeur_gth = ech['t_dG_echangeur_gth']
        t_G_echangeur_gth = ech['t_G_echangeur_gth']
        t_Tinitsol = ech['t_Tinitsol']
        G_courtterme = ech['G_courtterme']
        Sbat = data['Sbat']
        chargesinternes = data['chargesinternes']
        _GSh = data['GSh']
        h_geocooling = data['h_geocooling']
        f_calc_Temission = data['f_calc_Temission']
        T_coupure_PAC = data['T_coupure_PAC']
        
        nb_h_reduit = 24-(heure_arretchauffage-heure_debutchauffage)

        # concatenation des besoins sur n annees
        # t_chauf=np.tile(tab_chauf,nyears)
        # on rajoute une journee pour passer les effets de bords
        
        t_chauf = np.concatenate([tab_chauf] * nyears + [[0.] * 24])
        t_Text = repeat_without_copy(Text, nyears)
        if do_ECS:
            if tab_ECS is None:
                t_ECS = np.zeros(len(tab_chauf) * nyears + 1)
                t_ECS[heure_arretchauffage::24] = Eecs
                Eecs_annuel = Eecs * 365
            else:
                t_ECS = np.concatenate([tab_ECS] * nyears + [[0.]])
                Eecs_annuel = sum(tab_ECS)
        if do_raf:
            t_raf = repeat_without_copy(tab_raf, nyears)
            _H_bat_geoc = h_geocooling * Sbat
            _alpha = _H_bat_geoc / (1+_H_bat_geoc*Rb/dim)
            _beta = (   1./_H_bat_geoc
                        + 1./(  2*data['debit_gth']
                                *params_base.rho_eau_g
                                *params_base.Cp_eau_g
                                )
                        )
            _gamma = 1./(   data['debit_gth']
                            *params_base.rho_eau_g
                            *params_base.Cp_eau_g
                            )
            # Avec rafraichissement, on s'interesse aux nombres d'heures d'inconfort
            t_Tbat = Text + chargesinternes/_GSh # Temperature du batiment = Text + sources internes
            t_Tbat[:jour_debutrafraichissement*24] = 0.
            t_Tbat[jour_arretrafraichissement*24:] = 0.
            t_Tbat_inconf = t_Tbat-T_cons_froid > delta_T
            nb_h_inconfort_sans_raf = sum(t_Tbat_inconf)
            t_Tbat = repeat_without_copy(t_Tbat, nyears)
            # Temperature du fluide a l'entree du batiment
            Tfbi_min = data['Tfbi_min']
        else:
            nb_h_inconfort_sans_raf = None
        if do_AC:
            t_clim = repeat_without_copy(tab_AC, nyears)
            T_coupure_PAC_cool = data['HP_shutdown_temp_cool']
            Tflinem = data['emitter_inlet_temp_cool']
            DTem = data['emitter_temp_drop_cool']
            COP_backup_AC = data['COP_backup_AC']
        else:
            COP_backup_AC = 1
        # Température en paroi de forage
        t_Tb = np.zeros_like(t_Tinitsol)
        # Température à l'entrée de l'échangeur
        t_Tfleech = np.zeros_like(t_Tinitsol)
        t_Tflsech = np.zeros_like(t_Tinitsol)
        Vfl_PAC = data['debit_gth']     # [m3/s]
        cpfl = params_base.Cp_eau_g     # [J/kg/K]
        rhofl = params_base.rho_eau_g   # [kg/m3]
        cpfl_V = rhofl*cpfl             # [J/m3/K]
        # Température à l'entrée de l'émetteur
        t_Tfbi = np.zeros_like(t_Tinitsol)
        
        # initialisation du nombre heures de production d'ECS
        nb_h_ECS = 0
        # initialisation de la puissance de chauffage durant les heures 
        # de production d'ECS, à reporter plus tard
        P_chauf_report = 0.   
        
        # construction table des puissances source froide (signe - pour raf)
        t_Pf = np.zeros(nbdt)
        # Puissance échangée échangeur gth / sol:
        # - mode chauf/ecs: = P frigorifique > 0
        # - mode géocooling: = P calorifique = P frigorifique < 0
        # - mode climatisation: = P calorifique < 0
        t_Pheatexch = np.zeros(nbdt)
        # Puissance échangée côté émetteur:
        # - mode chauf/ecs: = P calorifique > 0
        # - mode géocooling: = P calorifique = P frigorifique < 0
        # - mode climatisation: = P frigorifique < 0
        t_Pemitter = np.zeros(nbdt)
        # construction table des puissances absorbée    
        t_Pa = np.zeros(nbdt)
        # Table identifiant le mode de fonctionnement
        # 0: rien
        # 1: chaud
        # 2: ECS
        # 3: géocooling
        # 4: climatisation
        t_mode_HP = np.zeros(nbdt, dtype=int)
        # construction table charge PAC ou geocooling
        t_charge = np.zeros(nbdt)
        t_puissance = np.zeros(nbdt)
        t_dpuissance = np.empty(nbdt) # dp[i] = p[i+1]-p[i]
        
        # Backup powers for BHE field
        # With BHE field, power that cannot be supplied by geothermy is relayed
        # to supply systems.
        t_Pbackup_heat = np.zeros_like(t_Pf)
        t_Pbackup_AC = np.zeros_like(t_Pf)
        
        if data['use_aggregation']:
            n_steps_agg = data['n_steps_agg']
            n_steps_imm = data['n_steps_imm']
            n_agg = 0 # number of aggregated blocks
            i_agg_update = n_steps_agg + n_steps_imm # index to reach for updating number of aggregations
            i_agg_end = 0 # index for aggregation end / immediate start
            is_agg = [i_agg_update, i_agg_end, n_agg]
            n_steps_agg_max = nbdt//n_steps_agg + 1
            t_puissance_agg = np.zeros(n_steps_agg_max) # mean powers for aggregated blocks
            t_dG_agg = np.zeros(nbdt) # delta G for aggregated blocks
            # Really fast way of computing cumulative sum
            # [ t_dG_echangeur_gth[j:j+n_steps_agg].sum()
            #   for j in range(n_steps_imm, nbdt-n_steps_agg+1)
            #   ]
            # Notice that first n_steps_imm and last n_steps_agg-1 terms are
            # never used in aggregation (provided that n_steps_imm >= n_steps_agg
            t_dG_agg[n_steps_imm:nbdt-n_steps_agg+1] = fftconvolve(
                t_dG_echangeur_gth[n_steps_imm:],
                np.ones(n_steps_agg, dtype=int),
                mode='valid'
                )
            aggregation_tabs = [is_agg,
                                n_steps_agg,
                                t_puissance_agg,
                                # t_dpuissance_agg,
                                # t_G_agg,
                                t_dG_agg,
                                ]
        else:
            aggregation_tabs = None

        # initialisation temp paroi de forage, entrées échangeur et émetteur
        t_Tb[0] = t_Tinitsol[0]
        t_Tfleech[0] = t_Tb[0]
        t_Tflsech[0] = t_Tb[0]
        t_Tfbi[0] = t_Tb[0]
        t_nb_h_ECS = np.zeros_like(t_chauf, dtype=int)
        t_P_chauf_report = np.zeros_like(t_chauf)
        t_t_Pc_ECS = np.zeros_like(t_chauf)
        
        # Initialisation de la température à l'entrée de l'émetteur

        for i in range(nbdt):
            if do_ECS and t_ECS[i] > 0.:
                # Priorité à l'ECS
                nb_h_ECS, P_chauf_report = update_puissance_mode_ECS(
                    dim, i, t_Tb,
                    t_Pf, t_Pa, t_charge,
                    t_chauf, t_ECS,
                    nb_h_ECS, P_chauf_report, t_Tfbi, t_Tfleech,
                    (   nb_h_reduit, T_condens_ECS, T_coupure_PAC_ECS,
                        Tflee_func, Pc, Pf, Pa, Rb, Vfl_PAC, cpfl_V
                        )
                )
                t_Pheatexch[i] = t_Pf[i]
                Pc_val  = t_Pf[i] + t_Pa[i]
                t_Pemitter[i] = Pc_val
                t_mode_HP[i] = 2
            elif do_raf and t_raf[i] > 0.:
                # Pas de besoin d'ECS & Calcul du rafraichissement (géocooling)
                update_puissance_mode_RAF(
                    dim, i, t_Tb,
                    t_Pf, t_charge,
                    t_Text, t_raf, t_Tbat, t_Tfbi, t_Tfleech,
                    (chargesinternes, _GSh, Tfbi_min, _alpha, _beta, _gamma)
                )
                t_Pheatexch[i] = t_Pf[i]
                t_Pemitter[i] = t_Pf[i]
                t_mode_HP[i] = 3
            elif t_chauf[i] > 0. :
                # Calcul du chauffage
                update_puissance_mode_chauf(
                    dim, i, t_Tb,
                    t_Pf, t_Pa, t_charge, t_Pbackup_heat,
                    t_Text, t_chauf, t_Tfbi, t_Tfleech, t_Tflsech,
                    (   T_coupure_PAC, Tflee_func, f_calc_Temission, Pc, Pf, Pa,
                        Rb, Vfl_PAC, cpfl_V, exch_type
                        )
                )
                t_Pheatexch[i] = t_Pf[i]
                Pc_val  = t_Pf[i] + t_Pa[i]
                t_Pemitter[i] = Pc_val
                t_mode_HP[i] = 1
            elif do_AC and t_clim[i] > 0.:
                # Pas de besoin d'ECS, de géocooling ou de chauffage
                # --> Calcul du rafraichissement (climatisation)
                update_puissance_mode_clim(
                    dim, i, t_Tb,
                    t_Pf, t_Pa, t_charge, t_Pbackup_AC,
                    t_clim, t_Tfbi, t_Tfleech, t_Tflsech,
                    (   T_coupure_PAC_cool, Tfloutcond_func, Tflinem, DTem,
                        Pc, Pf, Pa, Rb, Vfl_PAC, cpfl_V, exch_type
                        )
                     )
                Pc_val  = t_Pf[i]   - t_Pa[i]
                # -10 = (  -7.5 ) -  (+2.5)  
                # en mode climatisation , le froid produit est compté en négatif        
                # rappel : côté terrain, on compte en positif les puissances perdues par le terrain.
                # Pc est alors négatif car gagné par le terrain
                t_Pheatexch[i] = Pc_val
                t_Pemitter[i] = t_Pf[i]
                t_mode_HP[i] = 4
            t_nb_h_ECS[i] = nb_h_ECS
            t_P_chauf_report[i] = P_chauf_report
            # Mise à jour de la température à la paroi au pas de temps suivant
            t_Tb[i+1] = update_Tb(
                dim, i, nbdt,
                t_Pheatexch,
                t_charge,
                t_puissance,
                t_dpuissance,
                # (t_Tinitsol, G_courtterme, t_dG_echangeur_gth),
                (t_Tinitsol, G_courtterme, t_dG_echangeur_gth, t_G_echangeur_gth), # debug
                aggregation_tabs
                )
            # Mise à jour de Tfbi et Tfleech au pas de tps suivant en supposant
            # que l'échangeur sera désactivé (pas de chauff, ni d'ECS ni de raf)
            # Si l'éch est actif au pas de tps suivant, Tfbi et Tfleech
            # seront mises à jour dans la fonction update_puissance_mode_*
            # correspondante
            if i < nbdt-1:
                # Tfbi: calcul avec Pf=0 (partie bâtiment)
                if do_raf:
                    t_Tfbi[i+1] = t_Text[i+1] + chargesinternes/_GSh
                else:
                    t_Tfbi[i+1] = np.nan
                # Pf = 0 (partie sol)
                t_Tfleech[i+1] = t_Tb[i+1]
                t_Tflsech[i+1] = t_Tb[i+1]
    # Calcul du COP et de la consommation en phase de réduit chaque annee
    COP_yearly = np.zeros(nyears)
    chargePAC  = t_charge*(t_Pa!=0)
    Pelec_HP = t_Pa*t_charge
    nhours_pump_standby = nhours
    nhours_heating = (  jour_arretchauffage + 365
                        - jour_debutchauffage
                        ) * 24
    nhours_cooling = (  jour_arretrafraichissement
                        - jour_debutrafraichissement
                        ) * 24
    
    if dealing_with_building:
        COP_yearly_heat = np.zeros(nyears)
        COP_yearly_AC = np.zeros(nyears)
        HP_mode_heat = t_mode_HP==1
        HP_mode_DHW = t_mode_HP==2
        HP_mode_raf = t_mode_HP==3
        HP_mode_AC = t_mode_HP==4
        HP_mode_cool = np.logical_or(HP_mode_raf, HP_mode_AC)    
        Pelec_HP_heat = np.array(Pelec_HP)
        Pelec_HP_heat[np.logical_not(HP_mode_heat)] = 0.
        Pelec_HP_AC = np.array(Pelec_HP)
        Pelec_HP_AC[np.logical_not(HP_mode_AC)] = 0.
        Pf_heat_yearly = np.zeros_like(COP_yearly)
        Pc_heat_yearly = np.zeros_like(COP_yearly)
        Pelec_HP_heat_yearly = np.zeros_like(COP_yearly)
        Pbackup_heat_yearly = np.zeros_like(COP_yearly)
        Pf_AC_yearly = np.zeros_like(COP_yearly)
        Pc_AC_yearly = np.zeros_like(COP_yearly)
        Pelec_HP_AC_yearly = np.zeros_like(COP_yearly)
        Pbackup_AC_yearly = np.zeros_like(COP_yearly)
        # Heat COP
        if do_heat:
            Pf_heat = t_Pheatexch * t_charge
            Pf_heat[np.logical_not(HP_mode_heat)] = 0.
            Pf_heat_yearly[:] = [
                Pf_heat[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            Pc_heat = t_Pemitter * t_charge
            Pc_heat[np.logical_not(HP_mode_heat)] = 0.
            Pc_heat_yearly[:] = [
                Pc_heat[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            Pelec_HP_heat_yearly[:] = [
                Pelec_HP_heat[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            Pbackup_heat_yearly[:] = [
                t_Pbackup_heat[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            COP_yearly_heat[:] =  ( Pc_heat_yearly
                                    )/(
                                    Pelec_HP_heat_yearly+Pbackup_heat_yearly
                                    )
        # AC COP
        if do_AC:
            Pf_AC = t_Pemitter * t_charge
            Pf_AC[np.logical_not(HP_mode_AC)] = 0.
            Pf_AC_yearly[:] = [
                Pf_AC[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            Pc_AC = t_Pheatexch * t_charge
            Pc_AC[np.logical_not(HP_mode_AC)] = 0.
            Pc_AC_yearly[:] = [
                Pc_AC[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            Pelec_HP_AC_yearly[:] = [
                Pelec_HP_AC[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            Pbackup_AC_yearly[:] = [
                t_Pbackup_AC[i*nhours:(i+1)*nhours].sum()
                for i in range(nyears)
                ]
            COP_yearly_AC[:] =  (   Pf_AC_yearly
                                    )/(
                                    Pelec_HP_AC_yearly
                                    + Pbackup_AC_yearly/COP_backup_AC
                                    )
        # Standby
        pump_standby_power = np.zeros(nyears)
        summer_standby = do_heat and not do_AC
        winter_standby = do_AC and not do_heat
        if summer_standby:
            nhours_pump_standby -= nhours_cooling
        elif winter_standby:
            nhours_pump_standby -= nhours_heating
        standby_power_one_year = nhours_pump_standby*puissance_reduit
        pump_standby_power_yearly = np.full_like(   COP_yearly,
                                                    standby_power_one_year
                                                    )
        # Account for inactive percentage of heatpump during an active hour
        # pump_standby_power = puissance_reduit*np.array((1-t_charge))
        # summer_standby = do_heat and not do_AC
        # winter_standby = do_AC and not do_heat
        # if summer_standby:
            # pump_standby_power[HP_mode_AC] = 0.
        # elif winter_standby:
            # pump_standby_power[HP_mode_heat] = 0.
        # pump_standby_power_yearly = np.zeros_like(COP_yearly)
        # pump_standby_power_yearly[:] = [
                # pump_standby_power[i*nhours:(i+1)*nhours].sum()
                # for i in range(nyears)
                # ]
        # Overall COP
        delivered_power = ( Pc_heat_yearly
                            + Pf_AC_yearly
                            )
        consumed_power = (  Pelec_HP_heat_yearly
                            + Pbackup_heat_yearly
                            + Pelec_HP_AC_yearly
                            + Pbackup_AC_yearly/COP_backup_AC
                            + pump_standby_power_yearly
                            )
        COP_yearly = delivered_power/consumed_power
        # Pseudo code. Delete when above code validated
        # pow_tot = [pc_heat + pf_ac for pc_heat,pf_ac in zip(Pc_heat_tot, Pf_AC_tot)]
        # Pelec_tot = [pelec_heat + pelec_ac in zip(Pelec_heat_tot, Pelec_AC_tot)]
        # standby_power_one_year = puissance_reduit * 24*365 # Wh
        # if (do_heat and not do_AC):
            # standby_power_one_year -= puissance_reduit * nombre_d'heures_durée_estivale
        # elif (do_AC and not do_heat):
            # standby_power_one_year -= puissance_reduit * nombre_d'heures_durée_hivernale
        # pow_pump =  standby_power_one_year # circulateurs
        # # COP_tot = [ (   pc_heat+pc_ac+pbackup_heat+pbackup_ac
                        # # )/(
                        # # pelec+pbackup_heat+pbackup_ac/cop_backup_ac+pow_pump
                        # # )  for _ in range(nyears)
                        # # ]
    else:
        for i in range(nyears):
            if do_ECS :
                # En cas d'ECS, la PAC reste en veille toute l'année
                standby_power_one_year = puissance_reduit * nhours # Wh
            else :
                # Sans ECS, la PAC est coupée en période estivale
                standby_power_one_year = puissance_reduit * (
                    nhours_heating
                    - sum(chargePAC[(i*365*24):(i*365*24+24*jour_arretchauffage)])
                    - sum(chargePAC[(i*365*24+24*jour_debutchauffage):
                                    ((i+1)*365*24)
                                    ])
                    
                )  # Wh
                if do_raf:
                    # En cas de géocooling, la PAC reste en veille sur la période de rafraîchissement aussi
                    standby_power_one_year += puissance_reduit * nhours_heating
            if do_ECS:
                COP_yearly[i] = (sum(tab_chauf) + Eecs_annuel) / (
                    sum(Pelec_HP[i*len(tab_chauf):(i+1)*len(tab_chauf)])
                    + standby_power_one_year
                )
            else:
                COP_yearly[i] = sum(tab_chauf) / (
                    sum(Pelec_HP[i*len(tab_chauf):(i+1)*len(tab_chauf)])
                    + standby_power_one_year
                )
            if print_messages:
                print("COP %d = %.2f" % (i+1, COP_yearly[i]))
    
    # #######################
    # #dumping data for testing cython main loop
    # if abs(dim-120) < 1.e-6:
        # dump_file = 'test_data.yaml'
        # # t_ECS_unmod = np.zeros(len(tab_chauf) * nyears + 1)
        # # t_ECS_unmod[heure_arretchauffage::24] = Eecs
        # # t_chauf_unmod = np.concatenate([tab_chauf] * nyears + [[0.] * 24])
        # # t_Tbat_unmod = Text + chargesinternes/_GSh # Temperature du batiment = Text + sources internes
        # # t_Tbat_unmod[:jour_debutrafraichissement*24] = 0.
        # # t_Tbat_unmod[jour_arretrafraichissement*24:] = 0.
        # # t_Tbat_unmod = repeat_without_copy(t_Tbat_unmod, nyears)
        # data_to_dump = dict(
                        # # dim=dim,
                        # # t_Tb=t_Tb,
                        # t_Pf=t_Pf,
                        # # t_Pa=t_Pa,
                        # t_charge=t_charge,
                        # # t_Text=t_Text,
                        # # t_raf=t_raf,
                        # # t_ECS=t_ECS,
                        # # t_chauf=t_chauf,
                        # # t_ECS_unmod=t_ECS_unmod,
                        # # t_chauf_unmod=t_chauf_unmod,
                        # # do_ECS=do_ECS,
                        # # do_raf=do_raf,
                        # # nbdt=nbdt,
                        # # t_Tbat=t_Tbat,
                        # # t_Tbat_unmod=t_Tbat_unmod,
                        # # chargesinternes=chargesinternes,
                        # # Rb=Rb,
                        # # _GSh=_GSh,
                        # # _H_bat_geoc=_H_bat_geoc,
                        # # debit_gth=float(Vfl_PAC),
                        # # Cp_eau_g=cpfl,
                        # # rho_eau_g=rhofl,
                        # # T_out_condens=data['PAC_T_out_condens'],
                        # # T_in_evap=data['PAC_T_in_evap'],
                        # # Pc_tab=data['PAC_Pc'],
                        # # Pa_tab=data['PAC_Pa'],
                        # # Pc = data['Pc'],
                        # # Pf = data['Pf'],
                        # # Pa = data['Pa'],
                        # # T_condens_ECS=T_condens_ECS,
                        # # T_coupure_PAC_ECS=T_coupure_PAC_ECS,
                        # # nb_h_reduit=nb_h_reduit,
                        # # T_coupure_PAC=T_coupure_PAC,
                        # # Temission_minimum=data['Temission_minimum'],
                        # # Temission1=data['Temission1'],
                        # # Temission2=data['Temission2'],
                        # # Text1=data['Text1'],
                        # # Text2=data['Text2'],
                        # t_Tinitsol=t_Tinitsol,
                        # # t_dG_echangeur_gth=t_dG_echangeur_gth,
                        # # lambda_sol=data['lambda_sol'],
                        # # rhocp_sol=data['rhocp_sol']
                        # t_puissance=t_puissance,
                        # t_Tb = t_Tb
                        # # G_courtterme=G_courtterme
                        # )
        # import yaml
        # with open(dump_file, 'w') as wt:
            # yaml.dump(data_to_dump, wt)
    # Calcul des énergies totales sur la dernière année (kWh)
    nb_h_inconfort_avec_raf = None
    # Production
    # - chauffage (appoint)
    E_app = data['E_appoint']/1000.
    # - chauffage (PAC)
    E_ch_prod = tab_chauf.sum()/1000.
    # - ECS
    E_ecs = None
    if do_ECS:
        E_ecs = Eecs_annuel/1000.
    # - géocooling
    E_raf_prod = None
    if do_raf:
        # La demande en géocooling sur un an est E_raf_prod=tab_raf.sum()/1000.
        # Cette demande n'est pas forcément assouvie à 100%. La production
        # réelle est l'énergie réinjectée dans le sol (i.e., <0)
        E_raf_prod = t_Pf[-nhours:]*t_charge[-nhours:]
        E_raf_prod = E_raf_prod[t_Pf[-nhours:] < 0.]
        E_raf_prod = -E_raf_prod.sum()/1000.
        t_Tbat_inconf = t_Tbat[-nhours:]-T_cons_froid > delta_T
        nb_h_inconfort_avec_raf = sum(t_Tbat_inconf)
    # Consommation
    # - électrique
    E_elec = standby_power_one_year + Pelec_HP[-nhours:].sum()
    E_elec /= 1000.
    # - énergie extraite du sol (i.e., > 0)
    E_ch_cons = t_Pf[-nhours:]*t_charge[-nhours:]
    E_ch_cons = E_ch_cons[t_Pf[-nhours:] > 0.]
    E_ch_cons = E_ch_cons.sum()/1000.
    # - énergie réinjectée dans le sol
    E_raf_cons = None
    if do_raf:
        E_raf_cons = E_raf_prod
        
    results_building={}
    if dealing_with_building:
        E_heat_prod = None # E_ch_prod
        E_backup_heat = None # E_app
        E_heat_cons = None # E_ch_cons = E_heat_cons + E_DHW_cons
        E_elec_heat = None # within E_elec
        E_DHW_prod = None # E_ecs
        E_backup_DHW = None # E_app
        E_DHW_cons = None # E_ch_cons = E_heat_cons + E_DHW_cons
        E_elec_DHW = None # within E_elec
        E_geocool_prod = None # E_raf_prod
        E_AC_prod = None
        E_backup_AC = None
        E_AC_cons = None
        E_elec_AC = None
        E_elec_backup_AC = None
        P_backup_max_AC = None
        P_backup_max_heat = None
        if do_heat:
            # E_heat_prod = tab_chauf.sum()/1000.
            E_backup_heat = Pbackup_heat_yearly[-1]/1000.
            HP_mode_heat_last_year = HP_mode_heat[-nhours:]
            Pf_heat_last_year = np.array(   t_Pheatexch[-nhours:]
                                            *t_charge[-nhours:]
                                            )
            Pf_heat_last_year[np.logical_not(HP_mode_heat_last_year)] = 0.
            E_heat_cons = Pf_heat_last_year.sum()/1000.
            Pc_heat_last_year = np.array(   t_Pemitter[-nhours:]
                                            *t_charge[-nhours:]
                                            )
            Pc_heat_last_year[np.logical_not(HP_mode_heat_last_year)] = 0.
            E_elec_heat = Pelec_HP_heat_yearly[-1]/1000.
            P_backup_max_heat = t_Pbackup_heat.max()/1000.
            E_heat_prod = Pc_heat_last_year.sum()/1000 # tab_chauf.sum() = E_heat_prod + E_backup_heat
        if do_AC:
            # E_AC_prod = tab_AC.sum()/1000.
            Pf_AC_last_year = np.array( t_Pemitter[-nhours:]
                                        *t_charge[-nhours:]
                                        )
            HP_mode_AC_last_year = HP_mode_AC[-nhours:]
            Pf_AC_last_year[np.logical_not(HP_mode_AC_last_year)] = 0.
            E_AC_prod = -Pf_AC_last_year.sum()/1000.
            E_backup_AC = Pbackup_AC_yearly[-1]/1000.
            Pc_AC_last_year = np.array( t_Pheatexch[-nhours:]
                                        *t_charge[-nhours:]
                                        )
            Pc_AC_last_year[np.logical_not(HP_mode_AC_last_year)] = 0.
            E_AC_cons = -Pc_AC_last_year.sum()/1000.
            E_elec_AC = Pelec_HP_AC_yearly[-1]/1000.
            E_elec_backup_AC = E_backup_AC/COP_backup_AC
            P_backup_max_AC = t_Pbackup_AC.max()/1000.
        results_building=dict(  
                                Pf_heat_yearly = Pf_heat_yearly,
                                Pc_heat_yearly = Pc_heat_yearly,
                                Pc_AC_yearly = Pc_AC_yearly,
                                Pf_AC_yearly = Pf_AC_yearly,
                                Pelec_HP_heat_yearly = Pelec_HP_heat_yearly,
                                Pbackup_heat_yearly = Pbackup_heat_yearly,
                                Pelec_HP_AC_yearly = Pelec_HP_AC_yearly,
                                Pbackup_AC_yearly = Pbackup_AC_yearly,
                                Pelec_backup_AC_yearly = (  Pbackup_AC_yearly
                                                            /COP_backup_AC
                                                            ),
                                Pheatexch = np.abs(t_Pheatexch * t_charge),
                                Pemitter = np.abs(t_Pemitter * t_charge),
                                Pelec_HP = Pelec_HP,
                                Pbackup_heat = t_Pbackup_heat,
                                Pbackup_AC = t_Pbackup_AC,
                                E_heat_prod = E_heat_prod,
                                E_backup_heat = E_backup_heat,
                                E_heat_cons = E_heat_cons,
                                E_elec_heat = E_elec_heat,
                                E_DHW_prod = E_DHW_prod,
                                E_backup_DHW = E_backup_DHW,
                                E_DHW_cons = E_DHW_cons,
                                E_elec_DHW = E_elec_DHW,
                                E_geocool_prod =E_geocool_prod,
                                E_AC_prod = E_AC_prod,
                                E_backup_AC = E_backup_AC,
                                E_AC_cons = E_AC_cons,
                                E_elec_AC = E_elec_AC,
                                E_elec_backup_AC=E_elec_backup_AC,
                                P_backup_max_heat=P_backup_max_heat,
                                P_backup_max_AC=P_backup_max_AC,
                                Theatexch_out=t_Tflsech[:-1]
                                )
    
    # Export des tables de besoins finales (modifiées par l'algo)
    tabs = [None, None, None, None]
    imin = 0 #nhours*(nyears-1)
    imax = nhours*nyears
    tabs[0] = t_chauf[imin:imax]
    if do_ECS:
        tabs[1] = t_ECS[imin:imax]
    if do_raf:
        tabs[2] = t_raf[imin:imax]
    if do_AC:
        tabs[3] = t_clim[imin:imax]
    # Various customizable data
    additional_data = []
    additional_data.append(t_Tb[:-1])
    additional_data.append(t_Tfleech[:-1])
    additional_data.append(results_building)
    # additional_data.append(t_Tflsech[:-1])
    # # Mean temperature in heat exchanger
    # t_Tflmoyech = t_Tb[:-1] - t_puissance*Rb/dim
    # additional_data.append(t_Tflmoyech)
    # additional_data.append(t_puissance)
    # additional_data.append(t_Pheatexch)
    # additional_data.append(t_Pemitter*t_charge)
    # additional_data.append(t_Pbackup_heat)
    # additional_data.append(t_Pbackup_AC)
    # additional_data.append(Pelec_HP_AC)
    # additional_data.append(t_charge)
    # additional_data.append(COP_yearly_heat)
    # additional_data.append(COP_yearly_AC)
    # tabs.append(t_Tb[:imax])
    # tabs.append(t_Tfbi[imin:imax])
    # tabs.append(t_Tfleech[imin:imax])
    # tabs.append(t_Pa[imin:imax])
    # tabs.append(t_charge[imin:imax])
    # np.savetxt('t_Tb.txt', t_Tb) # debug
    # np.savetxt('t_puissance.txt', t_puissance)# debug
    # np.savetxt('t_Tfleech.txt', t_Tfleech)# debug
    # np.savetxt('t_Tflsech.txt', t_Tflsech)# debug
    return (COP_yearly,
            (E_app, E_ch_prod, E_ecs, E_raf_prod),
            (E_elec, E_ch_cons, E_raf_cons),
            (nb_h_inconfort_sans_raf, nb_h_inconfort_avec_raf),
            tabs,
            additional_data
            # (t_Text[-nhours:], t_Tbat[-nhours:], t_Tb[-nhours:], t_Tinitsol[-nhours:], t_Tfleech[-nhours:])
            # (t_chauf[-nhours:], t_ECS[-nhours:], t_raf[-nhours:])
            )
    

def update_puissance_mode_ECS(  dim, i, t_Tb,
                                t_Pf, t_Pa, t_charge,
                                t_chauf, t_ECS,
                                nb_h_ECS, P_chauf_report, t_Tfbi, t_Tfleech,
                                args_in_tuple,
                                ):
    (   nb_h_reduit, T_condens_ECS, T_coupure_PAC_ECS, Tflee_func, Pc, Pf, Pa,
        Rb, Vfl_PAC, cpfl_V) = args_in_tuple
    nb_h_ECS += 1
    if nb_h_ECS > nb_h_reduit:
        raise SousDimensionnementError(
            "Pas assez de puissance pour produire l'ECS "
            "pendant la période de réduit de nuit"
        )
    P_chauf_report += t_chauf[i]
    t_chauf[i] = 0.
    Tflee = Tflee_func(dim, T_condens_ECS, t_Tb[i])
    if Tflee < T_coupure_PAC_ECS:
        raise SousDimensionnementError(
            "Température trop basse à l'évaporateur "
            "pour production ECS"
       )
    t_Pc_ECS = Pc(T_condens_ECS, Tflee)
    t_Pf[i] = Pf(T_condens_ECS, Tflee)
    t_Pa[i] = Pa(T_condens_ECS, Tflee)
    t_charge[i] = t_ECS[i] / t_Pc_ECS
    if t_charge[i] > 1:
        t_charge[i] = 1.
        t_ECS[i+1] += (t_ECS[i] - t_Pc_ECS)
        t_ECS[i] = t_Pc_ECS
    else:  # derniere heure de production ECS
        if P_chauf_report > 0:
            t_chauf[i+1:i+1+nb_h_reduit-nb_h_ECS] += P_chauf_report/(nb_h_reduit-nb_h_ECS)
            if nb_h_ECS == nb_h_reduit:
                raise SousDimensionnementError(
                    "Plus de temps pour récupérer "
                    "le retard de chauffage nocturne"
                )
        nb_h_ECS = 0
        P_chauf_report = 0.
    # T entree echangeur
    Pf_extraite = t_Pf[i]*t_charge[i] # > 0
    t_Tfleech[i] = t_Tb[i] - Pf_extraite*(  Rb/dim
                                            + 1./2/Vfl_PAC/cpfl_V
                                            )
    # T entree emetteur
    t_Tfbi[i] = Tflee
    return nb_h_ECS, P_chauf_report


def update_puissance_mode_RAF(  dim, i, t_Tb,
                                t_Pf, t_charge,
                                t_Text, t_raf, t_Tbat, t_Tfbi, t_Tfleech,
                                args_in_tuple
                                ):
    # Température dans le bâtiment à "l'équilibre géocooling" et dans le cas où
    # la PAC tourne à pleine puissance.
    chargesinternes, _GSh, Tfbi_min, _alpha, _beta, _gamma = args_in_tuple
    Tbat =(
        (
            _GSh * t_Text[i]
            + chargesinternes
            + _alpha * t_Tb[i]
        )
        / (_GSh + _alpha)
    )
    #Puissance de rafraîchissement par géocooling réellement utilisée
    t_Pf[i] = - max(_GSh * (t_Text[i] - Tbat) + chargesinternes, 0)
    if t_Pf[i] < 0:
        t_charge[i] = min(- t_raf[i] / t_Pf[i], 1)
        # Température réelle dans le bâtiment
        Pf_injectee = -t_Pf[i]*t_charge[i] # > 0
        t_Tbat[i] = t_Text[i]-(Pf_injectee - chargesinternes)/_GSh
        # T entree emetteur
        Tfbi = t_Tbat[i] - Pf_injectee*_beta
        if Tfbi < Tfbi_min:
            Tfbi = Tfbi_min
            Tbat = (Tfbi + (_GSh*t_Text[i]
                            + chargesinternes
                            )*_beta
                    )/(
                    1+_GSh*_beta
                    )
            t_Pf[i] = - (_GSh * (t_Text[i] - Tbat) + chargesinternes)
            t_charge[i] = min(-t_raf[i]/t_Pf[i], 1)
            Pf_injectee = -t_Pf[i]*t_charge[i]
            t_Tbat[i] = t_Text[i]-(Pf_injectee - chargesinternes)/_GSh
        t_Tfbi[i] = Tfbi
        # Température entrée échangeur
        t_Tfleech[i] = Tfbi + Pf_injectee*_gamma

def update_puissance_mode_clim( dim, i, t_Tb,
                                t_Pf, t_Pa, t_charge, t_Pbackup_AC,
                                t_clim, t_Tfbi, t_Tfleech, t_Tflsech,
                                args_in_tuple
                                ):
    (   T_coupure_PAC_cool, Tfloutcond_func, Tflinem, DTem, Pc, Pf, Pa,
        Rb, Vfl_PAC, cpfl_V, exch_type
        ) = args_in_tuple
    Tfloutev = Tflinem
    Tflinev = Tfloutev + DTem
    Tfloutcond = Tfloutcond_func(dim, Tflinev, t_Tb[i])
    if Tfloutcond > T_coupure_PAC_cool:
        raise SousDimensionnementError(
            "Température trop haute au condenseur"
            )
    # Negative powers in cooling mode
    t_Pc = -Pc(Tfloutcond, Tflinev) # Use lin. interp result in Tfloutcond_func?
    t_Pf[i] = -Pf(Tfloutcond, Tflinev)
    t_Pa[i] = Pa(Tfloutcond, Tflinev)
    t_charge[i] = -t_clim[i] / t_Pf[i]
    if t_charge[i] > 1:
        if exch_type == 'BHE_field':
            overpower = t_clim[i] + t_Pf[i] # /!\ Pf <0. in cooling
            t_Pbackup_AC[i] = overpower
            t_charge[i] = 1.
        else:
            # AC not currently accessible to single house but left anyway
            raise SousDimensionnementError(
                "PAC sous-dimensionnée pour l'appel de puissance en"
                " climatisation"
                )
    # Fluid inlet temperature in heat exchanger
    Pc_injected = -t_Pc*t_charge[i] # > 0
    t_Tfleech[i] = t_Tb[i] + Pc_injected*(  Rb/dim
                                            + 1./2/Vfl_PAC/cpfl_V
                                            )
    t_Tflsech[i] = t_Tfleech[i] - Pc_injected * 1./Vfl_PAC/cpfl_V
    # Fluid inlet temperature in emitter is 
    t_Tfbi[i] = Tflinem

def update_puissance_mode_chauf(dim, i, t_Tb,
                                t_Pf, t_Pa, t_charge, t_Pbackup_heat,
                                t_Text, t_chauf, t_Tfbi, t_Tfleech, t_Tflsech,
                                args_in_tuple
                                ):
    (   T_coupure_PAC, Tflee_func, f_calc_Temission, Pc, Pf, Pa,
        Rb, Vfl_PAC, cpfl_V, exch_type) = args_in_tuple
    Tflee = Tflee_func(dim, f_calc_Temission(t_Text[i]), t_Tb[i])
    # Attention : ceci suppose que l'écart de température à l'évaporateur 
    # est constant (non-nul), c'est-à-dire que la PAC tourne : 
    # d'où un décalage par rapport à la température sans perturbation
    if Tflee < T_coupure_PAC:
        raise SousDimensionnementError(
            "Température trop basse à l'évaporateur"
            )
    Temission = f_calc_Temission(t_Text[i])
    t_Pc = Pc(Temission, Tflee)
    t_Pf[i] = Pf(Temission, Tflee)
    t_Pa[i] = Pa(Temission, Tflee)
    t_charge[i] = t_chauf[i] / t_Pc
    if t_charge[i] > 1:
        if exch_type == 'BHE_field':
            overpower = t_chauf[i] - t_Pc
            t_Pbackup_heat[i] = overpower
            t_charge[i] = 1.
        else:
            raise SousDimensionnementError(
                "PAC sous-dimensionnée pour l'appel de puissance en chauffage"
                )
    # T entree echangeur
    Pf_extraite = t_Pf[i]*t_charge[i] # > 0
    t_Tfleech[i] = t_Tb[i] - Pf_extraite*(  Rb/dim
                                            + 1./2/Vfl_PAC/cpfl_V
                                            )
    t_Tflsech[i] = t_Tfleech[i] + Pf_extraite * 1./Vfl_PAC/cpfl_V
    # T entree emetteur
    t_Tfbi[i] = Tflee

def update_Tb(  dim, i, nbdt,
                t_Pheatexch,
                t_charge,
                t_puissance,
                t_dpuissance,
                convolution_tabs,
                aggregation_tabs
                ):
    # t_Tinitsol, G_courtterme, t_dG_echangeur_gth = convolution_tabs
    t_Tinitsol, G_courtterme, t_dG_echangeur_gth, t_G_echangeur_gth = convolution_tabs # debug
    correction = - t_Pheatexch[i] / dim * G_courtterme(t_charge[i])
    t_puissance[i] = t_charge[i] * t_Pheatexch[i]
    if i > 0:
        t_dpuissance[i-1] = t_puissance[i]-t_puissance[i-1]
    if i == nbdt-1:
        # Last step, no need to update next step
        # Remove when t_G_echangeur_gth of len nbdt+1 and not nbdt is given
        # or when aggregation is based on dP*G and not P*dG
        return
    if not aggregation_tabs is None:
        # (   is_agg, n_steps_agg, t_puissance_agg, t_dpuissance_agg, t_G_agg, t_dG_agg
        (   is_agg, n_steps_agg, t_puissance_agg, t_dG_agg
            ) = aggregation_tabs
        i_agg_update, i_agg_end, n_agg = is_agg
        if i == i_agg_update:
            # When reached, increases number of aggregated blocks
            t_puissance_agg[n_agg] = np.mean(
                t_puissance[i_agg_end:i_agg_end+n_steps_agg]
                )
            i_agg_update += n_steps_agg
            i_agg_end += n_steps_agg
            n_agg += 1
            is_agg[:] = i_agg_update, i_agg_end, n_agg
        # Immediate
        integrand_convol = np.dot(  t_puissance[i_agg_end:i],
                                    t_dG_echangeur_gth[1:i-i_agg_end+1][::-1]
                                    )
        integrand_convol -= t_puissance[0]*t_G_echangeur_gth[i+1]
        integrand_convol += t_puissance[0]*t_G_echangeur_gth[i]
        integrand_convol += t_puissance[i]*t_G_echangeur_gth[1]
        # Long-term (aggregated)
        integrand_convol += np.dot(
            t_puissance_agg[:n_agg],
            t_dG_agg[i-i_agg_end+1:i+1:n_steps_agg][::-1]
            )
    else:
        # # Previous formulation: erroneous?
        # integrand_convol = np.dot(  t_puissance[:i],
                                    # t_dG_echangeur_gth[1:i+1][::-1]
                                    # )
        # # additional terms to get same result as dP*G convolution below
        # integrand_convol -= t_puissance[0]*t_G_echangeur_gth[i+1]
        # integrand_convol += t_puissance[0]*t_G_echangeur_gth[i]
        # integrand_convol += t_puissance[i]*t_G_echangeur_gth[1]
        integrand_convol = (    t_puissance[0]*t_G_echangeur_gth[i]
                                + np.dot(   t_dpuissance[:i],
                                            t_G_echangeur_gth[1:i+1][::-1]
                                            )
                                )
 
    Tb = t_Tinitsol[i+1] - integrand_convol / dim
    Tb = Tb + correction
    return Tb
    
    
