# -*- coding: utf-8 -*-

import numpy as np
from ..models import params_base
from .simul import calcul_cop, SousDimensionnementError, repeat_without_copy
from ..bhe_field import error_messages as err_mess
try:
    import pathlib
    import sys
    CWD = pathlib.Path(__file__).parent
    cython_source_folder = CWD / 'cython_source_files'
    sys.path.append(cython_source_folder.as_posix())
    import simul_cython
    use_cython = True
except ImportError:
    use_cython = False

def calcul_dimensionnement(data, ech_gth, print_messages=False):

    dim = ech_gth['dim']
    dim_min = ech_gth['dim_min']
    dim_max = ech_gth['dim_max']
    dim_infty = ech_gth.get('dim_infty', 1000000)
    resolution_dim = ech_gth['resolution_dim']
    unite_dim = ech_gth['unite_dim']
    scop_dim = data['scop_dim']
    dim_unit = ech_gth['unite_dim']

    if (use_cython
        and data['heat_exchanger'] != 'BHE_field'
        ):
        Tflee = None
        Tfloutcond = None
        cython_data = prepare_cython_loop(data, ech_gth)
    else:
        Tflee = build_Tflinev_func(data, ech_gth)
        Tfloutcond = build_Tfloutcond_func(data, ech_gth)
        cython_data = None
    
    if print_messages:
        print("Début des calculs")
    
    COP_meilleur = [np.inf]*ech_gth['nb_annees']
    COP_prev = -np.inf
    COP_historique = []
    E_meilleur = []
    tabs_meilleur = []
    epsilon_COP = 1.e-3
    epsilon_dim = 1.e-6
    infos_message = [None, None, None]
    ### Comment traiter le cas "dim_min (initial) suffit" ?
    while ((dim_max-dim_min) > resolution_dim):


        if print_messages:
            print()
            print("dim_min=%.0f ; dim_max=%.0f" % (dim_min, dim_max))
            print("Essai avec %d %s" % (dim, unite_dim))
     

        try:
            COP_annuel, E_prod, E_cons, nb_h_inconfort, tabs, _ = calcul_cop(
                                        data, ech_gth, dim, Tflee, Tfloutcond,
                                        cython_data, print_messages
                                        )
            if COP_historique:
                COP_prev = COP_historique[-1][-1]
            # Il arrive que le système atteigne son COP maximal, qui est < scop_dim.
            # Le COP ne progresse plus et pour éviter de partir en boucle infinie,
            # on quitte la boucle.
            # A noter que si dim ne progresse plus (dim_step~0 ci-dessous), le COP
            # ne progresse plus non plus. Ce cas aura été détecté à l'itération
            # précédente (cf ci-dessous).
            COP_step = COP_annuel[-1] - COP_prev
            if abs(COP_step) < epsilon_COP*COP_annuel[-1]:
                COP_meilleur = COP_annuel
                E_meilleur = (E_prod, E_cons)
                tabs_meilleur = tabs
                infos_message[:2] = nb_h_inconfort
                infos_message[2] = (
                            'ATTENTION : le système a atteint son COP'
                            ' maximal et ne peut satisfaire la valeur cible.'
                            )
                break
            COP_historique.append(COP_annuel)
            surdim = (COP_annuel[-1] >= scop_dim)
        except SousDimensionnementError as e:
            surdim = False
            print(e)
 
        if surdim == False:
            dim_min = dim
        else:
            dim_max = dim
            COP_meilleur = COP_annuel
            E_meilleur = (E_prod, E_cons)
            tabs_meilleur = tabs
            infos_message[:2] = nb_h_inconfort
        
        if dim_max == np.Inf:
            dim = 2 * dim_min
        else:
            dim_step = resolution_dim * int(    np.ceil(float(dim_min + dim_max)
                                                / (2 * resolution_dim))
                                                ) - dim
            if abs(dim_step) < epsilon_dim*dim:
                # Dans certains cas, dim ne progresse plus (dim_step~0) et
                # l'algo part en boucle infinie.
                break
            dim += dim_step
            
        unrealistic_dim = dim>=dim_infty
        assert not unrealistic_dim, err_mess.get_message(   'unrealistic_dim',
                                                            dim=dim_infty,
                                                            unit=dim_unit
                                                            )
        # /!\ Some heat-exchangers need to be updated
        # Only horizontal (with tubes) so far, so code below only available for it!
        update_func = ech_gth.get('update_heat_exchanger')
        if update_func is not None:
            (   ech_gth['Rb'],
                ech_gth['t_G_echangeur_gth'],
                ech_gth['t_dG_echangeur_gth']
                ) = update_func(dim)
            if (use_cython
                and data['heat_exchanger'] != 'BHE_field'
                ):
                Tflee = None
                Tfloutcond = None
                cython_data = prepare_cython_loop(data, ech_gth)
            else:
                Tflee = build_Tflinev_func(data, ech_gth)
                Tfloutcond = build_Tfloutcond_func(data, ech_gth)
                cython_data = None
    # # Au besoin : pour exporter les tables correspondant au COP optimal
    # save_tabs(tabs_meilleur, data)
        
    return (dim, COP_meilleur, unite_dim, COP_historique, E_meilleur,
            infos_message, tabs_meilleur
            )
    
def build_Tflinev_func(data, heat_exch):
    """ Building the function estimating the evaporator inlet temp. of fluid
    
    data: dict, the system parameters as a dict
    heat_exch: dict, heat exchanger parameters as a dict
    
    Returns: function for computing evaporator inlet temperature of fluid
             Returned function depends on condenser outlet temperature of fluid
             and heat-exchanger borehole temperature.
    
    WARNING: valid for a heatpump under heating condition only.
    """
    rho_fl = params_base.rho_eau_g
    Cp_fl = params_base.Cp_eau_g
    Rb = heat_exch['Rb']
    debit_gth = data['debit_gth']
    _c1 =  1/(2*rho_fl*Cp_fl*debit_gth)
    
    # 
    Toutcond_vec = data['PAC_T_out_condens']  # Condenser outlet temp. of fluid
    Tinev_vec = data['PAC_T_in_evap']  # Evaporator inlet temp. of fluid
    Pf_tab = data['PAC_Pf'] # Heatpump cooling capacity
    
    def interpolate(a, b, fa, fb, x):
        """ Linear interpolation
        
        a: float, abscissa of 1st point
        b: float, abscissa of 2nd point
        fa: float, ordinate of 1st point
        fb: float, ordinate of 2nd point
        x: float, asbcissa at which to fetch value
        
        Returns fa + (x-a)*(fb-fa)/(b-a)
        
        """
        return (x-a)*(fb-fa)/(b-a)+fa

    def Tflinev(dim, Toutcond, Tb):
        """ Heatpump evaporator fluid inlet temperature under heating condition
        
        dim: float/int, heat exchanger dimension
        Toutcond: float, heatpump condenser outlet temperature of fluid
        Tb: float, borehole temperature of heat exchanger
        
        Returns heatpump evaporator inlet temperature of fluid
        
        NB: under heating conditions, the physical equilibrium yields a
            non-linear system of two equations with two unknowns, i.e.:
            - Tinev, the evaporator inlet temperature of fluid,
            - Pf, the heatpump cooling power.
            The two equations give:
            - first, a linear combination of Tinev, Pf, Tb and dim
            - second, Pf as a function of Tinev and Toutcond
            Toutcond, Tb and dim are known in the system.
            
            Instead of using a costly optimization to solve the non-linear
            system, a simplified strategy is adopted:
            - the heatpump manufacturer gives a performance table of Pf
              depending on Tinev and Toutcond,
            - using first equation, this performance table can be used to yield
              a table of Tb depending on Tinev and Toutcond
            - now, the Tb at equilibrium can be searched for in the table, and 
              the corresponding Tinev is found back using a linear interpolation
        """
        
        # Bounded values
        alpha = Rb / dim -  _c1 #changement +_c1 en -_c1 pour prise Tinevap
        Tb_tab = Pf_tab * alpha + Tinev_vec.reshape((1, -1))
        irow = np.digitize(
                [Toutcond],
                Toutcond_vec
                )[0] # Toutcond_vec[irow] is the closest value above Toutcond
        irow = min(irow, len(Toutcond_vec)-1) # bounding value
        row_below = Tb_tab[irow-1]
        row_above = Tb_tab[irow]
        icol_left = np.digitize(
                [Tb],
                row_below
                )[0] # Tb_tab[irow-1, icol_left] is the closest value above Tb
        icol_left = min(icol_left, len(row_below)-1)# bounding value
        icol_right = np.digitize(
                [Tb],
                row_above
                )[0] # Tb_tab[irow, icol_right] is the closest value above Tb
        icol_right = min(icol_right, len(row_above)-1)# bounding value
        # Deprecated, no control on indices, can raise errors
        # alpha = Rb / dim -  _c1 #changement +_c1 en -_c1 pour prise Tinevap
        # Tb_tab = Pf_tab * alpha + Tinev_vec.reshape((1, -1))
        # irow = np.digitize(
                # [Toutcond],
                # Toutcond_vec
                # )[0] # Toutcond_vec[irow] is the closest value above Toutcond
        # print()
        # print('Toutcond', Toutcond)
        # print('Toutcond_vec', Toutcond_vec)
        # print('Tb_tab', Tb_tab)
        # row_below = Tb_tab[irow-1]
        # row_above = Tb_tab[irow]
        # icol_left = np.digitize(
                # [Tb],
                # row_below
                # )[0] # Tb_tab[irow-1, icol_left] is the closest value above Tb
        # icol_right = np.digitize(
                # [Tb],
                # row_above
                # )[0] # Tb_tab[irow, icol_right] is the closest value above Tb
        # Deprecated, end
        Tinev_left = interpolate(   row_below[icol_left-1],
                                    row_below[icol_left],
                                    Tinev_vec[icol_left-1],
                                    Tinev_vec[icol_left],
                                    Tb
                                    )
        Tinev_right = interpolate(  row_above[icol_right-1],
                                    row_above[icol_right],
                                    Tinev_vec[icol_right-1],
                                    Tinev_vec[icol_right],
                                    Tb
                                    )
        Tinev = interpolate(    Toutcond_vec[irow-1],
                                Toutcond_vec[irow],
                                Tinev_left,
                                Tinev_right,
                                Toutcond
                                )
        return Tinev

    return Tflinev
    
def build_Tfloutcond_func(data, heat_exch):
    """ Building the function estimating the condenser outlet temp. of fluid
    
    data: dict, the system parameters as a dict
    heat_exch: dict, heat exchanger parameters as a dict
    
    Returns: function for computing condenser outlet temperature of fluid
             Returned function depends on evaporator inlet temperature of fluid
             and heat-exchanger borehole temperature.
    
    WARNING: valid for a heatpump under cooling condition only
             (air conditioning)
    """
    rho_fl = params_base.rho_eau_g
    Cp_fl = params_base.Cp_eau_g
    Rb = heat_exch['Rb']
    debit_gth = data['debit_gth']
    _c1 =  1/(2*rho_fl*Cp_fl*debit_gth)
    
    Toutcond_vec = data['PAC_T_out_condens']  # Condenser outlet temp. of fluid
    Tinev_vec = data['PAC_T_in_evap']  # Evaporator inlet temp. of fluid
    Pc_tab = data['PAC_Pc'] # Heatpump heating capacity
    Pa_tab = data['PAC_Pa']
    Pf_tab = data['PAC_Pf']
    
    def interpolate(a, b, fa, fb, x):
        """ Linear interpolation
        
        a: float, abscissa of 1st point
        b: float, abscissa of 2nd point
        fa: float, ordinate of 1st point
        fb: float, ordinate of 2nd point
        x: float, asbcissa at which to fetch value
        
        Returns fa + (x-a)*(fb-fa)/(b-a)
        
        """
        return (x-a)*(fb-fa)/(b-a)+fa

    def Tfloutcond(dim, Tinev, Tb):
        """ Heatpump condenser fluid outlet temperature under cooling condition
        
        dim: float/int, heat exchanger dimension
        Tinev: float, heatpump evaporator fluid inlet temperature
        Tb: float, borehole temperature of heat exchanger
        
        Returns heatpump condenser outlet temperature of fluid
        
        NB: under cooling conditions, the physical equilibrium yields a
            non-linear system of two equations with two unknowns, i.e.:
            - Toutcond, the condenser outlet temperature of fluid,
            - Pc, the heatpump heating power.
            The two equations give:
            - first, a linear combination of Toutcond, Pc, Tb and dim
            - second, Pc as a function of Tinev and Toutcond
            Tinev, Tb and dim are known in the system.
            
            Instead of using a costly optimization to solve the non-linear
            system, a simplified strategy is adopted:
            - the heatpump manufacturer gives a performance table of Pc
              depending on Tinev and Toutcond,
            - using first equation, this performance table can be used to yield
              a table of Tb depending on Tinev and Toutcond
            - now, the Tb at equilibrium can be searched for in the table, and 
              the corresponding Toutcond is found back using a linear
              interpolation
        """
        try:
            alpha = Rb / dim +  _c1
            Tb_tab = - Pc_tab * alpha + Toutcond_vec.reshape((-1,1))
            nrows, ncols = Tb_tab.shape
            icol = np.digitize(
                    [Tinev],
                    Tinev_vec
                    )[0] # Tinev_vec[icol] is the closest value above Tinev
            # icol forced within table bounds. Occurs when needed Pc is 
            # above (below) the absolute max (min) that the heatpump can provide.
            # Raise error instead? Same goes for irow_left and irow_right below
            # icol = max(min(icol, ncols-1), 1)
            col_left = Tb_tab[:,icol-1]
            col_right = Tb_tab[:,icol]
            irow_left = np.digitize(
                    [Tb],
                    col_left
                    )[0] # Tb_tab[irow_left, icol-1] is closest value above Tb
            # irow_left = max(min(irow_left, nrows-1), 1)
            irow_right = np.digitize(
                    [Tb],
                    col_right
                    )[0] # Tb_tab[irow_right, icol] is closest value above Tb
            # irow_right = max(min(irow_right, irow_right-1), 1)
            Toutcond_left = interpolate(col_left[irow_left-1],
                                        col_left[irow_left],
                                        Toutcond_vec[irow_left-1],
                                        Toutcond_vec[irow_left],
                                        Tb
                                        )
            Toutcond_right = interpolate(   col_right[irow_right-1],
                                            col_right[irow_right],
                                            Toutcond_vec[irow_right-1],
                                            Toutcond_vec[irow_right],
                                            Tb
                                            )
            Toutcond = interpolate( Tinev_vec[icol-1],
                                    Tinev_vec[icol],
                                    Toutcond_left,
                                    Toutcond_right,
                                    Tinev
                                    )
        # # See later: use linear interpolation instead of RectBivariateSplines?
        # # According to full AC test over 25y, run times drop from 41s to 35
        # # Still, results must be checked.
        # factor_left = ( Tb-col_left[irow_left-1]
                        # )/(
                        # col_left[irow_left]-col_left[irow_left-1]
                        # )
        # factor_right = (Tb-col_right[irow_right-1]
                        # )/(
                        # col_right[irow_right-1]-col_right[irow_right-1]
                        # )
        # factor_mid = (  Tinev-Tinev_vec[icol-1]
                        # )/(
                        # Tinev_vec[icol]-Tinev_vec[icol-1]
                        # )
        # Pc_left = ( Pc_tab[irow_left-1, icol-1]
                    # + factor_left * (   Pc_tab[irow_left, icol-1]
                                        # - Pc_tab[irow_left-1, icol-1]
                                        # )
                    # )
        # Pc_right = (Pc_tab[irow_right-1, icol]
                    # + factor_left * (   Pc_tab[irow_right, icol]
                                        # - Pc_tab[irow_right-1, icol]
                                        # )
                    # )
        # Pc_val = Pc_left + factor_mid*(Pc_right-Pc_left)
        # Pa_left = ( Pa_tab[irow_left-1, icol-1]
                    # + factor_left * (   Pa_tab[irow_left, icol-1]
                                        # - Pa_tab[irow_left-1, icol-1]
                                        # )
                    # )
        # Pa_right = (Pa_tab[irow_right-1, icol]
                    # + factor_left * (   Pa_tab[irow_right, icol]
                                        # - Pa_tab[irow_right-1, icol]
                                        # )
                    # )
        # Pa_val = Pa_left + factor_mid*(Pa_right-Pa_left)
        # Pf_left = ( Pf_tab[irow_left-1, icol-1]
                    # + factor_left * (   Pf_tab[irow_left, icol-1]
                                        # - Pf_tab[irow_left-1, icol-1]
                                        # )
                    # )
        # Pf_right = (Pf_tab[irow_right-1, icol]
                    # + factor_left * (   Pf_tab[irow_right, icol]
                                        # - Pf_tab[irow_right-1, icol]
                                        # )
                    # )
        # Pf_val = Pf_left + factor_mid*(Pf_right-Pf_left)    # print('Tie')
        except:
            # /!\ Valid for BHE field only.
            raise Exception(err_mess.get_message('undersized_bhe_field'))
            # error_text = (
                    # "La longueur de forage n’est pas suffisante pour répondre à"
                    # " la sollicitation de la PAC. Augmentez la longueur de"
                    # " forage ou réduisez la puissance de la PAC."
                    # " Vous pouvez prendre un ratio entre puissance de la PAC et"
                    # " longueur du forage de l’ordre de 30 W/m."
                    # )
            # raise Exception(error_text)
            # # Might be useful when results are out of bounds
            # print(Tinev, Tinev_vec)
            # print('Toc')
            # print(Toutcond_vec.reshape((-1,1)))
            # print('Tb')
            # print(Tb, Tb_tab)
            # print(icol-1, col_left.reshape((-1,1)))
            # print(irow_left, Tb_tab[irow_left])
            # print('Tout below', Toutcond_left)
            # print(icol, col_right.reshape((-1,1)))
            # print(irow_right, Tb_tab[irow_right])
            # print('Tout above', Toutcond_right)
            # print('Tout      ', Toutcond)
            # print(Tb_tab[irow_left-1, icol-1], '-----', Tb_tab[irow_right-1, icol])
            # print('|                                      |')
            # print('|                                      |')
            # print('|                                      |')
            # print('|                                      |')
            # print('|                                      |')
            # print(Tb_tab[irow_left, icol-1], '-----', Tb_tab[irow_right, icol])
            # print('Pc')
            # print(Pc_tab[irow_left-1, icol-1], '-----', Pc_tab[irow_right-1, icol])
            # print('|                                      |')
            # print('|                                      |')
            # print('|                                      |')
            # print('|                                      |')
            # print('|                                      |')
            # print(Pc_tab[irow_left, icol-1], '-----', Pc_tab[irow_right, icol])
            # print()
        return Toutcond
        
    return Tfloutcond


def checkbounds(func, *entries):
    bounds = tuple((e[0], e[-1]) for e in entries)

    def wrapper(*args):
        for i, (a, (bmin, bmax)) in enumerate(zip(args, bounds)):
            assert bmin <= a <= bmax, (
                "argument #%d (=%.2e) of %s is out of bounds (=[%.2e ; %.2e])"
                % (i, a, func, bmin, bmax)
            )
        return func(*args)

    return wrapper
    
def prepare_cython_loop(data, ech_gth):
    """ Prépare la boucle pour la version cython
    
    A OPTIMISER: la fonction devrait appraître au même endroit que celle pour 
    la préparation sans cython (cf simul.py). Ramener la préparation Python ici
    ou bien bouger la préparation cython dans simul.py?
    """
    # Préparation des différentes tables de puissance et de température
    dim = ech_gth['dim']
    chargesinternes = data['chargesinternes']    
    _GSh = data['GSh']
    h_geocooling = data['h_geocooling']
    Sbat = data['Sbat']
    heure_arretchauffage = params_base.heure_arretchauffage
    heure_debutchauffage = params_base.heure_debutchauffage
    jour_debutrafraichissement = params_base.jour_debutrafraichissement
    jour_arretrafraichissement = params_base.jour_arretrafraichissement
    tab_chauf = data['tab_chauf']
    tab_ECS = data['tab_ECS']
    tab_raf = data['tab_raf']
    nb_annees = ech_gth['nb_annees']
    Text = data['Text']
    do_ECS = int(data['do_ECS'])
    do_raf = int(data['do_raf'])
    Eecs = data['energie_ECS']
    T_cons_froid = params_base.T_cons_froid
    Tfbi_min = data['Tfbi_min']
    t_Tinitsol = ech_gth['t_Tinitsol']
    nbdt = ech_gth['nbdt']
    t_chauf = np.concatenate([tab_chauf] * nb_annees + [[0.] * 24])
    t_Text = repeat_without_copy(Text, nb_annees)
    t_ECS = np.zeros(len(tab_chauf) * nb_annees + 1)
    if do_ECS:
        if tab_ECS is None:
            t_ECS[heure_arretchauffage::24] = Eecs
            Eecs_annuel = Eecs * 365
        else:
            t_ECS = np.concatenate([tab_ECS] * nb_annees + [[0.]])
            Eecs_annuel = sum(tab_ECS)
    if do_raf:
        t_raf = repeat_without_copy(tab_raf, nb_annees)
        _H_bat_geoc = h_geocooling * Sbat
        t_Tbat = Text + chargesinternes/_GSh
        t_Tbat[:jour_debutrafraichissement*24] = 0.
        t_Tbat[jour_arretrafraichissement*24:] = 0.
        t_Tbat = repeat_without_copy(t_Tbat, nb_annees)
    else:
        t_Tbat = np.empty(nbdt)
        t_raf = np.empty(nbdt)
        chargesinternes = 0.
        _GSh = 0.
        _H_bat_geoc = 0.
        Tfbi_min = 0.
    nb_h_reduit = 24-(heure_arretchauffage-heure_debutchauffage)
    t_Tb = np.empty(nbdt+1)
    t_Pf = np.zeros(nbdt)
    t_Pa = np.zeros(nbdt)
    t_puissance = np.zeros(nbdt)
    t_Tfleech = np.zeros(nbdt+1)
    t_Tfbi = np.zeros(nbdt)
    t_charge = np.zeros(nbdt)
    t_dG_echangeur_gth = ech_gth['t_dG_echangeur_gth']
    t_G_echangeur_gth = ech_gth['t_G_echangeur_gth']
    
    # Préparation des fonctions de calcul de puissance
    Tc = np.array(data['PAC_T_out_condens'], dtype=float)
    Tf = np.array(data['PAC_T_in_evap'], dtype=float)
    Pf_tab = data['PAC_Pf']
    Pc_tab = data['PAC_Pc']
    Pa_tab = data['PAC_Pa']
    
    # Emitter data    
    emitter_Texternal = data['emitter_Texternal']
    emitter_Temission = data['emitter_Temission']
    
    # Préparation des paramètres
    heat_exchanger_types = {
        'corbeille': 1,
        'horizontal': 2,
        'vertical': 3,
        'mvertical': 4,
        }

    float_parameters = np.array([
                        chargesinternes, ech_gth['Rb'], _GSh, _H_bat_geoc,
                        params_base.T_condens_ECS, data['T_coupure_PAC_ECS'],
                        params_base.rho_eau_g, params_base.Cp_eau_g,
                        data['debit_gth'], data['T_coupure_PAC'], data['Text1'],
                        data['Temission1'], data['Text2'], data['Temission2'],
                        data['Temission_minimum'], data['lambda_sol'],
                        data['rhocp_sol'], Tfbi_min
                        ])
    heat_exch_type = heat_exchanger_types[data['heat_exchanger']]
    int_parameters = np.array([ nb_h_reduit,
                                heat_exch_type,
                                data['use_aggregation'],
                                data['n_steps_agg'],
                                data['n_steps_imm'],
                                ], dtype=np.int32)
    
    # ADD INTS
    # AGG PARAMS SET IN DONNEES.PY
    
    # NB: int32 forcé pour compatibilité avec serveur recette.
    #     Pour py2.7, int32 est le type par défaut.
    if heat_exch_type==6:
        float_parameters_hs = ech_gth['cython_float_params']
        int_parameters_hs = ech_gth['cython_int_params']
        Ghtub = ech_gth['t_G_echangeur_gth']
        Hhtub = ech_gth['t_H_echangeur_gth']
        Ipspa = ech_gth['t_I_echangeur_gth']
    else :
        float_parameters_hs = Ghtub = Hhtub = np.empty(0)
        Ipspa = np.empty((0,0))
        int_parameters_hs = np.empty(0, dtype=np.int32)
        

    return (
        simul_cython.make_loop(
            nbdt,
            do_ECS,
            do_raf,
            t_chauf,
            t_ECS,
            t_raf,
            t_Tb,
            t_Pf,
            t_Pa,
            t_puissance,
            t_charge,
            t_Tbat,
            t_Text,
            t_Tinitsol,
            t_Tfleech,
            t_Tfbi,
            t_dG_echangeur_gth,
            t_G_echangeur_gth,
            Tc,
            Tf,
            Pc_tab,
            Pa_tab,
            Pf_tab,
            emitter_Texternal,
            emitter_Temission,
            float_parameters,
            int_parameters,
            float_parameters_hs,
            int_parameters_hs,
            Ghtub,
            Hhtub,
            Ipspa
            ),
        (   t_chauf, t_ECS, t_raf, t_Pa, t_Pf, t_puissance, t_charge,
            t_Tbat, t_Tfleech, t_Tfbi, t_Tb
            )
        )
    
def save_tabs(tabs, data, folder='.'):
    """ Sauvegarde les tables des besoins thermiques, d'ECS et de géocooling
    
    tabs est la liste des trois tables, calculées sur la dernière année
    data est le dictionnaire des données
    folder est le dossier où faire la sauvegarde
    
    NB: - la table de besoin thermique est réhaussée de la puissance d'appoint.
        - les besoins thermiques qui dépassent la puissance maximale initiale
          sont tronqués à cette valeur.
    """
    from os.path import join as osjoin
    t_chauf_tot, t_ECS, t_froid = tabs
    nheures = 24*365
    if data['do_ECS']:
        mask = t_ECS>0.
        np.savetxt(osjoin(folder, 'ecs.dat'), t_ECS)
    else:
        mask = [False]*nheures
    t_appoint_tot = data['tab_appoint']
    unmask = np.logical_not(mask)
    t_chauf_tot[unmask] += t_appoint_tot[unmask]
    t_chauf_tot = np.minimum(t_chauf_tot, data['Pmax_tot'])
    np.savetxt(osjoin(folder, 'pbat_chaud.dat'), t_chauf_tot)
    if data['do_raf']:
        t_froid[mask] = 0.
        np.savetxt(osjoin(folder, 'pbat_froid.dat'), t_froid)
