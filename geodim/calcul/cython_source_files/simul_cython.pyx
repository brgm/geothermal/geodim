#coding: utf8
#cython: profile=True

""" Cython implementation of simul.py

NB: compiling options:
- cython.initializedcheck: checking initialization of memory views
- cython.boundscheck: checking containers not accessed outside of their bounds
- cython.wraparound: allowing access to containers from the end with neg indices
- cython.cdivision: ~ignoring ZeroDivisionError checks

So much to improve:
- convolution: parallel c-threads or python multiprocessing?
- parallel
- import heat exchangers convolution functions straight from source
- dfitpack and lapack: point to low-level Fortran functions, not to Python APIs

"""

import numpy as np
cimport numpy as np
from scipy.interpolate.dfitpack import regrid_smth
from scipy.interpolate.dfitpack import bispeu
from scipy.linalg.lapack import dgbsv
from scipy.signal import fftconvolve
from libc.math cimport sqrt, log10, pow, pi
from libc.stdlib cimport malloc, free
cimport cython
from cython.parallel cimport parallel
from multiprocessing import cpu_count
#from cpython cimport array
#import array

# Number of threads for parallel computations
# cimport openmp; openmp.omp_get_num_threads() returns 1?
cdef int N_PARALLEL_THREADS = cpu_count()
# Min number of operations per thread to run parallel. Serial run if below.
cdef int N_PARALLEL_MINOPERATIONS = 1

## Preparing import of heat exchangers specific functions
## /!\: could not make it work, see later
#from sys.path import append as spappend
#from os.path import join as opjoin
#spappend(opjoin('..', '..', 'echangeur_gth'))
#from cython_source_files cimport horizontal_cython

# HORIZ, VERT, MVERT, CORB EXCH DEVELOPMENTS
#############################################
    
@cython.cdivision(True)
cdef class G_courtterme:
    """Setting convolution function depending on heat exchanger type.
    /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
    OPTIMIZE THIS: create functions in heat exchanger cython files, then import
    them here
    /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
    """
    
    cdef double corbeille_mult
    cdef double horizontal_mult
    cdef double vertical_a0
    cdef double vertical_a1
    cdef double vertical_a2
    cdef double vertical_a3
    cdef double mvertical_mult1
    cdef double mvertical_mult2
    cdef double vertical_mult1
    cdef double vertical_mult2
    cdef double lgFo
    cdef double f_poly
    cdef int heat_exch_type
    
    def __init__(self, double lambda_sol, double rhocp_sol, int heat_exch_type):
        """ Setting parameters for convolution functions
        
        - lambda_sol [W/m/K]: soil heat conductivity
        - rhocp_sol [J/m3/K]: soil volumetric heat capacity
        - heat_exch_type: heat exchanger type
                            =1 for corbeille
                            =2 for horizontal
                            =3 for mvertical
                            =4 for vertical
        """
        cdef double diffusivite_sol = lambda_sol/rhocp_sol
        cdef double effusivite_sol = sqrt(lambda_sol*rhocp_sol)
        cdef double dt = 3600.
        cdef double corbeille_h = 2.7
        cdef double corbeille_r = 0.5*(1.18+1.10)/2
        self.corbeille_mult = sqrt(dt) / (  2*pi*sqrt(pi)
                                            *corbeille_h*corbeille_r
                                            *sqrt(lambda_sol*rhocp_sol)
                                            )
        self.horizontal_mult = sqrt(dt) / sqrt(pi*lambda_sol*rhocp_sol)
        self.vertical_a0 = -0.89129
        self.vertical_a1 = 0.36081
        self.vertical_a2 = -0.05508
        self.vertical_a3 = 3.59617e-3
        cdef double mvertical_rb2 = 0.065*0.065
        cdef double mvertical_hunit = 9.
        self.mvertical_mult1 = diffusivite_sol * dt / mvertical_rb2
        self.mvertical_mult2 = 1. / (lambda_sol*mvertical_hunit)
        cdef double vertical_rb2 = 0.08*0.08
        self.vertical_mult1 = diffusivite_sol * dt / vertical_rb2
        self.vertical_mult2 = 1. / lambda_sol
        self.heat_exch_type = 1 << heat_exch_type-1 # Shift operator -> 2**i
    
    cdef inline double G_courtterme_corbeille(self, double y):
        return (1-sqrt(1-y)) * self.corbeille_mult
    
    cdef inline double G_courtterme_horizontal(self, double y):
        return (1-sqrt(1-y)) * self.horizontal_mult
    
    cdef inline double G_courtterme_mvertical(self, double y):
        if y < 1e-6:
            return 0.0
        else:
            self.lgFo = log10(y * self.mvertical_mult1)
            self.f_poly = ( self.vertical_a0 + self.vertical_a1*self.lgFo
                            + self.vertical_a2*self.lgFo*self.lgFo
                            + self.vertical_a3*self.lgFo*self.lgFo*self.lgFo
                            )
            return pow(10, self.f_poly) * self.mvertical_mult2
        
    cdef inline double G_courtterme_vertical(self, double y):
        if y < 1e-6:
            return 0.0
        else:
            self.lgFo = log10(y * self.vertical_mult1)
            self.f_poly = ( self.vertical_a0 + self.vertical_a1*self.lgFo
                            + self.vertical_a2*self.lgFo*self.lgFo
                            + self.vertical_a3*self.lgFo*self.lgFo*self.lgFo
                            )
            return pow(10, self.f_poly) * self.vertical_mult2

    cdef inline double ev(self, double y):
        if self.heat_exch_type & 1:
            return self.G_courtterme_corbeille(y)
        if self.heat_exch_type & 2:
            return self.G_courtterme_horizontal(y)
        if self.heat_exch_type & 4:
            return self.G_courtterme_vertical(y)
        if self.heat_exch_type & 8:
            return self.G_courtterme_mvertical(y)
        return -1. # Error
    
    def __call__(self, double y):
        return self.ev(y)
    
cdef double f_calc_Temission(
    double Text, double[:] Texts, double[:] Temissions, double Temission_minimum
    ):
    """ Computing emitter emission temperature
    
    C version of the geodim.models.emitter.EmitterBase.get_emission_temperature
    
    """
    cdef Py_ssize_t imax = len(Texts)-2
    cdef Py_ssize_t i
    cdef double Text1
    cdef double Text2
    cdef double Tem1
    cdef double Tem2
    
    i = digitize(Text, Texts)-1
    if i < 0:
        i = 0
    if i > imax:
        i = imax
    Text1, Text2 = Texts[i:i+2]
    Tem1, Tem2 = Temissions[i:i+2]
    return f_calc_Temission_2values(
        Text, Text1, Tem1, Text2, Tem2, Temission_minimum
        )
    
cdef double f_calc_Temission_2values(
    double Text, double Text1, double Temission1, double Text2,
    double Temission2, double Temission_minimum
    ):
    """ Computing emitter emission temperature (with only 2 values in tables)
    
    C version of the geodim.models.emitter.Emitter.get_emission_temperature
    
    """
    cdef double Temission = (
        Temission1 * (Text-Text2) / (Text1-Text2)
        - Temission2 * (Text-Text1) / (Text1-Text2)
        )
    if Temission < Temission_minimum:
        return Temission_minimum
    return Temission
    
@cython.cdivision(True)
cdef double interpolate(double a, double b, double fa, double fb, double x):
    """ Simplified interpolation function for scalars """
    return (x-a)*(fb-fa)/(b-a)+fa
    

@cython.boundscheck(False)
@cython.wraparound(False)
cdef int digitize(double x, double[:] tab):
    """ Simplification of numpy digitize for Cython
    Notice tab must contain increasing numbers
    """
    cdef Py_ssize_t ntab = len(tab)
    cdef Py_ssize_t itab
    
    if x < tab[0]:
        return 0
    for itab in range(1, ntab):
        if x < tab[itab]:
            return itab
    return ntab
    
    
# NB: function below slitghly faster when tab is large
@cython.boundscheck(False)
@cython.wraparound(False)
cdef int digitize2(double x, np.ndarray[np.float64_t, ndim=1] tab, double step):
    """ Simplification of numpy digitize for Cython
    
    Notice tab must contain increasing, equally-spaced numbers
    """
    cdef Py_ssize_t ntab = len(tab)
    
    if x < tab[0]:
        return 0
    if x > tab[ntab-1]:
        return ntab
    return int((x-tab[0])/step)+1

    
@cython.boundscheck(False)
@cython.wraparound(False)
cdef class RectBivariateSpline:
    """ Cython implementation of scipy's integrate.RectBivariateSpline
    
    Implemented according to scipy.interpolate.fitpack2.py
    """
    
    # Attributes
    cdef int kx
    cdef int ky
    cdef double[:] tx
    cdef double[:] ty
    cdef double[:] tc
    
    def __init__(   self,
                    np.ndarray[np.float64_t, ndim=1] x,
                    np.ndarray[np.float64_t, ndim=1] y,
                    np.ndarray[np.float64_t, ndim=2] z
                    ):
        """ Computes RectBivariateSpline interpolation parameters
        - x are the abscissae
        - y are the ordinates
        - z are the values z(x, y) to interpolate
        """
        # zs must be flattened
        cdef np.ndarray[np.float64_t, ndim=1] zlin = z.flatten()
        # Setting bivariate spline parameters
        self.kx = 3
        self.ky = 3
        cdef int nx = len(x)-1
        cdef int ny = len(y)-1
        cdef int s = 0
        nx, self.tx, ny, self.ty, self.tc, _, _ = regrid_smth(
                                                    x, y, zlin,
                                                    x[0], x[nx], y[0], y[ny],
                                                    self.kx, self.ky, s
                                                    )
        self.tx = self.tx[:nx]
        self.ty = self.ty[:ny]
        self.tc = self.tc[:(nx - self.kx - 1) * (ny - self.ky - 1)]
    
    cdef double ev(self, double x, double y):
        """ Returns interpolated value z(x,y) """
        return bispeu(  self.tx, self.ty, self.tc, self.kx, self.ky,
                        [x], [y])[0][0] 
               
    def __call__(self, double x, double y):
        return self.ev(x, y)
        

cdef class Tflee_func:
    """ Class to build Tflee function on-the-fly """
    
    # Attributes
    cdef double _c1
    cdef double[:] _Tc
    cdef double[:] _Tf
    cdef double[:,:] _Pf_tab
    cdef double _Rb
    cdef double _alpha
    cdef double[:,:] _Tb
    cdef int _nrows
    cdef int _ncols
    cdef int _irow
    cdef int _icol
    cdef int _Ic
    cdef int _If_left
    cdef int _If_right
    cdef double _Tf_left
    cdef double _Tf_right
    
    def __init__(   self,
                    double rho_eau_g,
                    double Cp_eau_g,
                    double Rb,
                    double debit_gth,
                    np.ndarray[np.float64_t, ndim=1] Tc,
                    np.ndarray[np.float64_t, ndim=1] Tf,
                    np.ndarray[np.float64_t, ndim=2] Pf_tab
                    ):
        """ Computes generic parameters
        - rho_eau_g [kg/m3]: water density
        - Cp_eau_g [J/kg/K]: specific heat capacity
        - Rb [dim.K/W]: specific fluid-to-pipe thermal resistance, where [dim]
                        is the unit of the exchanger dimension
                        (e.g., [m2] for horizontal exchanger)
        - debit_gth [m3/h]: heat-pump volumic flowrate
        - Tc [°C]: temperatures at condenser exit
        - Tf [°C]: temperatures entering evaporator
        - Pf_tab [W]: external power exchanged with medium, where
                      Pf(Tc, Tf) = total power - electrical power consumed
        """
        self._Rb = Rb
        self._Tc = Tc
        self._Tf = Tf
        self._Pf_tab = Pf_tab
        self._Tb = np.zeros_like(Pf_tab)
        self._c1 =  1/(2*rho_eau_g*Cp_eau_g*debit_gth)
        self._nrows, self._ncols = np.shape(Pf_tab)
    
    cdef double ev(self, double dim, double Tc_obj, double Tb_obj):
        """ Computes Tflee (= temperature entering heat exchanger)
        - dim [dim]: exchanger dimension, with various units, where [dim]
                     is the unit of the exchanger dimension
                     (e.g., [m2] for horizontal exchanger)
        - Tc_obj [°C]: temperature at condenser exit
        - Tb_obj [°C]: temperature in the building
        """
        self._alpha = self._Rb / dim - self._c1 #changement +_c1 en -_c1 pour prise Tinevap
        #self._Tb = self._Tf + np.asarray(self._Pf_tab) * self._alpha
        for self._irow in range(self._nrows):
            for self._icol in range(self._ncols):
                self._Tb[self._irow, self._icol] = (self._Tf[self._icol]
                                        + self._Pf_tab[self._irow, self._icol]
                                        * self._alpha
                                        )
        self._Ic = digitize(Tc_obj, self._Tc)
        self._If_left = digitize(Tb_obj, self._Tb[self._Ic-1,:])
        self._If_right = digitize(Tb_obj, self._Tb[self._Ic,:])
        self._Tf_left = interpolate(self._Tb[self._Ic-1, self._If_left-1],
                                    self._Tb[self._Ic-1, self._If_left],
                                    self._Tf[self._If_left-1],
                                    self._Tf[self._If_left],
                                    Tb_obj
                                    )
        self._Tf_right = interpolate(   self._Tb[self._Ic, self._If_right-1],
                                        self._Tb[self._Ic, self._If_right], 
                                        self._Tf[self._If_right-1],
                                        self._Tf[self._If_right],
                                        Tb_obj
                                        )
        return interpolate( self._Tc[self._Ic-1],
                            self._Tc[self._Ic],
                            self._Tf_left,
                            self._Tf_right,
                            Tc_obj
                            )
    
    def __call__(self, double dim, double Tc_obj, double Tb_obj):
        return self.ev(dim, Tc_obj, Tb_obj)

cdef void solve_banded( np.ndarray[np.float64_t, ndim=2] ab,
                        np.ndarray[np.float64_t, ndim=1] b,
                        np.ndarray[np.float64_t, ndim=2] ab2
                        ):
    """ A simplification of scipy.linalg.solve_banded
    
    ab: array, (2, n)-mat,
    b: array, n-vec,
    ab2: array, (3, n)-mat with ab2[0] = 0.
    
    Simplifications over scipy.linalg.solve_banded:
    - all checks are disabled
    - lower and upper are forced as 1 and 0
    - overwrite_ab and overwrite_b are forced as True
    
    Result is stored on the fly in b (because overwrite_b=True)
    """
    ab2[1:, :] = ab
    dgbsv(1, 0, ab2, b, 1, 1)
    
# Pure C does nothing. Did I get something wrong?
#cdef extern from "convolve_sum.c":
#                    
#    void convolve_n_rows(   double *f_conv, double *g_conv, double *out,
#                            int j_start, int n_cols,
#                            int g_irow, int i_start, int n_rows, int f_i_start
#                            )
#                    
#    void convolve_n_rows_flip(  double *f_conv, double *g_conv, double *out,
#                                int j_start, int n_cols,
#                                int g_irow, int i_start, int n_rows, int f_i_end
#                                )

#@cython.boundscheck(False)
#@cython.wraparound(False)
cdef inline void convolve_n_rows(
                        double *f_conv, double *g_conv, double *out,
                        int j_start, int j_end, int n_cols,
                        int g_irow, int i_start, int n_rows, int f_i_start
                        ) nogil:
    """Computing f_conv[i+f_i_start]*g_conv[g_irow] on interval [j_start; j_end-1]
    for n_rows rows
    Result is stored in out[i+i_start]
    """
    cdef int i
    cdef int j
    for i in range(n_rows):
        for j in range(j_start, n_cols):
            out[(i+i_start)*n_cols+j] += (  f_conv[(i+f_i_start)*n_cols+j_start]
                                            *g_conv[g_irow*n_cols+j-j_start]
                                            )
                                            
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void convolve_n_rows_flip(
                        double *f_conv, double *g_conv, double *out,
                        int j_start, int n_cols,
                        int g_irow, int i_start, int n_rows, int f_i_end
                        ) nogil:
    """Computing f_conv[f_i_end-(i+1)]*g_conv[g_irow] on interv [j_start; ncols]
    for n_rows rows
    Result is stored in out[i+i_start]
    """
    cdef int i
    cdef int j
    for i in range(n_rows):
        for j in range(j_start, n_cols):
            out[(i+i_start)*n_cols+j] += (  f_conv[(f_i_end-(i+1))*n_cols
                                                    +j_start
                                                    ]
                                            *g_conv[g_irow*n_cols+j-j_start]
                                            )
                                            
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void convolve_cy(
                        double[:,:] f_conv, double[:,:] g_conv, double[:,:] out,
                        int j_start, int n_cols,
                        int g_irow, int i_start, int n_rows, int f_i_start
                        ):
    """ Same as convolve_n_rows with MemoryViews instead of pointers """
    with nogil:
        convolve_n_rows(&f_conv[0,0], &g_conv[0,0], &out[0,0],
                        j_start, n_cols, n_cols,
                        g_irow, i_start, n_rows, f_i_start
                        )
                    
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void convolve_cy_flip(
                        double[:,:] f_conv, double[:,:] g_conv, double[:,:] out,
                        int j_start, int n_cols,
                        int g_irow, int i_start, int n_rows, int f_i_end
                        ):
    """ Same as convolve_n_rows_flip with MemoryViews instead of pointers """
    with nogil:
        convolve_n_rows_flip(   &f_conv[0,0], &g_conv[0,0], &out[0,0],
                                j_start, n_cols, g_irow, i_start, n_rows,
                                f_i_end
                                )

@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void convolve_n_rows_gv(
                        double [:,:] f_conv, double[:] g_conv,
                        double [:,:] out,
                        int j_start, int n_cols,
                        int n_rows
                        ):
    """ Same as convolve_cy, but g_conv = vector and f_conv/out unshifted """
    with nogil:
        convolve_n_rows(&f_conv[0,0], &g_conv[0], &out[0,0],
                        j_start, n_cols, n_cols,
                        0, 0, n_rows, 0
                        )
                        
#@cython.boundscheck(False)
#@cython.wraparound(False)
#@cython.cdivision(True)
cdef inline void convolve_cypar(
                        double[:,:] f_conv, double[:,:] g_conv, double[:,:] out,
                        int j_start, int n_cols,
                        int g_irow, int i_start, int n_rows, int f_i_start
                        ):
    """ Parallel version of convolve_cy """
    cdef int id
    cdef int ncols_tot = n_cols - j_start
    cdef int step = ncols_tot//N_PARALLEL_THREADS + 1
    cdef int jstart_mp
    cdef int jend_mp
    if step < N_PARALLEL_MINOPERATIONS:
        convolve_cy(    f_conv, g_conv, out, j_start, n_cols, g_irow, i_start,
                        n_rows, f_i_start
                        )
    else:
        with nogil, parallel(num_threads=N_PARALLEL_THREADS):
            id = cython.parallel.threadid()
            jstart_mp = j_start + id*step
            jend_mp = min(j_start + (id+1)*step, n_cols)
            convolve_n_rows(&f_conv[0,0], &g_conv[0,0], &out[0,0],
                            jstart_mp, jend_mp, n_cols,
                            g_irow, i_start, n_rows, f_i_start
                            )

@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void convolve_n_rows_gvpar(
                        double [:,:] f_conv, double[:] g_conv,
                        double [:,:] out,
                        int j_start, int n_cols,
                        int n_rows
                        ):
    """ Parallel version of convolve_n_rows_gv """
    cdef int id
    cdef int ncols_tot = n_cols - j_start
    cdef int ncols_mp = ncols_tot//N_PARALLEL_THREADS
    cdef int[:] jstart_mp = np.array([id*ncols_mp for id in range(N_PARALLEL_THREADS)])
    cdef int[:] jend_mp = np.array([ncols_mp for id in range(N_PARALLEL_THREADS-1)
                            ] + [ncols_tot-ncols_mp*(N_PARALLEL_THREADS-1)]
                            )
    print('nc tot', ncols_tot)
    print('nc mp', ncols_mp)
    print('jstarts', np.array(jstart_mp))
    print('jends', np.array(jend_mp))
    print('out bef', np.array(out))
    if ncols_mp < N_PARALLEL_MINOPERATIONS:
        print('Non par call')
        convolve_n_rows_gv(f_conv, g_conv, out, j_start, n_cols, n_rows)
    else:
        with nogil, parallel(num_threads=N_PARALLEL_THREADS):
            id = cython.parallel.threadid()
            with gil:
                print('Thread /jstart / jend', id, jstart_mp[id], jend_mp[id])
            convolve_n_rows(&f_conv[0,0], &g_conv[0], &out[0,0],
                            jstart_mp[id], jend_mp[id], n_cols,
                            0, 0, n_rows, 0
                            )
    print('out after', np.array(out))
                            
cpdef void test_convolve_n_rows_gv(
                        double [:,:] f_conv, double[:] g_conv,
                        double [:,:] out,
                        int j_start, int n_cols,
                        int n_rows
                        ):
        convolve_n_rows_gv(f_conv, g_conv, out, j_start, n_cols, n_rows)
        
cpdef void test_convolve_n_rows_gvpar(
                        double [:,:] f_conv, double[:] g_conv,
                        double [:,:] out,
                        int j_start, int n_cols,
                        int n_rows
                        ):
        convolve_n_rows_gvpar(f_conv, g_conv, out, j_start, n_cols, n_rows)
        
        
cdef class make_loop:
    """Main class to compute the installation powers over the years
    """
    
    # Generic attributes
    cdef int ii
    cdef int jj
    cdef int nbdt
    cdef int do_ECS
    cdef int do_raf
    cdef int nb_h_reduit
    cdef int nb_h_ECS
    cdef double chargesinternes
    cdef double Rb
    cdef double GSh
    cdef double H_bat_geoc
    cdef double T_condens_ECS
    cdef double T_coupure_PAC_ECS
    cdef double Tfleech_const
    cdef double Tfbi_const
    cdef double T_coupure_PAC
    cdef double P_chauf_report
    cdef RectBivariateSpline Pc
    cdef RectBivariateSpline Pf
    cdef RectBivariateSpline Pa
    cdef Tflee_func Tflee_function
    cdef double Pf_exchanged
    cdef double[:] t_Tb
    cdef double[:] t_Tb_init
    cdef double[:] t_Pf
    cdef double[:] t_Pa
    cdef double[:] t_charge
    cdef double[:] t_chauf
    cdef double[:] t_ECS
    cdef double[:] t_raf
    cdef double[:] t_chauf_init
    cdef double[:] t_ECS_init
    cdef double[:] t_Tbat
    cdef double[:] t_Tbat_init
    cdef double[:] t_Text
    cdef double[:] t_Tinitsol
    cdef double[:] t_Tfleech
    cdef double[:] t_Tfbi
    cdef object t_dG_echangeur_gth # will be casted as np.ndarray
    cdef object t_G_echangeur_gth # will be casted as np.ndarray
    cdef object t_puissance # will be casted as np.ndarray
    cdef double[:] t_dpuissance
    cdef double Text1
    cdef double Temission1
    cdef double Text2
    cdef double Temission2
    cdef double Temission_minimum
    cdef double[:] emitter_Texternal,
    cdef double[:] emitter_Temission,
    cdef G_courtterme G_courtterme_function
    cdef int error_status
    # Domestic Hot Water
    cdef double Pc_ECS
    cdef int nb_h_delta
    cdef double Tflee
    # Geocooling
    cdef double alpha
    cdef double Tfbi_min
    cdef double Tbat
    # Heating
    cdef double Temission
    cdef double t_Pc
    # Temperature updating
    cdef double integrand_convol
    cdef object integrand_convol_storage # will be casted as np.ndarray
    cdef double correction
    # Aggregation
    cdef int use_aggregation
    cdef int i_agg_end
    cdef int i_agg_update
    cdef int n_agg
    cdef int n_steps_agg
    cdef int n_steps_imm
    cdef int n_steps_agg_max
    cdef double[:] t_puissance_agg
    cdef double[:] t_dG_agg
    # cdef int[:] convolve_agg
    
    def __init__(   self,
                    int nbdt,
                    int do_ECS,
                    int do_raf,
                    np.ndarray[np.float64_t, ndim=1] t_chauf,
                    np.ndarray[np.float64_t, ndim=1] t_ECS,
                    np.ndarray[np.float64_t, ndim=1] t_raf,
                    np.ndarray[np.float64_t, ndim=1] t_Tb,
                    np.ndarray[np.float64_t, ndim=1] t_Pf,
                    np.ndarray[np.float64_t, ndim=1] t_Pa,
                    np.ndarray[np.float64_t, ndim=1] t_puissance,
                    np.ndarray[np.float64_t, ndim=1] t_charge,
                    np.ndarray[np.float64_t, ndim=1] t_Tbat,
                    np.ndarray[np.float64_t, ndim=1] t_Text,
                    np.ndarray[np.float64_t, ndim=1] t_Tinitsol,
                    np.ndarray[np.float64_t, ndim=1] t_Tfleech,
                    np.ndarray[np.float64_t, ndim=1] t_Tfbi,
                    np.ndarray[np.float64_t, ndim=1] t_dG_echangeur_gth,
                    np.ndarray[np.float64_t, ndim=1] t_G_echangeur_gth,
                    np.ndarray[np.float64_t, ndim=1] Tc,
                    np.ndarray[np.float64_t, ndim=1] Tf,
                    np.ndarray[np.float64_t, ndim=2] Pc_tab,
                    np.ndarray[np.float64_t, ndim=2] Pa_tab,
                    np.ndarray[np.float64_t, ndim=2] Pf_tab,
                    double[:] emitter_Texternal,
                    double[:] emitter_Temission,
                    double[:] parameters_f,
                    int[:] parameters_i,
                    double[:] parameters_f_for_hs,
                    int[:] parameters_i_for_hs,
                    np.ndarray[np.float64_t, ndim=1] Ghtub_for_hs,
                    np.ndarray[np.float64_t, ndim=1] Hhtub_for_hs,
                    np.ndarray[np.float64_t, ndim=2] Ipspa_for_hs
                    ):
        """ Initializes tables and functions
        - nbdt: number of time steps
        
        - parameters_f:
            0: chargesinternes
            1: Rb
            2: _GSh
            3: _H_bat_geoc
            4: T_condens_ECS
            5: T_coupure_PAC_ECS
            6: rho_eau_g,
            7: Cp_eau_g,
            8: debit_gth,
            9: T_coupure_PAC,
            10: Text1,
            11: Temission1,
            12: Text2,
            13: Temission2
            14: Temission_minimum,
            15: lambda_sol,
            16: rhocp_sol,
            17: Tfbi_min
        - parameters_i:
            0: nb_h_reduit
            1: heat_exch_type
                =1 for corbeille
                =2 for horizontal
                =3 for mvertical
                =4 for vertical
            2: use_aggregation
            (parameters below available only if use_aggregation = 1)
            3: n_steps_agg
            4: n_steps_imm
        """
        # Generic attributes
        self.nbdt = nbdt
        self.do_ECS = do_ECS
        self.do_raf = do_raf
        self.nb_h_ECS = 0
        self.nb_h_reduit = parameters_i[0]
        self.chargesinternes = parameters_f[0]
        self.Rb = parameters_f[1]
        self.GSh = parameters_f[2]
        self.H_bat_geoc = parameters_f[3]
        self.T_condens_ECS = parameters_f[4]
        self.T_coupure_PAC_ECS = parameters_f[5]
        self.P_chauf_report = 0.
        self.Tfleech_const = 1./(   parameters_f[8]
                                    *parameters_f[6]
                                    *parameters_f[7]
                                    )
        if self.do_raf:
            self.Tfbi_const = ( 1./self.H_bat_geoc
                                + 1./(  2*parameters_f[8]
                                        *parameters_f[6]
                                        *parameters_f[7]
                                        )
                                )
        else:
            self.Tfbi_const = 0.
        self.T_coupure_PAC = parameters_f[9]
        self.Text1 = parameters_f[10]
        self.Temission1 = parameters_f[11]
        self.Text2 = parameters_f[12]
        self.Temission2 = parameters_f[13]
        self.Temission_minimum = parameters_f[14]
        self.Tfbi_min = parameters_f[17]
        self.t_Text = t_Text
        self.t_Tinitsol = t_Tinitsol
        self.t_Tfleech = t_Tfleech
        self.t_Tfbi = t_Tfbi
        self.t_dG_echangeur_gth = t_dG_echangeur_gth
        self.t_G_echangeur_gth = t_G_echangeur_gth
        self.t_puissance = t_puissance
        self.t_dpuissance = np.empty(self.nbdt)
        self.error_status = 0
        self.integrand_convol_storage = np.zeros(self.nbdt)
        # Emitter data        
        self.emitter_Texternal = emitter_Texternal
        self.emitter_Temission = emitter_Temission
        # Functions for compression power, electrical power consumed, and
        # external power exchanged with medium
        self.Pc = RectBivariateSpline(Tc, Tf, Pc_tab)
        self.Pa = RectBivariateSpline(Tc, Tf, Pa_tab)
        self.Pf = RectBivariateSpline(Tc, Tf, Pf_tab)
        # Tflee function
        self.Tflee_function = Tflee_func(   parameters_f[6],
                                            parameters_f[7],
                                            self.Rb,
                                            parameters_f[8],
                                            Tc,
                                            Tf,
                                            Pf_tab)
        # Convolution functions
        self.G_courtterme_function = G_courtterme(
            parameters_f[15], parameters_f[16], parameters_i[1]
            )
        # Aggregation
        self.use_aggregation = parameters_i[2]
        if self.use_aggregation:
            self.n_steps_agg = parameters_i[3]
            self.n_steps_imm = parameters_i[4]
            self.n_agg = 0
            self.i_agg_update = self.n_steps_agg + self.n_steps_imm
            self.i_agg_end = 0     
            n_steps_agg_max = self.nbdt//self.n_steps_agg + 1
            self.t_puissance_agg = np.zeros(n_steps_agg_max)
            self.t_dG_agg = np.zeros(self.nbdt)
            # self.convolve_agg = np.ones(self.n_steps_agg, dtype=int)
            for self.ii in range(self.n_steps_imm, self.nbdt-self.n_steps_agg+1):
                self.t_dG_agg[self.ii] = self.t_dG_echangeur_gth[self.ii:self.ii+self.n_steps_agg].sum()
            # Raises "TypeError: must be real number, not list"
            # self.t_dG_agg[self.n_steps_imm:self.nbdt-self.n_steps_agg+1
                # ] = [   self.t_dG_echangeur_gth[j:j+self.n_steps_agg].sum()
                        # for j in range(self.n_steps_imm, self.nbdt-self.n_steps_agg+1)
                        # ]
            # Really fast way of computing cumulative sum
            # [ t_dG_echangeur_gth[j:j+n_steps_agg].sum()
            #   for j in range(n_steps_imm, nbdt-n_steps_agg+1)
            #   ]
            # Notice that first n_steps_imm and last n_steps_agg-1 terms are
            # never used in aggregation (provided that n_steps_imm >= n_steps_agg
            # Raises "TypeError: only size-1 arrays can be converted to Python scalars"
            # self.t_dG_agg[self.n_steps_imm:self.nbdt-self.n_steps_agg+1
                # ] = fftconvolve(
                    # self.t_dG_echangeur_gth[self.n_steps_imm:],
                    # self.convolve_agg,
                    # mode='valid'
                    # )
        # Tabs below will be modified by the algorithm
        # Keep them linked with external arrays to have access from Python
        # NB: tabs declarations with readonly attribute could also do the job
        self.t_Tb = t_Tb
        self.t_Tb[0] = self.t_Tinitsol[0]
        self.t_Tfleech[0] = self.t_Tb[0]
        self.t_Tfbi[0] = self.t_Tb[0]
        self.t_Pf = t_Pf
        self.t_Pa = t_Pa
        self.t_charge = t_charge
        self.t_chauf = t_chauf
        self.t_ECS = t_ECS
        self.t_raf = t_raf
        self.t_Tbat = t_Tbat
        # Independant copies for resetting purposes
        self.t_Tb_init = np.array(self.t_Tb)
        self.t_chauf_init = np.array(t_chauf)
        self.t_ECS_init = np.array(t_ECS)
        self.t_Tbat_init = np.array(t_Tbat)
        
    cdef void reset_settings(self):
        """ Resets data to prepare a new dimensioning run """
        self.t_Tb[:] = self.t_Tb_init
        self.t_Tfleech[1:] = 0.
        self.t_Tfbi[1:] = 0.
        self.t_Tfleech[0] = self.t_Tb[0]
        self.t_Tfbi[0] = self.t_Tb[0]
        self.t_chauf[:] = self.t_chauf_init
        self.t_ECS[:] = self.t_ECS_init
        #self.t_Tbat[:] = self.t_Tbat_init
        self.reset_Tbat()
        self.integrand_convol_storage[:] = 0.
        self.nb_h_ECS = 0
        self.P_chauf_report = 0.
        self.error_status = 0
        self.t_Pa[:] = 0.
        self.t_Pf[:] = 0.
        self.t_charge[:] = 0.
        if self.use_aggregation:
            self.t_puissance_agg[:] = 0.
            self.n_agg = 0
            self.i_agg_update = self.n_steps_agg + self.n_steps_imm
            self.i_agg_end = 0
        
    cpdef void reset_Tbat(self):
        """ Resets t_Tbat
        
        To this date, must be accessible from Python (see simul.py).
        Should be optimized to be handled in cython only.
        """
        self.t_Tbat[:] = self.t_Tbat_init
        
    cdef int update_puissance_mode_ECS(self, double dim, int i):
        """ Updating tabs due to Domestic Hot Water heating
        
        - dim: installation dimension
        - i: index in time table
        """
        self.nb_h_ECS += 1
        if self.nb_h_ECS > self.nb_h_reduit:
            return 1 # Error 1
        self.P_chauf_report += self.t_chauf[i]
        self.t_chauf[i] = 0.
        self.Tflee = self.Tflee_function.ev(dim, self.T_condens_ECS,
                                            self.t_Tb[i]
                                            )
        if self.Tflee < self.T_coupure_PAC_ECS:
            return 2 # Error 2
        self.Pc_ECS = self.Pc.ev(self.T_condens_ECS, self.Tflee)
        self.t_Pf[i] = self.Pf.ev(self.T_condens_ECS, self.Tflee)
        self.t_Pa[i] = self.Pa.ev(self.T_condens_ECS, self.Tflee)
        self.t_charge[i] = self.t_ECS[i] / self.Pc_ECS
        if self.t_charge[i] > 1:
            self.t_charge[i] = 1.
            self.t_ECS[i+1] += (self.t_ECS[i] - self.Pc_ECS)
            self.t_ECS[i] = self.Pc_ECS
        else:
            if self.P_chauf_report > 0:
                self.nb_h_delta = self.nb_h_reduit-self.nb_h_ECS
                for self.jj in range(i+1, i+1+self.nb_h_delta):
                    self.t_chauf[self.jj] += self.P_chauf_report/self.nb_h_delta
                if self.nb_h_ECS == self.nb_h_reduit:
                    return 3 # Error 3
            self.nb_h_ECS = 0
            self.P_chauf_report = 0.
            
        # T entree echangeur
        self.Pf_exchanged = self.t_Pf[i]*self.t_charge[i] # Pf_exch > 0
        self.t_Tfleech[i] = self.t_Tb[i] - self.Pf_exchanged*(
                                                        self.Rb/dim
                                                        + self.Tfleech_const/2
                                                        )
        # T entree emetteur
        self.t_Tfbi[i] = self.Tflee
        
        return 0
                    
    cdef int update_puissance_mode_RAF( self,
                                        double dim,
                                        int i,
                                        double Text
                                        ):
        """ Updating tabs due to geocooling
        
        - dim: installation dimension
        - i: index in time table
        - Text: external temperature at time i*Dt
        """
        
        self.Tbat =(
            (self.GSh*Text + self.chargesinternes + self.alpha*self.t_Tb[i])
            / (self.GSh + self.alpha)
            )
                            
        self.t_Pf[i] = - (self.GSh * (Text - self.Tbat) + self.chargesinternes)
        if self.t_Pf[i] < 0:
            self.t_charge[i] = - self.t_raf[i] / self.t_Pf[i]
            if self.t_charge[i] > 1:
                self.t_charge[i] = 1
            self.Pf_exchanged = -self.t_Pf[i]*self.t_charge[i] # Pf_exch > 0
            self.t_Tbat[i] = (  Text
                                - ( self.Pf_exchanged-self.chargesinternes
                                    )/self.GSh
                                )
            # T entree emetteur
            self.t_Tfbi[i] = self.t_Tbat[i] - self.Pf_exchanged*self.Tfbi_const
            if self.t_Tfbi[i] < self.Tfbi_min:
                self.t_Tfbi[i] = self.Tfbi_min
                self.Tbat = (   self.t_Tfbi[i] + (  self.GSh*Text
                                                    + self.chargesinternes
                                                    )*self.Tfbi_const
                                )/(
                                1+self.GSh*self.Tfbi_const
                                )
                self.t_Pf[i] = - (  self.GSh*(Text-self.Tbat)
                                    + self.chargesinternes
                                    )
                self.t_charge[i] = - self.t_raf[i] / self.t_Pf[i]
                if self.t_charge[i] > 1:
                    self.t_charge[i] = 1
                self.Pf_exchanged = -self.t_Pf[i]*self.t_charge[i]
                self.t_Tbat[i] = (  Text
                                    - ( self.Pf_exchanged-self.chargesinternes
                                        )/self.GSh
                                    )
            # T entree echangeur
            self.t_Tfleech[i] = (   self.t_Tfbi[i]
                                    + self.Pf_exchanged*self.Tfleech_const
                                    )
        else:
            self.t_Pf[i] = 0.
            self.t_charge[i] = 0.
            self.t_Tbat[i] = (Text + self.chargesinternes)/self.GSh
        return 0
                            
    cdef int update_puissance_mode_chauf(   self,
                                            double dim,
                                            int i,
                                            double Text
                                            ):
        """ Updating tabs due to heating
        
        - dim: installation dimension
        - i: index in time table
        - Text: external temperature at time i*Dt
        """
        
        self.Temission = f_calc_Temission(
            Text, self.emitter_Texternal, self.emitter_Temission,
            self.Temission_minimum
            )
        self.Tflee = self.Tflee_function.ev(dim, self.Temission, self.t_Tb[i])
        if self.Tflee < self.T_coupure_PAC:
            return 4 # Error 4
        self.t_Pc = self.Pc.ev(self.Temission, self.Tflee)
        self.t_Pf[i] = self.Pf.ev(self.Temission, self.Tflee)
        self.t_Pa[i] = self.Pa.ev(self.Temission, self.Tflee)
        self.t_charge[i] = self.t_chauf[i] / self.t_Pc
        if self.t_charge[i] > 1:
            return 5 # Error 5
        # T entree echangeur
        self.Pf_exchanged = self.t_Pf[i]*self.t_charge[i] # Pf_exch > 0
        self.t_Tfleech[i] = self.t_Tb[i] - self.Pf_exchanged*(
                                                        self.Rb/dim
                                                        + self.Tfleech_const/2
                                                        )
        # T entree emetteur
        self.t_Tfbi[i] = self.Tflee
        return 0
        
    cdef void update_Tb(self, double dim, int i):
        """ Updates soil temperature at contact with heat exchanger
        
        - dim: installation dimension
        - i: index in time table
        """
        self.correction = -(self.t_Pf[i] / dim
                            * self.G_courtterme_function.ev(self.t_charge[i])
                            )
        self.t_puissance[i] = self.t_charge[i] * self.t_Pf[i]
        if i > 0:
            self.t_dpuissance[i-1] = self.t_puissance[i]-self.t_puissance[i-1]
        if i == self.nbdt-1:
            # Last step, no need to update next step
            # Remove when t_G_echangeur_gth of len nbdt+1 and not nbdt is given
            # or when aggregation is based on dP*G and not P*dG
            return
        if self.use_aggregation:
            if i == self.i_agg_update:
                # When reached, increases number of aggregated blocks
                self.t_puissance_agg[self.n_agg] = np.mean(
                    self.t_puissance[
                        self.i_agg_end:
                        self.i_agg_end+self.n_steps_agg
                        ]
                    )
                self.i_agg_update += self.n_steps_agg
                self.i_agg_end += self.n_steps_agg
                self.n_agg += 1
            # Immediate
            self.integrand_convol = np.dot(
                self.t_puissance[self.i_agg_end:i],
                self.t_dG_echangeur_gth[1:i-self.i_agg_end+1][::-1]
                )
            # Direct loop faster for small number of elements (~100)
            # self.integrand_convol = 0.
            # for self.ii in range(self.i_agg_end,i):
                # self.integrand_convol += (
                    # self.t_puissance[self.ii]
                    # * self.t_dG_echangeur_gth[i-self.ii]
                    # )
            self.integrand_convol += (
                - self.t_puissance[0]*self.t_G_echangeur_gth[i+1]
                + self.t_puissance[0]*self.t_G_echangeur_gth[i]
                + self.t_puissance[i]*self.t_G_echangeur_gth[1]
                )
            # Long-term (aggregated)
            # self.integrand_convol += np.dot(
                # self.t_puissance_agg[:self.n_agg],
                # self.t_dG_agg[i-self.i_agg_end+1:i+1:self.n_steps_agg][::-1]
                # )
            for self.ii in range(self.n_agg):
                self.integrand_convol += (
                    self.t_puissance_agg[self.ii]
                    * self.t_dG_agg[i-((self.ii+1)*self.n_steps_agg-1)]
                    )

        else:
            # Optimize by vectorizing products
            self.integrand_convol_storage[i+1:] += (
                                        self.t_puissance[i]
                                        *self.t_dG_echangeur_gth[1:self.nbdt-i]
                                        )
            self.integrand_convol = (
                            self.integrand_convol_storage[i]
                            - self.t_puissance[0]*self.t_G_echangeur_gth[i+1]
                            + self.t_puissance[0]*self.t_G_echangeur_gth[i]
                            + self.t_puissance[i]*self.t_G_echangeur_gth[1]
                            )     
            # For dp*G formulation, convol storage fails.
            # See later
            # self.integrand_convol = (
            #                     self.t_puissance[0]*self.t_G_echangeur_gth[i]
            #                     + np.dot(   self.t_dpuissance[:i],
            #                                 self.t_G_echangeur_gth[1:i+1][::-1]
            #                                 )
            #                     )
            ## Initial method
            #self.integrand_convol = np.dot( self.t_puissance[:i],
            #                                self.t_dG_echangeur_gth[1:i+1][::-1]
            #                                )
            ## Full looping not optimized
            #self.integrand_convol = 0.
            #for self.jj in range(i):
            #    self.integrand_convol += (  self.t_puissance[self.jj]
            #                                *self.t_dG_echangeur_gth[i-self.jj]
            #                                )
        self.t_Tb[i+1] = self.t_Tinitsol[i+1] - self.integrand_convol / dim
        self.t_Tb[i+1] += self.correction
        
    cdef int ev(self, double dim):
        """ Runs loop
        
        - dim: installation dimension
        """
        self.reset_settings()
        self.alpha = self.H_bat_geoc / (1+self.H_bat_geoc*self.Rb/dim)
        for self.ii in range(self.nbdt):
            if self.do_ECS and self.t_ECS[self.ii] > 0.:
                self.error_status = self.update_puissance_mode_ECS(dim, self.ii)
            elif self.do_raf and self.t_raf[self.ii] > 0.:
                self.error_status = self.update_puissance_mode_RAF(
                                                            dim, self.ii,
                                                            self.t_Text[self.ii]
                                                            )
            elif self.t_chauf[self.ii] > 0.:
                self.error_status = self.update_puissance_mode_chauf(
                                                            dim, self.ii,
                                                            self.t_Text[self.ii]
                                                            )
            if self.error_status:
                break
            self.update_Tb(dim, self.ii)
            # Updating Tfleech and Tfbin at next step assuming heat exchanger
            # will be off. Whenever exchanger is on, update_puissance_mode_*
            # functions will take care of properly updating these temperatures
            if self.ii < self.nbdt-1:
                # Tfbi: Pf=0 (building section)
                if self.do_raf:
                    self.t_Tfbi[self.ii+1] = (self.t_Text[self.ii+1]
                                        + self.chargesinternes/self.GSh
                                        )
                else:
                    self.t_Tfbi[self.ii+1] = np.nan
                # Pf = 0 (soil section)
                self.t_Tfleech[self.ii+1] = self.t_Tb[self.ii+1]
        return self.error_status

    def __call__(self, double dim):
        return self.ev(dim)

    # Functions below are for testing purposes only
    def make_loop_ECS(  self,
                        double dim,
                        np.ndarray[int, ndim=1] run_indices
                        ):
        """ Testing loop for HDW only """
        for ii in range(self.nbdt):
            if run_indices[ii]:
                self.update_puissance_mode_ECS(dim, ii)
                
    def make_loop_RAF(  self,
                        double dim,
                        np.ndarray[int, ndim=1] run_indices,
                        np.ndarray[double, ndim=1] t_Text
                        ):
        """ Testing loop for geocooling only """
        for ii in range(self.nbdt):
            if run_indices[ii]:
                self.update_puissance_mode_RAF(dim, ii, t_Text[ii])
                
    def make_loop_chauf( self,
                        double dim,
                        np.ndarray[int, ndim=1] run_indices,
                        np.ndarray[double, ndim=1] t_Text
                        ):
        """ Testing loop for heating only """
        for ii in range(self.nbdt):
            if run_indices[ii] and self.t_chauf[ii] > 0.:
                self.update_puissance_mode_chauf(dim, ii, t_Text[ii])
                
                
# DEPRECATED
#############################################

# # Initial implementation of RectBivariateSpline.
# # Actually is an implementation of SmoothBivariateSpline
# from scipy.interpolate._fitpack import _bispev
# from scipy.interpolate._fitpack import _surfit
# cdef class SmoothBivariateSpline:
#     """ Cython implementation of scipy's integrate.SmoothBivariateSpline """
#     
#     # Attributes
#     cdef int kx
#     cdef int ky
#     cdef double[:] tx
#     cdef double[:] ty
#     cdef double[:] tc
#     
#     def __init__(   self,
#                     np.ndarray[np.float64_t, ndim=1] x,
#                     np.ndarray[np.float64_t, ndim=1] y,
#                     np.ndarray[np.float64_t, ndim=2] z
#                     ):
#         """ Computes SmoothBivariateSpline interpolation parameters
#         - x are the abscissae
#         - y are the ordinates
#         - z are the values z(x, y) to interpolate
#         """
#         # Inputs must be flattened
#         cdef np.ndarray[np.float64_t, ndim=1] xlin = np.repeat(x, len(y))
#         cdef np.ndarray[np.float64_t, ndim=1] ylin = np.tile(y, len(x))
#         cdef np.ndarray[np.float64_t, ndim=1] zlin = z.flatten()
#         # Various parameters
#         # Set according to documentations:
#         # - https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.bisplrep.html#scipy.interpolate.bisplrep
#         # - https://github.com/scipy/scipy/blob/master/scipy/interpolate/fitpack/surfit.f
#         cdef int m = len(xlin)
#         self.kx = 3
#         self.ky = 3
#         cdef int nxest = 2*self.kx+3
#         cdef int nxest2 = int(self.kx+sqrt(m/2))+1
#         if nxest2 > nxest:
#             nxest = nxest2
#         cdef int nyest = 2*self.ky+3
#         cdef int nyest2 = int(self.ky+sqrt(m/2))+1
#         if nyest2 > nyest:
#             nyest = nyest2
#         cdef double[:] w = np.ones(m)
#         cdef int u = nxest - self.kx - 1
#         cdef int v = nyest - self.ky - 1
#         cdef int km = self.kx + 1
#         if self.ky > self.kx:
#             km = self.ky + 1
#         cdef int ne = nxest
#         if nyest > nxest:
#             ne = nyest
#         cdef int bx = self.kx*v + self.ky + 1
#         cdef int by = self.ky*u + self.kx + 1
#         cdef int b1 = bx
#         cdef int b2 = b1 + v - self.ky
#         if bx > by:
#             b1 = by
#             b2 = b1 + u - self.kx
#         lwrk1 = u*v*(2+b1+b2) + 2*(u+v+km*(m+ne)+ne-self.kx-self.ky) + b2 + 1
#         
#         # Setting bivariate spline parameters
#         cdef int iopt = 0
#         cdef int s = 0
#         cdef double eps = 1.e-16
#         cdef int lwrk2 = 1
#         self.tx,self.ty,self.tc,_ = _surfit(
#                             xlin, ylin, zlin, w, x.min(), x.max(), y.min(),
#                             y.max(), self.kx, self.ky, iopt, s, eps, None, None,
#                             nxest, nyest, None, lwrk1, lwrk2
#                             )
# 
#     def __call__(self, double x, double y):
#         """ Returns interpolated value z(x,y) """
#         return _bispev( self.tx, self.ty, self.tc, self.kx, self.ky, [x],
#                         [y], 0, 0)[0][0]
#                         
#     # Functions below to test fastness of cdef functions: not faster
#     cdef double ev(   self, double x, double y):
#         return bispeu(  self.tx, self.ty, self.tc, self.kx, self.ky,
#                         [x], [y])[0][0]
#                         
#     def test_p(self, np.ndarray[np.float64_t, ndim=1] xs, np.ndarray[np.float64_t, ndim=1] ys):
#         cdef int nxs = len(xs)
#         cdef int nys = len(ys)
#         cdef np.ndarray[np.float64_t, ndim=1] zs = np.zeros(nxs*nys)
#         cdef int i
#         cdef int j
#         for i in range(nxs):
#             for j in range(nys):
#                 zs[i+j*nys] = self(xs[i], ys[j])
#         return zs
#             
#     def test_c(self, np.ndarray[np.float64_t, ndim=1] xs, np.ndarray[np.float64_t, ndim=1] ys):
#         cdef int nxs = len(xs)
#         cdef int nys = len(ys)
#         cdef np.ndarray[np.float64_t, ndim=1] zs = np.zeros(nxs*nys)
#         cdef int i
#         cdef int j
#         for i in range(nxs):
#             for j in range(nys):
#                 zs[i+j*nys] = self.ev(xs[i], ys[j])
#         return zs
