"""
Cleaning folders and files created by cython compilation.

"""

from os import remove
from os.path import exists
from shutil import rmtree
import glob

folders = ('build',)
file_extensions = ('*.c', '*.o', '*.pyd', '*.h', '*.so', '*.html', '*.prof')
exceptions = ('convolve_sum.c',)

def clean_all(  folders = folders,
                file_extensions = file_extensions,
                exceptions = exceptions
                ):
    """ Deleting folders and files
    
    folders: list, folders to get rid of
    file_extensions: list, file extensions to get rid of
    exceptions: list, folders and files NOT to delete
    """
    for folder in folders:
        if (exists(folder)
            and not folder in exceptions
            ):
            rmtree(folder)
    for ext in file_extensions:
        for ffile in glob.glob(ext):
            if (exists(ffile)
                and not ffile in exceptions
                ):
                remove(ffile)

if __name__ == '__main__':
    clean_all()
