"""
Compiling cython.
Run script or call "python compile.py build_ext --inplace"

See https://cython.readthedocs.io/en/latest/src/tutorial/cython_tutorial.html
"""

import setuptools # solves "error: Unable to find vcvarsall.bat" in Python 2.7
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy
from platform import system

current_os = system()
if current_os == 'Windows':
    comp_args = ['/openmp']
    link_args = []
elif current_os == 'Linux':
    comp_args = ['-fopenmp']
    link_args = ['-fopenmp']
else:
    raise Exception('Other OS than Windows or Linux not supported')

target_script = 'simul_cython'
ext_modules = [
    Extension(
        target_script,
        ['{}.pyx'.format(target_script)],
        extra_compile_args=comp_args,
        extra_link_args=link_args,
    )
]

setup(
    ext_modules=cythonize(  ext_modules,
                            annotate=True # to see c/python interactions (.html)
                            ),
    include_dirs=[numpy.get_include()],
    script_args=['build_ext'],
    options={'build_ext':{'inplace':True}}
)

# setup(
    # ext_modules=cythonize(  "test_sum.pyx",
                            # annotate=True # to see c/python interactions (.html)
                            # ),
    # include_dirs=[numpy.get_include()],
    # script_args=['build_ext'],
    # options={'build_ext':{'inplace':True}}
# )
