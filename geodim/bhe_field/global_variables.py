""" A set of global variables for BHE field

TODO: fuse with geodim/global_variables.py

"""

import pathlib

from ..models import load_models

CWD = pathlib.Path(__file__).parent

SUPPORTED_LANGUAGES = ('fra', )
DEFAULT_LANGUAGE = 'fra'
if not DEFAULT_LANGUAGE in SUPPORTED_LANGUAGES:
    raise Exception('Default language not listed as supported.')
    
SUPPORTED_ENCODINGS = dict((
    ('UTF-8', 'utf8'),
    ('UTF-8 BOM', 'utf-8-sig'),
    ('UTF-16/UCS-2', 'utf16'),
    ))

DATA_SUBFOLDER = CWD/ 'reference_power_needs'

BUILDING_TYPES = dict((
    ('Maison récente, bien isolée', 'House_High_Insulation'),
    ('Maison ancienne, mal isolée', 'House_Low_Insulation'),
    ('Hôtel récent, bien isolé', 'Hotel_High_Insulation'),
    ('Hôtel ancien, mal isolée', 'Hotel_Low_Insulation'),
    ('Bâtiment de bureau récent, bien isolé','Office_High_Insulation'),
    ('Bâtiment de bureau ancien, mal isolé', 'Office_Low_Insulation')
    ))
    
LOCATIONS = dict((
    ('Gène (A)', 'Genoa'),
    ('Koper (B)', 'Koper'),
    ('Laigueglia (C)', 'Laigueglia'),
    ('Turin (D)', 'Turin'),
    ('Graz (E)', 'Graz'),
    ('Davos (F)', 'Davos')
    ))

# Reference heatpump data.
# Among others, contains output and absorbed power tables.
DEFAULT_HEATPUMP = 'GNEO_18H'
DEFAULT_HEATPUMP = load_models.load_heat_pump(DEFAULT_HEATPUMP)._asdict()

# Emitters data
# - temperature drop of fluid in emitter (=Tinevap - Toutevap, >0 for cooling)
EMITTER_TEMP_DROP_COOL = 5. #°C