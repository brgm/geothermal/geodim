""" Functions dealing with data acquisition and processing for BHE fields """

import numpy as np

from . import global_variables as gv
from . import error_messages as err_mess
from ..models import load_models
        
def get_preset_needs(city_name, building_type):
    """
    Importing power needs for given city and building type
    
    Parameters
    ----------
    city_name: str/int
        Name of city. If int, index in LOCATIONS.
    building_type: str/int
        Building type. If int, index in BUILDING_TYPES.
    
    Returns
    -------
    out: dict
        Power needs in [W]
    
    """
    if isinstance(city_name, int):
        names = list(gv.LOCATIONS.values())
        city_name = names[city_name]
    if isinstance(building_type, int):
        types = list(gv.BUILDING_TYPES.values())
        building_type = types[building_type]
    datafile = f'{building_type}_{city_name}'
    data = load_models.load_building(datafile)
    res = [getattr(data,attr).copy() for attr in ('heat','dhw','cool')]
    return res
    
def get_power(
    t_behavior, do_heat=None, do_ECS=None, do_raf=None, do_AC=None,
    city_index=None, type_index=None, pbat_chaud_total_custom = None,
    eecs_custom = None, pbat_froid_custom = None, *args, **kwargs
    ):
    """
    Computing thermal needs over a year.
    
    Parameters
    ----------
    t_behavior: int
        Flag for source of data (user input or preset)
    do_heat: bool
        ``True`` if heating is to be accounted for
    do_ECS: bool
        ``True`` if DHW is to be accounted for
    do_raf: bool
        ``True`` if cooling is to be accounted for
    do_AC: bool
        ``True`` if AC is to be accounted for
    city_index: int
        Index of city in list of available cities
    type_index: int
        Index of building type in list of available buildings
    pbat_chaud_total_custom: float/array,
        Total heat power desired by user or table of hourly heat needs over a
        year (user input)
    eecs_custom: float/array
        Total DHW power desired by user or table of hourly DHW needs over a
        year (user input)
    pbat_froid_custom: float/array
        Total cool power desired by user or table of hourly cooling needs over a
        year (user input)
    
    Returns
    -------
    out: np.ndarray
        1st row gives hourly heat needs over a year [W]
        2nd row gives hourly domestic hot water needs over a year [W]
        3rd row gives hourly AC needs over a year [W]
    
    """
    ndts = 8760
    powers = np.zeros((3, ndts))
    numerical_zero = 1.e-15
    if t_behavior == 1:
        # Power needs uploaded by user
        heat = pbat_chaud_total_custom
        dhw = eecs_custom
        cool = pbat_froid_custom
        at_least_one_file = False
        if do_heat:
            assert not heat is None, err_mess.get_message('pbat_chaud_missing')
            assert len(heat) == ndts, err_mess.get_message('pbat_chaud_nvals')
            at_least_one_file = True
            powers[0] = heat
        if do_ECS:
            assert not dhw is None, err_mess.get_message('dhw_missing')
            assert len(dhw) == ndts, err_mess.get_message('dhw_nvals')
            at_least_one_file = True
            powers[1] = dhw
        if do_raf or do_AC:
            assert not cool is None, err_mess.get_message('pbat_froid_missing')
            assert len(cool) == ndts, err_mess.get_message('pbat_froid_nvals')
            at_least_one_file = True
            positive_values = (cool > 0).any()
            negative_values = (cool < 0).any()
            same_signs = positive_values!=negative_values
            assert same_signs, err_mess.get_message('pbat_froid_mixed_signs')
            if negative_values:
                cool = np.abs(cool)
            powers[2] = cool
        assert at_least_one_file, err_mess.get_message('custom_files_missing')
    elif t_behavior == 2:
        heat, dhw, cool = get_preset_needs(city_index, type_index)
        custom_pows_tot = ( pbat_chaud_total_custom,
                            eecs_custom,
                            pbat_froid_custom
                            )
        for i, (ys, custom_pow_tot) in enumerate(   zip((heat, dhw, cool),
                                                        custom_pows_tot
                                                        )
                                                    ):
            pow_tot = np.sum(ys)
            if not custom_pow_tot is None:
                scale = custom_pow_tot/pow_tot
                ys *= scale
                no_need = ( scale < numerical_zero
                            or not np.isfinite(scale)
                            )
                if no_need:
                    ys[:] = 0.
            powers[i] = ys
    return powers
    
def build_power_tables(
    pow_ref, cop_ref, ref_hp=gv.DEFAULT_HEATPUMP, *args, **kwargs
    ):
    """
    Updates heat pump power tables with custom data
    
    Parameters
    ----------
    pow_ref: float
        Reference heat output at Tevap=0 and Tcond=35°C
    cop_ref: float
        Reference coefficient of performance at same temperatures
    ref_hp: dict
        Heatpump data (including reference power tables)
    
    Returns
    -------
    out: dict
        Copy of ref_hp with adjusted power tables
    """
    hp_dict = ref_hp.copy()
    result = dict(lines=[])
    iTevap0 = 0
    iTcond35 = 1
    # NB: powers accessed using tables[T_out_condens, T_in_evap]
    Pc = np.array(hp_dict['Pc']) 
    Pa = np.array(hp_dict['Pa'])
    alpha_Pc = pow_ref/Pc[iTcond35, iTevap0]
    Pc *= alpha_Pc
    alpha_Pa = pow_ref/cop_ref / Pa[iTcond35, iTevap0]
    Pa *= alpha_Pa
    # Tables completed for T_in_evap>=25
    if ref_hp['T_in_evap'][-1] < 25:
        T_in_evap_cor = np.concatenate((ref_hp['T_in_evap'],np.array([25])))
        Pc_duplique = np.transpose(np.array([Pc[:,-1]]))
        Pc_cor = np.concatenate((Pc,Pc_duplique),axis=1)
        Pa_duplique = np.transpose(np.array([Pa[:,-1]]))
        Pa_cor = np.concatenate((Pa,Pa_duplique),axis=1)
    else:
        T_in_evap_cor = ref_hp['T_in_evap']
        Pc_cor = Pc
        Pa_cor = Pa
    hp_dict['T_in_evap'] = T_in_evap_cor
    hp_dict['Pc'] = Pc_cor
    hp_dict['Pa'] = Pa_cor
    hp_dict['debit_nominal'] = hp_dict['debit_nominal']*alpha_Pc
    hp_dict['puissance_reduit'] = 0 # cutting out standby power
    return hp_dict

def set_field_preset_layout(
    preset_layout, dist, nb_x, nb_y, nb_rows=None, *args, **kwargs
    ):
    """
    Returns coordinates of BHE field positions for presetted layouts
    
    Parameters
    ----------
    preset_layout: int
        Id of preset layout
    dist: float
        Distance betweens boreholes in x and y directions
    nb_x: int
        Number of boreholes along x direction
    nb_y: int
        Number of boreholes along y direction
    nb_rows: int
        Number of rows for non-rectangle layouts
    
    Returns
    -------
    out1: np.ndarray
        X positions for boreholes
    out2: np.ndarray
        Y positions for boreholes
        
    Note
    ----
    A square layout is always built, then truncated and/or carved out depending
    on the desired layout.
        
    """
    # Default: rectangle
    datax, datay = (np.mgrid[0:nb_x, 0:nb_y]) * dist
    if preset_layout == 1:
        return datax.flatten(), datay.flatten()
    assert not nb_rows is None, err_mess.get_message('n_rows_missing')
    mask = np.ones_like(datax, dtype=bool)
    if preset_layout == 2:
        # Hollow rectangle
        assert nb_x >= 2*nb_rows, err_mess.get_message(
                                            'insufficient_n_rows_x',
                                            nb_rows=2*nb_rows
                                            )
        assert nb_y >= 2*nb_rows, err_mess.get_message(
                                            'insufficient_n_rows_y',
                                            nb_rows=2*nb_rows
                                            )
        mask[nb_rows:-nb_rows, nb_rows:-nb_rows] = False
    elif preset_layout == 3:
        # L
        assert nb_x >= nb_rows, err_mess.get_message(
                                            'insufficient_n_rows_x',
                                            nb_rows=nb_rows
                                            )
        assert nb_y >= nb_rows, err_mess.get_message(
                                            'insufficient_n_rows_y',
                                            nb_rows=nb_rows
                                            )
        mask[nb_rows:, nb_rows:] = False
    elif preset_layout == 4:
        # U
        assert nb_x >= 2*nb_rows, err_mess.get_message(
                                            'insufficient_n_rows_x',
                                            nb_rows=2*nb_rows
                                            )
        assert nb_y >= nb_rows, err_mess.get_message(
                                            'insufficient_n_rows_y',
                                            nb_rows=nb_rows
                                            )
        mask[nb_rows:-nb_rows, nb_rows:] = False
    else:
        raise Exception(err_mess.get_message('unsupported_layout'))
    return datax[mask].flatten(), datay[mask].flatten()
    
def get_field_points(
    layout_input_mode, preset_layout=None, dist=None, nb_x=None, nb_y=None,
    nb_rows=None, field_points=None, *args, **kwargs
    ):
    """
    Returns coordinates of BHE field positions
    
    Parameters
    ----------
    layout_input_mode: int
        Way coordinates are given (direct or preset layouts)
    preset_layout: int
        Id of preset layout
    dist: float
        Distance betweens boreholes in x and y directions
    nb_x: int
        Number of boreholes along x direction
    nb_y: int
        Number of boreholes along y direction
    nb_rows: int
        Number of rows for non-rectangle layouts
    field_points: list/tuple/np.darray
        (x,y) positions for direct input
        
    Returns
    -------
    out1: np.ndarray
        X positions for boreholes
    out2: np.ndarray
        Y positions for boreholes
        
    """
    use_preset_layout = layout_input_mode==1
    if use_preset_layout:
        return set_field_preset_layout(
            preset_layout, dist, nb_x, nb_y, nb_rows, *args, **kwargs
            )
    else:
        if isinstance(field_points, (list, tuple)):
            try:
                field_points = np.array(field_points, dtype=float)
            except:
                raise Exception(err_mess.get_message('field_points_format'))
        if isinstance(field_points, np.ndarray):
            if field_points.ndim == 1:
                assert len(field_points) == 2, err_mess.get_message(
                                                'field_points_format'
                                                )
                return np.array([field_points]).T
            elif field_points.ndim == 2:
                assert field_points.shape[1] == 2, err_mess.get_message(
                                                'field_points_format'
                                                )
                return field_points.T
    raise Exception(err_mess.get_message('field_points_input'))
                
