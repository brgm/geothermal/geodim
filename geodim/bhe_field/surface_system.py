""" BHE submodule for dealing with surface system inputs

Corresponding webpage fieldset is "Système de surface"

Functions pointing to data_processing -> keep namespace/fieldset correspondance

"""

from . import data_processing as dp
from . import global_variables as gv

def build_power_tables(data_in, ref_hp=gv.DEFAULT_HEATPUMP, *args, **kwargs):
    """
    Updates heat pump power tables with custom data
    
    Parameters
    ----------
    data_in: dict
        Data for building the power table
    ref_hp: dict
        Heatpump data (including reference power tables)
    
    Returns
    -------
    out: dict
        copy of ref_hp with adjusted power tables
    
    """
    pow_ref = data_in['hp_pc'] * 1000. # kW -> W
    cop_ref = data_in['hp_cop_ref']
    return dp.build_power_tables(pow_ref, cop_ref, ref_hp)
    