""" Regrouping all necessary error messages.

Error messages are sent to the web interface and are end-user-specific.
Be as explicit as possible.

"""

from . import global_variables as gv

UNSUPPORTED_ERROR_TYPE = {
    'fra': (    "Erreur non prise en charge.\n"
                "Message Python: {err_mess_init}"
            ),
    'eng': (    "Error not supported.\n"
                "Python message: {err_mess_init}"
            ),
    }
    
MISSING_PARAMETER = {
    'fra': "Paramètre '{key}' manquant pour l'erreur de type '{error_type}'",
    'eng': "Parameter '{key}' missing for error of type '{error_type}'"
    }
    
MESSAGES_TEMPLATES = dict(
    custom_files_missing={
        'fra': 'Aucun fichier de besoin fourni.',
        'eng': 'No thermal needs uploaded.',
        },
    pbat_chaud_missing={
        'fra': 'Fichier des besoins de chauffage non fourni.',
        'eng': 'Heat source file missing.',
        },
    pbat_chaud_null={
        'fra': "Besoins de chauffage nuls sur l'année.",
        'eng': 'No heating needs over the year.',
        },
    pbat_chaud_nvals={
        'fra': 'Le fichier des besoins thermiques doit contenir 8760 valeurs.',
        'eng': 'Heat source file must contain 8760 values.',
        },
    dhw_missing={
        'fra': 'Fichier des besoins ECS non fourni.',
        'eng': 'DHW source file missing.',
        },
    dhw_nvals={
        'fra': 'Le fichier des besoins ECS doit contenir 8760 valeurs.',
        'eng': 'DHW source file must contain 8760 values.',
        },
    pbat_froid_missing={
        'fra': 'Fichier des besoins en rafraîchissement non fourni.',
        'eng': 'Cooling source file missing.',
        },
    pbat_froid_nvals={
        'fra': (    'Le fichier des besoins en rafraîchissement doit contenir'
                    ' 8760 valeurs.'
                    ),
        'eng': 'Cooling source file must contain 8760 values.',
        },
    pbat_froid_mixed_signs={
        'fra': (
            'Les besoins en rafraîchissement doivent être positifs ou négatifs,'
            ' mais pas un mélange des deux.'
            ),
        'eng': (
            'Cooling source file must give needs as positive or negative values'
            'but not a mixing of both.',
            )
        },
    T_cons_froid_missing={
        'fra': 'Température de consigne pour le rafraîchissement non fourni.',
        'eng': 'Cooling control temperature missing.',
        },
    external_temp_missing={
        'fra': 'Fichier des températures extérieures non fourni.',
        'eng': 'External temperatures file missing.',
        },
    external_temp_nvals={
        'fra': (    'Le fichier des températures extérieures doit contenir'
                    ' 8760 valeurs.'
                    ),
        'eng': 'External temperatures file must contain 8760 values.',
        },
    surface_missing={
        'fra': 'Surface habitable non fournie.',
        'eng': 'Floor area missing.',
        },
    n_rows_missing={
        'fra': 'Nombre de rangées de sondes non fourni.',
        'eng': 'Number of rows for BHE field layout missing.',
        },
    insufficient_n_rows_x={
        'fra': (    "Il n'y a pas assez de rangées selon x"
                    " (minimum {nb_rows})"
                    ),
        'eng': (    "Insufficient number of rows along x"
                    " (minimum {nb_rows})"
                    )
        },
    insufficient_n_rows_y={
        'fra': (    "Il n'y a pas assez de rangées selon y"
                    " (minimum {nb_rows})"
                    ),
        'eng': (    "Insufficient number of rows along y"
                    " (minimum {nb_rows})"
                    )
        },
    field_points_input={
        'fra': (    "Problème dans l'entrée de l'implantation des puits:"
                    " choisir une implantation prédéfinie ou fournir une liste"
                    " de couples de nombres réels"
                    ),
        'eng': (    "Error in submitting positions of boreholes: choose a"
                    " preset layout or provide a list of couples of floats"
                    )
        },
    field_points_format={
        'fra': (    "Format invalide pour les implantations de puits:"
                    " fournir un couple de nombres réels par ligne"
                    ),
        'eng': (    "Invalid format for list of field points:"
                    " provide one couple of floats at each line"
                    ),
        },
    unsupported_layout={
        'fra': "Configuration d'implantation non reconnue.",
        'eng': 'Unknown layout for BHE field'
        },
    undersized_bhe_field={
        'fra': (    "La longueur de forage n’est pas suffisante pour répondre à"
                    " la sollicitation de la PAC. Augmentez la longueur de"
                    " forage ou réduisez la puissance de la PAC."
                    " Vous pouvez prendre un ratio entre puissance de la PAC et"
                    " longueur du forage de l’ordre de 30 W/m."
                    ),
        'eng': (    "Borehole length insufficient for satisfying heatpump load."
                    " Increase borehole length or reduce heatpump load."
                    " A standard load to length ratio is 30 W/m."
                    ),
        },
    unrecognized_excel_file={
        'fra': (    'Le fichier a une extension .xlsx ou .xls mais n\'a pas pu'
                    ' être traité comme un fichier Excel. Réessayer au format'
                    ' csv : dans Excel, "Enregistrer sous" et choisir'
                    ' "CSV (séparateur : point-virgule)(*.csv)" pour le type'
                    ' de fichier'
                    ),
        'eng': (    'File extension is .xlsx or .xls, but file could not be'
                    ' handled as Excel file. Try with a text file: in Excel,'
                    ' select "Save As" and choose "Unicode Text (*.txt)" for'
                    ' the format.'
                    )
        },
    unsupported_file_encoding={
        'fra': (    'Encodage de fichier non pris en charge.'
                    ' Encodages supportés: {}'.format(
                                        ', '.join(gv.SUPPORTED_ENCODINGS.keys())
                                        )
                    ),
        'eng': (    'Unsupported encoding for datafile.'
                    ' Supported encodings: {}'.format(
                                        ', '.join(gv.SUPPORTED_ENCODINGS.keys())
                                        )
                    )
        },
    unrecognized_file_format={
        'fra': (    "Le format de fichier n'est pas reconnu :"
                    # Revert to this when description available online
                    # " Voir la description du format attendu."
                    " soumettre un fichier Excel ou texte, et vérifier "
                    " la cohérence des données (même nombre de valeurs par"
                    " ligne, et même séparateur si plusieurs valeurs par"
                    " ligne)."
                    ),
        'eng': (    'Unexpected file format.'
                    ' Report to description of expected format.'
                    )
        },
    invalid_custom_data_power_needs={
        'fra': (    "Données d'entrée non valides pour les besoins thermiques :"
                    "fournir un fichier texte de 8760 lignes avec une seule"
                    " valeur par ligne."
                    ),
        'eng': (    'Invalid input for power needs: provide a 8760-row text'
                    'file with a single value on each row.'
                    )
        },
    invalid_custom_data_borehole_positions={
        'fra': (    "Données d'entrée non valides pour les forages : fournir un"
                    " fichier texte de N lignes (N sera le nombre de forages)"
                    " avec deux valeurs par ligne (coordonnées X et Y de chaque"
                    " forage)."
                    ),
        'eng': (    'Invalid input for borehole positions : provide a N-row'
                    ' text file (N will be the number of boreholes) with two'
                    ' values on each row (X and Y coordinates of borehole).'
                    )
        },
    unrealistic_dim={
        'fra': (    "PAC largement sous-dimensionnée : limite maximum"
                    " d'échangeur atteinte ({dim} {unit}). Choisir une PAC plus"
                    " puissante ou essayer d'ajouter de l'appoint."
                    ),
        'eng': (    "Undersized heatpump : maximum heat exchanger size reached"
                    " ({dim} {unit}). Select a more powerful heat pump or try"
                    " adding backup power."
                    ),
        },
    invalid_elevation={
        'fra': (    "Problème dans l'évaluation de l'altitude. La position"
                    " est-elle bien située en France métropolitaine?"
                    ),
        'eng': (    "Cannot get altitude. Coordinates might be outside of"
                    " continental France."
                    ),
        },
    elevation_request_failed={
        'fra': (    "Problème dans l'évaluation de la température."
                    " Vérifier que le jeu (longitude, latitude, profondeur) = "
                    " (2.478036°, 48.8732°, 100m) donne environ 13,3°C."
                    " Merci de signaler le problème si l'erreur persiste."
                    ),
        'eng': (    "Cannot get température. Please chack that test parameters "
                    " (long, lat, depth) = (2.478036°, 48.8732°, 100m) return"
                    " approximately 13.3°C."
                    " If problem persist, please give us a report."
                    ),
        },
    invalid_input_longitude={
        'fra': (    "Fournir un flottant pour la longitude (degrés décimaux)."
                    ),
        'eng': (    "Give a float for longitude (decimal degrees)."
                    ),
        },
    invalid_input_latitude={
        'fra': (    "Fournir un flottant pour la latitude (degrés décimaux)."
                    ),
        'eng': (    "Give a float for latitude (decimal degrees)."
                    ),
        },
    invalid_custom_emitter_ncols={
        'fra': (    "Fournir un fichier avec deux colonnes exactement."
                    ),
        'eng': (    "File must provide exactly two-columns."
                    ),
        },
    invalid_custom_emitter_texts={
        'fra': (    "Les températures extérieures doivent être strictement"
                    " croissante et ne pas contenir de doublon."
                    ),
        'eng': (    "External temepratures must be strictly increasing and"
                    " must not contain repeating values."
                    ),
        },
    )

def get_message(  error_type, language=gv.DEFAULT_LANGUAGE,
                        *args, **kwargs
                        ):
    """ Returning an error missing for specific cases
    
    error_type: str, type of error encountered
    language: str, desired language for message
    
    *args and **kwargs are passed to message template. Some messages expect
    precise keys
    
    Returns error message
    """
    if not language in gv.SUPPORTED_LANGUAGES:
        language = gv.DEFAULT_LANGUAGE
    message_template = MESSAGES_TEMPLATES.get(  error_type,
                                                UNSUPPORTED_ERROR_TYPE
                                                )
    try:
        # print(message_template)
        # print(message_template[language])
        return message_template[language].format(**kwargs)
    except KeyError as e:
        return MISSING_PARAMETER[language].format(  key=e.args[0],
                                                    error_type=error_type
                                                    )
        
if __name__ == '__main__':
    print(get_message('custom_files_missing'))
    print()
    print(get_message('custom_files_missing', language='eng'))
    print()
    print(get_message('insufficient_n_rows_x'))
    print()
    print(get_message('insufficient_n_rows_x', nb_rows=10))
    print()
    print(get_message('michel', err_mess_init='michel ne veut pas'))
    print()