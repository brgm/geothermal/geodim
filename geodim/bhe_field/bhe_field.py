""" BHE submodule for dealing with BHE fields

Corresponding webpage fieldset is "Champ de sondes"

Functions pointing to data_processing -> keep namespace/fieldset correspondance

"""

import numpy as np
from scipy.integrate import quad as sciint_quad
from scipy.spatial.distance import pdist as scispa_pdist
from scipy.special import erfc as scispe_erfc
import warnings
from . import data_processing as dp
from . import building_specs as bs
from . import global_variables as gv
from . import surface_system as ss
from . import error_messages as err_mess

from ..models import params_base
from .. import calcul
from ..resources import load_resources
from ..models import load_models
# from geodim import calcul
# import geodim.calcul as calcul
from ..donnees import regression_lineaire
import os

warnings.filterwarnings("ignore", category=RuntimeWarning)

class BHE_field_exchg:
    """ Heat exchanger model for Borehole Heat Exchangers field """
        
    def __init__(self, data_in, *args, **kwargs):
        """ Initializing properties """
        self.diffusivity_soil = None    # Soil diffusivity [m2/s]
        self.lambda_soil = None         # Soil thermal conductivity [W/m/K]
        self.Pf = None                  # Heat output table [W]
        self.p_predim_SGV_sol = None    # Predimensionning linear power [W/m]
        self.t_Tinitsoil = None         # Soil initial temperature table [°C]
        self.grad_gth = None            # Geothermal gradient [°C/m]
        self.rb = None                  # Single borehole radius [m]
        self.Rb = None                  # Thermal resistance [m.K/W] (double U)
        self.H_unit = None              # Single borehole length [m]
        self.dim_unit = None            # Dimensioning unit [m]
        self.resolution_dim = None      # Dimensioning step [m]
        self.dim_min = None             # Minimum dimension [m]
        self.dim_max = None             # Maximum dimension [m]
        self.H_init = None              # 1st guess for init borehole length [m]
        self.dim = None                 # 1st guess for dimensioning value [m]
        self.dt = None                  # Timestep [s]
        self.nyears = None              # Number of years [-]
        self.nbdt1y = None              # Number of timesteps for one year
        self.nbdt = None                # Total number of timesteps
        self.nbdtgeom = None            # Tot num of tsteps in geom progression
        self.t_G_FLS = None             # Table for short term response [-]
        self.t_G_echangeur_gth = None   # Table for long term response [m.K/W]
        self.t_dG_echangeur_gth = None  # Table for long term resp. step [m.K/W]
        self.t_G_shortterm = None       # Table for short term response [m.K/W]
        self.charge_digitize_step = None# Step for digitizing G_shortterm [-]
        self.a_B = None                 # Long-term Gfunction parameters 1
        self.a_Gmax = None              # Long-term Gfunction parameters 2
        self.pows = None                # Long-term Gfunction parameters 3
        self.a_Gshort = None            # Short-term Gfunction parameters
        self.field_points = None        # (x,y) positions of single BHE [m]
        self.tstar = None               # Adimensioned time table [-]
        self.initialize_G_parameters()
        
    def build(self, data_in):
        """ Building heat exchanger properties and convolution functions
        
        data_in: dict, input properties
        
        Returns dict of properties and convolution functions
        
        NB: uses all default values. Proceed manually if custom values needed.
        
        """
        self.set_dim_properties()
        self.set_properties_from_input(data_in)
        self.set_dimensioning_params()
        self.set_simulation_params() # Also calls self.set_soil_ini_temp()
        Hstar = self.H_unit / self.rb
        # self.build_t_dG_echangeur_gth(Hstar, self.nbdtgeom)
        self.build_t_G_echangeur_gth(Hstar, self.nbdtgeom)
        heat_exch = {}
        heat_exch['Rb'] = self.Rb
        heat_exch['t_dG_echangeur_gth'] = self.t_dG_echangeur_gth
        heat_exch['t_G_echangeur_gth'] = self.t_G_echangeur_gth
        heat_exch['dim_min'] = self.dim_min
        heat_exch['dim_max'] = self.dim_max
        heat_exch['dim'] = self.dim
        heat_exch['unite_dim'] = self.dim_unit
        heat_exch['t_Tinitsol'] = self.t_Tinitsoil
        heat_exch['G_courtterme'] = self.G_shortterm
        heat_exch['resolution_dim'] = self.resolution_dim
        heat_exch['nb_annees'] = self.nyears
        heat_exch['nbdt'] = self.nbdt
        return heat_exch
        
    def update_dim(self, H_unit, data_in):
        """ update tables from new dim """
        self.set_soil_ini_temp(data_in)
        self.dim = self.H_unit * len(self.field_points)
        Hstar = self.H_unit / self.rb
        # self.build_t_dG_echangeur_gth(Hstar, self.nbdtgeom)
        self.build_t_G_echangeur_gth(Hstar, self.nbdtgeom)
        return dim, self.t_Tinitsol, self.G_shortterm, self.t_dG_echangeur_gth
        
    def set_properties_from_input(self, data_in):
        """ Setting some properties from input
        
        data_in: dict, input properties
        
        """
        self.diffusivity_soil = data_in['diffusivite_sol']
        self.lambda_soil = data_in['lambda_sol']
        self.Pf = data_in['Pf']
        self.p_predim_SGV_sol = data_in['p_predim_SGV_sol']
        self.ini_temp_soil = data_in['Tmoy_init_sol']
        self.grad_gth = data_in['grad_gth'] / 100.
        self.ini_temp_ref_depth = data_in['Prof_sonde_test']
        if 'field_points' in data_in:
            self.field_points = np.array(data_in['field_points']) / self.rb
        else:
            #correct to direct array return
            # self.field_points = dp.set_field_layout(data_in)
            x, y = set_field_layout(data_in)
            self.field_points = np.vstack((x,y)).T / self.rb
        
    def checking_for_default_values(self, props, *args, **kwargs):
        """ A useful function for checking if default values should be used 
        
        props: list, names of properties to set
        
        Returns True if all properties were set, False otherwise
        
        NB: a decorator could be used for a more pythonic way
        
        """
        if args:
            err_mess = 'Provide {} values: {}'.format(  len(props),
                                                        ', '.join(props)
                                                        )
            assert len(args) == len(props), err_mess
            for arg, prop in zip(args, props):
                setattr(self, prop, arg)
            return True
        elif kwargs:
            for key in kwargs:
                assert key in kwargs, 'Provide a value for {}'.format(key)
                setattr(self, prop, key)
            return True
        return False
    
    def set_soil_ini_temp(self):
        """ Setting soil initial temperatures """
        ini_temp = (    self.ini_temp_soil
                        + ( self.H_unit
                            -self.ini_temp_ref_depth
                            )/2.*self.grad_gth
                        )
        self.t_Tinitsoil = np.ones(self.nbdt+1)*ini_temp
        
    def set_dim_properties(self, *args, **kwargs):
        """ Dimensions for single borehole """
        props = 'rb', 'Rb'
        try:
            props_were_set = self.checking_for_default_values(  props,
                                                                *args,
                                                                **kwargs
                                                                )
        except AssertionError as e:
            print(str(e))
            props_were_set = False
        if not props_were_set:
            # Default values
            self.rb = 0.08
            self.Rb = 0.06
            
    def set_dimensioning_params(self, *args, **kwargs):
        """ Setting parameters for dimensioning algorithm """
        props = (   'dim_unit', 'resolution_dim', 'dim_min', 'dim_max',
                    'H_unit'
                    )
        try:
            props_were_set = self.checking_for_default_values(  props,
                                                                *args,
                                                                **kwargs
                                                                )
        except AssertionError as e:
            print(str(e))
            props_were_set = False
        if not props_were_set:
            # Default values
            self.dim_unit = 'mètres forés (par sonde)'
            self.resolution_dim = len(self.field_points)
            self.dim_min = self.resolution_dim 
            self.dim_max = np.Inf
            self.H_unit = self.Pf(35,-3) / self.p_predim_SGV_sol
        self.dim = self.H_unit * len(self.field_points)
        
    def set_simulation_params(self, *args, **kwargs):
        """ Setting time parameters for simulation """
        props = 'dt', 'nyears', 'nbdt1y', 'nbdt', 'nbdtgeom'
        try:
            props_were_set = self.checking_for_default_values(  props,
                                                                *args,
                                                                **kwargs
                                                                )
        except AssertionError as e:
            print(str(e))
            props_were_set = False
        if not props_were_set:
            # Default values
            self.dt = 3600
            self.nyears = 25
            self.nbdt1y = 8760 # = 24*365 hours
            self.nbdt = int(self.nbdt1y*self.nyears)
            self.nbdtgeom = 50
        # NB tstar table must be so that
        # - G[0] = G(dt)
        # - G[-1] = G((nbdt+1)*dt)
        # - dG[-1] = G((nbdt+2)*dt) - G((nbdt+1)*dt)
        self.tstar = (  np.arange(1, self.nyears*self.nbdt1y+3) * self.dt
                        * self.diffusivity_soil/self.rb**2
                        )
        # Arrays must be resetted
        self.set_soil_ini_temp()
        
    def initialize_G_parameters(self):
        """ Parameters for short and long-term convolution functions
        
        Short-term: Bernier et al 2000
        Long-term: Maragna in GRETA WP5
        
        """
        self.a_B  = np.empty((4, 18))
        self.a_B[0] = (
                    766.424221673433, -2764.34845445812, -611.122474707132,
                    10975.7896352633, -43.6171378210021, 302.775545106501,
                    -23222.8379584065, 28.4621753131487, 6.66255638365812,
                    -65.7726816574567, 28588.6346976424, -14.0691451809219,
                    1.69062623135355, -1.08002450331321, 5.35883018984484,
                    -20560.8313379754, 8024.47240038740, -1313.20871139817
                    )
        self.a_B[1] = (
                    634.348447093481, -2228.56731336476, -520.842776551937,
                    8993.80295989717, -36.0793554401739, 257.383323992776,
                    -19223.3624759965, 29.6470357708811, 3.55400168302447,
                    -55.6204133046768, 23810.6663489641, -12.6137411418725,
                    0.906303478140495, -0.580234380210780, 4.50133886278077,
                    -17197.6172042741, 6732.85202054277, -1104.42027310515
                    )
        self.a_B[2] = (
                    299.666689741067, -1047.88569498495, -249.822072284212,
                    4297.06645533796, -19.3762865469942, 123.528026831922,
                    -9260.07789187051, 17.7449590412956, 1.22574023539798,
                    -26.6108441355207, 11520.8211253187, -6.49304305352856,
                    0.0960732358826631, -0.155167506118803, 2.14224386406506,
                    -8345.50738169485, 3274.00230264458, -537.850947556710
                    )
        self.a_B[3] = (
                    78.8959732086024, -275.663816097948, -66.6485441056920,
                    1151.97207741424, -6.84248570872393, 33.1202331897133,
                    -2500.30848350036, 6.02703997110152, 0.445452620454810,
                    -7.13538889381848, 3121.37903129927, -1.90820769416969,
                    -0.0945559293063914, -0.0281369032678862, 0.572818880136602,
                    -2266.03205903642, 890.356550924535, -146.433226034611 
                    )
        self.a_Gmax  = np.array((
                    -4.54591309793369, 6.11198369200399, 3.70812511774311,
                    -19.3729933279062, 4.74609661470291, -2.27799553464758,
                    19.7300967229399, -1.52530718028554, -1.16526522211288,
                    0.603217576010512, -7.08647749152244, 0.0345824664405554,
                    0.255809786216122, 0.0746364148232094, -0.0546491941978544,
                    -1.90912202614037, 2.24177260361469, -0.486451888485472
                    ))
        self.pows = np.array((
                    (0, 0), (1, 0), (0, 1),
                    (2, 0), (1, 1), (0, 2),
                    (3, 0), (2, 1), (1, 2),
                    (0, 3), (4, 0), (3, 1),
                    (2, 2), (1, 3), (0, 4),
                    (5, 0), (6, 0), (7, 0)
                    ))
        self.a_Gshort = np.array((
                    -0.89129,
                    0.36081,
                    -0.05508,
                    3.59617e-3
                    ))
            
    def G_shortterm(self, y, digitized=True):
        """ Short term function
            
        y: float/array, load factor for last time step
        digitized: bool, if True, uses a digitized version of function. Might be
                   less precise but way faster to compute
                   
        NB: short-term function deactivated for BHE field so far
        """
        return 0
        single_value = isinstance(y, (float, int))
        Hstar = self.H_unit / self.rb
        if not digitized:
            Fo = self.diffusivity_soil * y * self.dt / self.rb**2
            if single_value:
                return (self.Gfunc_FLS( Hstar, [Fo]) / self.lambda_soil)[0]
            return self.Gfunc_FLS( Hstar, Fo) / self.lambda_soil
        else:
            if self.t_G_shortterm is None:
                self.build_t_G_shortterm(Hstar)
            if single_value:
                y = int(y/self.charge_digitize_step)
                return self.t_G_shortterm[y]
            y = np.array(y/self.charge_digitize_step, dtype=int)
            return self.t_G_shortterm[y]
        # # Deprecated: infinite cylindrical heat source, see Bernier 2000
        # if y < 1e-6:
            # return 0.0
        # else:
            # Fo = self.diffusivity_soil * y * self.dt / self.rb**2
            # lgFo = np.log10(Fo)
            # f_poly = np.dot(self.a_Gshort, np.power(lgFo, (0, 1, 2, 3)))
            # return 10**f_poly / self.lambda_soil
          
    def Gfunc_FLS(self, Hstar, tstar):
        """ Auto-response function for Finite Line Source
        
        Hstar: float/array, adimensioned height of the BHE
        dstar: float/array, adimensioned distance between interacting BHE
        
        NB: as developped in Maragna and Loveridge 2019
        
        """
        
        omega = Hstar/2./np.sqrt(tstar)
        beta = 1./Hstar
        b2p1 = beta**2+1
        b2p4 = beta**2+4
        Da = (  np.sqrt(b2p1)*scispe_erfc(omega*np.sqrt(b2p1))
                - beta*scispe_erfc(omega*beta)
                - 1./omega/np.sqrt(np.pi)*( np.exp(-omega**2*b2p1)
                                            - np.exp(-omega**2*beta**2)
                                            )
                )
        Db = (  np.sqrt(b2p1)*scispe_erfc(omega*np.sqrt(b2p1))
                - 1./2*(beta*scispe_erfc(omega*beta)
                        + np.sqrt(b2p4)*scispe_erfc(omega*np.sqrt(b2p4))
                        )
                - 1./omega/np.sqrt(np.pi)*( np.exp(-omega**2*b2p1)
                                            - 1./2*(np.exp(-omega**2*beta**2)
                                                    + np.exp(-omega**2*b2p4)
                                                    )
                                            )
                )
        epsabs_matlab = 1.e-10
        epsrel_matlab = 1.e-6
        G_FLS = 1./(2*np.pi)*(  -Da
                                + np.array([
                                    sciint_quad(
                                        lambda z: ( scispe_erfc(om*z)
                                                    / np.sqrt(z**2-beta**2)
                                                    ),
                                        beta,
                                        np.sqrt(b2p1),
                                        epsabs = epsabs_matlab,
                                        epsrel=epsrel_matlab
                                    )[0]
                                    for om in omega
                                    ])
                                - Db
                                - np.array([
                                    sciint_quad(
                                        lambda z: ( scispe_erfc(om*z)
                                                    / np.sqrt(z**2-beta**2)
                                                    ),
                                        np.sqrt(b2p1),
                                        np.sqrt(b2p4),
                                        epsabs = epsabs_matlab,
                                        epsrel=epsrel_matlab
                                    )[0]
                                    for om in omega
                                    ])
                                )
        # Small integral values are badly estimated
        # => results bring up some biased negative values (negligible magnitude)
        G_FLS[G_FLS < 0] = 0.
        return G_FLS
            
    def Gfunc(self, Hstar, dstar, tstar, mult=False):
        """ Computing interaction functions for a BHE field
        
        Hstar: float/array, adimensioned height of the BHE
        dstar: float/array, adimensioned distance between interacting BHE
        tstar: float/array, adimensioned time
        mult: bool, putting broadcasting aside if True (see below)
        
        Returns array Gs storing interaction functions
        
        NB:
            o if Hstar, dstar and tstar are floats, return value Gs is a float.
              Use Gfunc_scal() instead, which is more efficient with floats.
            o broadcasting applies, so if Hstar, dstar and tstar are arrays,
              Gs will be of shape (len(Hstar), len(dstar), len(tstar)) where
              Gs[i,j,k] = Gfunc(Hstar[i], dstar[j], tstar[k]).
              All float/array combination is supported:
                 ___________________________________________
                | Hstar | dstar | tstar | Gs shape          |
                |_______|_______|_______|___________________|
                | array | array | float | len(H*) x len(d*) |
                |_______|_______|_______|___________________|
                | array | float | array | len(H*) x len(t*) |
                |_______|_______|_______|___________________|
                | float | array | array | len(d*) x len(t*) |
                |_______|_______|_______|___________________|
                | array | float | float | len(H*)           |
                |_______|_______|_______|___________________|
                | float | array | float | len(d*)           |
                |_______|_______|_______|___________________|
                | float | float | array | len(t*)           |
                |_______|_______|_______|___________________|
            o if mult is True, broadcasting is turned off. Hstar, dstar and tstar
              must be vectors of same length, and return value will be as
              Gs[i] = Gfunc(Hstar[i], dstar[i], tstar[i])
        
        """
        if mult:
            assert np.shape(Hstar) == np.shape(dstar) == np.shape(tstar)
        Y_ds = np.tile( 1./np.log10(dstar),
                        18
                        ).reshape((18, -1)) # 18 x len(dstar) array
        Y_hs = np.tile( np.log10(Hstar),
                        18
                        ).reshape((18, -1)) # 18 x len(Hstar) array
        for ar, ps in zip((Y_ds, Y_hs), self.pows.T):
            ar[ps==0] = 1.
            pow_on = ps > 1
            ar[pow_on] = np.power(  ar[pow_on],
                                    ps[pow_on, np.newaxis]
                                    )
        if mult:
            Ys = Y_hs*Y_ds # 18 x len(Hstar) array
        else:
            Ys = (  Y_hs[:,:,np.newaxis]
                    * Y_ds[:,np.newaxis,:]
                    ) # 18 x len(Hstar) x len(dstar) array
        nts = len(tstar) if np.shape(tstar) else 1
        Bns = np.tensordot(self.a_B, Ys, axes=1)
        Bns[:] = np.power(Bns, 2)
        Bns[[0, 2]] *= -1
        Gmax = np.tensordot(self.a_Gmax,
                            Ys,
                            axes=1
                            )**2 # len(Hstar) x len(dstar) array, len(Hstar) if mult
        log_ts = np.ones((4, nts))
        log_ts[1] = np.log10(tstar)
        log_ts[2] = np.power(np.log10(tstar), 2)
        log_ts[3] = log_ts[1]*log_ts[2] # faster than calling np.power(_, 3)
        if mult:
            Gs = 1 + np.tanh(np.sum(Bns*log_ts, axis=0)) # len(Hstar)
            Gs *= Gmax/2
        else:
            # NB profiling: tanh is costly
            Gs = 1 + np.tanh(np.tensordot(  np.moveaxis(Bns, 0, -1),
                                            log_ts, axes=1
                                            )
                                ) # len(Hstar) x len(dstar) x len(tstar) array
            Gs *= Gmax[:,:,np.newaxis]/2
            # Reducing dimensions if Hstar, dstar or tstar are floats
            # Simpler way of doing this?
            _, nhs, nds = Ys.shape
            if nts == 1:
                if nds == 1:
                    if nhs == 1:
                        Gs = Gs[0,0,0]
                    else:
                        Gs = np.reshape(Gs, nhs)
                else:
                    if nhs == 1:
                        Gs = np.reshape(Gs, nds)
                    else:
                        Gs = np.reshape(Gs, (nhs, nds))
            else:
                if nds == 1:
                    if nhs == 1:
                        Gs = np.reshape(Gs, nts)
                    else:
                        Gs = np.reshape(Gs, (nhs, nts))
                else:
                    if nhs == 1:
                        Gs = np.reshape(Gs, (nds, nts))
        return Gs
        
    def Gfunc_scal(self, Hstar, dstar, tstar):
        """ Computing interaction function for two Borehole Heat Exchangers
        
        Unvectorized form of Gfunc(), see full doc in there

        Hstar: float, adimensioned height of the BHE
        dstar: float, adimensionned distance between interacting BHE
        tstar: float, adimensioned time
        
        Returns Gfunc(Hstar, dstar, tstar)
        
        """
        Y_ds = np.power(1./np.log10(dstar), self.pows[:,0])
        Y_hs = np.power( np.log10(Hstar), self.pows[:,1])
        Ys = Y_ds*Y_hs
        Gmax = np.dot(self.a_Gmax, Ys)**2
        b = np.sum([aa*yy for aa,yy in zip(self.a_Gmax, Ys)])
        c = np.power(np.log10(tstar),np.arange(4))
        Bns = np.dot(self.a_B, Ys)
        Bns = Bns**2
        Bns[[0,2]]*=-1
        return Gmax/2*(1+np.tanh(np.sum(Bns*c)))
        
    def Gfunc_field(self, Hstar, tstar, field_points, *args, **kwargs):
        """ Computing global interaction func for a BHE field
        
        Hstar: float/array, adimensioned height of the BHE
        tstar: float/array, adimensioned time
        field_points: array, list of 2d coordinates of all BHE in field
        
        See Gfunc description for additional args and kwargs
        
        Returns array Gs of shape len(H*) x len(t*)
        
        """
        ds = scispa_pdist(field_points) # dist(X[i], Y[j]) for all i < j
        ds, count = np.unique(ds, return_counts=True)
        if len(ds) > 1:
            count = 2*np.tile(count, len(tstar)).reshape(len(tstar), -1).T
            Gs = self.Gfunc(Hstar, ds, tstar)
            Gs[:] *= count
            axis_to_sum_on = 1
            if isinstance(Hstar, float):
                axis_to_sum_on = 0
            return Gs.sum(axis=axis_to_sum_on)/len(field_points)
        else:
            return self.Gfunc(Hstar, ds, tstar)
        
    def build_t_dG_echangeur_gth(self, Hstar, ngeomsteps=None):
        """ Building long-term G function tables
        
        DEPRECATED, see build_t_G_echangeur_gth below
        
        Hstar: float/array, adimensioned height of the BHE
        ngeomsteps: int, total number of steps in the geomspace times table.
                    If not None, t_dG_echangeur_gth is not built on the complete
                    (linear) time table but on a (shorter) geomspace.
                    t_dG_echangeur_gth at each linear time is then retrieved by
                    linear interpolation.
        
        NB: for long term: dG[0] = G[0], dG[i] = G[i] - G[i-1] for i > 0
        
        """
        if not ngeomsteps is None:
            ts_geom = ( np.geomspace(   self.tstar[0]+1, self.tstar[-1]+1,
                                        num=ngeomsteps
                                        )
                        - 1
                        )
            if len(self.field_points) > 1:
                self.t_dG_echangeur_gth = np.interp(
                                        self.tstar, ts_geom,
                                        self.Gfunc_field(   Hstar, ts_geom,
                                                            self.field_points
                                                            )
                                        ) / self.lambda_soil
            else:
                self.t_dG_echangeur_gth = np.zeros_like(self.tstar)
            self.t_G_FLS = (    np.interp(
                                        self.tstar, ts_geom,
                                        self.Gfunc_FLS(Hstar, ts_geom)
                                        )
                                /self.lambda_soil
                                # + self.Rb # Rb already in Tb computation
                                )
        else:
            if len(self.field_points) > 1:
                self.t_dG_echangeur_gth = self.Gfunc_field( Hstar, self.tstar,
                                                            self.field_points
                                                            ) / self.lambda_soil
            else:
                self.t_dG_echangeur_gth = np.zeros_like(self.tstar)
            self.t_G_FLS = (self.Gfunc_FLS(Hstar, self.tstar)/self.lambda_soil
                            # + self.Rb # Rb already in Tb computation
                            )
        self.t_dG_echangeur_gth += self.t_G_FLS
        self.t_dG_echangeur_gth[1:] -= self.t_dG_echangeur_gth[:-1]
        
    def build_t_G_echangeur_gth(self, Hstar, ngeomsteps=None):
        """ Building long-term G function tables
        
        Hstar: float/array, adimensioned height of the BHE
        ngeomsteps: int, total number of steps in the geomspace times table.
                    If not None, t_G_echangeur_gth is not built on the complete
                    (linear) time table but on a (shorter) geomspace.
                    t_G_echangeur_gth at each linear time is then retrieved by
                    linear interpolation.
        
        NB:
        - G table starts at first timestep: G[0] = G(t=dt)
          and G[i] = G(t=(i+1)*dt)
        - dG[i] = G[i+1]-G[i] = G((i+2)*dt) - G((i+1)*dt)
        
        """
        if not ngeomsteps is None:
            ts_geom = np.geomspace( self.tstar[0], self.tstar[-1],
                                    num=ngeomsteps
                                    )
            if len(self.field_points) > 1:
                self.t_G_echangeur_gth = np.interp(
                                        self.tstar, ts_geom,
                                        self.Gfunc_field(   Hstar, ts_geom,
                                                            self.field_points
                                                            )
                                        ) / self.lambda_soil
            else:
                self.t_G_echangeur_gth = np.zeros_like(self.tstar)
            self.t_G_FLS = (    np.interp(
                                        self.tstar, ts_geom,
                                        self.Gfunc_FLS(Hstar, ts_geom)
                                        )
                                /self.lambda_soil
                                # + self.Rb # Rb already in Tb computation
                                )
        else:
            if len(self.field_points) > 1:
                self.t_G_echangeur_gth = self.Gfunc_field( Hstar, self.tstar,
                                                            self.field_points
                                                            ) / self.lambda_soil
            else:
                self.t_G_echangeur_gth = np.zeros_like(self.tstar)
            self.t_G_FLS = (self.Gfunc_FLS(Hstar, self.tstar)/self.lambda_soil
                            # + self.Rb # Rb already in Tb computation
                            )
        self.t_G_echangeur_gth += self.t_G_FLS
        self.t_dG_echangeur_gth = np.diff(self.t_G_echangeur_gth)[1:]
        self.t_G_echangeur_gth = self.t_G_echangeur_gth[:-1]
        
    def build_t_G_shortterm(self, Hstar, nchargesteps=200, ngeomsteps=None):
        """ Building short-term G function table
        
        Hstar: float, adimensioned height of the BHE
        charge_step: float, step for digitizing G_shortterm function into 
                     t_G_shortterm table.
        ngeomsteps: int, total number of steps in the geomspace charge table.
                    If not None, t_G_shortterm is not built on the full (linear)
                    charge table but on a (shorter) geomspace.
                    t_G_shortterm at each linear time is then retrieved by
                    linear interpolation.
        
        Notice that short-term function acts on last-step only and, for a given
        dimension, is a function of system charge only. t_G_shortterm[i] thus
        gives G_shortterm(charge), where i = int(charge/charge_step)
        
        """
        charge = np.linspace(0., 1., nchargesteps)
        self.charge_digitize_step = 1./(nchargesteps-1)
        tstar = self.diffusivity_soil * charge * self.dt / self.rb**2
        if not ngeomsteps is None:
            ts_geom = np.geomspace(tstar[0]+1, tstar[-1]+1, num=ngeomsteps) - 1
            
            self.t_G_shortterm = (  np.interp(
                                        tstar, ts_geom,
                                        self.Gfunc_FLS(Hstar, ts_geom)
                                        )
                                    /self.lambda_soil
                                    )
        else:
            self.t_G_shortterm = self.Gfunc_FLS(Hstar, tstar)/self.lambda_soil         
        
    # /!\ everything below is a hard implementation of dimensioning
    # So far, current implementation of BHE_field is poorly compatible with
    # dimensioning "calcul" subpackage. Future implementation will tend to
    # harmonize calcul subpackage and other heat exchangers with BHE_field
    # into a more Pythonic way (classes)
    
    def prebuild_for_run(self, data_in, use_aggregation=True):
        """ a brutal implementation of building data. To be optimized """
        def build_data(data_in):
            """ Building all useful data """
            from scipy.interpolate import RectBivariateSpline
            data = data_in.copy()
            heat_exch_type = 'BHE_field'
            ###########
            # Power needs
            data_in['sorted'] = False   # Never sort data for computations
            if data_in['t_behavior'] == 1:
                do_heat = data_in['do_heat']
                do_ECS = data_in['do_ECS']
                do_AC = data_in['do_AC']
                # forcing heat only
                # do_ECS = False
                # do_AC = False
                data['do_heat'] = True
                data['do_ECS'] = False # DHW forced off for BHE
                data['do_raf'] = False # Geocooling always turned off for BHE
                data['do_AC'] = do_AC
            elif data_in['t_behavior'] == 2:
                do_heat = True
                do_ECS = True
                do_AC = True
                data['do_heat'] = do_heat
                data['do_ECS'] = False
                data['do_raf'] = False
                data['do_AC'] = do_AC
            else:
                raise Exception("Problème dans le mode d'obtention choisi")
            use_preset = data_in['t_behavior'] ==2
            pow_data = bs.get_power(data_in)
            pow_data = pow_data[0] * 1000 # kW->W
            tab_chauf = np.array(pow_data[0])
            tab_ECS = np.array(pow_data[1])
            tab_AC = np.array(pow_data[2])
            tab_raf = None
            if do_heat:
                if do_ECS:
                    if do_AC:
                        # +Heat +ECS +cool
                        pass
                    else:
                        # +Heat +ECS -cool
                        tab_raf = None
                else:
                    tab_ECS = None
                    if do_AC:
                        # +Heat -ECS +cool
                        pass
                    else:
                        # +Heat -ECS -cool
                        tab_raf = None
            else:
                tab_chauf = None
                if do_ECS:
                    if do_AC:
                        # -Heat +ECS +cool
                        pass
                    else:
                        # -Heat +ECS -cool
                        tab_raf = None
                else:
                    tab_ECS = None
                    if do_AC:
                        # -Heat -ECS +cool
                        pass
                    else:
                        # -Heat -ECS -cool
                        raise Exception('At least one active function in: heating, HDW and cooling')
            energie_ECS = None # useless since HDW power set in tab_ECS
            ###########
            # Heat pump
            ref_hp = gv.DEFAULT_HEATPUMP
            hp_dict = ss.build_power_tables(data_in, ref_hp)
            debit_gth = data_in.get('debit_gth', hp_dict['debit_nominal'])
            puissance_reduit = hp_dict['puissance_reduit']
            T_coupure_PAC = min(hp_dict['T_in_evap'])
            T_coupure_PAC_ECS = T_coupure_PAC # Possibilité de modifier au besoin
            T_shutdown_HP_cool = np.inf
            Temission_minimum = hp_dict['T_emission_minimum']
            # # Forcing some data for comparing with Karl's case
            # iTevap0 = 0
            # iTcond35 = 1
            # alpha_Pc = data_in['hp_pc']/hp_dict['Pc'][iTcond35, iTevap0]
            # hp_dict['Pc'][:] = data_in['hp_pc']*1.e3
            # hp_dict['Pa'] = hp_dict['Pc']/data_in['hp_cop_ref']
            T_coupure_PAC = -np.inf
            # debit_gth = 30.7/3600
            # Temission_minimum = Temission_minimum*alpha_Pc
            # # Also, set load at 1. max in simul.update_puissance_mode_chauf:
            # t_charge[i] = min(t_charge[i], 1.)
            if hp_dict['T_in_evap'][-1] < 25:
                T_in_evap_cor = np.concatenate((hp_dict['T_in_evap'], np.array([25])))
                Pc_duplique = np.transpose(np.array([hp_dict['Pc'][:,-1]]))
                Pc_cor = np.concatenate((hp_dict['Pc'], Pc_duplique),axis=1)
                Pa_duplique = np.transpose(np.array([hp_dict['Pa'][:,-1]]))
                Pa_cor = np.concatenate((hp_dict['Pa'], Pa_duplique),axis=1)
            else:
                T_in_evap_cor = hp_dict['T_in_evap']
                Pc_cor = hp_dict['Pc']
                Pa_cor = hp_dict['Pa']
            Pc = RectBivariateSpline(hp_dict['T_out_condens'], T_in_evap_cor, Pc_cor)
            Pf = RectBivariateSpline(hp_dict['T_out_condens'], T_in_evap_cor, Pc_cor-Pa_cor)
            Pa = RectBivariateSpline(hp_dict['T_out_condens'], T_in_evap_cor, Pa_cor)
            data['PAC_T_out_condens'] = hp_dict['T_out_condens']
            data['PAC_T_in_evap'] = T_in_evap_cor
            data['PAC_Pf'] = Pc_cor-Pa_cor
            data['PAC_Pc'] = Pc_cor
            data['PAC_Pa'] = Pa_cor
            ###########
            # Backups
            data['COP_backup_AC'] = data_in['powextra_cop']
            ###########
            # Emitter
            emitter=data['emitter']
            use_custom_emitter = isinstance(emitter, dict) 
            if use_custom_emitter:
                emitter = load_models.load_emitter(**emitter)
            else:
                assert isinstance(emitter, str), 'Emitter must be str or dict'
                emitter = load_models.load_emitter(emitter)
            emitter_Texternal = emitter.get_external_temperatures()
            emitter_Temission = emitter.get_emission_temperatures()
            Text1 = emitter_Texternal[0]
            Text2 = emitter_Texternal[-1]
            Temission1 = emitter_Temission[0]
            Temission2 = emitter_Temission[-1]
            h_geocooling = 3.
            Tfbi_min = 18.
            def f_calc_Temission(
                Text, Tem_min=Temission_minimum,
                interp_func=emitter.get_emission_temperature
                ):
                Tem = interp_func(Text)
                return max(interp_func(Text), Tem_min)
            data['emitter_inlet_temp_cool'] =  data_in['emitter_tcool']
            data['emitter_temp_drop_cool'] =  gv.EMITTER_TEMP_DROP_COOL
            ###########
            # Ground
            use_custom_ground_th_props = data['t_prop_mode']==1
            if use_custom_ground_th_props:
                lambda_sol = data_in['ground_thermal_conductivity']
                rhocp_sol = data_in['ground_heat_capacity']*1.e6 # MJ->J
            else:
                ground = load_models.load_ground(data['ground_type'])
                lambda_sol = ground.get_thermal_conductivity()
                rhocp_sol = ground.get_volumetric_heat_capacity()
            diffusivite_sol = lambda_sol/rhocp_sol
            p_predim_SGV_sol = 50. # W/m. Value seems useless?
            data['Tmoy_init_sol'] = data_in['ground_tini']
            if (data_in['t_ini_mode'] == 1
                and data_in['ground_t_depth_update']
                ):
                    data['grad_gth'] = data_in['ground_gth_grad']
                    data['Prof_sonde_test'] = data_in['ground_sample_bh_depth']
            else:
                data['grad_gth'] = 0.
                data['Prof_sonde_test'] = 0.
            ###########
            # External temperature
            if data_in['t_behavior'] == 1:
                Text = data_in.get('external_temp_custom')
                assert not Text is None, err_mess.get_message(
                                                        'external_temp_missing'
                                                        )
                assert len(Text) == 8760, err_mess.get_message(
                                                        'external_temp_nvals'
                                                        )
            elif data_in['t_behavior'] == 2:
                location = data_in['location']
                if isinstance(location, int):
                    names = list(gv.LOCATIONS.values())
                    city_name = names[location]
                else:
                    city_name = location
                Text = load_models.load_outside(city_name).get_temperatures()
            heat_exch_type = 'BHE_field'
            ###########
            # Building data
            Gbat = None
            hspbat = None
            Sbat = 150.
            GSh = None
            chargesinternes = None
            # if do_raf:
                # T_cons_froid = params_base.T_cons_froid
                # a_froid, b_froid, r2_froid = regression_lineaire(   Pbat_froid,
                                                                    # Text
                                                                    # )
                # GSh = a_froid
                # chargesinternes = a_froid*T_cons_froid+b_froid
            ###########
            # Supporting power
            E_appoint = 0.
            P_appoint = 0.
            tab_appoint = np.zeros(8760)
            if do_heat:
                Pmax_PAC = np.max(tab_chauf)
            else:
                Pmax_PAC = 0.
                
            # Aggregation    
            data['use_aggregation'] = use_aggregation
            data['n_steps_agg'] = 2880 # hours in an aggregated block (120 days)
            data['n_steps_imm'] = 2880 # min hours in short-term convol (120 days)
            
            data['Text'] = Text
            data['tab_chauf'] = tab_chauf
            data['tab_ECS'] = tab_ECS
            data['tab_raf'] = tab_raf
            data['tab_AC'] = tab_AC
            data['debit_gth'] = debit_gth
            data['puissance_reduit'] = puissance_reduit
            data['T_coupure_PAC'] = T_coupure_PAC
            data['T_coupure_PAC_ECS'] = T_coupure_PAC_ECS
            data['HP_shutdown_temp_cool'] = T_shutdown_HP_cool
            data['Temission_minimum'] = Temission_minimum
            data['Temission1'] = Temission1
            data['Temission2'] = Temission2
            data['Text1'] = Text1
            data['Text2'] = Text2
            data['emitter_Texternal'] = emitter_Texternal
            data['emitter_Temission'] = emitter_Temission
            data['h_geocooling'] = h_geocooling
            data['Tfbi_min'] = Tfbi_min
            data['f_calc_Temission'] = f_calc_Temission
            data['Pc'] = Pc
            data['Pf'] = Pf
            data['Pa'] = Pa
            data['lambda_sol'] = lambda_sol
            data['rhocp_sol'] = rhocp_sol
            data['diffusivite_sol'] = diffusivite_sol
            data['p_predim_SGV_sol'] = p_predim_SGV_sol
            data['energie_ECS'] = energie_ECS
            
            data['E_appoint'] = E_appoint
            data['P_appoint'] = P_appoint
            data['tab_appoint'] = tab_appoint
            data['Pmax_tot'] = Pmax_PAC + P_appoint
            data['heat_exchanger'] = heat_exch_type
            
            # data['scop_dim'] = data_in['scop_dim']
            
            data['Gbat'] = Gbat
            data['Sbat'] = Sbat
            data['hspbat'] = hspbat
            data['GSh'] = GSh
            data['chargesinternes'] = chargesinternes
            
            return data
            
        def build_exch(data_in):
            # Single borehole properties
            Rb = data_in['bhe_Rb'] # resistance
            rb = data_in.get('bhe_rb', 0.07) # radius, set as global
            self.set_dim_properties(    rb,
                                        Rb
                                        )
            # Various properties
            self.set_properties_from_input(data_in)
            # Dimensioning params
            dim_unit = 'mètres forés (par sonde)'
            resolution_dim = 1.
            dim_min = resolution_dim 
            dim_max = np.Inf
            H_unit = data_in['bhe_depth']
            dim = H_unit*len(self.field_points) # total length of exchange
            self.set_dimensioning_params(   dim_unit,
                                            resolution_dim,
                                            dim_min,
                                            dim_max,
                                            H_unit
                                            )
            # Simulation params + also calls heat_exch.set_soil_ini_temp()        
            dt = 3600
            nyears = data_in.get('nyears', 25)
            nbdt1y = 8760 # = 24*365 hours
            nbdt = int(nbdt1y*nyears)
            nbdtgeom = 50
            self.set_simulation_params( dt,
                                        nyears,
                                        nbdt1y,
                                        nbdt,
                                        nbdtgeom
                                        )
            Hstar = H_unit / rb
            # self.build_t_dG_echangeur_gth(Hstar, nbdtgeom)
            self.build_t_G_echangeur_gth(Hstar, nbdtgeom)
            dict_exch = {}
            dict_exch['Rb'] = Rb
            dict_exch['t_dG_echangeur_gth'] = self.t_dG_echangeur_gth
            dict_exch['t_G_echangeur_gth'] = self.t_G_echangeur_gth
            dict_exch['dim_min'] = dim_min
            dict_exch['dim_max'] = dim_max
            dict_exch['dim'] = dim
            dict_exch['unite_dim'] = dim_unit
            dict_exch['t_Tinitsol'] = self.t_Tinitsoil
            dict_exch['G_courtterme'] = self.G_shortterm
            dict_exch['resolution_dim'] = resolution_dim
            dict_exch['nb_annees'] = nyears
            dict_exch['nbdt'] = nbdt
            return dict_exch
            
        data = build_data(data_in)
        ech_gth = build_exch(data)
        Tflee_func = calcul.dimensionnement.build_Tflinev_func(data, ech_gth)
        Tfloutcond_func = calcul.dimensionnement.build_Tfloutcond_func( data,
                                                                        ech_gth
                                                                        )
        self.data = data
        self.ech_gth = ech_gth
        self.Tflee_func = Tflee_func
        self.Tfloutcond_func = Tfloutcond_func
        
    def direct_run(
        self, data_in, use_aggregation=True, print_messages=False, **kwargs
        ):
        """ a brutal implementation of a direct run. To be optimized. """
        self.prebuild_for_run(data_in, use_aggregation)
        return calcul.dimensionnement.calcul_cop(
            self.data,
            self.ech_gth,
            self.dim,
            self.Tflee_func,
            self.Tfloutcond_func,
            cython_data = None,
            print_messages = print_messages
            )
                                                
    # def dimensioning_run(   self, data_in, target_cop, use_aggregation=True,
                            # print_messages=False
                            # ):
        # """ a brutal implementation of a dimensioning run. To be optimized.
        
        # NO: convolution G fucntions depend on dim: need to rebuild at each step
        # See below
        
        # """
        # # self.prebuild_for_run(data_in, use_aggregation)
        # # self.data['scop_dim'] = target_cop
        # # return calcul.dimensionnement.calcul_dimensionnement(
                                                # # self.data,
                                                # # self.ech_gth,
                                                # # print_messages=print_messages
                                                # # )
                                                
    def dimensioning_run(   self, data_in, target_cop, use_aggregation=True,
                            print_messages=False
                            ):
        """ a brutal implementation of a dimensioning run. To be optimized. """
        self.prebuild_for_run(data_in, use_aggregation)

        dim = 100.#self.ech_gth['dim']
        dim_min = self.ech_gth['dim_min']
        dim_max = self.ech_gth['dim_max']
        resolution_dim = self.ech_gth['resolution_dim']
        unite_dim = self.ech_gth['unite_dim']
        scop_dim = target_cop

        # use_cython = False
        # if use_cython:
            # Tflee = None
            # cython_data = prepare_cython_loop(self.data, self.ech_gth)
        # else:
            # Tflee = self.build_Tflinev_func(self.data, self.ech_gth)
            # cython_data = None
        
        if print_messages:
            print("Début des calculs")
    
        COP_meilleur = [np.inf]*self.ech_gth['nb_annees']
        COP_prev = -np.inf
        COP_historique = []
        E_meilleur = []
        tabs_meilleur = []
        epsilon_COP = 1.e-3
        epsilon_dim = 1.e-6
        infos_message = [None, None, None]
        ### Comment traiter le cas "dim_min (initial) suffit" ?
        while ((dim_max-dim_min) > resolution_dim):


            if print_messages:
                print()
                print("dim_min=%.0f ; dim_max=%.0f" % (dim_min, dim_max))
                print("Essai avec %d %s" % (dim, unite_dim))
         

            try:
                data_dim = data_in.copy()
                data_dim['bhe_depth'] = dim
                (   COP_annuel, E_prod, E_cons, nb_h_inconfort, tabs, _
                    )= self.direct_run(data_dim, use_aggregation, print_messages)
                if COP_historique:
                    COP_prev = COP_historique[-1][-1]
                # Il arrive que le système atteigne son COP maximal, qui est < scop_dim.
                # Le COP ne progresse plus et pour éviter de partir en boucle infinie,
                # on quitte la boucle.
                # A noter que si dim ne progresse plus (dim_step~0 ci-dessous), le COP
                # ne progresse plus non plus. Ce cas aura été détecté à l'itération
                # précédente (cf ci-dessous).
                COP_step = COP_annuel[-1] - COP_prev
                if abs(COP_step) < epsilon_COP*COP_annuel[-1]:
                    COP_meilleur = COP_annuel
                    E_meilleur = (E_prod, E_cons)
                    tabs_meilleur = tabs
                    infos_message[:2] = nb_h_inconfort
                    infos_message[2] = ('ATTENTION : le système a atteint son COP'
                                ' maximal et ne peut satisfaire la valeur cible.'
                                )
                    break
                COP_historique.append(COP_annuel)
                surdim = (COP_annuel[-1] >= scop_dim)
            except calcul.dimensionnement.SousDimensionnementError as e:
                surdim = False
                if print_messages:
                    print(e)
     
            if surdim == False:
                dim_min = dim
            else:
                dim_max = dim
                COP_meilleur = COP_annuel
                E_meilleur = (E_prod, E_cons)
                tabs_meilleur = tabs
                infos_message[:2] = nb_h_inconfort
            
            if dim_max == np.Inf:
                dim = 2 * dim_min
            else:
                dim_step = resolution_dim * int(    np.ceil(float(dim_min + dim_max)
                                                    / (2 * resolution_dim))
                                                    ) - dim
                if abs(dim_step) < epsilon_dim*dim:
                    # Dans certains cas, dim ne progresse plus (dim_step~0) et
                    # l'algo part en boucle infinie.
                    break
                dim += dim_step
                    
        # # Au besoin : pour exporter les tables correspondant au COP optimal
        # save_tabs(tabs_meilleur, data)
                
        return (dim, COP_meilleur, unite_dim, COP_historique, E_meilleur,
                infos_message, tabs_meilleur
                )
    
def set_field_layout(data_in):
    """ Returns coordinates of BHE field positions
    
    data_in: dict, data for building the positions
        
    """
    keys = dict(
        layout_input_mode='layout_input_mode',
        preset_layout='preset_layout',
        dist='bhe_dist',
        nb_x='bhe_nb_x',
        nb_y='bhe_nb_y',
        nb_rows='bhe_nb_rows',
        field_points='bhe_layout_points'
        )
    kwargs = dict((kwarg, data_in.get(key, None)) for kwarg,key in keys.items())
    return dp.get_field_points(**kwargs)