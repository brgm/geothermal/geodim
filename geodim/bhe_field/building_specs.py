#coding: utf8

""" BHE submodule for dealing with building specs inputs

Corresponding webpage fieldset is "Caractéristiques du bâtiment"

Functions pointing to data_processing -> keep namespace/fieldset correspondance

"""

from . import data_processing as dp
from . import global_variables as gv

def get_power(data_in):
    """
    Computing thermal needs over a year.
    
    Heating and/or DHW and/or cooling can be considered as well.
    Thermal needs can be computed or given by user.
    
    Parameters
    ----------
    data_in: dict
        Data provided for computing power needs
        
        Warning
        -------
        All input and output powers must be in kW and MWh/y
    
    Returns
    -------
    out1: np.ndarray
        1st row gives hourly heat needs over a year [W]
        2nd row gives hourly domestic hot water needs over a year [W]
        3rd row gives hourly AC needs over a year [W]
    out2: list
        Total power needs over the year for heat, DHW and AC (resp.) [MWh/y]
    out3: list
        Max power needs over the year for heat, DHW and AC (resp.) [kW]
    out4: list
        Max scaled power needs over the year for heat, DHW and AC (resp.) [kW]
    
    """
    t_behavior = data_in['t_behavior']
    powers = None
    powers_tot = None
    powers_max = None
    powers_max_scaled = None
    do_heat = ( 'pbat_chaud_total_custom' in data_in
                and not data_in['pbat_chaud_total_custom'] is None
                )
        
    do_ECS = (  'eecs_custom' in data_in
                and not data_in['eecs_custom'] is None
                )
        
    do_raf = (  'pbat_froid_custom' in data_in
                and not data_in['pbat_froid_custom'] is None
                )
    do_AC = do_raf
    pbat_chaud_total_custom = None
    eecs_custom = None
    pbat_froid_custom = None
    city_index = None
    type_index = None
    if t_behavior == 1:
        if do_heat:
            pbat_chaud_total_custom = data_in['pbat_chaud_total_custom']
        if do_ECS:
            eecs_custom = data_in['eecs_custom']
        if do_raf:
            pbat_froid_custom = data_in['pbat_froid_custom']
    elif t_behavior == 2:
        city_index = data_in['location']
        type_index = data_in['type']
        scaling = False
        # MWh/y -> Wh/y
        if do_heat:
            pbat_chaud_total_custom = data_in['pbat_chaud_total_custom']*1.e6
            scaling = True
        if do_ECS:
            eecs_custom = data_in['eecs_custom']*1.e6
            scaling = True
        if do_raf:
            pbat_froid_custom = data_in['pbat_froid_custom']*1.e6
            scaling = True
        do_heat = True
        do_ECS = True
        do_raf = True 
        do_AC = True 
    numerical_zero = 1.e-15
    powers = dp.get_power(  t_behavior, do_heat, do_ECS, do_raf, do_AC,
                            city_index,  type_index, pbat_chaud_total_custom,
                            eecs_custom, pbat_froid_custom,
                            )/1.e3 # W -> kW
    pows_tot = [sum(ys)/1.e3 for ys in powers] # MWh/y
    pows_max = [max(ys) for ys in powers] # kW
    if t_behavior == 2:
        if scaling:
            powers_max_scaled = pows_max
        else:
            powers_tot = pows_tot
            powers_max = pows_max
            
    return powers, powers_tot, powers_max, powers_max_scaled
