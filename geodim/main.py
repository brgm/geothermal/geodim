"""
Running the direct and dimensioning computations

"""

from . import donnees
from . import echangeur_gth
from . import calcul
from . import interface
import time

def compute_cli():
    raise NotImplementedError('CLI not implemented yet')
    brut_data = interface.cli()
    compute(brut_data)

def compute(
    brut_data, besoins_thermiques_custom={}, use_aggregation=False,
    print_messages=False, dim=None, return_all=False, **kwargs
    ):
    """
    Running a simulation for a single house model
    
    Parameters
    ----------
    brut_data: dict
        The model inputs (see ``donnees.traitement`` function).
    besoins_thermiques_custom: dict
        The custom heat, DHW (optional) and cooling (optional) power needs.
        Needs must be given as hourly needs over a year (8760 values), in [W]
        (see ``donnees.traitement`` function)
    use_aggregation: bool
        Whether aggregation should be used for updating the ground temperature
        at borehole wall
    print_messages: bool
        Whether the run should print progression info.
    dim: int/float
        The heat exchanger dimension. If a value is provided, a durect run is
        launched with that value.
        
        If None, a dimensioning run is launched and the model searches for the
        best dimension that fits the expectations.
    return_all: bool
        If True, additional results are returned by the function, see returns
        below
        
    Returns
    -------
    out1: dict
        Computation result, see format in ``calcul.dimensionnement.calcul_cop``
    out2: models.ground_heat_exchangers.GroundHE (optional)
        The heat exchanger used
    out3: dict (optional)
        The input data once transformed by processing stage
    
    """
    t0 = time.time()
    data = donnees.traitement(
        brut_data, besoins_thermiques_custom, use_aggregation,
        )
    t1 = time.time()
    ech_gth = echangeur_gth.build(data)
    t2 = time.time()
    if dim is None:
        result = calcul.calcul_dimensionnement(data, ech_gth, print_messages)
    else:
        Tflee_func = calcul.dimensionnement.build_Tflinev_func(data, ech_gth)
        Tfloutcond_func = calcul.dimensionnement.build_Tfloutcond_func( data,
                                                                        ech_gth
                                                                        )
        result = calcul.dimensionnement.calcul_cop(
                                                data, ech_gth, dim, Tflee_func,
                                                Tfloutcond_func,
                                                print_messages=False,
                                                cython_data=None
                                                )
    t3 = time.time()
    if print_messages:
        print('Process time: {} s'.format(t1-t0))
        print('Build time: {} s'.format(t2-t1))
        print('Loop time: {} s'.format(t3-t2))
    if return_all:
        return result, ech_gth, data
    return result