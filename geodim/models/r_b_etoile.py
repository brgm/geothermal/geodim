"""
Advanced borehole thermal resistance 

References
----------
Johan Claesson & Saqib Javed (2019) 
https://doi.org/10.1080/23744731.2019.1620565
"""

from math import *
import numpy as np

# constantes du systèmes
# constantes pour calcul du facteur de résistance thermique Rv
H=200
Vf=0.00020833333333333 #conversion en m3/s 
cf=4180
rho_f=997
R_v = H / (rho_f * cf * Vf)

# constantes pour calcul du local borehole thermal resistance between fluid in
# the U-pipe and the borehole wall, Rb en K/(W/m)
R_p=0.05
lamb_b = 1.5
lamb = 3
rb=57.50*pow(10,-3)
rp=16*pow(10,-3)
# ces constantes sont nécessaires pour le calcul de Ra_a : internal borehole
# thermal resistance for adjacent inlet pipes, Ra_a en K/(W/m)

# calculs intermédiaires d'autres constantes 
beta = 2 * pi * lamb_b * R_p 
sigma = (lamb_b - lamb)/(lamb_b + lamb)
b_1 = (1 - beta)/(1 + beta)

# puisque la valeur de rc change selon les cas, on va créer les fonctions 
# permettant de calculer les constantes dépendant de rc 

def p_pc (rp, rc) : 
    """ permet de calculer un paramètre sans dimension (cf équation 15 du papier)
    
    Parameters  
    ----------  
    rp: float,  
        outer radius of the pipe making up the U-pipe, m
    rc: float,  
         radial distance between centers of symmetrically placed pipes and the borehole center, m
   
    Returns  
    -------  
    out: float   
        paramètre sans dimension ppc 
    """
    return pow(rp, 2)/(4*pow(rc, 2))

def p_c (rc, rb) : 
    """ permet de calculer un paramètre sans dimension (cf équation 15 du papier)
    
    Parameters  
    ----------  
    rb: float,  
        radius of the borehole, m
    rc: float,  
         radial distance between centers of symmetrically placed pipes and the borehole center, m
   
    Returns  
    -------  
    out: float   
        paramètre sans dimension pc   
    """
    return pow(rc, 2)/pow(rb**8 - rc**8, 1/4)
    
def p_b (rc, rb) : 
    """ permet de calculer un paramètre sans dimension (cf équation 15 du papier)
    
    Parameters  
    ----------  
    rb: float,  
        radius of the borehole, m
    rc: float,  
         radial distance between centers of symmetrically placed pipes and the borehole center, m
   
    Returns  
    -------  
    out: float   
        paramètre sans dimension pb  
    """
    return pow(rb, 2)/pow(rb**8 - rc**8, 1/4)
    
# ici, on crée une fonction récursive pour calculer Rb pour différentes
# valeurs de J (J définit donc l'ordre de récurrence, relation de récurrence
# valable pour J=0 et J=1 seulement)

def R_b (J, rc, R_p, lamb_b, rb, rp, sigma) :
    """ permet de calculer Rb
    
    Parameters  
    ----------  
    rb: float,  
        radius of the borehole, m
    rc: float,  
        radial distance between centers of symmetrically placed pipes and the borehole center, m 
    J: int,
        number of multipoles, J=0, 1, and 10 for zeroth-, first-, and 10th-order calculation, respectively, dimensionless
R_p: float,
        total fluid-to-pipe resistance for a single pipe, i.e., one leg of the U-pipe, K/(W/m)
    lamb_b: float,
        thermal conductivity of the borehole/grout, W/(m-K)
    rp: float,  
        outer radius of the pipe making up the U-pipe, m
    sigma: float,
        thermal conductivity ratio, dimensionless
    
    Returns  
    -------  
    out: float   
        local borehole thermal resistance between fluid in the U-pipe and the borehole wall, K/(W/m)
    """
    R_b_0 = R_p/4 + (1/(8*pi*lamb_b))*(np.log(rb**4/(4*rp*(rc**3))) + sigma * np.log(rb**8/(rb**8-rc**8)))
    R_b_rec = R_b_0
    if J>0 :
        R_b_rec -= (1/(8*pi*lamb_b)) * (b_1 * p_pc(rp, rc)*pow(3-8*sigma*pow(p_c(rc,rb),4),2))/(1+b_1*p_pc(rp, rc)*(5+64*sigma*pow(p_c(rc,rb),4)*pow(p_b(rc,rb),4)))
    return R_b_rec
    
# maintenant, pour pouvoir calculer R_a_a pour le cas J=1, on va devoir 
# calculer les termes de la matrice M et du vecteur V
    
def R_a_a (J, rc, R_p, lamb_b, rb, rp, sigma, b_1) :
    """ permet de calculer Ra^a
    
    Parameters  
    ----------  
    rb: float,  
        radius of the borehole, m
    rc: float,  
        radial distance between centers of symmetrically placed pipes and the borehole center, m 
    J: int,
        number of multipoles, J=0, 1, and 10 for zeroth-, first-, and 10th-order calculation, respectively, dimensionless
R_p: float,
        total fluid-to-pipe resistance for a single pipe, i.e., one leg of the U-pipe, K/(W/m)
    lamb_b: float,
        thermal conductivity of the borehole/grout, W/(m-K)
    rp: float,  
        outer radius of the pipe making up the U-pipe, m
    sigma: float,
        thermal conductivity ratio, dimensionless
    b_1: float,
        dimensionless parameter, see Equation 15
    Returns  
    -------  
    out: float   
        internal borehole thermal resistance for adjacent inlet pipes, K/(W/m)
    """
    #création de la matrice M 
    M_1_1 = 1 + 16*b_1*sigma*p_pc(rp,rc)*(3*pow(p_c(rc,rb),3)*pow(p_b(rc,rb),5)+pow(p_c(rc,rb),7)*p_b(rc,rb))
    M_2_1 = b_1 * p_pc(rp, rc)
    M_2_2 = -1 - 16*b_1*sigma*p_pc(rp,rc)*(3*pow(p_c(rc,rb),5)*pow(p_b(rc,rb),3)+pow(p_b(rc,rb),7)*p_c(rc,rb))
    #M = np.array([M_1_1, -M_2_1], [M_2_1, M_2_2])
    
    #création du vecteur V 
    V_1 = 1 - 8*sigma*pow(p_c(rc,rb),3)*p_b(rc,rb)
    V_2 = 3 + 8*sigma*pow(p_b(rc,rb),3)*p_c(rc,rb)
    #V = [V_1, V_2]
    
    #calcul de R_a_a
    R_a_0 = 2*R_p + (2/(2*pi*lamb_b))*(np.log(2*rc/rp) + sigma * np.log((rb**2 + rc**2)/(rb**2-rc**2)))
    R_a_rec = R_a_0
    if J>0 :
        # R_a_rec_faux += (2/(2*pi*lamb_b)) * (b_1 * p_pc(rp, rc)/2)*(pow(V[2],2)*M[1][1]-2*V[1]*V[2]*M[2][1]-pow(V[1],2)*M[2][2])/(M[1][1]*M[2][2]+pow(M[2][1],2))
        R_a_rec += (2/(2*pi*lamb_b)) * (b_1 * p_pc(rp, rc)/2)*(pow(V_2,2)*M_1_1-2*V_1*V_2*M_2_1-pow(V_1,2)*M_2_2)/(M_1_1*M_2_2+pow(M_2_1,2))
    return R_a_rec

def R_b_etoile(J, rc, R_p=R_p, lamb_b=lamb_b, rb=rb, rp=rp, sigma=sigma, b_1=b_1) :
    """ permet de calculer Rb*
    
    Parameters  
    ----------  
    rb: float,  
        radius of the borehole, m
    rc: float,  
        radial distance between centers of symmetrically placed pipes and the borehole center, m 
    J: int,
        number of multipoles, J=0, 1, and 10 for zeroth-, first-, and 10th-order calculation, respectively, dimensionless
R_p: float,
        total fluid-to-pipe resistance for a single pipe, i.e., one leg of the U-pipe, K/(W/m)
    lamb_b: float,
        thermal conductivity of the borehole/grout, W/(m-K)
    rp: float,  
        outer radius of the pipe making up the U-pipe, m
    sigma: float,
        thermal conductivity ratio, dimensionless
    b_1: float,
        dimensionless parameter, see Equation 15
        
    Returns  
    -------  
    out: float   
        effective borehole thermal resistance, K/(W/m)
    """
    return (R_b(J, rc, R_p, lamb_b, rb, rp, sigma) + (R_v**2)/(6*R_a_a(J, rc, R_p, lamb_b, rb, rp, sigma, b_1)))


def test_calcul_Rb_etoile():
    """ Ici on teste d'abord si les valeurs en J=0 concordent puis ensuite en J=1 """    
    #J=0
    test_values = [0.02263, 0.02811, 0.04150]
    ref_results_0 = [0.11348,0.09320,0.05899]
    test_results_0 = [R_b_etoile(0, val, R_p, lamb_b, rb, rp, sigma, b_1) for val in test_values]
    assert np.allclose(ref_results_0, test_results_0, pow(10,-5), atol=np.inf)
    #J=1
    ref_results_1 = [0.10889,0.08887, 0.05590]
    test_results_1 = [R_b_etoile(1, val, R_p, lamb_b, rb, rp, sigma, b_1) for val in test_values]
    assert np.allclose(ref_results_1, test_results_1, pow(10,-5), atol=np.inf)
    
if __name__ == "__main__" :
    # test
    rc = 0.02263
    # rc = 0.02811
    # rc = 0.04150
    p_pc(rp, rc)
    p_b(rc, rb)
    p_c(rc, rb)
    print(R_b (0, rc, R_p, lamb_b, rb, rp, sigma))
    print(R_b (1, rc, R_p, lamb_b, rb, rp, sigma))
    print(R_a_a (0, rc, R_p, lamb_b, rb, rp, sigma, b_1))
    print(R_a_a (1, rc, R_p, lamb_b, rb, rp, sigma, b_1))
    print(R_b_etoile (0, rc, R_p, lamb_b, rb, rp, sigma, b_1))
    # print(R_b_etoile (1, rc, R_p, lamb_b, rb, rp, sigma, b_1))
