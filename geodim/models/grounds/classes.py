""" Classes for ground models """
from dataclasses import dataclass
import pathlib

from ..load_yaml import load_yaml
    
CWD = pathlib.Path(__file__).parent
SOURCE_DIR = CWD/'data'
    
@dataclass
class Ground:
    """
    Class holding grounds
        
    Parameters
    ----------
    name: str
        Name for ground
    thermal_conductivity: float
        Thermal conductivity of soil [W/m/K]
    volumetric_heat_capacity: float
        Thermal conductivity of soil [J/m3/K]
    p_predim_SGV: float
        First guess for heat power per unit length of vertical heat
        exchanger [W/m]
    p_predim_mSGV: float
        First guess for heat power per unit length of microvertical heat
        exchanger [W/m]
    
    """
    name: str
    thermal_conductivity: float = None
    volumetric_heat_capacity: float = None
    p_predim_SGV: float = None
    p_predim_mSGV: float = None
    
    @property
    def thermal_diffusivity(self):
        """ Returning thermal diffusivity """
        return self.thermal_conductivity / self.volumetric_heat_capacity
        
    def get_name(self):
        """ Returning name of ground as a str """
        return self.name
        
    def get_thermal_diffusivity(self):
        """ Returning thermal diffusivity """
        return self.thermal_diffusivity
        
    def get_thermal_conductivity(self):
        """ Returning thermal conductivity """
        return self.thermal_conductivity
        
    def get_volumetric_heat_capacity(self):
        """ Returning volumetric heat capacity """
        return self.volumetric_heat_capacity
        
    def get_p_predim_SGV(self):
        """ Returning first guess for heat per unit length of vert. HE """
        return self.p_predim_SGV
        
    def get_p_predim_mSGV(self):
        """ Returning first guess for heat per unit length of microvert. HE """
        return self.p_predim_mSGV
        
    def get_lambda(self):
        """ An alias for get_thermal_conductivity """
        return self.get_thermal_conductivity()
        
    def get_rhocp(self):
        """ An alias for get_volumetric_heat_capacity """
        return self.get_volumetric_heat_capacity()

    @classmethod
    def from_name(cls, name):
        """ Constructing from default models """
        fname = SOURCE_DIR/f'{name}.yml'
        assert fname.is_file(), f'Ground model "{fname}" not found'
        data = load_yaml(fname)
        name = data['name']
        lambd = data.get('lambda')
        rhocp = data.get('rhocp')
        p_predim_SGV = data.get('p_predim_SGV')
        p_predim_mSGV = data.get('p_predim_mSGV')
        return cls(name, lambd, rhocp, p_predim_SGV, p_predim_mSGV)