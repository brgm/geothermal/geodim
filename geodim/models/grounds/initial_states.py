""" Computing initial ground temperatures """
import math
import numpy as np

def temp_average_at_depth(external_temp, grad_gth, depth):
    """ Temperature depending on geothermal gradient (averaged on a given period
    
    Parameters
    ----------
    external_temp: np.ndarray/float,
        The external temperature at ground surface.
    grad_gth: float,
        The geothermal gradient [°C/m].
    depth: float,
        The depth where to compute ground temperature [m]
        
    Returns
    -------
    out: float/np.ndarray,
        The averaged temperature at given depth.
        np.ndarray of floats if external_temp is a container.
    
    """
    multiple_values = hasattr(external_temp, __len__)
    average_temp = np.average(external_temp) + grad_gth * depth
    if multiple_values:
        nbdt = len(multiple_values)
        return np.repeat(average_temp, nbdt)
    return average_temp
    
def temp_integrated_along_height(Text, alpha, ztop, height=None):
    """ Temperature integrated along given height
    
    The model is based on the 1D-diffusion PDE along vertical direction below
    ground surface (z<0, =-depth) and assumes a semi-infinite medium with a
    periodical temperature on ground surface (= boundary condition).
    The solution is computed at a fixed elevation z=ztop, or is integrated along
    an interval [ztop-height;ztop].
    
    
    Parameters
    ----------
    Text: np.ndarray,
        Hourly external temperature over a year [°C].
    alpha: float,
        Ground thermal diffusivity in [m2/s].
    ztop: float,
        Elevation from ground surface (<0) (= -depth) in [m].
    height: float,
        Height along which to compute the temperature.
        If None or 0, the temperature is computed at ztop.
        Otherwise, temperature is computed through an integral along
        [ztop-height;ztop].
    
    Returns
    -------
    out: np.ndarray,
        The hourly temperature at given depth.
    
    References
    ----------
    Xavier Moch (2021), Températures souterraines : note mathématique, in French
    Add link as soon as document available online.
    
    """
    nbdt = 365*24
    is_hourly_data = len(Text)==nbdt
    assert is_hourly_data, 'Need hourly external temperatures during one year'
    is_flat_ground_he = (
        height is None
        or height==0
        )
    nbdt = nbdt
    omega = 2*math.pi/(nbdt*3600) # Pulsation frequency [s^-1]
    n = int(nbdt//2)
    c_U = math.sqrt(omega / (2 * alpha))
    U = np.fft.fft(Text)
    Kpos = np.arange(n)
    Kneg = np.arange(n, nbdt)
    Kneg = (nbdt-Kneg)*np.exp(-1j*math.pi)
    sqrtKpos = np.sqrt(Kpos)
    sqrtKneg = np.sqrt(Kneg, dtype=complex)
    if is_flat_ground_he:
        # Pos
        U[:n] *= np.exp((1+1j)*ztop*c_U*np.sqrt(sqrtKpos))
        # Neg
        # NB: Kneg-nbdt = (nbdt-Kneg)*exp(-1j*pi) but sqrt root different
        #     - sqrt(Kneg-nbdt) = root1
        #     - sqrt((nbdt-Kneg)*np.exp(-1j*math.pi)) = root2 = -1j * root1
        #     Using root2 enables keeping the formulation unchanged
        U[n:] *= np.exp((1+1j)*ztop*c_U*np.sqrt(sqrtKneg))
        ground_temp = np.fft.ifft(U).real
    else:
        zbottom = ztop-height
        c_mult = (1-1j) / height * math.sqrt(alpha/(2*omega))
        U[0] /= c_mult
        # Pos
        sqrtKpos = sqrtKpos[1:]
        U[1:n] *= 1./sqrtKpos * (
                np.exp((1+1j) * ztop * c_U * sqrtKpos)
                - np.exp((1+1j) * zbottom * c_U * sqrtKpos)
                ) 
        # Neg (see NB above)
        U[n:] *= 1./sqrtKneg * (
                np.exp((1+1j) * ztop * c_U * sqrtKneg)
                - np.exp((1+1j) * zbottom * c_U * sqrtKneg)
                )
        ground_temp = (c_mult*np.fft.ifft(U)).real
    return ground_temp