#coding: utf8

import collections
import numpy as np

def ground_ini_temp_from_location(elevation, depth, x_L93, y_L93):
    """ Computing ground initial temperature according to point location
    
    Reference: http://infoterre.brgm.fr/rapports/RP-68858-FR.pdf
    (eq 1 and table 1)
    
    elevation: float, altitude of ground surface above point [m]
    depth: float, true vertical depth of point [m]
    x_L93: float, easting of point [m]
    y_L93: float, norhting of point [m]
    
    Returns: float, ground initial temperature at given point [m]
    
    """
    polyn = np.array([
        [0,0,0,0], [1,0,0,0], [0,0,0,1], [0,1,0,0], [2,0,1,0],
        [0,1,0,1], [1,0,2,0], [3,0,0,0], [0,3,0,0], [0,2,0,0],
        [0,1,1,1], [0,1,0,2], [0,0,3,0], [0,0,1,0], [0,2,0,1],
        [0,0,2,0], [1,1,1,0], [1,0,1,0], [1,0,0,1], [0,0,1,1]
        ])
    coeff = np.array([
        15.8971521461331, -0.00269644681466041, -0.00617497507607523,
        -0.0284699231580076, 3.26714012588536e-08, 1.41335988796087e-05,
        -7.53125084231468e-08, -2.67719000279698e-09, -2.32447069028511e-07,
        0.000197835305813504, 9.42071645075709e-08, -2.76419289186288e-09,
        1.46535957924818e-08, 0.00193139457348857, 2.91265827270430e-08,
        1.01943529635030e-05, 8.41018944060053e-09, -2.07809053812761e-05,
        -2.73420488040877e-06, -7.33155411391564e-06
        ])
    x_L93_0 = 7e5
    y_L93_0 = 6.6e6
    temp_ground = np.power(
        [elevation, depth, (x_L93-x_L93_0)/1e3, (y_L93-y_L93_0)/1e3]
        , polyn
        ).prod(axis=1)
    temp_ground = np.dot(temp_ground, coeff)
    
    return temp_ground

# gradient géothermique
grad_gth = 3.3/100       #degre C/m (gradient GTH = 3.3 degres C par 100 m)