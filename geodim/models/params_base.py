##########################
# Bâtiment
############################
import datetime as dt

Eecsunit=2.5*3.6e6                  # Besoin énergétique pour la production d'ECS par habitant et par jour (2.5 kWh/j/pers) [J/personne/jour]
T_cons_chaud=19.                    # Température de consigne en chauffage
T_cons_chaud_reduit=16.             # Température de consigne en chauffage réduit
heure_debutchauffage=6              # Heure du passage entre réduit de nuit et consigne de chauffage
heure_arretchauffage=22             # Heure de passage entre consigne de chauffage et réduit de nuit
non_leap_year = 1991                # Needs a non-leap year, year of Python 1st parution is a relevant candidate ;)
jour_arretchauffage = dt.datetime(non_leap_year,5,15).timetuple().tm_yday # Arrêt du chauffage au 15 mai
jour_debutchauffage = dt.datetime(non_leap_year,10,15).timetuple().tm_yday # Reprise du chauffage au 15 octobre
T_cons_froid=26.
jour_debutrafraichissement=jour_arretchauffage   # Début du rafraichissement au 15 mai 
jour_arretrafraichissement=jour_debutchauffage     # Fin du rafraichissement au 15 octobre
chargeinternehab=100.               # Charges internes par habitant [W]
chargesinternesbat=6.               # Charges internes en W/m2
#h_geocooling=15.                   # coefficient d'échange thermique du plancher rafraichissant en géocooling [W/(m2.K)]

##############
## Loi d'eau #
##############
#
#Temission1=35.          #°C : température d'alimentation des émetteurs à Text1
#Text1=-10.              #°C
#Temission2=24.          #°C : température d'alimentation des émetteurs à Text2
#Text2=7.                #°C
##Temission_minimum=22.   #°C : température sous laquelle il est impossible d'alimenter les émetteurs
#
T_condens_ECS=50.       #°C : température représentative du fonctionnement PAC pour l'ECS

########################
# Paramètres physiques #
########################

#proprietes de l'eau glycolee et debit
rho_eau_g=1002          # masse volumique [kg/m3]
Cp_eau_g=3700           # capacite calorifique [J/kg/K]
lambda_eau_g=0.417      # conductivité thermique [W/m/K]