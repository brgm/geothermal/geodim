""" Classes for emitter models

TODO: EmitterFromFile more generic and Emitter more specific

"""
from dataclasses import dataclass
import pathlib

import numpy as np

from ..load_yaml import load_yaml
    
CWD = pathlib.Path(__file__).parent
SOURCE_DIR = CWD/'data'
    
@dataclass
class Altitude:
    """
    Class holding altitudes
        
    Parameters
    ----------
    name: str
        Name for altitude
    altitude_min: float
        Minimum altitude considered [m]
    altitude_max: float
        Maximum altitude considered [m]
    temperature_correction: float
        External temperature correction to account for altitude [°C]
        
    """
    name: str
    altitude_min: float = -np.inf
    altitude_max: float = np.inf
    temperature_correction: float = 0.
        
    def get_name(self):
        """ Returning name of altitude as a str """
        return self.name
        
    def get_min_altitude(self):
        """ Returning minimum altitude considered """
        return self.altitude_min
        
    def get_max_altitude(self):
        """ Returning maximum altitude considered """
        return self.altitude_max
        
    def get_bound_altitudes(self):
        """ Returning both minimum and maximum altitudes considered """
        return (self.altitude_min, self.altitude_max)
        
    def get_temperature_correction(self):
        """ Returning external temperature correction """
        return self.temperature_correction
        
    def iter_bound_altitudes(self):
        """ Iterate through minimum and maximum altitudes considered """
        for alt in self.get_bound_altitudes():
            yield alt
        
    def __iter__(self):
        return self.iter_bound_altitudes()
            
    @classmethod
    def from_name(cls, name):
        """ Constructing from default models """
        fname = SOURCE_DIR/f'{name}.yml'
        assert fname.is_file(), f'Altitude model "{fname}" not found'
        data = load_yaml(fname)
        kwargs = dict(name= data['name'])
        alt_min = data.get('alt_min')
        alt_max = data.get('alt_max')
        cor_T = data.get('cor_T')
        if alt_min is not None:
            kwargs['altitude_min'] = alt_min
        if alt_max is not None:
            kwargs['altitude_max'] = alt_max
        if cor_T is not None:
            kwargs['temperature_correction'] = cor_T
        return cls(**kwargs)