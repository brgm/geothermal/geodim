""" Functions for loading models """
import pathlib

from . import altitudes
from . import buildings
from . import emitters
from . import ground_heat_exchangers
from . import grounds
from . import heat_pumps
from . import load_yaml
from . import outsides

CWD = pathlib.Path(__file__).parent

def get_available_models(folder, with_names=False):
    """ Returning available models of given kind
    
    Parameters
    ----------
    folder: str/pathlib.Path,
        The folder to load data from.
    with_names: bool,
        Whether the models names should also be returned
        
    Returns
    -------
    out: list,
        Loadable models in folder as strings.
        If with_names is True, will be list of tuples (model, name in yaml file)
    """
    folder = pathlib.Path(folder)
    if not folder.is_absolute():
        folder = CWD/folder
    model_files = pathlib.Path(folder, 'data').glob('*.yml')
    if not with_names:
        return [model.stem for model in model_files]
    else:
        models = []
        return [(model.stem, load_yaml.load_yaml(model)['name'])
            for model in model_files
            ]
    return files

def load_emitter(
    name, external_temperatures=None, emission_temperatures=None,
    h_geocooling=None
    ):
    """ Loading an emitter
    
    Parameters
    ----------
    name: str
        Name for emitter
    external_temperatures: list/tuple/np.ndarray
        External temperatures [°C]
    emission_temperatures: list/tuple/np.ndarray
        Corresponding emission temperatures [°C]
    h_geocooling: float,
        Transfer coef between fluid and building/house [W/(m2.K)]
        Notice that coefficient is an effective value embedding:
        
        - properties of fluid in emitter,
        - emitter properties
    
        Note
        ----
        If `external_temperatures` and `emission_temperatures` are provided, a
        custom model is loaded.
    
    Returns
    -------
    out: emitter.Emitter,
        The emitter model
    
    """
    use_custom = (
        external_temperatures is not None
        and emission_temperatures is not None
        )
    if use_custom:
        return emitters.Emitter(
            name, external_temperatures, emission_temperatures, h_geocooling
            )
    return emitters.EmitterTwoPoints.from_name(name)
    
def load_heat_pump(
    name, nominal_flowrate=None, heating_power=None, power_consumption=None,
    standby_power=None
    ):
    """ Loading a heat pump
    
    Parameters
    ----------
    name: str
        Name of heat pump to load
    nominal_flowrate: float
        Flowrate in heatpump [m3/h]
        = débit nominal
    heating_power: float
        Reference heating power [kW]
        = puissance calorifique
    power_consumption: float
        Reference consumption power [kW]
        = puissance absorbée compresseur
    standby_power: float
        Heat pump standby power [W]
        = puissance réduite
    
        Note
        ----
        If `nominal_flowrate`, `heating_power`, `power_consumption` and
        `standby_power` are provided, a custom model is loaded.
    
    Returns
    ------
    out: heat_pump.HeatPump,
        The heat pump model
    
    """
    use_custom = (
        nominal_flowrate is not None
        and heating_power is not None
        and power_consumption is not None
        and standby_power is not None
        )
    if use_custom:
        return heat_pumps.HeatPump.from_custom_inputs(
            name, nominal_flowrate, heating_power, power_consumption,
            standby_power
            )
    return heat_pumps.HeatPump.from_name(name)
    
def load_custom_heat_pump(
    name, nominal_flowrate, heating_power, power_consumption, standby_power
    ):
    """ Loading a custom heat pump
    
    Parameters
    ----------
    name: str,
        Name for heat pump
    nominal_flowrate: float,
        Flowrate in heatpump [m3/h]
        = débit nominal
    heating_power: float,
        Reference heating power [kW]
        = puissance calorifique
    power_consumption: float,
        Reference consumption power [kW]
        = puissance absorbée compresseur
    standby_power: float,
        Heat pump standby power [W]
        = puissance réduite
       
    Returns
    -------
    out: heat_pump.HeatPumpCustom,
        The heat pump model
    
    """
    return heat_pumps.HeatPumpCustom(
        name, nominal_flowrate, heating_power, power_consumption, standby_power
        )

def load_ground(name):
    """ Loading a ground type
    
    Parameters
    ----------
    name: str,
        Name of ground to load
    
    Returns
    -------
    out: grounds.Ground, the ground model
    
    """
    return grounds.Ground.from_name(name)

def load_altitude(name):
    """ Loading an altitude
    
    Parameters
    ----------
    name: str,
        Name of altitude to load
    
    Returns
    -------
    out: altitudes.Altitude, the altitude model
    
    """
    return altitudes.Altitude.from_name(name)
    
def load_outside(name, temperatures=None):
    """ Loading outside (= external temperatures)
    
    Parameters
    ----------
    name: str,
        Name of outside
    temperatures: np.ndarray,
        Custom outside temperatures [°C]
        If None, name should correspond to a preexisting model
    
    Returns
    -------
    out: outsides.Outside,
        The model for outside/external temperatures
    
    """
    if temperatures is not None:
        return outsides.Outside(name, temperatures)
    return outsides.Outside.from_name(name)
    
def load_building(name, power_needs=None):
    """ Loading building (= power needs)
    
    Parameters
    ----------
    name: str,
        Name of building to load
    power_needs: np.ndarray,
        Custom building power needs [W]
        If None, name should correspond to a preexisting model
    
    Returns
    -------
    out: buildings.Building,
        The model for building/power needs
    
    """
    if power_needs is not None:
        return buildings.Building(name, power_needs)
    return buildings.Building.from_name(name)