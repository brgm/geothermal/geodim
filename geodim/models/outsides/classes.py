""" Classes for outsides (external temperatures) """
from dataclasses import dataclass, field
import pathlib

import numpy as np
from ..load_yaml import load_yaml
    
CWD = pathlib.Path(__file__).parent
SOURCE_DIR = CWD/'data'

@dataclass
class Outside:
    """
    Class holding external temperatures
        
    Parameters
    ----------
    name: str
        Name for outside
    temperatures: np.ndarray
        External temperatures [°C]
    hdd: float
        Heating degree day [°C]
    cdd: float
        Cooling degree day [°C]
        
    """
    name: str
    temperatures: np.ndarray = field(default_factory=np.array)
    hdd: float = None
    cdd: float = None
    
    def get_temperatures(self, index=None):
        if index is not None:
            return self.temperatures[index]
        return self.temperatures
    
    def __getitem__(self, index):
        return self.temperatures[index]
    
    def __iter__(self):
        for temp in self.temperatures:
            yield temp
            
    @classmethod
    def from_npz(cls, fname, arr_name, hdd, cdd):
        """ Constructing from stored data """
        temperatures = np.load(fname)[arr_name]
        return cls(f'external_temperature_{arr_name}', temperatures, hdd, cdd)
            
    @classmethod
    def from_name(cls, name):
        """ Constructing from default models """
        fname = SOURCE_DIR/f'{name}.yml'
        assert fname.is_file(), f'Outside model "{fname}" not found'
        data = load_yaml(fname)
        name = data['name']
        hdd = data.get('HDD')
        cdd = data.get('CDD')
        fname = SOURCE_DIR/data['file']
        return cls.from_npz(fname, name, hdd, cdd)
