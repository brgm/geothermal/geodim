""" Classes for heat pump models """
from dataclasses import dataclass, field
import pathlib

import numpy as np
from scipy.interpolate import RectBivariateSpline

from ..load_yaml import load_yaml
    
CWD = pathlib.Path(__file__).parent
SOURCE_DIR = CWD/'data'

@dataclass
class HeatPump:
    """
    Class holding heat pump models
    
    Parameters
    ----------
    name: str,
        Name for heat pump
    emission_min_temperature: float
        Minimum for emission temperature [°C]
        = température d'émission minimum
    nominal_flowrate: float
        Flowrate in heatpump [m3/s]
        = débit nominal
    standby_power: float
        Heat pump standby power [W]
        = puissance réduite
    condenser_outlet_temperatures: (N,)-array
        reference condenser outlet temperatures [°C]
        = températures de sortie condenseur
    evaporator_inlet_temperatures: (M,)-array
        Reference evaporator inlet temperatures [°C]
        = températures d'entrée évaporateur
    power_consumptions: (N,M)-array
        Reference consumption powers measured at reference evaporator inlet
        and condenser outlet temperatures [W]
        = puissance absorbée compresseur
    heating_powers: (N,M)-array
        Reference heating powers measured at reference evaporator inlet
        and condenser outlet temperatures [W]
        Should be cooling_powers + power_consumptions
        = puissance calorifique
    cooling_powers: (N,M)-array
        Reference cooling powers measured at reference evaporator inlet
        and condenser outlet temperatures [W]
        Should be heating_powers - power_consumptions
        = puissance frigorifique
        
    """
    name: str
    emission_min_temperature: float
    nominal_flowrate: float
    standby_power: float
    condenser_outlet_temperatures: np.ndarray = field(default_factory=np.array)
    evaporator_inlet_temperatures: np.ndarray = field(default_factory=np.array)
    power_consumptions: np.ndarray = field(default_factory=np.array)
    heating_powers: np.ndarray = None
    cooling_powers: np.ndarray = None
    Pa: callable = None
    Pc: callable = None
    Pf: callable = None
    
    def __post_init__(self):
        # Forcing array types
        self.condenser_outlet_temperatures = np.array(
            self.condenser_outlet_temperatures
            )
        self.evaporator_inlet_temperatures = np.array(
            self.evaporator_inlet_temperatures
            )
        self.power_consumptions = np.array(self.power_consumptions)
        # Computing missing power table
        heating_was_set = self.heating_powers is not None
        cooling_was_set = self.cooling_powers is not None
        if heating_was_set and not cooling_was_set:
            self.heating_powers = np.array(self.heating_powers)
            self.cooling_powers = self.heating_powers - self.power_consumptions
        elif cooling_was_set:
            self.cooling_powers = np.array(self.cooling_powers)
            self.heating_powers = self.cooling_powers + self.power_consumptions
        else:
            raise Exception('Give at least heating_powers or cooling_powers')
        # Adding corrections when data for T_in_evap >= 25 is missing
        if self.evaporator_inlet_temperatures[-1] < 25:
            self.evaporator_inlet_temperatures = np.hstack((
                self.evaporator_inlet_temperatures,
                [25.]
                ))
            self.power_consumptions = np.column_stack((
                self.power_consumptions,
                self.power_consumptions[:,-1]
                ))
            self.cooling_powers = np.column_stack((
                self.cooling_powers,
                self.cooling_powers[:,-1]
                ))
            self.heating_powers = np.column_stack((
                self.heating_powers,
                self.heating_powers[:,-1]
                ))
        # Setting power functions
        # NB: if functions set as properties, evaluation times is ~15x slower
        self.Pc = RectBivariateSpline(
            self.t_T_out_cond, self.t_T_in_evap, self.t_Pc
            )
        self.Pa = RectBivariateSpline(
            self.t_T_out_cond, self.t_T_in_evap, self.t_Pa
            )
        self.Pf = RectBivariateSpline(
            self.t_T_out_cond, self.t_T_in_evap, self.t_Pf
            )
        
    @property
    def t_T_in_evap(self):
        return self.evaporator_inlet_temperatures
        
    @property
    def t_T_out_cond(self):
        return self.condenser_outlet_temperatures
        
    @property
    def t_Pc(self):
        return self.heating_powers
        
    @property
    def t_Pa(self):
        return self.power_consumptions
        
    @property
    def t_Pf(self):
        return self.cooling_powers
    
    def get_name(self):
        """ Returning name of heat pump as a str """
        return self.name
        
    def get_emission_min_temperature(self):
        """ Returning emission minimum temperature as a float in [°C] """
        return self.emission_min_temperature
        
    def get_nominal_flowrate(self):
        """ Returning nominal flowrate as a float in [m3/s] """
        return self.nominal_flowrate
        
    def get_condenser_outlet_temperatures(self):
        """ Returning array of reference condenser outlet temperatures [°C] """
        return self.condenser_outlet_temperatures
        
    def get_evaporator_inlet_temperatures(self):
        """ Returning array of reference evaporator inlet temperatures [°C] """
        return self.evaporator_inlet_temperatures
        
    def get_heating_powers(self):
        """ Returning array of reference heating powers [W] """
        return self.heating_powers
        
    def get_cooling_powers(self):
        """ Returning array of reference heating powers [W] """
        return self.get_cooling_powers
        
    def get_power_consumptions(self):
        """ Returning array of reference consumption powers [W] """
        return self.power_consumptions
        
    def get_standby_power(self):
        """ Returning standby power as a float [W] """
        return self.standby_power
        
    def iter_reference_powers(self):
        """ Iterate through reference Tinevap, Toutcond, Pc and Pa """
        tinevs, toutconds = np.meshgrid(
            self.evaporator_inlet_temperatures,
            self.condenser_outlet_temperatures
            )
        for tinev, toutcond, pc, pa in zip(
            tinevs.flat,
            toutconds.flat,
            self.heating_powers.flat,
            self.power_consumptions.flat
            ):
            yield tinev, toutcond, pc, pa
        
    def _asdict(self):
        """
        Returning main attributes as a dict
        
        Warning
        -------
        Kept for backward compatibility, remove when ok
        
        """
        return dict(
            nom=self.name,
            T_emission_minimum=self.emission_min_temperature,
            debit_nominal=self.nominal_flowrate,
            puissance_reduit=self.standby_power,
            T_in_evap=self.evaporator_inlet_temperatures,
            T_out_condens=self.condenser_outlet_temperatures,
            Pc=self.heating_powers,
            Pa=self.power_consumptions
            )
        
    def __iter__(self):
        return self.iter_reference_powers()
            
    @classmethod
    def from_name(cls, name):
        """ Constructing from default models """
        source_file = SOURCE_DIR/f'{name}.yml'
        assert source_file.is_file(), f'Heat pump model "{name}" not found'
        kw_mapping = dict(
            name='nom',
            emission_min_temperature='T_emission_minimum',
            nominal_flowrate='debit_nominal',
            condenser_outlet_temperatures='T_out_condens',
            evaporator_inlet_temperatures='T_in_evap',
            heating_powers='Pc',
            power_consumptions='Pa',
            standby_power='puissance_reduit'
            )
        data = load_yaml(source_file, kw_mapping)
        data = dict(item for item in data.items() if item[0] in kw_mapping)
        # Converting to SI units
        if data['nominal_flowrate'] is not None:
            data['nominal_flowrate'] /= 3600. # [m3/h] -> [m3/s]
        if data['heating_powers'] is not None:
            data['heating_powers'] = (
                np.array(data['heating_powers'])*1000. # [kW] -> [W]
                )
        if data['power_consumptions'] is not None:
            data['power_consumptions'] = (
                np.array(data['power_consumptions'])*1000. # [kW] -> [W]
                )
        return cls(**data)
    
    @classmethod
    def from_custom_inputs(
        cls, name, nominal_flowrate, heating_power, power_consumption,
        standby_power, reference_heat_pump='GKUB_33H'
        ):
        """
        Constructor for custom heatpump
        
        Parameters
        ----------
        name: str,
            Name for heat pump
        nominal_flowrate: float
            Flowrate in heatpump [m3/h]
        heating_power: float
            Heating power measured at reference condenser outlet and evaporator
            inlet temperatures [kW]
        power_consumption: float
            Power consumption measured at reference condenser outlet and
            evaporator inlet temperatures [kW]
        standby_power: float,
            Heat pump standby power [W]
        reference_heat_pump: str
            A heat pump model available in data sources.
            This heat pump will provide all parameters that are missing, i.e.:
            
            - minimum emission temperature
            - condenser outlet temperatures (N values)
            - evaporator outlet temperatures (M values)
        
        """
        ref_hp = cls.from_name(reference_heat_pump)
        emission_min_temperature = ref_hp.emission_min_temperature
        condenser_outlet_temperatures = ref_hp.condenser_outlet_temperatures
        evaporator_inlet_temperatures = ref_hp.evaporator_inlet_temperatures
        heating_powers = compute_powers(
            heating_power,
            condenser_outlet_temperatures[..., None],
            evaporator_inlet_temperatures,
            'heating'
            )
        power_consumptions = compute_powers(
            power_consumption,
            condenser_outlet_temperatures[..., None],
            evaporator_inlet_temperatures,
            'consumption'
            )
        # /!\ convert to SI units
        return cls(
            name, emission_min_temperature, nominal_flowrate/3600., standby_power,
            condenser_outlet_temperatures, evaporator_inlet_temperatures,
            power_consumptions*1000, heating_powers*1000
            )
    
def compute_powers(reference_power, T_cd, T_ev, type):
    """
    Computing a heating or consumption power
    
    Parameters
    ----------
    reference_power: float
        The reference power [kW]
    T_cd: float/np.ndarray
        Condensor outlet temperature at which to get power [°C].
        If np.ndarray, must be of shape (1, N)
    T_ev: float/np.ndarray
        Evaporator inlet temperature at which to get power [°C].
        If np.ndarray, must be of shape (M,)
    type: str
        'heating' for computing heating power and 'consumption' for
        computing power consumption
          
    Returns
    -------
    out: float/(M, N)-array,
        Computed power
        
    """
    if type == 'heating':
        a = 1.340680709
        b = -0.015275019
        c = 0.043362445
        d = -0.000171046
        e = 0.00014563
        f = -0.000596629
    elif type == 'consumption':
        a = 0.737052196
        b = -0.002109879
        c = 1.21797E-06
        d = -3.45568E-08
        e = 0.000281067
        f = 4.33994E-08
    else :
        raise Exception(
            'Error: can only compute "heating" or "consumption" power types.'
            )
    return reference_power*(
        a
        + b*T_cd  + c*T_ev
        + d*T_cd*T_ev
        + e* T_cd**2
        + f * T_ev**2
        )