""" Classes for buildings (power needs) """
from dataclasses import dataclass, field
import pathlib

import numpy as np
import yaml
    
CWD = pathlib.Path(__file__).parent
SOURCE_DIR = CWD/'data'

@dataclass
class Building:
    """ Class holding building power needs """
    name: str
    needs: np.ndarray = field(default_factory=np.array)
    heat: np.ndarray = field(init=False)
    dhw: np.ndarray = field(init=False)
    cool: np.ndarray = field(init=False)
    
    def __post_init__(self):
        self.heat, self.dhw, self.cool = self.needs
        
    def get_power_needs(self, index=None):
        if index is not None:
            return self.needs[index]
        return self.needs
    
    def get_heating_needs(self, index=None):
        if index is not None:
            return self.heat[index]
        return self.heat
    
    def get_dhw_needs(self, index=None):
        if index is not None:
            return self.dhw[index]
        return self.dhw
    
    def get_cool_needs(self, index=None):
        if index is not None:
            return self.cool[index]
        return self.cool
    
    def __getitem__(self, index):
        return self.needs[index]
    
    def __iter__(self):
        for need in self.needs:
            yield need
            
    @classmethod
    def from_npz(cls, fname, arr_name):
        powers = np.load(fname)[arr_name]
        kind = fname.stem.lower()
        return cls(f'{kind}_{arr_name}', powers)
            
    @classmethod
    def from_name(cls, name):
        fname = SOURCE_DIR/f'{name}.yml'
        with fname.open(encoding='utf8') as rd:
            data = yaml.safe_load(rd)
        kind, city = name.rsplit('_', 1)
        name = city
        fname = SOURCE_DIR/data['file']
        return cls.from_npz(fname, name)