""" Classes for emitter models

TODO: EmitterFromFile more generic and Emitter more specific

"""
from dataclasses import dataclass, field
import pathlib

import numpy as np

from ..load_yaml import load_yaml
    
CWD = pathlib.Path(__file__).parent
SOURCE_DIR = CWD/'data'
    
@dataclass
class Emitter:
    """
    Class holding emitters
        
    Parameters
    ----------
    name: str
        Name for emitter
    external_temperatures: list/tuple/np.ndarray
        External temperatures [°C]
    emission_temperatures: list/tuple/np.ndarray
        Corresponding emission temperatures [°C]
    h_geocooling: float
        Transfer coef between fluid and building/house [W/(m2.K)]
        Notice that coefficient is an effective value embedding:
        
        - properties of fluid in emitter,
        - emitter properties
   
    Note
    ----
    Heat exchanges between fluid in emitter and building/house are given
    by P = h_geocooling * S_exch * (T_building - T_fl_em)
    where
    
    - P [W] is the heat exchanged from building to fluid in emitter
      (P>0 in cooling mode)
    - S_exch [m2] is the heat exchange surface
    - T_building [°C] the mean temperature in the building/house
    - T_fl_em [°C] the mean temperature of the fluid in the emitter
        
    """
    name: str
    external_temperatures: np.ndarray = field(default_factory=np.array)
    emission_temperatures: np.ndarray = field(default_factory=np.array)
    h_geocooling: float = None
    
    def __post_init__(self):
        self.external_temperatures = np.array(self.external_temperatures)
        self.emission_temperatures = np.array(self.emission_temperatures)
    
    @property
    def temperatures(self):
        return np.vstack((
            self.external_temperatures,
            self.emission_temperatures
            )).T
        
    def get_name(self):
        """ Returning name of emitter as a str """
        return self.name
        
    def get_temperatures(self):
        """ Returning external and emission temps (list of float pairs) """
        return self.temperatures
        
    def get_emission_temperatures(self):
        """ Returning all emission temperatures as a list """
        return self.emission_temperatures
        
    def get_external_temperatures(self):
        """ Returning all external temperatures as a list """
        return self.external_temperatures
        
    def get_h_coefficient(self):
        """ Returning h_coefficient as a float """
        return self.h_geocooling
        
    def set_h_coefficient(self, value):
        """ Setting h_coefficient
        
        Parameters
        ----------
        value: float,
            Value for h_coefficient [W/(m2.K)]
        
        """
        self.h_geocooling = value
        
    def iter_emission_temperatures(self):
        """ Iterate through external and emission temperatures """
        for ext_temp, em_temp in self.temperatures:
            yield ext_temp, em_temp
            
    def get_emission_temperature(self, Text):
        """
        
        Interpolate Temission=f(Text) 
        
        Note
        ----
        Similar - but faster when called with a scalar - to
        scipy.interpolate.interp1d(
            self.external_temperatures,
            self.emission_temperatures,
            bounds_error=False,
            fill_value=(
                self.emission_temperatures[0], self.emission_temperatures[-1]
                )
            )
        
        Parameters
        ----------
        Text: float
            External temperature to get corresponding emission temp of (in [°C])
            
        Returns
        -------
        out: float
            Emission temperature in [°C]
        
        """
        i = np.digitize(Text, self.external_temperatures)-1
        # # Cap min and max values
        # if i<0:
            # return self.emission_temperatures[0]
        # if i >= len(self.external_temperatures)-1:
            # return self.emission_temperatures[-1]
        # Extrapolate min and max values
        i = max(i, 0)
        i = min(i, len(self.external_temperatures)-2)
        Text1, Text2 = self.external_temperatures[i:i+2]
        Tem1, Tem2 = self.emission_temperatures[i:i+2]
        dT = Text1-Text2
        return Tem1 * (Text-Text2) / dT - Tem2 * (Text-Text1)/dT
        
    def __iter__(self):
        return self.iter_emission_temperatures()
        
@dataclass
class EmitterTwoPoints(Emitter):
    """ A simplified emitter based on two temperature points only """
    
    @property
    def Temission1(self):
        return self.emission_temperatures[0]
        
    @property
    def Temission2(self):
        return self.emission_temperatures[1]
    
    @property
    def Text1(self):
        return self.external_temperatures[0]
        
    @property
    def Text2(self):
        return self.external_temperatures[1]
            
    def get_emission_temperature_1(self):
        """ Returning emitter emission temperature 1 as float"""
        return self.Temission1
            
    def get_emission_temperature_2(self):
        """ Returning emitter emission temperature 2 as float"""
        return self.Temission2
            
    def get_external_temperature_1(self):
        """ Returning emitter external temperature 1 as float"""
        return self.Text1
            
    def get_external_temperature_2(self):
        """ Returning emitter external temperature 2 as float"""
        return self.Text2
        
    def get_emission_temperature(self, Text):
        """ Interpolate Temission=f(Text) 
        
        Parameters
        ----------
        Text: float,
            External temperature to get corresponding emission temp of (in [°C])
            
        Returns
        -------
        out: float,
            Emission temperature in [°C]
        
        """
        dT = self.Text1-self.Text2
        # Uncomment to cap min and max values
        # If commented, similar to doing a linear extrapolation
        # if Text <= self.Text1:
            # return self.Temission1
        # if Text >= self.Text2:
            # return self.Temission2
        return (
            self.Temission1 * (Text-self.Text2) / dT
            - self.Temission2 * (Text-self.Text1)/dT
            )
            
    @classmethod
    def from_name(cls, name):
        """ Constructing from default models """
        source_file = SOURCE_DIR/f'{name}.yml'
        assert source_file.is_file(), f'Emitter model "{fname}" not found'
        data = load_yaml(source_file)
        name = data['name']
        Texts = [data.get(kw) for kw in ('Text1', 'Text2')]
        Tems = [data.get(kw) for kw in ('Temission1', 'Temission2')]
        h_geocooling = data.get('h_geocooling')
        return cls(name, Texts, Tems, h_geocooling)