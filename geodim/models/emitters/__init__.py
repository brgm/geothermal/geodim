""" Models for emitters """
from .classes import Emitter
from .classes import EmitterTwoPoints