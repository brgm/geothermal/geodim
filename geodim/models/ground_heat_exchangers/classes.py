""" Classes for ground heat exchangers models """
import math
import numpy as np
import pathlib
from scipy.special import erf

from ..grounds import initial_states
from ..load_yaml import load_yaml
from ..params_base import lambda_eau_g

CWD = pathlib.Path(__file__).parent
SOURCE_DIR = CWD/'data'
    
"""
Sonde géothermique verticale --> Borehole heat exchanger
Echangeur de chaleur de proche surface --> Shallow heat exchanger 
corbeille = "coil heat exchanger"
champ de sonde = "field of borehole heat exchangers"
"""

SQRTPI = np.sqrt(np.pi)

class GroundHEBase:
    """ A base class for ground heat exchangers """
    def __init__(self, name, parameters, *args, **kwargs):
        """ Setting groud heat exchanger data
        
        Parameters
        ----------
        name: str,
            Name for ground heat exchanger.
        parameters: dict,
            Name and values for ground heat exchanger specific parameters.
       
        """
        self.name = name
        self.parameters = {}
        if isinstance(parameters, dict):
            self.parameters.update(parameters)
        self.set_parameters_as_attributes(self.parameters) #?
        
    def get_name(self):
        """ Returning name of heat pump as a str """
        return self.name
        
    def set_parameters_as_attributes(self, parameters=None):
        """ Making duplicates of parameters as attributes
        
        May not be relevant.
        
        """
        is_valid = isinstance(parameters, dict)
        if is_valid:
            for key,val in parameters.items():
                setattr(self, key, val)
        
    def set_dimensioning_params(self, pf_ref, p_predim, *args, **kwargs):
        """ Setting initial dimensioning parameters
        
        Parameters
        ----------
        pf_ref: float,
            Reference cooling power in [W].
        p_predim: 
            First guess on the heat-exchanger specific power [W/dim] where dim
            is the heat-exchanger dimension SI unit.
        
        """
        # S_init = Pf(35,-3) / p_predim_horiz  # estimation surface echangeur 
        # dim = int(math.ceil(S_init))  # valeur depart dimensionnement
        self.parameters['dim'] = pf_ref/p_predim
        
    def set_simulation_params(self, timestep, nyears, *args, **kwargs):
        """ Setting some simulation parameters
        
        Parameters
        ----------
        timestep: int/float,
            Simulation time step in [s].
        nyears: int,
            Number of years considered.
            
        """
        nb_seconds_1_year = 3600*24*365
        nbdt_1year = int(nb_seconds_1_year/timestep)
        self.parameters['nbdt'] = (nbdt_1year*nyears)

    def set_Rb(self):
        """ Setting heat-exchanger fluid-ground specific resistance """
        self.parameters['Rb'] = None
        raise NotImplementedError
        
    def f_function(self, *args, **kwargs):
        """ Function to integrate for computing g-function """
        raise NotImplementedError
        
    def g_function(self, *args, **kwargs):
        """ Heat-exchanger / ground interaction function """
        raise NotImplementedError
        
    def g_function_short(self, *args, **kwargs):
        """ Heat-exchanger / ground short-term interaction function """
        raise NotImplementedError
        
    def compute_g_table(self, *args, **kwargs):
        """  Computing g-function at different timesteps
        
        Warning: computations expect G[i] = G((i+1)*dt), where dt is simulation
        timestep.
        
        """
        raise NotImplementedError
        
    def compute_dg_table(self, *args, **kwargs):
        """ Computing g-function delta at different timesteps 
        
        Warning: computations expect dG[i] = dG((i+2)*dt-(i+1)*dt), where dt is
        simulation timestep.
        
        """
        raise NotImplementedError
    #
    # WORK IN PROGRESS
    # give methods for:
    # - building HE
    # - accessing parameters
    # - converting parameters to a dict?
    # 
        
class GroundHE(GroundHEBase):
    """ geodim-specific ground heat-exchanger object
    
    Specificities:
    - data fetched from the source directory
    - set some parameters automatically from the data
    """
    def __init__(self, fname, *args, **kwargs):
        """ Setting ground heat-exchanger data from a source file
        
        Parameters
        ----------
        fname: str,
            Name of an available file in source directory.
        """
        source_file = SOURCE_DIR/f'{fname}.yml'
        assert source_file.is_file(), f'Ground model "{fname}" not found'
        data = load_yaml(source_file)
        name = data.pop('name')
        assert not name is None, 'data must provide a "name" key'
        data.update(kwargs)
        GroundHEBase.__init__(self, name, data, *args, **kwargs)
        
    def set_simulation_params(self):
        timestep = self.parameters['dt']
        nyears = self.parameters['nyears']
        GroundHEBase.set_simulation_params(self, timestep, nyears)
        
    def set_dimensioning_params(self, heatpump):
        """
        Parameters
        ----------
        heatpump: geodim.models.heat_pumps.HeatPump
        """
        Toutcond_ref = 35
        Tinevap_ref = -3
        pf_ref = float(heatpump.Pf(Toutcond_ref,Tinevap_ref))
        p_predim = self.parameters['p_predim']
        GroundHEBase.set_dimensioning_params(self, pf_ref, p_predim)
        
    def collect_properties(self):
        """ Only the generic ones? Adds some specific? """
        properties = dict(
            Rb = self.parameters['Rb'],
            t_G_echangeur_gth = self.t_G_ground_he,
            t_dG_echangeur_gth = self.t_dG_ground_he,
            dim_min = self.parameters['dim_min'],
            dim_max = self.parameters['dim_max'],
            dim = self.parameters['dim'],
            unite_dim = self.parameters['dim_unit'],
            t_Tinitsol = self.t_Tgroundini,
            G_courtterme = self.gshort,
            resolution_dim = self.parameters['dim_resolution'],
            nb_annees = self.parameters['nyears'],
            nbdt = self.parameters['nbdt'],
            )
        return properties

    def __iter__(self):
        """ For an easy use with dict() (compatibility with current code) """
        for key,val in self.collect_properties().items():
            yield key,val 

class FlatShallowHE(GroundHE):
    """ Flat shallow heat-exchanger (= horizontal)
    """
    def __init__(self, *args, **kwargs):
        fname = 'horizontal'
        GroundHE.__init__(self, fname)
    
    def build(self, heatpump, ground, external_temperatures, *args, **kwargs):
        self.set_dimensioning_params(heatpump)
        self.set_simulation_params()
        self.set_Rb()
        self.t_Tgroundini = self.compute_ground_ini_temp(
            ground, external_temperatures
            )            
        # Precomputing G values
        # Runs expect:
        # - G[0] = G(t=Dt); G[i] = G(t=(i+1)*Dt)
        # - dG[0] = G(t=2*Dt)-G(t=Dt); dG[i] = G(t=(i+2)*Dt)-G(t=(i+1)*Dt)
        dt = self.parameters['dt']
        nbdt = self.parameters['nbdt']
        zhoriz = self.parameters['zhoriz']
        alpha = ground.get_thermal_diffusivity()
        lambd = ground.get_thermal_conductivity()
        rhocp = ground.get_volumetric_heat_capacity()
        t_G_ground_he = self.compute_g_table(dt, nbdt+2, alpha, lambd, zhoriz)
        t_dG_ground_he = np.diff(t_G_ground_he)
        self.t_dG_ground_he = t_dG_ground_he[1:]
        self.t_G_ground_he = t_G_ground_he[1:-1]
        self.gshort = lambda load: self.g_function_short(load, dt, lambd, rhocp)
            
    def f_function(self, x):
        rbar = self.parameters['coefref']
        return (1 + rbar*np.exp(-x**2))/(x**2)
        
    def f_function_primitive(self, x):
        rbar = self.parameters['coefref']
        return -( (1 + np.exp(-x**2)*rbar)/x + SQRTPI*rbar*erf(x) )
        
    def g_function(self, t, alpha, lambd, zhoriz):
        """ Computing G = integral of f_horiz
        
        Analytical definition is: 
        G[i] = G(t=i*dt) = (
            -zhoriz/(lambda*np.sqrt(pi))
            * integral(f_function, -zhoriz/np.sqrt(a_sol*i*dt), inf)
            )
        
        NB:
        - G[i] gives G(t=i*Dt), hence G[0] = G(t=0)
        - numerical evaluation of integral is spared here by using the
          analytical expression of primitive and of f_horiz limit through infty
          
        Parameters
        ----------
        t: float/np.ndarray,
            Time in [s].
        alpha: float,
            Ground thermal diffusivity in [m2/s].
        lambd: float,
            Ground thermal conductivity in [W/m/K].
        zhoriz: float,
            Elevation from ground surface [m] (<0).
        
        Returns
        -------
        out: float/np.ndarray,
            g-function value at time(s) t.
        
        """
        x = -zhoriz/np.sqrt(alpha*t)
        prim_limit = -SQRTPI*zhoriz
        res = prim_limit-self.f_function_primitive(x)
        return -zhoriz/(lambd*SQRTPI) * res
        
    def g_function_short(self, load, dt, lambd, rhocp):
        """ Heat-exchanger / ground short-term interaction function
        
        Calcul court terme par modele source cylindrique infinie
        Resolution par equation Bernier (2000)
        
        Parameters
        ----------
        y: float,
            Taux de charge sur dernier pas de temps.
        dt: float,
            Timestep in [s].
        lambd: float,
            Ground thermal conductivity in [W/m/K].
        rhocp: float,
            Volumetric heat capacity in [J/m3/K].
        
        """
        _ci = math.pi*lambd*rhocp
        return math.sqrt(dt) * (1-np.sqrt(1-load)) / math.sqrt(_ci)
        
    def compute_g_table(self, dt, nbdt, alpha, lambd, zhoriz):
        """ Computing g-function at different timesteps
          
        Parameters
        ----------
        dt: float,
            Timestep in [s].
        nbdt: int,
            The number of timesteps.
            G function will be computed on int values in range [0;nbdt-1].
        alpha: float,
            Ground thermal diffusivity in [m2/s].
        lambd: float,
            Ground thermal conductivity in [W/m/K].
        zhoriz: float,
            Elevation from ground surface [m] (<0).
            
        """
        times = dt*np.arange(1, nbdt)
        Ghoriz = np.zeros(nbdt)
        Ghoriz[1:] = self.g_function(times, alpha, lambd, zhoriz)
        return Ghoriz
        
    def compute_dg_table(self, dt, nbdt, alpha, lambd, zhoriz):
        """ Computing g-function delta at different timesteps """
        Gs = self.compute_g_table(dt, nbdt+1, alpha, lambd, zhoriz)
        return np.diff(Gs)
        
    def compute_ground_ini_temp(self, ground, external_temperatures):
        """ Should not be computed here, see later """
        t_Tgroundini = initial_states.temp_integrated_along_height(
            external_temperatures,
            ground.get_thermal_diffusivity(),
            self.parameters['zhoriz']
            )
        t_Tgroundini = np.tile(t_Tgroundini, self.parameters['nyears'])
        # Below: remove when nbdt+1 not necessary in main loop anymore
        t_Tgroundini = np.hstack((t_Tgroundini, t_Tgroundini[0]))
        return t_Tgroundini
    
    def set_Rb(self):
        DN = self.parameters['DN']
        SDR = self.parameters['SDR']
        pastube = self.parameters['pastube']
        lambda_PE = self.parameters['lambda_PE']
        Nu = self.parameters['Nu']
        eptu = DN/(1000.*SDR)  # tube thickness [m]
        rtue = DN/2000.  # rayon exterieur tube [m]
        rtui = rtue - eptu  # rayon hydraulique tube [m]
        Ltot = 1./pastube  # longueur de tube par m2 pose [m/m2]
        # resistance paroi [m2.K/W]
        rthpe = math.log(rtue/rtui)/(lambda_PE*2*math.pi*Ltot)
        # resistance ecoulement [m2.K/W]
        rthecoulement = 1/(Nu*lambda_eau_g*math.pi*Ltot)
        # resistance totale fluide paroi
        self.parameters['Rb'] = rthpe + rthecoulement