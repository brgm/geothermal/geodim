""" Just a utility function for loading data from a yaml file """
import yaml

def load_yaml(file, kw_mapping=None, **kwargs):
    """
    Safe loading of a yml file
    
    Parameters
    ----------
    file: str/pathlib.Path
        Full path to yml file
    kw_mapping: dict
        Mapping for modifying parameters names in loaded data.
        (key, val) pairs must give:
        
        - key: str
            new parameter name in the returned data dict.
        - val: str
            original parameter name in loaded file. Its value will be paired
            with the new parameter name in the returned data dict.
        
        If `None`, no mapping is done, and original parameters names in the file
        are kept.
        
    kwargs additional keyword arguments, will be added to the update data.
    
    Note
    ----
    Key priority in returned data will be: keys in kw_mapping will override keys
    in original file which will override keys in kwargs
    
    Returns
    -------
    out: dict
        Loaded data
    
    """
    with open(file, encoding='utf8') as rd:
        data = yaml.safe_load(rd)
    kwargs_init = data.copy()
    kwargs_all = kwargs.copy()
    if kw_mapping is None:
        kw_mapping = {}
    kwargs_new = dict(
        (arg, kwargs_init.pop(ref_key, None))
        for arg,ref_key in kw_mapping.items()
        )
    kwargs_all.update(kwargs_init)
    kwargs_all.update(kwargs_new)
    return kwargs_all