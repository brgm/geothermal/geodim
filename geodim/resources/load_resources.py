""" Functions for loading resources """
import pathlib

import pandas as pd

CWD = pathlib.Path(__file__).parent

MUNICIPALITIES_SOURCE = pathlib.Path(
    CWD, 'french_municipalities', 'municipalities_list.gz'
    )


def load_municipalities():
    """ Loading file of territorial France municipalities (=communes)
    
    Returns
    -------
    out: pd.DataFrame
        Municipalities names, zip codes, altitudes and climatic zones
    
    """
    return pd.read_csv(MUNICIPALITIES_SOURCE, sep=';')
    
if __name__ == '__main__':
    municipalities = load_municipalities()