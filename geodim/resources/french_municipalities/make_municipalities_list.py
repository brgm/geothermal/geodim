""" France municipalities (=communes)

Extracting minimum required amount of data from whole official data:
https://www.data.gouv.fr/fr/datasets/correspondance-entre-les-codes-postaux-et-codes-insee-des-communes-francaises/#_

"""
if __name__ == '__main__':
    import os
    import pandas as pd
    import pathlib
    import yaml

    # municipalities_file = 'correspondance-code-insee-code-postal.csv'
    # climate_zones_file = pathlib.Path(  '..',
                                        # 'climate_zones',
                                        # 'climate_zones_for_departments.yml'
                                        # )
    # climate_zones_header = 'Climate zones for territorial France departments'
    # outfile = 'municipalities_list.gz'
                                    
    # municipalities_headers = dict([ ('Code_INSEE', str),
                                    # ('Code_Postal', str),
                                    # ('Commune', str),
                                    # ('Departement', str),
                                    # ('Region', str),
                                    # ('Statut', str),
                                    # ('Altitude_Moyenne', str),
                                    # ('Superficie', str),
                                    # ('Population', str),
                                    # ('geo_point_2d', str),
                                    # ('geo_shape', str),     #A json, actually
                                    # ('ID_Geofla', str),
                                    # ('Code_Commune', str),
                                    # ('Code_Canton', str),
                                    # ('Code_Arrondissement', str),
                                    # ('Code_Departement', str),
                                    # ('Code_Region', str)
                                    # ])

    # # Loading municipalities
    # municipalities = pd.read_csv(   municipalities_file,
                                    # names=municipalities_headers.keys(),
                                    # dtype=municipalities_headers,
                                    # sep=';',
                                    # skiprows=1,
                                    # usecols=(1, 2, 6)
                                    # )
    # municipalities.sort_values(by='Commune', inplace=True)

    # # Loading climatic zones
    # with climate_zones_file.open() as rd:
        # climate_zones = yaml.safe_load(rd)
    # climate_zones = climate_zones[climate_zones_header]
    # climate_dptmts = dict([ (dptmt, zone)
                            # for zone, dptmts in climate_zones.items()
                            # for dptmt in dptmts
                            # ])
                            
    # # Exporting data
    # header_names = 'Commune'
    # header_codes = 'Code_postal'
    # header_altitudes = 'Altitude[m]'
    # header_climates = 'Zone_climatique'

    # municipalities['Department'] = municipalities['Code_Postal'].str[:2]
    # territorial_France_mask = municipalities['Department'].astype(int) <= 95
    # municipalities_territorial = municipalities[territorial_France_mask]
    # municipalities_territorial = municipalities_territorial[[
                                    # 'Commune',
                                    # 'Code_Postal',
                                    # 'Altitude_Moyenne',
                                    # 'Department'
                                    # ]]
    # municipalities_territorial.rename(  columns= {
                                            # 'Commune':header_names,
                                            # 'Code_Postal':header_codes,
                                            # 'Altitude_Moyenne':header_altitudes
                                            # },
                                        # inplace=True
                                        # )
    # municipalities_territorial[header_climates] = municipalities_territorial[
                                        # 'Department'
                                        # ].apply(
                                            # lambda dptmt: climate_dptmts[dptmt]
                                            # )
    # export_cols = [header_names, header_codes, header_altitudes, header_climates]
    # municipalities_territorial.to_csv(  outfile, columns=export_cols,
                                        # sep=';', index=False,
                                        # compression='gzip'
                                        # )

    # # # Just to check that climate_zones_file was well completed
    # # check = []
    # # for dptmt in climate_dptmts.keys():
        # # try:
            # # check.append(int(dptmt))
        # # except ValueError:
            # # pass
    # # check = sorted(check)
    # # assert check == list(range(1,96))

