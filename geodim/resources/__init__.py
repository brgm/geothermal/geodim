"""External data for the project:

o climate_zones
    RT2012 climate zones for each department, serves as input for content of
    folder below
o french_municipalities (= communes)
    Name, zip code, altitude and climatic zone for each french municipality.
    NB: only territorail France is considered (data to be used in accordance
        with RT2012 data which does not include DOM/TOM).    
"""
from . import load_resources