"""
Processing data for the single house section

"""

from .models import load_models
from .resources import load_resources
import numpy as np

import os.path

from .models import params_base as pb

def err_mess(cas, langue='fra'):
    """Regroupe tous les messages d'erreur selon le cas qui les génère
    
    langue peut-être 'fra' ou 'eng' selon la langue du site web
    
    Retourne le message d'erreur à afficher sur la page web.
    """
    if not langue in ('fra', 'eng'):
        langue = 'fra'
    message = {'eng': '', 'fra': ''}
    if cas == 'pbat_chaud_missing':
        message['fra'] = 'Fichier des besoins de chauffage non fourni.'
        message['eng'] = 'Heat source file missing.'
    elif cas == 'pbat_chaud_null':
        message['fra'] = "Besoins de chauffage nuls sur l'année."
        message['eng'] = 'No heating needs over the year.'
    elif cas == 'pbat_chaud_nvals':
        message['fra'] = 'Le fichier des besoins thermiques doit contenir 8760 valeurs.'
        message['eng'] = 'Heat source file must contain 8760 values.'
    elif cas == 'ecs_missing':
        message['fra'] = 'Fichier des besoins ECS non fourni.'
        message['eng'] = 'ECS source file missing.'
    elif cas == 'ecs_nvals':
        message['fra'] = 'Le fichier des besoins ECS doit contenir 8760 valeurs.'
        message['eng'] = 'ECS source file must contain 8760 values.'
    elif cas == 'pbat_froid_missing':
        message['fra'] = 'Fichier des besoins en rafraîchissement non fourni.'
        message['eng'] = 'Cooling source file missing.'
    elif cas == 'T_cons_froid_missing':
        message['fra'] = 'Température de consigne pour le rafraîchissement non fourni.'
        message['eng'] = 'Cooling control temperature missing.'
    elif cas == 'pbat_froid_nvals':
        message['fra'] = 'Le fichier des besoins en rafraîchissement doit contenir 8760 valeurs.'
        message['eng'] = 'Coooling source file must contain 8760 values.'
    elif cas == 'surface_missing':
        message['fra'] = 'Surface habitable non fournie.'
        message['eng'] = 'Floor area missing.'
    elif cas == 'geocooling_not_supported':
        message['fra'] = "Géocooling non supporté par l'émetteur {emitter}"
        message['eng'] = "Geocooling not supported by {emitter} emitter"
        
    return message[langue]
    
def max_Pbat(data_in, besoins_thermiques_custom={}):
    """Calcul de la puissance maximum à délivrer dans l'année.
    
    - data_in:  données incluant les caractéristiques du bâtiment.
        Dans ce cas, la table de puissance est calculée à partir de
        fonctions préétablies, puis son maximum est retourné.
    - besoins_thermiques_custom: données incluant les tables de puissance.
        Dans ce cas, la fonction retourne le max de cette table.
        Si l'ECS et la géocooling sont demandés, retourne aussi leurs max.
        Si cette variable n'est pas vide, c'est ce cas de figure qui est traité
        par défaut.
    """
    # météo
    nom_fmeteo = data_in['nom_fmeteo']
    altitude = data_in['altitude']
    if nom_fmeteo.startswith('T'):
        nom_fmeteo = nom_fmeteo[1:]
    Text = load_models.load_outside(nom_fmeteo).get_temperatures()
    # prise en compte altitude
    cor_T = load_models.load_altitude(altitude).get_temperature_correction()
    Text += cor_T
    
    # données d'entrée = tables des besoins thermiques 
    if besoins_thermiques_custom:
        max_pbat_ecs = None
        max_pbat_froid = None
        numerical_zero = 1.e-15
        Pbat_chaud_total = besoins_thermiques_custom['Pbat_chaud_total_custom']
        assert not Pbat_chaud_total is None, err_mess('pbat_chaud_missing')
        assert len(Pbat_chaud_total) == len(Text), err_mess('pbat_chaud_nvals')
        if data_in['do_ECS']:
            tab_ECS = besoins_thermiques_custom['Eecs_custom']
            assert not tab_ECS is None, err_mess('ecs_missing')
            assert len(tab_ECS) == len(Text), err_mess('ecs_nvals')
            max_pbat_ecs = max(tab_ECS)
        if data_in['do_raf']:
            assert 'surface' in data_in, err_mess('surface_missing')
            Pbat_froid = besoins_thermiques_custom['Pbat_froid_custom']
            T_cons_froid = besoins_thermiques_custom['T_cons_froid']
            assert not Pbat_froid is None, err_mess('pbat_froid_missing')
            assert not T_cons_froid is None, err_mess('T_cons_froid_missing')
            assert len(Pbat_froid) == len(Text), err_mess('pbat_froid_nvals')
            max_pbat_froid = max(Pbat_froid)
        return (max(Pbat_chaud_total), max_pbat_ecs, max_pbat_froid)

    # données d'entrée = caractéristiques de la maison        
    isolation = data_in['isolation']
    surface = data_in['surface']
    hauteur = data_in['hauteur']
    nb_occupants = data_in['nb_occupants']
    # bâtiment
    Gbat = float(isolation)     # [W/m3/K]
    Sbat = float(surface)       # [m2]
    hspbat = float(hauteur)     # [m]
    nbhab = int(nb_occupants)
    Eecs = nbhab*pb.Eecsunit    # [J/jour]
    chargesinternes = nbhab*pb.chargeinternehab+Sbat*pb.chargesinternesbat # [W]

    t_T_chaud_jour = np.tile(pb.T_cons_chaud_reduit, 24)
    t_T_chaud_jour[pb.heure_debutchauffage:pb.heure_arretchauffage] = pb.T_cons_chaud
    t_T_chaud = np.tile(t_T_chaud_jour, 365)
    
    Pbat_chaud_total = np.maximum(Gbat*Sbat*hspbat*(t_T_chaud-Text)-chargesinternes,0)

    return max(Pbat_chaud_total)   # Puissance affichee en kW

def regression_lineaire(pbat, Text, double_regression=False):
    """ Cherche les coefficients a et b tels que pbat = a*Text + b
    
    - pbat:
        besoins thermiques du bâtiment sur un an et par heure (8760 valeurs)
    - Text:
        température extérieure sur un an et par heure (8760 valeurs)
    - double_regression:
        si True, la fonction sépare les données en deux ensemble selon que pbat
        soit > ou < à la première régression, puis pratique deux régressions
        linéaires indépendantes sur ces deux ensembles
    
    Retourne a, b et le coefficient de détermination (R2).
    """
    # Régression linéaire sur les valeurs positives de pbat uniquement
    numerical_zero = 1.e-15
    Text_pos = Text[pbat > numerical_zero]
    pbat_pos = pbat[pbat > numerical_zero]

    # Régression linéaire sur l'ensemble des valeurs positives
    A_pos = np.vstack([Text_pos, np.ones(len(Text_pos))]).T
    (a,b), residus = np.linalg.lstsq(A_pos, pbat_pos, rcond=None)[:2]
    var_tot = sum((pbat_pos-pbat_pos.mean())**2)
    r2 = 1 - residus[0]/var_tot
    
    if double_regression:
        # Double régression linéaire en supposant qu'il y ait deux températures
        # de consigne : une de cycle normal et une de cycle réduit
        # NB: dans ce cas, les deux coefficients a sont identiques mais les b
        # sont différents
        pbat_pos_normal = pbat_pos >= a*pbat_pos+b
        pbat_pos_reduit = np.logical_not(pbat_pos_normal)
        Text_pos_normal = Text_pos[pbat_pos_normal]
        Text_pos_reduit = Text_pos[pbat_pos_reduit]
        pbat_pos_normal = pbat_pos[pbat_pos_normal]
        pbat_pos_reduit = pbat_pos[pbat_pos_reduit]

        A_pos_normal = np.vstack([  Text_pos_normal,
                                    np.ones(len(Text_pos_normal))]).T
        A_pos_reduit = np.vstack([  Text_pos_reduit,
                                    np.ones(len(Text_pos_reduit))]).T
        (a_normal,b_normal), residus_normal = np.linalg.lstsq(
                                                        A_pos_normal,
                                                        pbat_pos_normal,
                                                        rcond=None)[:2]
        (a_reduit,b_reduit), residus_reduit = np.linalg.lstsq(
                                                        A_pos_reduit,
                                                        pbat_pos_reduit,
                                                        rcond=None)[:2]
        var_tot_normal = sum((  pbat_pos_normal
                                -pbat_pos_normal.mean())**2)
        r2_normal = 1 - residus_normal[0]/var_tot_normal
        var_tot_reduit = sum((  pbat_pos_reduit
                                -pbat_pos_reduit.mean())**2)
        r2_reduit = 1 - residus_reduit[0]/var_tot_reduit
        
        # Retourne les résultats de la double régression si elle donne un
        # meilleur coefficient de détermination
        if (r2_normal >= r2
            and r2_reduit >= r2):
            return (np.mean((a_normal, a_reduit)),
                    (b_normal, b_reduit),
                    np.mean((r2_normal, r2_reduit))
                    )
            
    return a, b, r2

def traitement( data_in, besoins_thermiques_custom={}, use_aggregation=False,
                **kwargs):
    data = {}
    
    # météo
    nom_fmeteo = data_in['nom_fmeteo']
    altitude = data_in['altitude']
    if nom_fmeteo.startswith('T'):
        nom_fmeteo = nom_fmeteo[1:]
    Text = load_models.load_outside(nom_fmeteo).get_temperatures()
    # prise en compte altitude
    cor_T = load_models.load_altitude(altitude).get_temperature_correction()
    Text += cor_T
    
    # bâtiment
    P_appoint = data_in['P_appoint'] * 1000.
    emitter = data_in['emitter']
    heatpump = data_in['heatpump']
    ground_type = data_in['ground_type']
    user_id = data_in['user_id']
    data['do_heat'] = True # Always true for single houses
    data['do_ECS'] = data_in['do_ECS']
    data['do_raf'] = data_in['do_raf']
    Gbat = None
    Sbat = None
    hspbat = None
    GSh = None
    nbhab = None
    Eecs = None
    tab_ECS = None  # Besoins ECS heure par heure
    Pbat_froid = None
    T_cons_froid = None
    chargesinternes = None
    if besoins_thermiques_custom:
        # données d'entrée = tables des besoins thermiques 
        Pbat_chaud_total = besoins_thermiques_custom['Pbat_chaud_total_custom']
        assert not Pbat_chaud_total is None, err_mess('pbat_chaud_missing')
        assert len(Pbat_chaud_total) == len(Text), err_mess('pbat_chaud_nvals')
        # a_chaud, b_chaud, r2_chaud = regression_lineaire(Pbat_chaud_total,
                                                        # Text, True)
        if data['do_ECS']:
            tab_ECS = besoins_thermiques_custom['Eecs_custom']
            assert not tab_ECS is None, err_mess('ecs_missing')
            assert len(tab_ECS) == len(Text), err_mess('ecs_nvals')
        if data['do_raf']:
            Sbat = float(data_in['surface'])# [m2]
            Pbat_froid = besoins_thermiques_custom['Pbat_froid_custom']
            T_cons_froid = besoins_thermiques_custom['T_cons_froid']
            assert not Pbat_froid is None, err_mess('pbat_froid_missing')
            assert not T_cons_froid is None, err_mess('T_cons_froid_missing')
            assert len(Pbat_froid) == len(Text), err_mess('pbat_froid_nvals')
            T_cons_froid = float(T_cons_froid)
            a_froid, b_froid, r2_froid = regression_lineaire(Pbat_froid, Text)
            GSh = a_froid
            chargesinternes = a_froid*T_cons_froid+b_froid
    else:
        # données d'entrée = caractéristiques de la maison
        Gbat = float(data_in['isolation'])  # [W/m3/K]
        Sbat = float(data_in['surface'])    # [m2]
        hspbat = float(data_in['hauteur'])  #
        nbhab = int(data_in['nb_occupants'])
        GSh = Gbat*Sbat*hspbat
        Eecs = nbhab*pb.Eecsunit   # [J/jour]
        chargesinternes = ( nbhab*pb.chargeinternehab
                            + Sbat*pb.chargesinternesbat)  # [W]
        # Besoins en puissance de chauffage
        t_T_chaud_jour = np.tile(pb.T_cons_chaud_reduit, 24)
        t_T_chaud_jour[ pb.heure_debutchauffage:
                        pb.heure_arretchauffage] = pb.T_cons_chaud
        t_T_chaud = np.tile(t_T_chaud_jour, 365) # [°C] consigne de chauffage heure par heure
        Pbat_chaud_total = np.maximum(  Gbat*Sbat*hspbat*(t_T_chaud-Text)
                                        - chargesinternes, 0)
        # Besoins en puissance de rafraîchissement
        T_cons_froid = pb.T_cons_froid
        Pbat_froid = np.maximum(Gbat*Sbat*hspbat*(Text-T_cons_froid)
                                +chargesinternes, 0)
                                
        # # Au besoin : pour exporter les tables
        # t_ECS = np.zeros_like(Pbat_chaud_total)
        # if data['do_ECS']:
            # t_ECS[pb.heure_arretchauffage::24] = Eecs/3600.
            # # t_ECS[pb.heure_arretchauffage-12::12] = Eecs/3600./2.
            # np.savetxt('ecs.dat', t_ECS)
        # t_chaud = np.array(Pbat_chaud_total)
        # # t_chaud[t_ECS>0] = 0.
        # np.savetxt('pbat_chaud.dat', t_chaud)
        # if data['do_raf']:
            # t_froid = np.array(Pbat_froid)
            # t_froid[t_ECS>0] = 0.
            # np.savetxt('pbat_froid.dat', t_froid)

    # Besoins en puissance de chauffage
    Pmax_PAC = np.max(Pbat_chaud_total) - P_appoint # [W] Puissance max de chauffage que doit fournir la PAC (appoint déduit)
    Pbat_chaud = np.maximum(np.minimum(Pbat_chaud_total, Pmax_PAC), 0)
    # Arrêt estival du chauffage
    Pbat_chaud_total[pb.jour_arretchauffage*24:pb.jour_debutchauffage*24] = 0.
    Pbat_chaud[pb.jour_arretchauffage*24:pb.jour_debutchauffage*24] = 0.
    tab_appoint = Pbat_chaud_total-Pbat_chaud # [W] Puissance de chauffage assurée par l'appoint
    E_appoint = np.sum(tab_appoint) # [Wh] Energie de chauffage assurée par l'appoint
    tab_chauf = Pbat_chaud
    assert np.sum(Pbat_chaud) > 0., err_mess('pbat_chaud_null')
    # Arrêt hivernal du rafraîchissement
    if data['do_raf']:
        Pbat_froid[:pb.jour_debutrafraichissement*24] = 0.
        Pbat_froid[pb.jour_arretrafraichissement*24:] = 0.
        tab_raf = Pbat_froid
    else:
        tab_raf = None
    # Air conditioning is turned off for single house
    data['do_AC'] = False
    tab_AC = None
    data['HP_shutdown_temp_cool'] = None
    data['emitter_inlet_temp_cool'] =  None
    data['emitter_temp_drop_cool'] =  None
    
    # Traitement data PAC
    if heatpump == 'mapac':
        heatpump = dict(
            name='MAPAC_USER%d'%user_id,
            nominal_flowrate=data_in['debit_nominal_maPAC'],
            standby_power=data_in['puissance_reduit_maPAC'],
            heating_power=data_in['Pc_maPAC'],
            power_consumption=data_in['Pa_maPAC']
            )
        pac = load_models.load_heat_pump(**heatpump)
    else:
        pac = load_models.load_heat_pump(heatpump)
    T_in_evap_cor = pac.get_evaporator_inlet_temperatures()
    T_out_condens = pac.get_condenser_outlet_temperatures()
    debit_gth = pac.get_nominal_flowrate()
    puissance_reduit = pac.get_standby_power()
    T_coupure_PAC = T_in_evap_cor.min()
    T_coupure_PAC_ECS = T_coupure_PAC # Possibilité de modifier au besoin
    
    Temission_minimum = pac.get_emission_min_temperature()
    Pc = pac.Pc
    Pf = pac.Pf
    Pa = pac.Pa

    data['PAC_T_out_condens'] = T_out_condens
    data['PAC_T_in_evap'] = T_in_evap_cor
    data['PAC_Pf'] = pac.t_Pf
    data['PAC_Pc'] = pac.get_heating_powers()
    data['PAC_Pa'] = pac.get_power_consumptions()
    
    # Traitement choix émetteur
    #############
    # Loi d'eau #
    #############
    use_custom_emitter = isinstance(emitter, dict) 
    if use_custom_emitter:
        emitter = load_models.load_emitter(**emitter)
    else:
        assert isinstance(emitter, str), 'Emitter must be str or dict'
        emitter = load_models.load_emitter(emitter)
    emitter_Texternal = emitter.get_external_temperatures()
    emitter_Temission = emitter.get_emission_temperatures()
    Text1 = emitter_Texternal[0]
    Text2 = emitter_Texternal[-1]
    Temission1 = emitter_Temission[0]
    Temission2 = emitter_Temission[-1]

    #############
    # Coefficient d'échange émetteur
    # Temp min du fluide à l'entrée du bâtiment (cas rafraîchissement via PC)
    #############
    h_geocooling = None
    Tfbi_min = None
    if data['do_raf']:
        if emitter.name == 'Plancher chauffant':
            h_geocooling = data_in['h_geocooling']
            Tfbi_min = data_in['tfbi_min']
        elif use_custom_emitter:
            h_geocooling = emitter.get_h_coefficient()
            Tfbi_min = data_in['tfbi_min']
        else:
            h_geocooling = emitter.get_h_coefficient()
            # Tfbi_min ignorés pour les autres émetteurs
            # NB: ds calcul, besoin Tfbi < Tfbi_min, donc laisser np.inf
            Tfbi_min = np.inf
    
    if data['do_raf']:
        assert h_geocooling is not None, err_mess(
            'geocooling_not_supported').format(emitter=emitter.name)
            
    def f_calc_Temission(
        Text, Tem_min=Temission_minimum,
        interp_func=emitter.get_emission_temperature
        ):
        Tem = interp_func(Text)
        return max(interp_func(Text), Tem_min)
            
    # Affectation propriétés sol
    ground = load_models.load_ground(ground_type)
    lambda_sol = ground.get_thermal_conductivity()
    rhocp_sol = ground.get_volumetric_heat_capacity()
    diffusivite_sol = ground.get_thermal_diffusivity()
    p_predim_SGV_sol = ground.get_p_predim_SGV()
    p_predim_mSGV_sol = ground.get_p_predim_mSGV()
    
#    # initialisation table des besoins horaires d'ECS [W]
#    tab_ECS=np.tile(np.insert(np.zeros(23),heure_arretchauffage,Eecs/3600.),365)
    if Eecs is None:
        energie_ECS = None
    else:
        energie_ECS = Eecs / 3600.
        
    # Aggregation
    # WARNING: not sure about behavior if n_steps_imm <= n_steps_agg, 
    data['use_aggregation'] = use_aggregation
    # Init: --> errors
    # n_steps_agg_default = 720 # hours in an aggregated block (60 days)
    # n_steps_imm_default = 1440 # min hours in short-term convol (30 days)
    # # Final
    # n_steps_agg_default = 360 # hours in an aggregated block (15 days)
    # n_steps_imm_default = 2880 # min hours in short-term convol (120 days)
    # New implementation
    n_steps_agg_default = 2880 # hours in an aggregated block (120 days)
    n_steps_imm_default = 2880 # min hours in short-term convol (120 days)
    data['n_steps_agg'] = data_in.get('n_steps_agg', n_steps_agg_default) 
    data['n_steps_imm'] = data_in.get('n_steps_imm', n_steps_imm_default)

    data['Text'] = Text
    data['tab_chauf'] = tab_chauf
    data['tab_ECS'] = tab_ECS
    data['tab_raf'] = tab_raf
    data['tab_AC'] = tab_AC
    data['debit_gth'] = debit_gth
    data['puissance_reduit'] = puissance_reduit
    data['T_coupure_PAC'] = T_coupure_PAC
    data['T_coupure_PAC_ECS'] = T_coupure_PAC_ECS
    data['Temission_minimum'] = Temission_minimum
    data['Temission1'] = Temission1
    data['Temission2'] = Temission2
    data['Text1'] = Text1
    data['Text2'] = Text2
    data['emitter_Texternal'] = emitter_Texternal
    data['emitter_Temission'] = emitter_Temission
    data['h_geocooling'] = h_geocooling
    data['Tfbi_min'] = Tfbi_min
    data['f_calc_Temission'] = f_calc_Temission
    data['Pc'] = Pc
    data['Pf'] = Pf
    data['Pa'] = Pa
    data['lambda_sol'] = lambda_sol
    data['rhocp_sol'] = rhocp_sol
    data['diffusivite_sol'] = diffusivite_sol
    data['p_predim_SGV_sol'] = p_predim_SGV_sol
    data['p_predim_mSGV_sol'] = p_predim_mSGV_sol
    data['energie_ECS'] = energie_ECS
    
    data['E_appoint'] = E_appoint
    data['P_appoint'] = P_appoint
    data['tab_appoint'] = tab_appoint
    data['Pmax_tot'] = Pmax_PAC + P_appoint
    data['heat_exchanger'] = data_in['heat_exchanger']
    
    data['scop_dim'] = data_in['scop_dim']
    
    data['Gbat'] = Gbat
    data['Sbat'] = Sbat
    data['hspbat'] = hspbat
    data['GSh'] = GSh
    data['chargesinternes'] = chargesinternes

    return data
