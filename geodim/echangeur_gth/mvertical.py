import math
import numpy as np
import numpy.fft as fft
from scipy import special
from scipy import integrate
try:
    import pathlib
    import sys
    CWD = pathlib.Path(__file__).parent
    cython_source_folder = CWD / 'cython_source_files'
    sys.path.append(cython_source_folder.as_posix())
    import mvertical_cython
    use_cython = True
except ImportError:
    use_cython = False


# Dimensions forage
zhaut = -1  # cote haut SGV [m]
zbas = -10  # cote bas SGV [m]
rb = 0.065  # rayon [m]

def build(data):

    # recup data
    Text = data['Text']
    diffusivite_sol = data['diffusivite_sol']
    lambda_sol = data['lambda_sol']
    Pf = data['Pf']
    p_predim_mSGV_sol = data['p_predim_mSGV_sol']   

    # caracteristiques mSGV
    H_unit = zhaut-zbas  # longueur unitaire mSGV [m]
    Rb = 0.1  # resistance thermique [m.K/W] (cas simple U)
    Rb /= H_unit  # resistance thermique [K/W]
    
    # parametres dimensionnement mSGV
    unite_dim = "micro-échangeurs verticaux"
    resolution_dim = 1
    # nombre mini et maxi de micro-échangeurs
    dim_min = resolution_dim  # nombre mini de mSGV
    dim_max = np.Inf  # nombre maxi de mSGV
    H_init = Pf(35,-3) / p_predim_mSGV_sol  # estimation hauteur totale mSGV 
    dim = int(math.ceil(H_init / H_unit))  # valeur depart dimensionnement
    
    # parametres simulation
    dt = 3600  # pas de temps [s]
    nb_annees = 2  # nombre d'annees
    nbdt1an = 3600*24*365/dt # nombre de pas de temps par an
    nbdt = int(nbdt1an*nb_annees)  # nombre de pas de temps total
    
    ####################
    # Calcul Tinit sol #
    ####################    
        
    def calc_Tinitsol(Text, a_sol, zhaut, zbas, H_unit_mSGV):
        omega = 2*math.pi/(365*24*3600)
        c_mult = (1-1j) / H_unit_mSGV * math.sqrt(a_sol/(2*omega))
        c_U = math.sqrt(omega / (2 * a_sol))
        U = fft.fft(Text)
        U[0] /= c_mult
        n = int(nbdt1an//2)
        Kpos = np.arange(1, n)
        Kneg = np.arange(n, nbdt1an)
        # Pos
        sqrtKpos = np.sqrt(Kpos)
        U[1:n] *= 1./sqrtKpos * (
                np.exp((1+1j) * zhaut * c_U * sqrtKpos)
                - np.exp((1+1j) * zbas * c_U * sqrtKpos)
                ) 
        # Neg
        # NB: Kneg-nbdt1an = (nbdt1an-Kneg)*exp(-1j*pi) but sqrt root different
        #     - sqrt(Kneg-nbdt1an) = root1
        #     - sqrt((nbdt1an-Kneg)*np.exp(-1j*math.pi)) = root2 = -1j * root1
        #     Using root2 enables keeping the formulation unchanged
        Kneg = (nbdt1an-Kneg)*np.exp(-1j*math.pi)
        sqrtKneg = np.sqrt(Kneg, dtype=complex)
        U[n:] *= 1./sqrtKneg * (
                np.exp((1+1j) * zhaut * c_U * sqrtKneg)
                - np.exp((1+1j) * zbas * c_U * sqrtKneg)
                )
        t_TNans = np.tile((c_mult*fft.ifft(U)).real,nb_annees)
        return np.concatenate((t_TNans,np.array([t_TNans[0]])))
        
    t_Tinitsol = calc_Tinitsol(Text, diffusivite_sol, zhaut, zbas, H_unit)

        
    ###############
    # Calcul Gfls #
    ###############

    if not use_cython:
        def ierf(x):
            return x*special.erf(x)-(1-np.exp(-x**2))/np.sqrt(np.pi)

        # fonction Claesson et Javed (2011)
        def Ycj(h,d):
            return 2*ierf(h) + 2*ierf(h+2*d) - ierf(2*(h+d)) - ierf(2*d)

        def f_mvert(y):
            return np.exp(-rb**2*y) * Ycj(H_unit*y,-zhaut*y) / (H_unit*y**2)
    else:
        # Augmentation des performances en utilisant la version compilée des
        # fonctions ci-dessus
        import ctypes
        from scipy import LowLevelCallable
        array_doubles = ctypes.c_double * 3
        user_data = array_doubles(rb, H_unit, zhaut)
        ptr = ctypes.cast(ctypes.pointer(user_data), ctypes.c_void_p)
        f_mvert = LowLevelCallable.from_cython(mvertical_cython, 'f_mvert', ptr)
        
    _ci = 1/np.sqrt(4*diffusivite_sol*dt) 

    def calc_Gfls(nbdt):
        """ Computing G = integral of f_mvert
        
        Analytical definition is: 
        G[i] = G(t=i*dt) = (    1/(4*pi*H_unit*lambda_sol)
                                * integral( f_mvert,
                                            _ci/np.sqrt(i),
                                            inf
                                            )
                                )
        
        NB:
        - G[i] gives G(t=i*Dt), hence G[0] = G(t=0)
        - numerical evaluation of integral is spared here by using the
          analytical expression of primitive and of f_horiz limit through infty
        
        """
        Gfls = np.empty(nbdt)
        Gfls[0] = 0.
        for i in range(1,nbdt):
            Gfls[i] = integrate.quad(f_mvert,_ci/np.sqrt(i),np.inf)[0]
        Gfls /= 4*np.pi*H_unit*lambda_sol
        return Gfls
        
    def calc_dGfls(nbdt):
        """ Computing G steps
        
        NB:
        - dG[i] = G[i+1]-G[i]
        - dG[0] = G(t=Dt) - G(t=0)
        - spares on integral evaluation times:
            o dG[i] is faster to compute than G[i]
            o G[i] = np.cumsum(dG[i])
        
        DEPRECATED since analytical solution of G is used (way faster).
        """
        dGfls = np.zeros(nbdt)
        # support=np.divide(_ci, range(nbdt+1)) not used to avoid warning print
        support = np.zeros(nbdt+1)
        support[0] = np.inf
        support[1:] = _ci / np.sqrt(np.arange(1, nbdt+1))
        dGfls[:] = [   integrate.quad(f_mvert, support[ix], x)[0]
                        for ix,x in enumerate(support[:-1], 1)
                        ]
        dGfls /= 4*np.pi*H_unit*lambda_sol
        return dGfls
        
    # Runs expect:
    # - G[0] = G(t=Dt); G[i] = G(t=(i+1)*Dt)
    # - dG[0] = G(t=2*Dt)-G(t=Dt); dG[i] = G(t=(i+2)*Dt)-G(t=(i+1)*Dt)
    t_dG_echangeur_gth = calc_dGfls(nbdt+1)  # pre-calcul de valeurs de G
    G0 = 0. # = G(t=0), ~integrate(f_mvert, np.inf, np.inf)
    t_G_echangeur_gth = np.cumsum(t_dG_echangeur_gth) + G0
    t_G_echangeur_gth = t_G_echangeur_gth[:-1]
    t_dG_echangeur_gth = t_dG_echangeur_gth[1:]
    
    # t_G_echangeur_gth_0 = calc_Gfls(nbdt+2)
    # t_dG_echangeur_gth_0 = np.diff(t_G_echangeur_gth_0)
    # t_G_echangeur_gth_0 = t_G_echangeur_gth_0[1:-1]
    # t_dG_echangeur_gth_0 = t_dG_echangeur_gth_0[1:]
    # assert np.allclose(t_dG_echangeur_gth_0, t_dG_echangeur_gth)
    # assert np.allclose(t_G_echangeur_gth_0, t_G_echangeur_gth)
    
    ########################
    # Calcul G_court_terme #
    ########################

    # Calcul court terme par modele source cylindrique infinie
    # Resolution par equation Bernier (2000)
    # y = taux de charge sur dernier pas de temps
    def G_courtterme(y):
        if y < 1e-6:
            return 0.0
        else:    
            _a0 = -0.89129
            _a1 = 0.36081
            _a2 = -0.05508
            _a3 = 3.59617e-3
            Fo = diffusivite_sol * y * dt / rb**2
            lgFo = math.log10(Fo)
            f_poly = _a0 + _a1*lgFo + _a2*lgFo**2 + _a3*lgFo**3
            return 10**f_poly / (lambda_sol*H_unit)
    
    egth = {}
    egth['Rb'] = Rb
    egth['t_G_echangeur_gth'] = t_G_echangeur_gth
    egth['t_dG_echangeur_gth'] = t_dG_echangeur_gth
    egth['dim_min'] = dim_min
    egth['dim_max'] = dim_max
    egth['dim'] = dim
    egth['unite_dim'] = unite_dim
    egth['t_Tinitsol'] = t_Tinitsol
    egth['G_courtterme'] = G_courtterme
    egth['resolution_dim'] = resolution_dim
    egth['nb_annees'] = nb_annees
    egth['nbdt'] = nbdt

    return egth