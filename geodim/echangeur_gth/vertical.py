import math
import numpy as np
import scipy as sp
from scipy import integrate
from scipy.special import expi
from ..models import data_sol

try:
    import pathlib
    import sys
    CWD = pathlib.Path(__file__).parent
    cython_source_folder = CWD / 'cython_source_files'
    sys.path.append(cython_source_folder.as_posix())
    import vertical_cython
    use_cython = True
except ImportError:
    use_cython = False

# Dimensions forage
rb = 0.08  # rayon [m]
Rb = 0.06  # resistance thermique [m.K/W] (cas double U)
H_unit = 100.  # longueur unitaire SGV [m]


def build(data):
    """ Building heat exchanger
    
    data: dict, heat exchanger parameters
    """

    # recup data
    Text = data['Text']
    diffusivite_sol = data['diffusivite_sol']
    lambda_sol = data['lambda_sol']
    Pf = data['Pf']
    p_predim_SGV_sol = data['p_predim_SGV_sol']
    
    # parametres dimensionnement SGV
    unite_dim = "mètres forés"
    resolution_dim = 1.
    dim_min = resolution_dim  # profondeur minimale de forage
    dim_max = np.Inf  # profondeur maximale de forage
    H_init = Pf(35,-3) / p_predim_SGV_sol  # estimation hauteur totale SGV
    dim = int(math.ceil(H_init))  # valeur depart dimensionnement
 
    # parametres simulation
    dt = 3600  # pas de temps [s]
    nb_annees = 5  # nombre d'annees
    nbdt1an = 3600*24*365/dt # nombre de pas de temps par an
    nbdt = int(nbdt1an*nb_annees)  # nombre de pas de temps total

    ####################
    # Calcul Tinit sol #
    ####################    
             
    t_Tinitsol = np.repeat(np.average(Text) + data_sol.grad_gth * H_unit/2, nbdt+1)    
    
    ###############
    # Calcul Gils #
    ###############
    
    if not use_cython:
        # fonction integrande SGV
        def f_sgv(x):
            return math.exp(-x**2)/x
    else:
        # Augmentation des performances en utilisant la version compilée des
        # fonctions ci-dessus
        # A revoir : peut-être plus nécessaire avec la calcul direct de la primitive?
        import ctypes
        from scipy import LowLevelCallable
        f_sgv = LowLevelCallable.from_cython(vertical_cython, 'f_sgv')
        
        
    def calc_Gils(lambda_sol, X_sgv, nbdt):
        """ Computing G = integral of f_sgv
        
        Analytical definition is: 
        G[i] = G(t=i*dt) = (    1/(2*pi*lambda)
                                * integral( f_sgv,
                                            X_sgv/np.sqrt(i),
                                            inf
                                            )
                                )
        
        NB:
        - G[i] gives G(t=i*Dt), hence G[0] = G(t=0)
        - numerical evaluation of integral is spared here by using the
          analytical expression of primitive and of f_horiz limit through infty
        
        """
        def f_sgv_primitive(x):
            return 0.5*expi(-x**2)
        prim_limit = 0.
        support = X_sgv / np.sqrt(np.arange(1, nbdt))
        Ghoriz = np.zeros(nbdt)
        Ghoriz[1:] += prim_limit-f_sgv_primitive(support)
        Ghoriz[1:] /= 2 * math.pi * lambda_sol
        return Ghoriz
        
    def calc_dGils(lambda_sol, X_sgv, nbdt):
        """ Computing G steps
        
        NB:
        - dG[i] = G[i+1]-G[i]
        - dG[0] = G(t=Dt) - G(t=0)
        - spares on integral evaluation times:
            o dG[i] is faster to compute than G[i]
            o G[i] = np.cumsum(dG[i])
        
        DEPRECATED since analytical solution of G is used (way faster).
        """
        dGils = np.zeros(nbdt)
        support = np.divide(X_sgv,
                            np.sqrt(np.arange(nbdt+1))
                            )  # divide = inf with den =0
        dGils[:] = [   integrate.quad(f_sgv, support[ix], x)[0]
                        for ix,x in enumerate(support[:-1], 1)
                        ]
        dGils /= 2 * math.pi * lambda_sol
        return dGils
        
    X_sgv = rb / (2*math.sqrt(diffusivite_sol*dt))
    # Runs expect:
    # - G[0] = G(t=Dt); G[i] = G(t=(i+1)*Dt)
    # - dG[0] = G(t=2*Dt)-G(t=Dt); dG[i] = G(t=(i+2)*Dt)-G(t=(i+1)*Dt)
    t_G_echangeur_gth = calc_Gils(lambda_sol, X_sgv, nbdt+2)
    t_dG_echangeur_gth = np.diff(t_G_echangeur_gth)
    t_dG_echangeur_gth = t_dG_echangeur_gth[1:]
    t_G_echangeur_gth = t_G_echangeur_gth[1:-1]
    
    # t_dG_echangeur_gth_0 = calc_dGils(lambda_sol, X_sgv, nbdt+1)
    # t_G_echangeur_gth_0 = np.cumsum(t_dG_echangeur_gth_0)[:-1]
    # t_dG_echangeur_gth_0 = t_dG_echangeur_gth_0[1:]
    # assert np.allclose(t_dG_echangeur_gth_0, t_dG_echangeur_gth)
    # assert np.allclose(t_G_echangeur_gth_0, t_G_echangeur_gth)
        
    ########################
    # Calcul G_court_terme #
    ########################

    # Calcul court terme par modele source cylindrique infinie
    # Resolution par equation Bernier (2000)
    # y = taux de charge sur dernier pas de temps
    def G_courtterme(y):
        if y < 1e-6:
            return 0.0
        else:    
            _a0 = -0.89129
            _a1 = 0.36081
            _a2 = -0.05508
            _a3 = 3.59617e-3
            Fo = diffusivite_sol * y * dt / rb**2
            lgFo = math.log10(Fo)
            f_poly = _a0 + _a1*lgFo + _a2*lgFo**2 + _a3*lgFo**3
            return 10**f_poly / lambda_sol
            
    egth = {}
    egth['Rb'] = Rb
    egth['t_G_echangeur_gth'] = t_G_echangeur_gth
    egth['t_dG_echangeur_gth'] = t_dG_echangeur_gth
    egth['dim_min'] = dim_min
    egth['dim_max'] = dim_max
    egth['dim'] = dim
    egth['unite_dim'] = unite_dim
    egth['t_Tinitsol'] = t_Tinitsol
    egth['G_courtterme'] = G_courtterme
    egth['resolution_dim'] = resolution_dim
    egth['nb_annees'] = nb_annees
    egth['nbdt'] = nbdt

    return egth