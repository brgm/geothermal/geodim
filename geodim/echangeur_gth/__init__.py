#coding: utf-8

""" Contains all heat exchanger types for individual house

Borehole heat exchangers field (dedicated to buildings) can be found in
submodule bhe_field.bhe_field

"""

from . import corbeille
from . import horizontal
from . import vertical
from . import mvertical

models = dict(
    (
        ('corbeille', corbeille),
        ('horizontal', horizontal),
        ('vertical', vertical),
        ('mvertical', mvertical),
        )
    )

def build(data, *args, **kwargs):
    nom_ech = data['heat_exchanger']
    if isinstance(nom_ech, int):
        nom_ech = list(models.keys())[nom_ech]
    return models[nom_ech].build(data, *args, **kwargs)