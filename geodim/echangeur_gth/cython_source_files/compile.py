"""
Compiling cython.
Run script or call "python compile.py build_ext --inplace"

See https://cython.readthedocs.io/en/latest/src/tutorial/cython_tutorial.html
"""

import setuptools # solves "error: Unable to find vcvarsall.bat" in Python 2.7
from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
    ext_modules=cythonize(  "*.pyx",
                            annotate=True # to see c/python interactions (.html)
                            ),
    include_dirs=[numpy.get_include()],
    script_args=['build_ext'],
    options={'build_ext':{'inplace':True}}
)
