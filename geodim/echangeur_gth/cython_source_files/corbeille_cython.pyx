#coding: utf8

""" Compilation C de fonctions liées à l'échangeur corbeille"""

import numpy as np
cimport numpy as np
from scipy.special.cython_special cimport erfc, i0
from libc.math cimport exp

cdef double SQRTPI = np.sqrt(np.pi)

# Fonctions
# NB: utilisation exp du module math plus rapide que celle de numpy
# peut etre car celle de numpy est vectorisée mais appellée sur un scalaire?
cdef api double ierfc(double z):
    return exp(-z**2)/SQRTPI - z*erfc(z)

cdef api double f_corb(double y, void *user_data):
    # Lecture paramètres
    cdef double diffusivite_sol = (<double *>user_data)[0]
    cdef double h = (<double *>user_data)[1]
    cdef double dh = (<double *>user_data)[2]
    cdef double db = (<double *>user_data)[3]
    cdef double zhaut = (<double *>user_data)[4]
    cdef double coefref = (<double *>user_data)[5]
    # Calcul
    cdef double r2 = (0.5*(dh+db)/2)**2
    cdef double zbas = zhaut - h
    cdef double ay = diffusivite_sol*y
    cdef double r4ay = np.sqrt(4*ay)
    cdef double ry = np.sqrt(y)
    cdef double res = i0(r2/(2*ay))*exp(-r2/(2*ay))/ry
    res *= ( ierfc(-h/r4ay) + ierfc(h/r4ay) - 2/SQRTPI
            + coefref * (ierfc(-2*zhaut/r4ay) + ierfc(-2*zbas/r4ay)
                         - 2*ierfc((-zbas-zhaut)/r4ay)
                         )
            )
    return res
    
# # Le calcul direct ci-dessous semble légèrement moins performant
# from scipy.integrate import quad
#     
# def f_corb_no_api(double y, np.ndarray user_data):
#     # Lecture paramètres
#     cdef double diffusivite_sol = user_data[2]
#     cdef double h = user_data[3]
#     cdef double dh = user_data[4]
#     cdef double db = user_data[5]
#     cdef double zhaut = user_data[6]
#     cdef double coefref = user_data[7]
#     # Calcul
#     cdef double r2 = (0.5*(dh+db)/2)**2
#     cdef double zbas = zhaut - h
#     cdef double ay = diffusivite_sol*y
#     cdef double r4ay = np.sqrt(4*ay)
#     cdef double ry = np.sqrt(y)
#     cdef double res = i0(r2/(2*ay))*exp(-r2/(2*ay))/ry
#     res *= ( ierfc(-h/r4ay) + ierfc(h/r4ay) - 2/SQRTPI
#             + coefref * (ierfc(-2*zhaut/r4ay) + ierfc(-2*zbas/r4ay)
#                          - 2*ierfc((-zbas-zhaut)/r4ay)
#                          )
#             )
#     return res
#     
# def calc_Gcorb(int nbdt, float dt, np.ndarray user_output, np.ndarray user_data):
#     # Lecture paramètres
#     cdef double lambda_sol = user_data[0]
#     cdef double rhocp_sol = user_data[1]
#     cdef double h = user_data[3]
#     cdef double alpha = 4*np.pi*h**2*np.sqrt(lambda_sol*rhocp_sol)
#     # Calcul
#     user_output[0] = 0.
#     cdef int i
#     for i in range(1, nbdt):
#         user_output[i] = quad(f_corb_no_api, i*dt, (i+1)*dt, args=user_data)[0]/alpha