#coding: utf8

""" Compilation C de fonctions liées à l'échangeur micro-vertical"""

import numpy as np
cimport numpy as np
from scipy.special.cython_special cimport erf
from libc.math cimport exp

cdef double SQRTPI = np.sqrt(np.pi)

# Fonctions
# NB: utilisation exp du module math plus rapide que celle de numpy
# peut etre car celle de numpy est vectorisée mais appellée sur un scalaire?
cdef api double ierf(double x):
    return x*erf(x)-(1-exp(-x**2))/SQRTPI

cdef api double Ycj(double h, double d):
    return 2*ierf(h) + 2*ierf(h+2*d) - ierf(2*(h+d)) - ierf(2*d)

cdef api double f_mvert(double y, void *user_data):
    # Lecture paramètres
    cdef double rb = (<double *>user_data)[0]
    cdef double H_unit = (<double *>user_data)[1]
    cdef double zhaut = (<double *>user_data)[2]
    return exp(-rb**2*y) * Ycj(H_unit*y,-zhaut*y) / (H_unit*y**2)