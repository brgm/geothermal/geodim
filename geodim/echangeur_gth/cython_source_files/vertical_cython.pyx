#coding: utf8

""" Compilation C de fonctions liées à l'échangeur vertical"""

import numpy as np
cimport numpy as np
from libc.math cimport exp

# Fonctions
# NB: utilisation exp du module math plus rapide que celle de numpy
# peut etre car celle de numpy est vectorisée mais appellée sur un scalaire?
cdef api double f_sgv(double x):
        return exp(-x**2)/x