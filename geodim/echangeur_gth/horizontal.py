""" Shallow horizontal heat exchanger depicted by N parallel tubes

The ntubes are equally distanced.

Note
----
- the final dimension is the total heat-exchanger (HE) surface
- as a first approximation, a square surface is forced
- the surface is thus conditioned by the number of tubes and their spacing:
  S = (Ntubes * step_tubes)**2
- surface can only take a fixed number of values and dimensioning is not linear
  anymore (i.e., the dimension step is not fixed)
- to keep a linear dimensioning, the dimensioning parameter becomes the number
  of tubes

"""

import math
import numpy as np
import numpy.fft as fft
from scipy.special import exp1

from ..models import params_base

use_cython = False # Unnecessary so far

# Dimensions echangeur horizontal
pastube = 0.5  # Pas entre 2 tubes [m]
zhoriz = -1.0  # cote d'enfouissement, negative [m]
p_predim_horiz = 30.  # ratio de dimensionnement initial [W/m2]   

# Caracteristiques tube et ecoulement   
DN = 20.  # diametre nominal [mm]
SDR = 11.  # DN/epaisseur
lambda_PE = 0.45  # conductivite thermique du PE [W/m/K]
Nu = 4.36  # Nusselt d'un ecoulement laminaire

def get_Rb(Lsurf, rtue, rtui, lambda_PE, lambda_water, Nu):
    """
    Computing thermal resistance per area
    
    Parameters
    ----------
    Lsurf: float,
        Length of a tube per area [m/m2]
    rtue: float,
        Tube external radius [m]
    rtui: float,
        Hydraulic radius [m]
    lambda_PE: float,
        Tube material (=PE) thermal conductivity [W/m/K]
    lambda_water: float,
        Water thermal conductivity [W/m/K]
    Nu: float,
        Nusselt number [-]
    
    Returns
    -------
    out: float,
        Fluid+tube thermal resistance per area [m2.K/W]
    
    """
    # Tube thermal resistance [K/W] (actually, [m2.K/W])
    rthpe = math.log(rtue/rtui)/(lambda_PE*2*math.pi*Lsurf)
    # Flow thermal resistance [K/W] (actually, [m2.K/W])
    rthflow = 1./(Nu*lambda_water*math.pi*Lsurf)
    return rthpe + rthflow
    
def get_tube_length(ntubes, step_tubes):
    """
    Computing total tube length
    
    A square surface and a finite number of tubes are assumed.
    Also, lengths of tube bends are ignored.
    
    Parameters
    ----------
    ntubes: int,
        Number of parallel tubes
    step_tubes: float,
        Regular spacing between tubes [m]
    
    Returns
    -------
    out: float,
        Total tube length [m2]
    
    """
    return ntubes * (ntubes-1) * step_tubes
    
def get_area(ntubes, step_tubes):
    """
    Computing heat-echanger surface
    
    A square surface and a finite number of tubes are assumed
    
    Parameters
    ----------
    ntubes: int,
        Number of parallel tubes
    step_tubes: float,
        Regular spacing between tubes [m]
    
    Returns
    -------
    out: float,
        Heat-exchanger surface [m2]
    
    """
    return ((ntubes-1) * step_tubes)**2
    
def get_dimensions(area, step_tubes):
    # ntubes for a square area
    ntubes = math.sqrt(area)/step_tubes + 1
    ntubes = max(int(ntubes), 2)
    # Side perpendicular to tubes
    width = (ntubes-1)*step_tubes
    # Side parallel to tubes
    length = area/width
    return length, width, ntubes
    
def get_ntubes(area, step_tubes):
    """
    Getting number of tubes in HE
    
    Note
    ----
    - we assume that surface is as close to a square as possible
      Surface conditioned by its side perpendicular to tubes = (Ntubes-1)*step.
    - if a square cannot be found, the side parallel to tubes is adjusted
    
    Returns
    -------
    out: int,
        The number of tubes in HE
    """
    ntubes = math.sqrt(area)/step_tubes + 1
    return max(int(ntubes), 2)
    

def build(data):

    # recup data
    Text = data['Text']
    diffusivite_sol = data['diffusivite_sol']
    lambda_sol = data['lambda_sol']
    rhocp_sol = data['rhocp_sol']
    Pf = data['Pf']
    depth = -zhoriz
    step_tubes = pastube

    # Parametres tube et ecoulement
    eptu = DN/(1000.*SDR)  # epaisseur tube [m]
    rtue = DN/2000.  # rayon exterieur tube [m]
    rtui = rtue - eptu  # rayon hydraulique tube [m]
    
    # parametres dimensionnement echangeur horizontal
    unite_dim = "m2 d'échangeur horizontal"
    resolution_dim = 5
    S_square_min = step_tubes**2
    dim_min = max(S_square_min, resolution_dim)
    dim_max = np.Inf
    S_init = Pf(35,-3) / p_predim_horiz
    dim = int(math.ceil(S_init)) # initial guess
    
    # parametres simulation
    dt = 3600  # pas de temps [s]
    nb_annees = 2  # nombre d'annees
    nbdt1an = 3600*24*365/dt # nombre de pas de temps par an
    nbdt = int(nbdt1an*nb_annees)  # nombre de pas de temps total
    coefref = -1  # condition isotherme en surface du sol

    def calc_Tinitsol(Text, a_sol, zhoriz):
        omega = 2*math.pi/(365*24*3600)
        c_U = math.sqrt(omega / (2 * a_sol))
        U = fft.fft(Text)
        n = int(nbdt1an//2)
        Kpos = np.arange(n)
        Kneg = np.arange(n, nbdt1an)
        # Pos
        sqrtKpos = np.sqrt(Kpos)
        U[:n] *= np.exp((1+1j)*zhoriz*c_U*np.sqrt(sqrtKpos))
        # Neg
        # NB: Kneg-nbdt1an = (nbdt1an-Kneg)*exp(-1j*pi) but sqrt root different
        #     - sqrt(Kneg-nbdt1an) = root1
        #     - sqrt((nbdt1an-Kneg)*np.exp(-1j*math.pi)) = root2 = -1j * root1
        #     Using root2 enables keeping the formulation unchanged
        Kneg = (nbdt1an-Kneg)*np.exp(-1j*math.pi)
        sqrtKneg = np.sqrt(Kneg, dtype=complex)
        U[n:] *= np.exp((1+1j)*zhoriz*c_U*np.sqrt(sqrtKneg))
        t_TNans = np.tile(fft.ifft(U).real,nb_annees)
        return np.concatenate((t_TNans,np.array([t_TNans[0]])))
        
    t_Tinitsol = calc_Tinitsol(Text, diffusivite_sol, zhoriz)
    
    ##############
    # G function #
    ##############
    
    def G_self(times, rext, diffusivity=None, depth=None, adim=False):
        """
        Computing tube self-response for shallow horizontal heat-exchanger
        
        Self-response is based on adimensioned variables:
        - Fourier times
        - length / depth
        
        Parameters
        ----------
        times: float/np.ndarray,
            Times at which to evaluate the self-response function [s]
            If adim is True, provide Fourier times [-]
        rext: float,
            Tube external radius [m]
            If adim, provide actual radius divided by depth of tube [-]
        diffusivity: float,
            Ground diffusivity [m2/s]
            If adim, not necessary
        depth: float,
            Depth of tubes [m]
            If adim, not necessary
        adim: bool,
            If True, parameters are already adimensioned.
            
        Returns
        -------
        out: float/np.ndarray,
            Self-response at desired times
        
        """
        if not adim:
            times = times*diffusivity/depth**2
            rext = rext/depth
        inv_times = np.divide(1., times)
        fact1 = (rext**2)/4.*inv_times
        fact2 = inv_times
        return (exp1(fact1) - exp1(fact2))/(4*np.pi)
        
    def G_interaction(times, distances, diffusivity=None, depth=None, adim=False):
        """
        Computing tube interaction for shallow horizontal heat-exchanger (HE)
        
        Function is based on adimensioned variables:
        - Fourier times
        - length / depth
        
        Parameters
        ----------
        times: float/np.ndarray,
            Times at which to evaluate the interaction response function [s]
            If adim is True, provide Fourier times [-]
        distances: float/np.ndarray,
            Distance between tubes [m]
            If adim, provide actual radius divided by depth of HE [-]
        diffusivity: float,
            Ground diffusivity [m2/s]
            If adim, not necessary
        depth: float,
            Depth of tubes [m]
            If adim, not necessary
        adim: bool,
            If True, parameters are already adimensioned.
            
        Returns
        -------
        out: float/np.ndarray,
            Interaction function at desired times and distances
            
        Notes
        -----
        Broadcasting applies:
         __________________________________________________
        | times | distances | G shape                      |
        |_______|___________|______________________________|
        | array |   float   | (len(times), )               |
        |_______|___________|______________________________|
        | float |   array   | (len(distances), )           |
        |_______|___________|______________________________|
        | array |   array   | (len(distances), len(times)) |
        |_______|___________|______________________________|
        
        """
        if not adim:
            times = times*diffusivity/depth**2
            distances = distances/depth
        dist2 = distances**2
        use_broadcasting = (
            isinstance(times, np.ndarray)
            and isinstance(dist2, np.ndarray)
            )
        if use_broadcasting:
            dist2 = dist2[:,np.newaxis]
        dist_mirror2 = dist2 + 4
        inv_times = 1./times#np.divide(1., times)
        fact1 = dist2/4.*inv_times
        fact2 = dist_mirror2/4.*inv_times
        return (exp1(fact1) - exp1(fact2))/(4*np.pi)
        
    def G_global(
        times, rext, step, ntubes, diffusivity=None, depth=None, adim=False
        ):
        """
        Computing global response for shallow horizontal heat-exchanger (HE)
        
        The heat-exchanger is assumed to be composed of equally spaced tubes
        buried at the same depth.
        
        Global response is based on adimensioned variables:
        
        - Fourier times
        - length / depth
        
        Parameters
        ----------
        times: float/np.ndarray,
            Times at which to evaluate the global response function [s]
            
            If adim is True, provide Fourier times [-]
        rext: float,
            Tube external radius [m]
            
            If adim, provide actual radius divided by depth HE [-]
        step: float,
            Step between tubes [m]
            
            If adim, provide actual step by depth of HE [-]
        ntubes: int,
            Number of tubes in HE [-]
        diffusivity: float,
            Ground diffusivity [m2/s]
            
            If adim, not necessary
        depth: float,
            Depth of tubes [m]
            If adim, not necessary
        adim: bool,
            If True, parameters are already adimensioned.
            
        Returns
        -------
        out: float/np.ndarray,
            Self-response at desired times
        
        """
        Gself =  G_self(times, rext, diffusivity, depth, adim)
        ninteractions = np.arange(1, ntubes)
        dists = ninteractions * step
        Ginter = G_interaction(times, dists, diffusivity, depth, adim)
        Gglob = Gself + 2*np.dot(Ginter.T, ninteractions[::-1])/ntubes
        return Gglob
    
    def calc_Ghoriz(ntubes, diffusivity, depth, rext, step, dt, nbdt):
        """
        Computing G global on different timesteps
        
        Note
        ----
        G[i] gives G(t=i*Dt), hence G[0] = G(t=0)
        
        """
        times = np.arange(1, nbdt) * dt
        fos = diffusivity/depth**2 * times
        rext_adim = rext / depth
        step_adim = step / depth
        Ghoriz = np.zeros(nbdt)
        Ghoriz[1:] = (
            G_global(fos, rext_adim, step_adim, ntubes, adim=True)
            *
            (1-1./ntubes)*step/lambda_sol
            )
        return Ghoriz
    
    ########################
    # Calcul G_court_terme #
    ########################

    # Calcul court terme par modele source cylindrique infinie
    # Resolution par equation Bernier (2000)
    # y = taux de charge sur dernier pas de temps
    def G_courtterme(y):
        _ci = math.pi*lambda_sol*rhocp_sol
        return np.sqrt(dt) * (1-np.sqrt(1-y)) / math.sqrt(_ci)
        
    ####################
    # Dimension update #
    ####################
    def update_Rb(dim):
        ntubes = get_ntubes(dim, step_tubes=step_tubes)
        Lsurf = 1./((1-1./ntubes)*step_tubes) # m/m2
        return get_Rb(
            Lsurf, rtue=rtue, rtui=rtui, lambda_PE=lambda_PE,
            lambda_water=params_base.lambda_eau_g, Nu=Nu
            )
    def update_Ghoriz(dim):
        ntubes = get_ntubes(dim, step_tubes=step_tubes)
        # Runs expect:
        # - G[0] = G(t=Dt); G[i] = G(t=(i+1)*Dt)
        # - dG[0] = G(t=2*Dt)-G(t=Dt); dG[i] = G(t=(i+2)*Dt)-G(t=(i+1)*Dt)
        t_G_echangeur_gth = calc_Ghoriz(
            ntubes, diffusivity=diffusivite_sol, depth=depth, rext=rtue,
            step=step_tubes, dt=dt, nbdt=nbdt+2
            )
        t_dG_echangeur_gth = np.diff(t_G_echangeur_gth)
        t_dG_echangeur_gth = t_dG_echangeur_gth[1:]
        t_G_echangeur_gth = t_G_echangeur_gth[1:-1]
        return t_G_echangeur_gth, t_dG_echangeur_gth
    def update_heat_exchanger(dim):
        Rb = update_Rb(dim)
        t_G, t_dG = update_Ghoriz(dim)
        return Rb, t_G, t_dG
        
    Rb, t_G_echangeur_gth, t_dG_echangeur_gth = update_heat_exchanger(dim)
    
    egth = {}
    egth['Rb'] = Rb
    egth['t_G_echangeur_gth'] = t_G_echangeur_gth
    egth['t_dG_echangeur_gth'] = t_dG_echangeur_gth
    egth['dim_min'] = dim_min
    egth['dim_max'] = dim_max
    egth['dim'] = dim
    egth['unite_dim'] = unite_dim
    egth['t_Tinitsol'] = t_Tinitsol
    egth['G_courtterme'] = G_courtterme
    egth['resolution_dim'] = resolution_dim
    egth['nb_annees'] = nb_annees
    egth['nbdt'] = nbdt
    
    egth['update_heat_exchanger'] = update_heat_exchanger
    egth['get_dimensions'] = lambda area: get_dimensions(area, step_tubes=step_tubes)

    return egth