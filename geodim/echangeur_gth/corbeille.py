""" Spiral-coil heat exchanger (Terrendis) """

import math
import numpy as np
import numpy.fft as fft
from scipy.special import i0
from scipy.special import erfc
from scipy import integrate
from ..models import params_base 
try:
    import pathlib
    import sys
    CWD = pathlib.Path(__file__).parent
    cython_source_folder = CWD / 'cython_source_files'
    sys.path.append(cython_source_folder.as_posix())
    import corbeille_cython
    use_cython = True
except ImportError:
    use_cython = False


properties = dict(
    # Dimensions corbeille geothermique (nouveau modele)
    H = 2.7,                # hauteur [m]
    Dh = 1.18,              # diametre haut corbeille
    Db = 1.10,              # diametre bas corbeille
    p = 0.13,               # pas espacement spires [m]
    zhaut = -1.,            # cote haut corbeille [m]
    p_predim_corb = 1000.,  # ratio de dimensionnement initial [W/corbeille]   

    # Caracteristiques tube et ecoulement (nouveau modele)   
    DN = 25.,               # diametre nominal [mm]
    SDR = 11.,              # DN/epaisseur
    lambda_PE = 0.45,       # conductivite thermique du PE [W/m/K]
    Nu = 4.36*2,            # Nusselt ecoulement laminaire double (rainurage)
    unite_dim='corbeilles'
    )

def build(data):
    H = properties['H']
    Dh = properties['Dh']
    Db = properties['Db']
    R = 0.5*(Dh+Db)/2 # rayon moyen [m]
    p = properties['p']
    zhaut = properties['zhaut']
    p_predim_corb = properties['p_predim_corb']
    DN = properties['DN']
    SDR = properties['SDR']
    lambda_PE = properties['lambda_PE']
    Nu = properties['Nu']
    unite_dim = properties['unite_dim']
    # recup data
    Text = data['Text']
    diffusivite_sol = data['diffusivite_sol']
    lambda_sol = data['lambda_sol']
    rhocp_sol = data['rhocp_sol']
    Pf = data['Pf']

    # Parametres tube et ecoulement
    eptu = DN/(1000.*SDR)  # epaisseur tube [m]
    rtue = DN/2000.  # rayon exterieur tube [m]
    rtui = rtue - eptu  # rayon hydraulique tube [m]
    zbas = zhaut - H  # cote bas corbeille [m]
    Ltot = H*math.sqrt(1+(2*math.pi*R/p)**2)  # longueur spiralee corbeille [m]
    # resistance paroi [K/W]
    rthpe = math.log(rtue/rtui)/(lambda_PE*2*math.pi*Ltot)
    # resistance ecoulement [K/W]
    rthecoulement = 1/(Nu*params_base.lambda_eau_g*math.pi*Ltot)
    # resistance totale fluide paroi
    Rb = rthpe + rthecoulement

    # parametres dimensionnement corbeille geothermique
    resolution_dim = 1
    dim_min = resolution_dim  # surface mini echangeur
    dim_max = np.Inf  # surface maxi echangeur
    N_init = Pf(35,-3) / p_predim_corb  # estimation nombre corbeilles 
    dim = int(math.ceil(N_init))  # valeur depart dimensionnement
    
    # parametres simulation
    dt = 3600  # pas de temps [s]
    nb_annees = 2  # nombre d'annees
    nbdt1an = 3600*24*365/dt # nombre de pas de temps par an
    nbdt = int(nbdt1an*nb_annees)  # nombre de pas de temps total
    coefref = -1  # condition isotherme en surface du sol
    
    ####################
    # Calcul Tinit sol #
    ####################    
    
    def calc_Tinitsol(Text, a_sol, zhaut, zbas, H):
        omega = 2*math.pi/(365*24*3600)
        c_mult = (1-1j) / H * math.sqrt(a_sol/(2*omega))
        c_U = math.sqrt(omega / (2 * a_sol))
        U = fft.fft(Text)
        U[0] /= c_mult
        n = int(nbdt1an//2)
        Kpos = np.arange(1, n)
        Kneg = np.arange(n, nbdt1an)
        # Pos
        U[1:n] *= 1./np.sqrt(Kpos) * (
                np.exp((1+1j) * zhaut * c_U * np.sqrt(Kpos))               
                - np.exp((1+1j) * zbas * c_U * np.sqrt(Kpos))
                ) 
        # Neg
        # NB: Kneg-nbdt1an = (nbdt1an-Kneg)*exp(-1j*pi) but sqrt root different
        #     - sqrt(Kneg-nbdt1an) = root1
        #     - sqrt((nbdt1an-Kneg)*np.exp(-1j*math.pi)) = root2 = -1j * root1
        #     Using root2 enables keeping the formulation unchanged
        Kneg = (nbdt1an-Kneg)*np.exp(-1j*math.pi)
        sqrtKneg = np.sqrt(Kneg, dtype=complex)
        U[n:] *= 1./sqrtKneg * (
                np.exp((1+1j) * zhaut * c_U * sqrtKneg)
                - np.exp((1+1j) * zbas * c_U * sqrtKneg)
                )
        t_TNans = np.tile((c_mult*fft.ifft(U)).real,nb_annees)
        return np.concatenate((t_TNans,np.array([t_TNans[0]])))
    
    t_Tinitsol = calc_Tinitsol(Text, diffusivite_sol, zhaut, zbas, H)
      
    ################
    # Calcul Gcorb #
    ################
  
    if not use_cython:
        def ierfc(z):
            return np.exp(-z**2)/np.sqrt(np.pi)-z*erfc(z)

        # fonction X. Moch (2015)
        def f_corb(y):
            a_sol = diffusivite_sol
            _ay = a_sol*y
            _r4ay = np.sqrt(4*_ay)
            res = i0(R**2/(2*_ay))*np.exp(-R**2/(2*_ay))/np.sqrt(y)
            res *= ( ierfc(-H/_r4ay) + ierfc(H/_r4ay) - 2/np.sqrt(np.pi)
                    + coefref * (ierfc(-2*zhaut/_r4ay) + ierfc(-2*zbas/_r4ay)
                                 - 2*ierfc((-zbas-zhaut)/_r4ay))
                    )
            return res
    else:
        # Augmentation des performances en utilisant la version compilée des
        # fonctions ci-dessus
        import ctypes
        from scipy import LowLevelCallable
        array_doubles = ctypes.c_double * 6
        user_data = array_doubles(diffusivite_sol, H, Dh, Db, zhaut, coefref)
        ptr = ctypes.cast(ctypes.pointer(user_data), ctypes.c_void_p)
        f_corb = LowLevelCallable.from_cython(corbeille_cython, 'f_corb', ptr)
        
    def calc_Gcorb(nbdt):
        """ Computing G = integral of f_corb
        
        WARNING: not sure about integration bounds.
        Wait for correction
        Use calc_Gcorb_init in teh first place
        
        Analytical definition is: 
        G[i] = G(t=i*dt) = (    1/(4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol))
                                * integral( f_corb,
                                            0,
                                            i*dt
                                            )
                                )
        
        NB:
        - G[i] gives G(t=i*Dt), hence G[0] = G(t=0)
        - numerical evaluation of integral is spared here by using the
          analytical expression of primitive and of f_horiz limit through infty
        
        """
        Gcorb = np.zeros(nbdt)
        # G(t=0) = 0, ~integrate(f,0,0)
        support = np.arange(1, nbdt)*dt
        Gcorb[1:] = [integrate.quad(f_corb,0, x)[0] for x in support]
        Gcorb /= 4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol)
        return Gcorb
        
    def calc_dGcorb(nbdt):
        """ Computing G steps
        
        WARNING: not sure about value of G(t=0)
        Wait for correction
        Use calc_Gcorb_init in teh first place
        
        NB:
        - dG[i] = G[i+1]-G[i]
        - dG[0] = G(t=Dt) - G(t=0)
        - spares on integral evaluation times:
            o dG[i] is faster to compute than G[i]
            o G[i] = np.cumsum(dG[i])
        
        DEPRECATED since analytical solution of G is used (way faster).
        """
        dGcorb = np.zeros(nbdt)
        support = np.arange(nbdt+1)*dt
        dGcorb[:] = [   integrate.quad(f_corb, x, support[ix])[0]
                        for ix,x in enumerate(support[:-1], 1)
                        ]
        dGcorb /= 4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol)
        return dGcorb
        
        
    def calc_Gcorb_init(nbdt):
        """ Initial function for computing G steps
        """
        Gcorb = np.empty(nbdt)
        Gcorb [0] = 0. # true?
        for i in range(1,nbdt):
            Gcorb[i] = integrate.quad(f_corb,i*dt,(i+1)*dt)[0]
        Gcorb /= 4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol)
        return Gcorb
        
    # G0 to be confirmed, waiting for formula of integral
    t_dG_echangeur_gth = calc_Gcorb_init(nbdt+1)
    G0 = 0. # True?
    t_G_echangeur_gth = np.cumsum(t_dG_echangeur_gth) + G0
    t_G_echangeur_gth = t_G_echangeur_gth[:-1]
    t_dG_echangeur_gth = t_dG_echangeur_gth[1:]
    
    # import time
    # t0 = time.time()
    # # Runs expect:
    # # - G[0] = G(t=Dt); G[i] = G(t=(i+1)*Dt)
    # # - dG[0] = G(t=2*Dt)-G(t=Dt); dG[i] = G(t=(i+2)*Dt)-G(t=(i+1)*Dt)
    # t_dG_echangeur_gth = calc_dGcorb(nbdt+1)  # pre-calcul de valeurs de G
    # G0 = calc_Gcorb(1)[0] # =G(t=0)
    # print('dG init', t_dG_echangeur_gth)
    # print('G0', G0)
    # t_G_echangeur_gth = np.cumsum(t_dG_echangeur_gth) + G0
    # t_G_echangeur_gth = t_G_echangeur_gth[:-1]
    # t_dG_echangeur_gth = t_dG_echangeur_gth[1:]
    # print('dG', time.time()-t0)
    
    # import time
    # t0 = time.time()
    # t_G_echangeur_gth_0 = calc_Gcorb(nbdt+2)
    # t_dG_echangeur_gth_0 = np.diff(t_G_echangeur_gth_0)
    # t_G_echangeur_gth_0 = t_G_echangeur_gth_0[1:-1]
    # t_dG_echangeur_gth_0 = t_dG_echangeur_gth_0[1:]
    # print('dG', time.time()-t0)
    
    
    # g_dt = integrate.quad(f_corb,dt,np.inf)[0] / (4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol) )
    # g_2dt = integrate.quad(f_corb,2*dt,np.inf)[0] / (4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol) )
    # g_ndt = integrate.quad(f_corb,nbdt*dt,np.inf)[0] / (4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol) )
    # g_np1dt = integrate.quad(f_corb,(nbdt+1)*dt,np.inf)[0] / (4*np.pi*H**2*np.sqrt(lambda_sol*rhocp_sol) )
    # print('G build time', time.time()-t0)
    # print()
    # print('G', t_G_echangeur_gth)
    # print('dG', t_dG_echangeur_gth)
    # print('G(Dt)', g_dt)
    # print('G(2xDt)', g_2dt)
    # print('G(nxDt)', g_ndt)
    # print('G((n+1)xDt)', g_np1dt)
    # print('DG(Dt)=G(2xDt)-G(Dt)', g_2dt-g_dt)
    # print('G(nDtt)', g_ndt)
    # print('G((n+1)xDt)', g_np1dt)
    # print('DG(nDt)=G((n+1)xDt)-G(nDt)', g_np1dt-g_ndt)
    
    # assert np.allclose(t_dG_echangeur_gth_0, t_dG_echangeur_gth)
    # assert np.allclose(t_G_echangeur_gth_0, t_G_echangeur_gth)
    
    # 1./0
    
    ########################
    # Calcul G_court_terme #
    ########################

    # Calcul court terme par modele source cylindrique infinie
    # Resolution par solution Moch (2005)
    # y = taux de charge sur dernier pas de temps
    def G_courtterme(y):
        _la = lambda_sol*rhocp_sol
        return (1-np.sqrt(1-y)) * np.sqrt(dt) / (2*np.pi**1.5*H*R*np.sqrt(_la))
    
    egth = {}
    egth['Rb'] = Rb
    egth['t_G_echangeur_gth'] = t_G_echangeur_gth
    egth['t_dG_echangeur_gth'] = t_dG_echangeur_gth
    egth['dim_min'] = dim_min
    egth['dim_max'] = dim_max
    egth['dim'] = dim
    egth['unite_dim'] = unite_dim
    egth['t_Tinitsol'] = t_Tinitsol
    egth['G_courtterme'] = G_courtterme
    egth['resolution_dim'] = resolution_dim
    egth['nb_annees'] = nb_annees
    egth['nbdt'] = nbdt
    

    return egth

# # Dimensions corbeille geothermique (ancien modele)
# H = 2.7  # hauteur [m]
# R = 0.5  # rayon moyen [m]
# p = 0.08  # pas espacement spires [m]
# zhaut = -1.  # cote haut corbeille [m]
# p_predim_corb = 1000.  # ratio de dimensionnement initial [W/corbeille]     

# # Caracteristiques tube et ecoulement (ancien modele)   
# DN = 25.  # diametre nominal [mm]
# SDR = 11.  # DN/epaisseur
# lambda_PE = 0.45  # conductivite thermique du PE [W/m/K]
# Nu = 4.36  # Nusselt d'un ecoulement laminaire
