# geodim

## About
**geodim** is intended to help dimensioning heat exchangers to fulfill some energy needs in a building.

The library comes with a variety of items gathered together to form so called *systems* (heat exchanger, heatpump, building...).

**geodim** is the physics core behind the heat exchanger dimensioning tool accessible on the French geological survey (BRGM) [website](https://plateforme-geothermie.brgm.fr/fr) (access restricted to attendees to the dedicated training course).

⚠ **geodim** is still at an early stage, and is meant to be restructured soon.

## Installation (TODO: update repo url)
So far, **geodim** only comes as a package that must be imported directly, neither wheel (`pip`, `conda`) nor setup files are available

You can either download the files directly from the [repo](repo_url), or clone it using git:
```shell
cd '%whatever_path_on_your_computer%'
git clone repo_url
```

Next thing is to set up your Python to be compatiple with the lib.
Creating an environment is recommended using, *e.g.*, [`conda`](https://docs.anaconda.com/free/miniconda/) :
```shell
conda create -n geodim python
conda activate geodim
pip install -r '%whatever_path_on_your_computer%/geodim/requirements.txt'
```

Finally, and because **geodim** is not actually installed, you will have to add the path to the lib in your Python scripts for Python being able to deal with it:
```python
import sys
geodim_dir = '%whatever_path_on_your_computer%/geodim'
sys.path.append(geodim_dir)
import geodim
```

### Optional stage: compiling cython files
The costliest operation in the modeled systems is the ground temperature update (*o(n²)* time complexity,  where *n* is the number of timesteps).

Building the heat-exchangers can also be a bit time-consuming, especially the spiral one (*corbeille*, in French).

Both these steps are alleviated using [`cython`](https://cython.readthedocs.io/en/stable/index.html) compilations.

For the `cython`compilation, you will need compilers (thank you, captain Obvious...):
- Linux: native `gcc`
- Windows: [`Visual Studio`](https://visualstudio.microsoft.com/fr/vs/features/cplusplus/) or [`MinGW`](https://www.mingw-w64.org/), see [this help](https://wiki.python.org/moin/WindowsCompilers) for Visual Studio

For compiling, open a terminal (when using `conda`, remember activating your environment first):
```
cd '%whatever_path_on_your_computer%/geodim/calcul/cython_source_files'
python compile.py build_ext --inplace
```

And same operation for the heat exchangers:
```
cd '%whatever_path_on_your_computer%/geodim/echangeur_gth/cython_source_files'
python compile.py build_ext --inplace
```

And... voilà!

More info on performance optimisation can be found in section 2 of public report [BRGM/RP-69797-FR](http://infoterre.brgm.fr/rapports/RP-69797-FR.pdf) (in French).

## Credits and acknowledgment
The **geodim** is an ever evolving project whose development is publicly funded from both (BRGM)[https://www.brgm.fr/fr] and (ADEME)[https://www.ademe.fr/].

More info on the tool content can be found on CARTODIM public reports available on [Infoterre](http://infoterre.brgm.fr/rechercher/search.htm) (in French).

If you are interested in the training course for the online version of the tool, please [get in touch with us](mailto:plateforme-geothermie@brgm.fr>).

## License
**geodim** is distributed under the [AGPL license](https://www.gnu.org/licenses/agpl-3.0.txt).

## Roadmap
Next steps are:
- complete documentation
- restructure code
- provide installs
- do CLI
